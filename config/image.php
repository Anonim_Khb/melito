<?php

return [

    'driver' => 'gd',

    /*
    |--------------------------------------------------------------------------
    | USERS
    |--------------------------------------------------------------------------
    */
    'headerLogoForColorful' => 'melito_img/logo/melito-l8.png',

    'headerLogoForWhite' => 'melito_img/logo/melito-l10.png',

    'email-logo' => 'melito_img/logo/melito-l10.png',

    'watermark' => public_path('melito_img/logo/melito-l8.png'),

    'AdsOfferImagesPath' => 'melito_img/users_img/ads_offer/',
    'AdsOfferImagesPathMini' => 'melito_img/users_img/ads_offer_mini/',
    'AdsOfferDefaultImage' => 'melito_img/users_img/ads_default/ads_default.png',

    'UserAvatarsDefaultImage' => 'melito_img/users_img/avatars_default/avatar-1.png',
    'UserAvatarsDefaultImageMini' => 'melito_img/users_img/avatars_default_mini/avatar-1.png',

    'UserAvatarsPath' => 'melito_img/users_img/avatars/',
    'UserAvatarsPathMini' => 'melito_img/users_img/avatars_mini/',
    'UserAvatarsDefaultPath' => 'melito_img/users_img/avatars_default/',
    'UserAvatarsDefaultPathMini' => 'melito_img/users_img/avatars_default_mini/',
    'UserAvatarsTempFolder' => 'melito_img/users_img/avatars_temp/',

    'icons_path' => 'melito_img/icons/',

    'performance_path' => 'melito_img/performance/',

    'LoadingDefaultImage' => 'melito_img/loading/loading_4.gif',
    'LoadingDefaultPath' => 'melito_img/loading/',

    'defaultSize' => [
        'avatar' => 500,
        'avatarMini' => 100,
        'adImage' => 1000,
        'adImageMini' => 180,
    ],

];
