<?php

return [

    'color' => [
        'success' => 'rgba(76, 175, 80, 0.84)',
        'error' => 'rgba(244, 67, 54, 0.84)',
        'warning' => 'rgba(255, 200, 56, 0.83)',
        'danger' => '',
    ],

    'support-answer' => [
        'text' => 'notification.header.support-answer',
        'icon_src' => 'support-1.png',
        'url' => 'account-my-support',
    ],

    'admin-personal-gift' => [
        'text' => 'notification.header.admin-personal-gift',
        'icon_src' => 'gift-1.png',
        'url' => 'account-my-donate',
    ],

    'ad-verify-success' => [
        'text' => 'notification.header.ad-verify-success',
        'icon_src' => 'ad-verify-success-1.png',
        'url' => 'account-my-ads',
    ],

    'ad-verify-error' => [
        'text' => 'notification.header.ad-verify-error',
        'icon_src' => 'ad-verify-error-1.png',
        'url' => 'account-my-ads',
    ],

    'send-email-for-user-ad' => [
        'text' => 'notification.header.send-email-for-user-ad',
        'icon_src' => 'email-1.png',
        'url' => null,
    ],

    'send-email-for-register-confirm' => [
        'text' => 'notification.header.send-email-for-register-confirm',
        'icon_src' => 'email-1.png',
        'url' => null,
    ],

    'for-all' => [

        'gift-take' => [
            'text' => 'notification.header.take-gift',
            'icon_src' => 'gift-1.png',
            'url' => 'shop-page',
        ],

        'discount' => [
            'text' => 'notification.header.discount',
            'icon_src' => 'discount-1.png',
            'url' => 'shop-page',
        ],

    ],

];