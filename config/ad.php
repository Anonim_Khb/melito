<?php

return [

    'status' => [
        'not-confirm',
        'active',
        'wait',
        'closed',
        'for-correct',
        'corrected',
    ],

    'admin-status' => [
        'active',
        'for-correct',
    ],
    'open-status' => [
        'active',
        'wait'
    ],
    'hidden-status' => [
        'closed',
        'for-correct',
        'corrected',
        'not-confirm',
    ],
    'verify-status' => [
        'wait',
        'corrected',
    ],
    'correct-status' => [
        'for-correct',
        'corrected',
    ],
    'edit-forbidden' => [
        'not-confirm',
        'closed',
    ],
];