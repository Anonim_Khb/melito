<?php

return [

	'secret'  => env('NOCAPTCHA_SECRET'),

	'sitekey' => env('NOCAPTCHA_SITEKEY'),

    'CaptchaEnabled' => true, // TRUE - ENABLES, FALSE - DISABLED

];
