<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserSettingsModelTest extends TestCase
{

    use DatabaseTransactions;


    public function testUserProfileModelCreateNew()
    {
        $user = factory(App\User::class)->create([]);

        $userProfile = new \App\Models\UserSetting();

        $userProfile::createNew($user->id);

        $this->seeInDatabase('user_settings', ['user_id' => $user->id]);
    }

}
