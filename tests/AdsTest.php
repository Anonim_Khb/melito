<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdsTest extends TestCase
{

    use DatabaseTransactions;


    public function testAdsOfferPageByAuthUserConfirmRegistrationYes()
    {
        $user = factory(App\User::class)->create([
            'status' => 'active'
        ]);

        $this->actingAs($user)
            ->visit('/ads/new')
            ->see('/ads/new')
            ->dontSee('email');
    }

// COOKIE (LOCALE) ERROR
//    public function testAdsOfferPageByAuthUserConfirmRegistrationNo()
//    {
//        $user = factory(App\User::class)->create([
//            'status' => 'pending'
//        ]);
//
//        $this->actingAs($user)
//            ->visit('/ads/new/offer')
//            ->see('/ads/new/offer')
//            ->dontSee('email');
//    }
//
//
//    public function testAdsOfferPageByGuest()
//    {
//        $this->visit('/ads/new/offer')
//            ->see('/ads/new/offer')
//            ->see('email');
//    }

}
