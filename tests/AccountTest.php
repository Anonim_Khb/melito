<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountTest extends TestCase
{

    use DatabaseTransactions;


    public function testSelfPersonalPageConfirmRegistrationYes()
    {
        $user = factory(App\User::class)->create([
            'status' => 'active'
        ]);

        factory(\App\Models\UserProfile::class)->create([
            'user_id' => $user->id
        ]);

        $data = [
            'name' => 'Testing Name',
            'gender' => 'male',
            'birthday' => '1990-01-01',
            'phone' => '+7-(909)-809-44-44.',
            'skype' => 'alvo4444.test',
            'whatsapp' => true,
            'viber' => '1',
            'messenger' => '1',
            'contact_another' => 'alvo4444.test and +7-(909)-809-44-44.',
            'alcohol' => 'no',
            'smoke' => 'no',
            'criminal' => 'no',
            'work' => 'yes',
            'additional' => 'Simple text for testing',
        ];

        $this->actingAs($user)
            ->visit('/account/personal')
            ->see('/account/personal')
            ->visit('/account')
            ->see('/account/personal')
            ->submitForm('AccountQuestionnaire', $data)
            ->seeInDatabase('user_profiles', [
                'user_id' => $user->id,
                'gender' => 'male',
                'whatsapp' => '1',
                'viber' => '1'
            ])
            ->see($data['birthday']);
    }


    public function testSelfPersonalPageUserConfirmRegistrationNo()
    {
        $user = factory(App\User::class)->create([
            'status' => 'pending'
        ]);

        $this->actingAs($user)
            ->visit('/account/personal')
            ->see('/register/problem');
    }


    public function testSelfPersonalPageByGuest()
    {
        $this->visit('/account/personal')
            ->see('/login');
    }

}
