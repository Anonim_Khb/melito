<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Jenssegers\Date\Date;

class AuthControllerTest extends TestCase
{

//    use DatabaseMigrations;
    use DatabaseTransactions;

    public function testAuthPage()
    {
        $user = factory(App\User::class)->create();

        $data = [
            'email' => $user->email,
            'password' => 'zzzzzz'
        ];

        $this->visit('/login')
            ->submitForm('loginForm', $data)
            ->seePageIs('/');
    }


    public function testRegisterPage()
    {
        $data = [
            'email' => 'abrakadabra@bb.bb',
            'password' => 'zzzzzz',
            'password_confirmation' => 'zzzzzz'
        ];

        $this->visit('/register')
            ->submitForm('registerForm', $data)
            ->seePageIs('/register/complete');
    }


    public function testRegisterCompletePage()
    {
        $this->visit('/register/complete');

        $this->assertResponseOk();
    }


    public function testRegisterConfirmation()
    {
        $user = factory(App\User::class)->create();

        $this->visit('/register/confirm/' . $user->hash)
            ->seeInDatabase('users', ['status' => 'active'])
            ->seePageIs('/');
    }


    public function testRegisterProblemPage()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/register/problem')
            ->submitForm('ResendConfirmEmail')
            ->seePageIs('/');
    }


    public function testUserRemindPassword()
    {
        $user = factory(App\User::class)->create();

        $data = [
            'email' => $user->email,
        ];

        $this->visit('/password/remind')
            ->submitForm('PassRemindForm', $data)
            ->seePageIs('/');
    }


    public function testLogout()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/logout')
            ->seePageIs('/');
    }


    public function testUserResetPassword()
    {
        $user = factory(App\User::class)->create([
            'email' => 'test@test.ts',
            'password_reset' => Date::now()->subMinutes(30),
        ]);

        $data = [
            'email' => 'test@test.ts',
            'new_password' => 'new?pass.word',
            'new_password_confirmation' => 'new?pass.word'
        ];

        $this->visit('/password/reset/' . $user->hash)
            ->see($user->hash)
            ->submitForm('PassReset', $data)
            ->seePageIs('/')
            ->visit('/logout')
            ->visit('/login')
            ->submitForm('loginForm', ['email' => 'test@test.ts', 'password' => 'new?pass.word',])
            ->seePageIs('/');
    }

}
