<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdsOfferModelTest extends TestCase
{

    use DatabaseTransactions;


    public function testAdsOfferExistsPhotosPath()
    {
        $this->assertFileExists(public_path(config('app.AdsOfferImagesPath')));
    }


    public function testCreateNewAdOfferModelMethod()
    {
        $user = factory(App\User::class)->create();

        $newOffer = factory(App\Models\AdsOffer::class)->create([
            'user_id' => $user->id
        ]);

        $adsOffer = new \App\Models\AdsOffer();

        $adsOffer::createAdOffer($user->id, $newOffer, 'active');

        $this->seeInDatabase('ads_offers', [
            'user_id' => $user->id,
            'status' => 'active',
            'city' => '2022890',
            'street_name' => 'Ким-Ю-Чена',
            'house_number' => 'дом 5 / корп 2',
            'house_type' => 'studio',
            'communal' => 'single',
            'sanitary_ware' => 'home',
            'wifi' => '0',
            'tv' => '0',
            'parking' => '1',
            'balcony' => '1',
            'garden' => '1',
            'security' => '1',
            'elevator' => '1',
            'jacuzzi' => '1',
            'boiler' => '1',
            'gim' => '1',
            'furniture' => '1',
            'appliances' => '1',
            'rent_price' => '5100',
            'add_pay' => '510',
            'room_meters' => '51',
            'available_from' => '2050-01-01',
            'prepayment' => '6+',
            'gender' => 'not-matter',
            'occupation' => 'professional',
            'age_from' => '25',
            'age_to' => '75',
            'children' => '1',
            'couples' => '1',
            'guests' => '1',
            'pets' => '1',
            'smoking' => '1',
        ]);
    }




}
