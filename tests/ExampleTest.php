<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{


    public function testMethod()
    {
        $this->call('GET', '/');

        $this->assertResponseOk();
    }


    public function testBasicExample()
    {
        $this->visit('/')
            ->see('Just Text')
            ->dontSee('Rails');

        $this->assertResponseOk();
    }


    public function testTestCase()
    {
        $this->assertEquals('works',
            'works',
            'This is OK'
        );

        $this->assertEquals('works',
            'works',
            'OK'
        );
    }


    //        $this->visit('/account/personal');
    //        $this->action('GET', 'AccountController@getMyAccount');
    //        $this->route('GET', 'account-personal');


}
