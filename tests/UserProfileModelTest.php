<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserProfileModelTest extends TestCase
{

    use DatabaseTransactions;


    public function testUserProfileModelCreateNew()
    {
        $user = factory(App\User::class)->create([]);

        $userProfile = new \App\Models\UserProfile();

        $userProfile::createNew($user->id);

        $this->seeInDatabase('user_profiles', ['user_id' => $user->id]);
    }


    public function testUserProfileModelExistsAvatarsPath()
    {
        $this->assertFileExists(public_path(config('image.UserAvatarsPath')));
    }


    public function testUserProfileModelExistsDefaultAvatarsPath()
    {
        $this->assertFileExists(public_path(config('image.UserAvatarsDefaultPath')));
    }

}
