<?php

//ENGLISH

return [

    'about' => [
        'shop-title' => 'Shop',
        'services-title' => 'Individual services',
        'balance' => 'Your balance',
        'add-funds' => 'Add funds',
        'have-gift' => 'Gift',
        'give-gift-btn' => 'Pick-Up Gift',
        'discount' => 'Saving',
    ],

    'buttons' => [
        'activate' => 'Activate',
        'buy' => 'Buy',
        'close' => 'Close',
        'confirm' => 'Confirm',
    ],

    'measurement' => [
        'times' => 'times',
        'onetime' => 'onetime',
        'day-1' => 'days',
        'day-2' => 'days',
        'day' => 'day'
    ],

    'packages' => [
        'titles' => [
            'extra' => 'Package «Extra»',
            'professional' => 'Package «Professional»',
            'standard' => 'Package «Standard»',
            'mini' => 'Package «Mini»',
        ],
    ],

    'goods' => [
        'ad_limit' => [
            'title' => 'Ads limit',
            'description' => 'Increase the limit of ads on one. The service has no time limits.'
        ],
        'top' => [
            'title' => 'TOP',
            'description' => 'You can put your ad on the top of the page will see it exactly.'
        ],
        'color' => [
            'title' => 'Highlight',
            'description' => 'You can decorate your ads highlighting it with any color to choose from for one night.'
        ],
        'auto_up' => [
            'title' => 'Autoraising',
            'description' => 'You can instruct us to raise your ad to the top of the list.'
        ],
        'vip' => [
            'title' => 'VIP',
            'description' => 'You can put your ad on the side panel, which will see it exactly.'
        ],
    ],

    'all-gp' => [
        'extra' => 'Package «Extra»',
        'professional' => 'Package «Professional»',
        'standard' => 'Package «Standard»',
        'mini' => 'Package «Mini»',
        'ad_limit' => '«Ads limit»',
        'top' => '«TOP»',
        'color' => '«Highlight»',
        'auto_up' => '«Autoraising»',
        'vip' => '«VIP»',
    ],

    'modal-confirm' => [
        'title' => 'Checkout purchase',
        'text' => 'To check the correctness of the transaction selected package/service and specify the number then press "Confirm"',
        'total' => 'Total',
    ],

    'transaction' => [
        'put' => 'Recharge balance',
        'buy' => 'Buying of the service/package',
        'spent' => 'Activation of the service/package',
        'admin-gift' => 'Administrator gift',
        'admin-gift-action' => 'Gift action',
    ],

    'robokassa' => [
        'inv_desc' => 'Depositing MELITO.RU'
    ],

    'payment' => [
        'title' => 'Add funds',
    ],

    'privileges' => [
        'color' => [
            'title' => 'Highlight',
            'select' => 'Select the color',
        ],
        'top' => [
            'title' => 'TOP',
        ],
        'auto_up' => [
            'title' => 'Autoraising',
        ],
        'vip' => [
            'title' => 'VIP',
        ],
        'modal' => [
            'btn' => 'Activate',
            'title' => 'Activate the service',
            'sum' => 'Sum',
            'description' => [
                'color' => 'To activate the service, select the color and enter the number of days during which your ad will stand out among the rest with the selected color and click on the "Activate" button.',
                'top' => 'To activate the service, enter the number of days during which your ad will be at the top of the list of displayed ads, and click on the "Activate" button.',
                'auto_up' => 'To activate the service, enter the number of times that your ad will automatically go up in the list and click on the "Activate" button.',
                'vip' => 'To activate the service, enter the number of days during which your ad will be on the side of the pages of our site and click on the "Activate" button.',
                'rules' => 'Please note that the activation of the service with your account will be written off selected services in the amount that you specify. If there is insufficient credit on your requested services system will warn of this fact. We recommend that you familiarize yourself with rules our service.',
            ],
        ],
    ],

];