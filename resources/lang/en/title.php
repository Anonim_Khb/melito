<?php

//ENGLISH

return [
    'login' => 'Login',
    'register' => 'Registration',
    'register-complete' => 'Registration complete',
    'register-not-confirm' => 'Registration has not been confirmed',
    'password-remind' => 'Reset password',
    'new-offer' => 'Offer room',
    'new-need' => 'Need room',
    'offer-open-watch' => 'Offer',
    'user-profile' => 'Profile',
    'shop-about' => 'Shop',
    'ad-edit' => 'Edit',
    'payment-success' => 'Payment system',
    'payment-fail' => 'Payment system',
    'support-general' => 'Support',
    'admin-general' => 'Control',
    'admin-support' => 'Support',
    'admin-notification' => 'Notification',
    'admin-gift' => 'Gifts',
    'admin-ads-validate' => 'Ads validate',
    'admin-shop' => 'Shop',
    'rules' => 'Rules',
    'faq' => 'Help',
];