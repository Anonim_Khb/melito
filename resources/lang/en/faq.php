<?php

//ENGLISH

$siteName = config('app.site_name');
$siteNameRu = config('app.site_name_ru');

return [

    'menu' => [
        'login-password' => 'Login and password',
        'ads-work' => 'Job advertisement',
        'placing-ads' => 'Placing ads',
        'ad-edit' => 'Edit ad',
    ],
    'login-password' => [
        'title' => 'Login and password',
        'body' => [
            'what-is-account' => [
                'question' => 'What is an account?',
                'answer' => '<p>Account on ' . $siteName . ' - is your personal account that is created when registering on the site for easy ad management. Currently, advertising, visible to other users and / or displayed in the results of searches on our website is possible only after registration. The Dashboard contains the information you provide about yourself when registering or submitting ads. The site administration does not send the registration data to third parties.</p>',
            ],
            'how-to-register' => [
                'question' => 'How to register?',
                'answer' => '<p>Please go to the <a href="' . route('register') . '">registration page</a>, fill out the fields and click "Register." After you receive an email to confirm the registration to the specified e-mail address upon registration.</p>
                    <p>When you register you can:
                    <ul><li><a href="' . route('ads-new-offer') . '">Serve ads</a> on ' . $siteName . '.</li>
                    <li>Manage your advertisement in the  <a href="' . route('account-my-ads') . '">Personal Office</a></li></ul>.</p>',
            ],
            'not-letter-of-confirmation' => [
                'question' => 'I have not received a letter of confirmation of registration. What to do?',
                'answer' => '<p>It is necessary to check the folder "Spam" in the electronic box - perhaps the letter fell into this folder. If a confirmation letter in the "Spam" does not ask for a <a href="' . route('registration-no-confirm') . '">letter confirming registration</a> again.</p>',
            ],
            'confirm-link-not-work' => [
                'question' => 'I received a letter of confirmation of registration, but the link does not work or is not active. What to do?',
                'answer' => '<p>Log into your <a href="' . route('account-my-ads') . '">Personal Office</a> - perhaps you have already confirmed their registration. If you were able to enter the Personal Cabinet, the registration is already confirmed. If you attempt to login fails, the request registration <a href="' . route('registration-no-confirm') . '">confirmation letter</a> again.</p>',
            ],
            'how-log-in' => [
                'question' => 'How do I log into your account at  ' . $siteName . '?',
                'answer' => '<p>To log in to an account on ' . $siteName . ':<ul><li>To get started, go to the <a href="' . route('login') . '">login page</a></li>
                    <li>In the form, type the email address provided during registration and password.</li>
                    <li>Press <b>Login</b>.</li></ul></p>',
            ],
            'different-accounts' => [
                'question' => 'As family and colleagues to register different accounts for submission of declarations about the different types of products?',
                'answer' => '<p>You can register individually - and each of you will indicate when registering your personal e-mail and personal contact details.</p>',
            ],
            'forgot-password' => [
                'question' => 'I forgot password.',
                'answer' => '<p>If you are having problems logging into your account at ' . $siteName . ', then reset the password. For this:<ul>
                    <li>If you are currently logged into the system, exit</li>
                    <li>Visit the password recovery page</li>
                    <li>Enter the email address that you used when registering</li>
                    <li>In return you will receive an email. Follow the instructions in the letter</li></ul></p>',
            ],
            'how-get-out' => [
                'question' => 'How to get out of ' . $siteName . '?',
                'answer' => '<p>To exit from ' . $siteName . ':<ul>
                    <li>Click in the upper right corner of any page on ' . $siteName . ' your username / email address.</li>
                    <li>In the list that appears, click Exit.</li></ul></p>',
            ],
            'account-hacked' => [
                'question' => 'It seems to me that my account has been hacked or someone is using it without my permission.',
                'answer' => '<p>We want to help you protect your account. If you believe that someone took over your account and / or your password to be cleared, then we advise you to change your password in your <a href="' . route('account-personal') . '">personal account</a>.</p>',
            ],
            'reset-password' => [
                'question' => 'How do I reset the password?',
                'answer' => '<p>If you know your current password, you can change your password via the <a href="' . route('account-personal') . '">Personal Office</a>.</p>
                    <p>If you can not log on to ' . $siteName . ', and you need to reset your password:<ul>
                    <li>Go to the <a href="' . route('login') . '">entrance</a> to ' . $siteName . '.</li>
                    <li>Click "Forgot password"</li>
                    <li>On the page, enter a password recovery email address that you used when registering.</li>
                    <li>In return you will receive an email. Follow the instructions in the letter.</li></ul></p>',
            ],
            'change-password' => [
                'question' => 'How to change your password?',
                'answer' => '<p>If you\'re signed in ' . $siteName . ', you can change the password in the <a href="' . route('account-personal') . '">Personal Office</a> in the "Settings"</p>',
            ],
            'password-strength' => [
                'question' => 'What is the minimum password strength, and how to create a secure password?',
                'answer' => '<p>When creating a new password, make sure that its length is at least 6 characters. Try using a sophisticated combination of letters, numbers and punctuation marks.</p>
                    <p>Minimum password length to ' . $siteName . ' must be at least 6 characters.</p>
                    <p>Create a password should be easy to remember for you but difficult to calculate someone else. For added security, your password should be different from ' . $siteName . ' password you use for other accounts, such as e-mail or bank account.</p>',
            ],
        ],
    ],
    'ads-work' => [
        'title' => 'Job advertisement',
        'body' => [
            'ads-about-product' => [
                'question' => 'How many ads about a product or service can I apply?',
                'answer' => '<p>A product or service can submit only one ad. Otherwise, the ads are considered the same, and will be blocked by the moderator.</p>
                    <p>Please follow the recommendations ' . $siteName . ':<ul>
                    <li>do not remove the ads from the market before the expiry date and do not submit it again;</li>
                    <li>do not use when applying the same ad headlines, photos and descriptions;</li>
                    <li>do not submit the same ad in multiple cities or categories - choose the most appropriate;</li>
                    <li>do not submit ads for the same product, facility or service, describing it in other words, as this article / object or meaning of service remains the same;</li>
                    <li>if the goods / objects are very similar quote distinguish them in the name of the parameter ad - for example, the same cars with different configuration, the same breed puppies different sex, etc .;</li>
                    <li>if the product is suitable for multiple categories at once, select a category and serve ads in it - for example, clothing, suitable for both girls and boys at the same time, is located in one of the subcategories.</li></ul></p>'
            ],
            'ads-limit' => [
                'question' => 'How many ads can be submitted to a single user?',
                'answer' => '<p>You can submit a limited number of ads. Limit announcements may vary as a guide site ' . $siteName . ', and by the user by using shopping services "advertisements Increase the limit."</p>'
            ],
            'ad-in-different-cities' => [
                'question' => 'Can I submit the same ad in different cities?',
                'answer' => '<p>Submission of the same ad in different cities will not violate the rules of ' . $siteName . ', if all the ads are different objects, different ads can be removed without explanation.</p>'
            ],
            're-submit-ad' => [
                'question' => 'Can I re-submit an ad?',
                'answer' => '<p>Re-submission of the same ads on ' . $siteName . ' prohibited. It does not help to sell goods or services faster, and degrades the quality of the search, thereby greatly reducing both the rate of purchases and sales velocity.</p>'
            ],
            'registration-to-post-ad' => [
                'question' => 'Whether registration to post an ad?',
                'answer' => '<p>To submit announcements on ' . $siteName . ' registration is required. Registration on the site allows you to manage ads from your account and enables them to activate their expiry date.</p>'
            ],
            'which-city' => [
                'question' => 'Which city in their ads?',
                'answer' => '<p>Always specify the city in which the object actually is.</p>
                    <p>For example, if you live in Moscow, and a property for sale in the Moscow region, Moscow region and specify the closest to your location real estate in the Moscow region from the list on the website.</p>'
            ],
            'phone-number' => [
                'question' => 'I need to set a phone number when submitting ads?',
                'answer' => '<p>The "Phone Number" is optional to fill in the placement of ads (or edit previously submitted).</p>'
            ],
            'ad-description' => [
                'question' => 'How to make a description of the ads?',
                'answer' => '<p>Describe exactly the product or service, as referred to in the title of the ad and which is depicted in the photographs. Do not place in the description of the price list (names and prices of other similar products), keywords and contact information (links to any website, phone, ICQ, and so on. D.). Otherwise, your ad will be rejected or blocked by the moderator.</p>'
            ],
            'ad-images' => [
                'question' => 'How to add photos in an ad?',
                'answer' => '<p>You can add photos when submitting or editing ads. To do this, the following page click the icon or the camera labeled "Add photos" next to "Photos" and upload images from your computer in any order and orientation. After you download the image, you can change the index picture, or remove them and complete the placement or editing the ad by clicking "Next."</p>
                    <p>Photos should be the format of .jpg, .jpeg, .png or .gif, its size should not exceed 10 MB. You can add up to 10 photos for classified (the number of photos may be changed without notice).</p>
                    <p>Photos posted on the site are protected by a watermark (votermark, water mark) ' . $siteName . ' with site logo in the bottom right corner.</p>'
            ],
            'ad-price' => [
                'question' => 'How to specify the price at the classifieds?',
                'answer' => '<p>Please follow these rules:<ul>
                    <li>the price must be a whole number, do not use commas, periods, spaces, and other punctuation marks;</li>
                    <li>the price should be precisely the one on which you are ready to sell. For example, if you rent a room at the price of 29,000 rubles, do not specify the price of 29 rubles 1 ruble 1,000,000 rubles;</li>
                    <li>specifies the full value of the object, not the price per square meter;</li>
                    <li>the price should be specified in rubles, while the word "ruble" in the price to add is not necessary.</li></ul></p>'
            ],
            'ads-check' => [
                'question' => 'How to check the ad?',
                'answer' => '<p>Verification of declarations made constantly as before the ads on the site and after. Please read the rules before entering ' . $siteName . ' ads.</p>'
            ],
            'ad-after-filling' => [
                'question' => 'Find Advertise on the site immediately after the filing?',
                'answer' => '<p>You can follow the link on the page immediately after the ads on the website or follow the link from the confirmation letter, which you will receive an e-mail provided during deployment. Also, ads will appear in the Dashboard.</p>
                    <p>Please note that each ad is checked and appear in search results on the site immediately, but after 20-30 minutes after the feeding.</p>'
            ],
            'why-not-add-photos' => [
                'question' => 'Why not add photos to the announcement?',
                'answer' => '<p>Photos can not be added for the following reasons:<ul>
                    <li>image size exceeds 10 MB</li>
                    <li>image format is not .jpg, .jpeg, .png or .gif</li>
                    <li>you have not installed the latest version of the browser (in this case, refresh your browser)</li>
                    <li>your Internet connection is slow or unstable</li></ul></p>
                    <p>Change the format and size of pictures can be in any program for working with images.</p>'
            ],
        ],
    ],
    'placing-ads' => [
        'title' => 'Placing ads',
        'body' => [
            'ad-number' => [
                'question' => 'Where to see the number of ads?',
                'answer' => '<p>In ads that are on the site, the number can be found on the page of ads description.</p>'
            ],
            'view-count' => [
                'question' => 'Where can I see the number of display ads?',
                'answer' => '<p>You can see the number of display ads on the page ads</p>'
            ],
            'view-count-reset' => [
                'question' => 'How to reset the views ads?',
                'answer' => '<p>Unfortunately, the reset can not be viewed ads.</p>'
            ],
            'how-find-ad' => [
                'question' => 'How to find an ad on the site?',
                'answer' => '<p>Search is designed in such a way that your ad will only appear when people search for that city / the area in which the / th it is placed, and all over Russia. Display your ads when searching for another city / other area possible.</p>
                    <p>You can check to see if your ad in the search results by going to the My Account tab in the "Announcements" and check the status you are interested in ads.</p>
                    <p>Each ad is being tested and appear in search results on the site immediately, but after 20-30 minutes after the filing, editing, or connect to additional services.</p>
                    <p>All ads placed on the site, sorted by date / price and match your search, the region and the selected category.</p>
                    <p>Make sure that you have exposed the search parameters correspond to the parameters that have been chosen by you when placing ads.</p>'
            ],
            'ad-to-bookmark' => [
                'question' => 'How to add to your bookmarks?',
                'answer' => '<p>If you are interested in an ad, and you want to quickly find this ad later, click on the star on the page ads. All of the selected ads will appear on a special page. You can always return to it by clicking on the tab "Bookmarks" at the top of any page.</p>'
            ],
            'user-ads' => [
                'question' => '',
                'answer' => '<p>All ads of the user can immediately see on the preview page profile in the "Ads".</p>
                    <p>The exception is users, hiding the section "Announcements" to view other users.</p>'
            ],
            'user-use-my-pictures' => [
                'question' => 'Another user is using my pictures in my ad. What to do?',
                'answer' => '<p>Please send to the Support service application that contains the number of ads that use your photos along with the originals - or a link to your site, which published photos. We will promptly consider the application.</p>'
            ],
            'cheated-transaction' => [
                'question' => 'I cheated in the transaction. What to do?',
                'answer' => '<p>Administration of this site is not involved in communications between the user and the transaction. Please be careful not to fall for tricks fraudsters. In such cases, you should contact the police. According to their official request, we will promptly provide all the technical information held.</p>'
            ],
            'ads-violate-rules-support' => [
                'question' => 'I found the ads that violate the rules. Submitted rooms for your support, but they are still on the site. Why?',
                'answer' => '<p>The support staff at each time a user inspect ads. Please use the "Report advertisement" on page ads. This will enable us to process a complaint more quickly.</p>'
            ],
        ],
    ],
    'ad-edit' => [
        'title' => 'Edit ad',
        'body' => [
            'how-ad-edit' => [
                'question' => 'How do I edit an ad?',
                'answer' => '<p>To edit an ad:<ul>
                    <li>go to My Account using your email address and password;</li>
                    <li>on the "Ads" find the ad that you want to edit and go to "edit" (called the ad);</li>
                    <li>edit message - make the necessary changes in the name, description and parameters of the ad, if necessary, add a photo;</li>
                    <li>check the information and click "Next".</li></ul></p>'
            ],
            'pay-ad-edit' => [
                'question' => 'Is editing ads paid?',
                'answer' => 'You can edit the ads for free. This option is free of charge'
            ],
            'hide-after-edit' => [
                'question' => 'I just successfully edited ads, but it does not appear on the site. Why?',
                'answer' => '<p>Each edited ads tested and appear in search results on the site immediately, but within 30 minutes of editing, after re-moderation.</p>'
            ],
            'not-save' => [
                'question' => 'Do not save changes when you edit the ad',
                'answer' => '<p>Perhaps the problem in your browser settings (browser - a program for viewing Internet pages). Please make sure you have the latest version of the browser; delete cookies and cache in your browser settings; Check whether JavaScript. Also try to edit an ad, using a different browser.</p>'
            ],
        ],
    ],

];