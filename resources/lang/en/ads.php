<?php

//ENGLISH

return array(

    'ad-status' => [
        'title' => 'Status: ',
        'not-confirm' => 'Register not confirm',
        'active' => 'Active',
        'wait' => 'Active not verify',
        'closed' => 'Hide',
        'for-correct' => 'Need editing',
        'corrected' => 'Awaiting verify',
    ],
    'ad-info' => [
        'info' => '<ul>
            <li><i class="fa fa-map-marker">&nbsp;</i> :city</li>
            <li>Views: :views</li>
            <li>Created: :create</li>
            <li>Updated: :update</li>
        </ul>'
    ],
    'new-offer' => [
        'not-select' => 'Not select',
        'title' => 'Offer room',
        'under-title' => 'Posting your room is FREE, quick and easy!',
        'locality' => [
            'title' => 'Locality',
            'city' => 'City',
            'select-city' => 'Select city',
        ],
        'street-name' => 'Street name',
        'house-number' => 'House number',
        'deal_type' => [
            'title' => 'Deal type',
            'rent' => 'Rent',
            'sale' => 'Sale',
        ],
        'house-type' => [
            'title' => 'Type of room',
            'apartment' => 'Apartment',
            'house' => 'House',
            'studio' => 'Studio',
        ],
        'communal' => [
            'title' => 'Rent type',
            'single' => 'For one',
            'several' => 'For several',
        ],
        'sanitary_ware' => [
            'title' => 'Sanitary ware',
            'home' => 'House',
            'street' => 'Street',
        ],
        'amenities' => [
            'title' => 'Amenities',
            'wifi' => 'Wi-Fi',
            'tv' => 'TV',
            'parking' => 'Parking',
            'balcony' => 'Balcony',
            'garden' => 'Garden',
            'security' => 'Security',
            'elevator' => 'Elevator',
            'jacuzzi' => 'Jacuzzi',
            'gim' => 'Gim',
            'furniture' => 'Furniture',
            'appliances' => 'Home appliances',
            'boiler' => 'Boiler',
        ],
        'rent-price' => [
            'title' => 'Price',
            'rub' => 'Ruble',
        ],
        'additional-payment' => 'Additional payment',
        'room-meters' => 'Area',
        'available-from' => 'Available from',
        'prepayment' => 'Prepayment (month)',
        'preferred' => [
            'title' => 'Preferred',
            'gender' => [
                'title' => 'Preferred gender',
                'male' => 'Male',
                'female' => 'Female',
                'not-matter' => 'Doesn\'t matter',
            ],
            'occupation' => [
                'title' => 'Preferred occupation',
                'student' => 'Student',
                'professional' => 'Professional',
                'not-matter' => 'Doesn\'t matter',
            ],
            'rules' => [
                'title' => 'Rules',
                'children' => 'Families & children accepted',
                'couples' => 'Couples accepted',
                'guests' => 'Guests',
                'pets' => 'Pets allowed',
                'smoking' => 'Smoking allowed',
            ],
            'age' => [
                'title' => 'Preferred age',
                'from' => 'From',
                'to' => 'To',
            ]
        ],
        'ad-present' => [
            'title' => 'Your ad',
            'ad-text' => [
                'title' => 'Ad text'
            ],
        ],
        'images' => [
            'button' => 'Select images'
        ],
        'form-button' => 'Create',
        'ad-floor' => 'Floor'
    ],

    'new-need' => [
        'title' => 'Room search',
        'rent-price' => [
            'title' => 'Price',
            'from' => 'From',
            'to' => 'To',
        ],
        'room-meters' => [
            'title' => 'Room meters',
            'from' => 'From',
            'to' => 'To',
        ],
        'advanced-search' => 'Advanced Search',
        'form-button' => 'Search',
        'additional-payment' => 'An additional payment',
        'entry' => 'Date of entry',
        'prepayment' => 'An prepayment',
        'gender' => [
            'title' => 'Your gender'
        ],
        'occupation' => [
            'title' => 'Your occupation'
        ],
        'age' => [
            'title' => 'Your age',
            'value' => 'years',
        ],
        'with-images' => 'Photo is necessary'
    ],

    'ad-watch' => [
        'city' => 'City: ',
        'views' => 'Views: ',
        'reg-not-confirm' => 'E-Mail not confirmed',
        'reg-confirm' => 'E-Mail confirmed',
        'null-data' => 'Not indicated',
        'ad-up' => 'Raised: ',
        'ad-edit' => 'Edit ad',
        'profile' => [
            'name' => 'Name',
            'gender' => [
                'title' => 'Gender',
                'male' => 'Male',
                'female' => 'Female',
            ],
            'age' => 'Years old',
            'href' => 'User profile',
            'write-letter' => 'Write to author',
        ],
        'send-email' => [
            'title' => 'Send message',
            'disabled-send' => 'To send a message to the user need to register',
        ],
        'street-name' => 'Street',
        'house-number' => 'House number',
    ],
    'ad-edit' => [
        'save-button' => 'Save',
    ],

);