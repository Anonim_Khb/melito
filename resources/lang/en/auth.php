<?php

//ENGLISH

return [

    'login' => [
        'title' => 'Login',
        'email' => 'Email address',
        'password' => 'Password',
        'remember-me' => 'Remember Me',
        'email-placeholder' => 'Enter Email',
        'password-placeholder' => 'Enter password',
        'btn' => 'Login',
        'forgot-password' => 'Forgot password?',
        'registration-btn' => 'Registration',
    ],

    'register' => [
        'title' => 'Register',
        'email' => 'Email address',
        'password' => 'Password',
        'password-confirm' => 'Confirm Password',
        'rules' => 'with <a href="'. route('rules-general').'"> rules and conditions </a> familiar',
        'email-placeholder' => 'Enter Email',
        'password-placeholder' => 'Enter password',
        'password-confirm-placeholder' => 'Confirm Password',
        'btn' => 'Register',
        'have-account' => 'Already have an account?'
    ],

    'registered' => [
        'title' => 'Successful registration',
        'text' => '<p>Dear user, your email - <b>: email </b> sent a letter to confirm your registration information.</p>
            <p>Attention! If you do not see the email in your inbox, check the folder «Spam».</p>',
        'why-need' => '<h5>Why do we need a registration confirmation?</h5>
            <ul>
            <li>Your ads that you have created or even create visible only after the registration</li>
            <li>Other users will get a chance to write to you in response to your ads</li>
            <li>You will get access to your personal office</li>
            <li>You will be able to fully manage your account, settings and announcements</li>
            <li>You can add Bookmarks to your favorite ads</li>
            <li>You will be able to receive prizes and bonuses</li>
            <li>You will be able to receive responses from technical support</li>
            <li>You will be able to leave feedback on the ads you are interested in</li>
            <li>and more</li>
            </ul>',
        'email-tooltip' => 'Mail :mail',
    ],

    'not-confirm' => [
        'title' => 'Register not confirm',
        'text' => '<p> You have not confirmed your registration. </p>
             <p> Dear user, previously to your email - <b>:email</b> sent a letter to confirm your registration information. </p>
             <p> Warning! If you do not see the email in your inbox, check the folder «Spam».</p>',
        're-send' => '<p> If you can not find the mail, we can send you it again</p>',
        'show-form' => 'Send mail again',
        'send-btn' => 'Send',
    ],

    'password-remind' => [
        'title' => 'Password recovery',
        'show-text' => 'Instructions for password recovery',
        'text' => '<p>To recover your password, follow these steps:
            <ul><li>In the field below enter the email address with which you registered on ' . config ('app.site_name'). ' and click «Send»</li>
            <li>Sign in Your Email box and get sent you an email</li>
            <li>Follow the instructions set out in the letter</li>
            </ul></p>
            <p> Warning! If you do not see the email in your inbox, check the folder «Spam».</p>',

        'email' => 'Enter Email',
        'email-placeholder' => 'Enter Email',
        'btn' => 'Send',
        'support' => 'Support',
    ],

    'password-reset' => [
        'title' => 'Password reset',
        'email' => 'Enter your Email',
        'email-placeholder' => 'Enter your Email',
        'new-password' => 'New password',
        'new-password-placeholder' => 'New password',
        'new-password-confirm' => 'Confirm password',
        'new-password-confirm-placeholder' => 'Confirm password',
        'btn' => 'Change',
    ],

];