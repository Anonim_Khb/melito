<?php

//ENGLISH

return array(

    //For function in helpers.php
    'user-status' => [
        'online' => 'Online',
        'offline' => 'Offline',
        'now-offline' => 'Was online: ',
        'in' => ' \i\n ',
    ],

    //Links && Titles
    'menu' => [
        'link-personal-info' => 'Personal',
        'link-message' => 'Messages',
        'link-ads' => 'Ads',
        'link-share' => 'Share',
        'link-setting' => 'Settings',
        'link-help' => 'Help',
        'link-donate' => 'Donate',
    ],
    //Personal
    'pr-part-general' => [
        'title' => 'General information',
        'avatar' => 'Avatar',
        'avatar-validation' => 'The file must be an image (jpeg, png, bmp, gif) may not be greater than 10 Mb',
        'avatar-change' => 'Change avatar',
        'avatar-remove' => 'Remove avatar',
        'name' => [
            'title' => 'Name',
            'placeholder' => 'Your name or nickname',
        ],
        'gender' => [
            'title' => 'Gender',
            'male' => 'Male',
            'female' => 'Female',
            'not_select' => 'Not selected',
        ],
        'birthday' => [
            'title' => 'Birthday',
            'placeholder' => 'yyyy-dd-mm',
        ],
        'avatar-delete' => [
            'title' => 'Remove avatar',
            'text' => '<p>Remove avatar - an irreversible process. You will be assigned a standard avatar.</p>
                <p>Are you sure you want to remove avatar?</p>',
            'btn-cancel' => 'Cancel',
            'btn-confirm' => 'Remove',
        ],
        'avatar-crop' => [
            'title' => 'Editing avatar',
            'avatar-crop-info' => 'Select the area of the image and click «Save»',
            'avatar-crop-btn' => 'Save',
        ],
    ],
    'pr-part-personal' => [
        'title' => 'Personal',
        'smoke' => [
            'title' => 'About smoking',
            'no' => 'Don\'t smoke',
            'yes' => 'Smoke',
        ],
        'alcohol' => [
            'title' => 'About alcohol',
            'yes' => 'Drink',
            'sometime' => 'Sometimes',
            'no' => 'Don\'t drink',
        ],
        'criminal' => [
            'title' => 'Criminal record',
            'no' => 'I don\'t have',
            'yes' => 'I have',
        ],
        'work' => [
            'title' => 'About work',
            'yes' => 'I work',
            'student' => 'I am a student',
            'business' => 'Own business',
            'no' => 'I am unemployed',
        ],
        'additional' => [
            'title' => 'Additional',
            'placeholder' => 'Any other information about you',
        ],
    ],
    'pr-part-contact' => [
        'title' => 'Contacts',
        'email' => 'Email',
        'phone' => [
            'title' => 'Phone',
            'placeholder' => 'Contact phone',
        ],
        'skype' => [
            'title' => 'Skype',
            'placeholder' => 'Login Skype',
        ],
        'contacts' => 'Available for communication',
        'available' => 'Another',
        'another-contact' => [
            'title' => 'Another',
            'placeholder' => 'Another way to communicate',
        ],
    ],
    //Buttons
    'save' => 'Save',

    'ads' => [
        'ads-count' => 'Ads',
        'watch' => 'Read more',
        'create' => 'Create',
        'none-ads' => 'You have no ads',
        'timestamp' => [
            'created' => 'Created',
            'updated' => 'Updated',
        ],
        'square-meters' => 'm<sup>2</sup>',
        'street-abbr' => 'st.',
        'view-count' => 'Views',
        'deal-type' => [
            'title' => 'Deal type',
            'rent' => 'For rent',
            'sale' => 'For sale',
        ],
        'btn' => [
            'title' => 'Actions',
            'premium' => 'Premium',
            'crop' => 'Cover',
            'edit' => 'Edit',
            'up' => [
                'title' => 'Pick up',
                'free' => 'Free',
                'premium' => 'Premium',
            ],
            'hide' => 'Hide',
            'show' => 'Publish',
        ],
        'ad-cover-modal' => [
            'title' => 'Cover',
            'text' => 'Please note that you are editing only cover the ad, not the photo itself. The successful presentation of the cover can contribute to the popularity of the ad.',
        ],
    ],

    'donate' => [
        'balance' => [
            'title' => 'Your balance',
        ],
        'goods' => [
            'title' => 'Your services',
        ],
        'privileges' => [
            'title' => 'Your orders',
            'btn-add' => 'Activate service',
            'expired' => 'Expired',
            'active' => 'Active',
        ],
        'transactions' => [
            'title' => 'Transactions',
            'columns' => [
                'type' => 'Type',
                'ad-id' => 'Ad',
                'name' => 'Name',
                'number' => 'Number',
                'sum' => 'Sum',
                'date' => 'Date',
            ],
        ],
    ],

    'bookmark' => [
        'delete-all' => 'Remove all bookmarks',
        'delete-one-bookmark' => 'Remove bookmark',
        'ad-watch' => 'Watch',
        'none-bookmarks' => 'You have no bookmarks',
        'modal-bad-title' => 'Removal confirmation',
        'modal-bad-text' => 'Removal will be irreversible. Are you sure you want to delete all your bookmarks?',
    ],

    'settings' => [
        'change-password' => [
            'title' => 'Change password',
            'old-password' => 'Old password',
            'new-password' => 'New password',
            'repeat-password' => 'Repeat password',
            'save-btn' => 'Save',
        ],
    ],

    'another' => [
        'not-exists' => 'not indicated',
        'exists' => 'exists',
        'not-ads' => 'User has no established ad',
    ],

);