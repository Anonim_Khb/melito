<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'email' => 'The :attribute must be a valid email address.',
    'filled' => 'The :attribute field is required.',
    'exists' => 'The selected :attribute is invalid.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'url' => 'The :attribute format is invalid.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'g-recaptcha-response' => [
            'captcha' => '"Captcha" not checked.',
        ],
        'pr_token' => [
            'required' => 'Security error, please try again.',
            'exists' => 'Security error. You may have entered a wrong Email. Please try again.',
        ],
        'rules' => [
            'required' => 'You must read and accept the rules',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'email' => '"Email"',
        'password' => '"Password"',
        'g-recaptcha-response' => '"Captcha"',
        'new_password' => '"New password"',

        //Account
        'name' => '"Name"',
        'gender' => '"Gender"',
        'birthday' => '"Birthday"',
        'phone' => '"Phone"',
        'skype' => '"Skype"',
        'whatsapp' => '"Whatsapp"',
        'viber' => '"Viber"',
        'messenger' => '"Messenger"',
        'contact_another' => '"Another way to communicate"',
        'alcohol' => '"About alcohol"',
        'smoke' => '"About smoking"',
        'criminal' => '"Criminal record"',
        'work' => '"About work"',
        'additional' => '"Additional"',
        'text_msg' => '"Text message"',
        'old_password' => '"Old password"',
        'new_password_1' => '"New password"',
        'new_password_2' => '"Repeat password"',

        //Ads Offer
        'city' => '"City"',
        'street_name' => '"Street name"',
        'house_number' => '"House number"',
        'house_type' => '"House type"',
        'communal' => '"Room type"',
        'sanitary_ware' => '"Sanitary ware"',
        'wifi' => '"Wi-Fi"',
        'tv' => '"TV"',
        'parking' => '"Parking"',
        'balcony' => '"Balcony"',
        'garden' => '"Garden"',
        'security' => '"Security"',
        'elevator' => '"Elevator"',
        'jacuzzi' => '"Jacuzzi"',
        'boiler' => '"Boiler"',
        'gim' => '"Gim"',
        'furniture' => '"Furniture"',
        'appliances' => '"Home appliances"',
        'rent_price' => '"Rent price"',
        'add_pay' => '"Additional pay"',
        'room_meters' => '"Room area"',
        'available_from' => '"Available from"',
        'prepayment' => '"Prepayment"',
        'gender' => '"Gender"',
        'occupation' => '"Occupation"',
        'age_from' => '"Age from"',
        'age_to' => '"Age to"',
        'children' => '"Children"',
        'couples' => '"Couples"',
        'guests' => '"Guests"',
        'pets' => '"Pets"',
        'smoking' => '"Smoking"',
        'ad_text' => '"Ad text"',
        'floor' => 'Floor',

        //Ads NEED
        'price_from' => '"Rent FROM"',
        'price_to' => '"Rent TO"',
        'advanced-search' => '"Advanced search"',
        'room_meters_from' => '"Room meters FROM"',
        'room_meters_to' => '"Room meters TO"',
        'entry' => '"Date of entry"',
        'age' => '"Age"',

        //Payment
        'sum' => '"Sum"',

        //Support
        'category' => '"Category"',
        'text' => '"Text"',
        'dialog_id' => '"Dialog"',
        'dialog' => '"Dialog"',

        //Admin
        'good_name' => '"Gift name"',
    ],

];
