<?php

//ENGLISH

return [

    'names' => [
        'dialog-count' => 'Ask a question: ',
        'category' => 'Category',
        'status' => 'Status',
        'from' => 'From',
    ],

    'category' => [
        'general' => 'General issue',
        'technical' => 'Technical problem',
        'pay' => 'Payments',
        'donate' => 'Donate',
        'wish-idea' => 'Wish / Idea',
        'cooperation' => 'Cooperation',
        'complaint' => 'Complaint'
    ],

    'dialog-status' => [
        'title' => 'Status',
        'wait' => 'Waiting answer',
        'agent' => 'Agent replied',
        'in-work' => 'In work',
        'closed' => 'Closed',
        'guest' => 'Question of guest',
    ],

    'fa-status' => [
        'wait' => [
            'class' => 'fa fa-clock-o',
            'color' => '#9ADE96',
        ],
        'agent' => [
            'class' => 'fa fa-user-secret',
            'color' => '#96B3DE',
        ],
        'in-work' => [
            'class' => 'fa fa-eye',
            'color' => '#DEC596',
        ],
        'closed' => [
            'class' => 'fa fa-lock',
            'color' => '#DE9B96',
        ],
        'guest' => [
            'class' => 'fa fa-question',
            'color' => '#DE96D2',
        ],
    ],

    'modal' => [
        'help-message' => [
            'title' => 'Contacting support',
            'category' => 'Select a category',
            'text' => 'Message text',
            'placeholder' => 'Detail the all your thoughts',
            'info-auth' => 'Dear user, the answer to his appeal You will be sent within 24 hours of sending the message. About the response specialist technical support services, we will notify you by e-mail and on our website.',
            'info-guest' => 'At the moment you are not logged on our website. You can not get a response to your appeal to the technical support, but we will respect your wishes. If you need assistance with our specialist, you must first login to your account.',
            'close-btn' => 'Cancel',
            'send-btn' => 'Send',
        ],
        'close-question' => [
            'call-bth' => 'Close question',
            'title' => 'Closed question',
            'body-text' => 'Attention! After the closure of the issue you can not get an answer from the agent of technical support and / or supplement existing information. Closing of the issue - an irreversible process. Are you sure you want to close this issue?',
            'btn-cancel' => 'Cancel',
            'btn-confirm' => 'Confirm',
        ],
    ],

    'author' => [
        'guest' => 'Guest',
        'agent' => 'Agent',
        'you' => 'You',
        'user' => 'User',
    ],

];