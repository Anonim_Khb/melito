<?php

//ENGLISH

return [

    'agent-number' => 'You agent #',

    'button' => [
        'admin-panel' => 'Admin panel',
    ],

    'support' => [
        'link' => 'Support',
        'title' => 'Support',
        'title-warning' => 'Attention',
        'about' => 'Dear customers, please note that opening a case the user changes the status of the appeal. After the change of the status of other agents in the case of your refusal will be able to take the matter to the work no earlier than 2 hours. It is desirable to respond to the open circulation immediately',
        'about-warning' => 'Pay your attention to the questions below. These are questions left unanswered. Questions may already belong to other agents, so they need to respond to pre-acquainted with the material',
        'category' => 'Category',
        'status' => 'Status',
        'agent' => 'Agent',
        'author' => 'Author',
        'last-update' => 'Last update',
        'send-message' => 'Send',
        'unblock-sending' => 'Unblock',
        'placeholder-answer' => 'Enter the answer and click "Send"',
        'set-close-question' => 'Close issue',
    ],

    'notification' => [
        'link' => 'Create notification',
        'title' => 'Create notification',
        'about' => 'Dear agents, any creation of alerts to be agreed with the management of the site. To create an alert - select the required notification and click on the button that activates automatically',
        'btn-create' => 'Create',
        'active-general' => [
            'title' => 'Active notifications',
            'about' => 'The list of active visits to all users with the possibility of complete removal',
            'type' => 'Notification',
            'created' => 'Created',
            'delete' => 'Delete',
        ],
    ],

    'gift' => [
        'link' => 'Gifts',
        'title' => 'Gift for all',
        'about' => 'Dear agents, the creation of gift shares to be agreed with the management of the site. To create a share - to select a service and press the button that activates automatically',
        'btn-create' => 'Give',
        'general-placeholder' => 'Enter a brief explanation of the reason for gift',
        'days' => 'Days',
        'personal' => [
            'title' => 'Gift for user',
            'about' => 'This section is intended for personal gifts, ie gifts to specific users',
            'good-name-placeholder' => 'Gift',
            'sum-placeholder' => 'Sum',
            'user-placeholder' => 'User',
        ],
    ],

    'ads' => [
        'link' => 'Ads',
        'title' => 'Ads validate',
        'about' => 'This section is checked for correct filling announcements. Note the status of the ad, it can be both active and hidden from the user, but is awaiting review',
        'image' => 'Title',
        'status' => 'Status',
        'comments' => 'Comments',
        'created' => 'Created',
        'updated' => 'Updated',
        'agent' => 'Agent',
        'comment' => 'Comment',
        'watch' => 'Watch',
        'comment-placeholder' => 'If the ad is in need of adjustment, be sure to enter it here and set the appropriate status',
        'comment-header' => 'Administrator',
        'privilege' => 'Connected services',
        'delete-ad' => [
            'modal-call-btn' => 'Delete',
            'title' => 'Ad delete',
            'body' => 'Please note that removing the ads it is removed from the database forever. Also keep records of the persons removing ads users.',
            'body-question' => 'Are you sure you want to delete this ad?',
            'btn-success' => 'Delete',
            'btn-cancel' => 'Cancel',
            'comment-placeholder' => 'Enter the reason for removal',
        ],
    ],

    'delete-transaction' => [
        'title' => 'Deleted ads',
        'about' => 'List of remote announcements indicating their administrators Remove',
        'ad-id' => 'Ad',
        'agent-id' => 'Agent',
        'comment' => 'Comment',
        'date' => 'Date',
    ],

    'shop' => [
        'link' => 'Shop',
        'title' => 'Store management',
        'about' => 'You can change the prices on certain services , and the packages . In this case you will receive automatically computed to see prices and set prices',
        'real-price' => 'Sum',
        'selling-price' => 'Sell',
    ],

    'transaction' => [
        'about' => 'Information on all the latest operations of users associated with the purchase or activation waste services',
        'user' => 'User',
    ],

];