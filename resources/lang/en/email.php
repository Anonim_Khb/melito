<?php

//ENGLISH

return array(

    'welcome-email' => [
        'title' => 'Confirm registration',
        'welcome' => 'Welcome!',
        'glad-to-see' => 'We are glad to see you on the service',
        'link-about' => 'You need to follow the link below to confirm your registration and to fully enjoy all the features of our project.',
        'btn-confirm' => 'Submit registration',
        'mail-about' => 'This letter was sent to you to confirm the authenticity of the data provided during registration, fraud prevention and confrontation in the use of someone else\'s property. Registration has been made on the site <a href="' . config('app.url') . '" target="_blank" style="color:#526983;">' . config('app.site_name_ru') . '</a>. In case you do not pass this check, no response, and confirmation is not required. Unconfirmed accounts, and with them all the information eventually removed automatically.',
    ],
    'remind-password-email' => [
        'title' => 'Reset password',
        'welcome' => 'Reset password',
        'letter-theme' => 'You or someone else on your behalf requesting forms to reset a forgotten password on the service ',
        'link-about' => 'If you have been initiated reset a forgotten password, you need to click on the link below the "Reset Password" and follow the instructions.',
        'link-time-life' => 'Please note that the link to change the password is valid 1 (one) hour.',
        'btn-reset' => 'Reset password',
    ],

    'message' => [
        'title' => 'New message',
        'theme' => 'You will be sent a new message from the page to view your ',
        'theme-link' => 'ad',
        'user-profile' => 'Sender profile',
        'mail-about' => 'This letter was sent to you after you write a personal message to another user, according to previously accepted the terms and conditions',
    ],

    'support-response' => [
        'title' => 'Support response',
        'welcome' => 'Support response',
        'letter-theme' => 'You got an answer technical support agent you created previously treated on site',
        'link-about' => 'To see the answer you need to pass the link below',
        'btn' => 'Read answer',
        'mail-about' => 'This letter was sent to you to alert you of a new response from the agent to support you previously created question / treatment. This el.pismo needs no response',
    ],

    'ad-verify' => [
        'title' => 'Your ad has been verify',
        'letter-theme' => 'Glad to inform you that you have created an ad successfully been tested and is now available for viewing.',
        'link-about' => 'Following the link below you can view, edit and improve your ads.',
        'btn' => 'Look',
        'mail-about' => 'This letter was sent to you in order to publicize the changed status of previously created by you and / or edited ads',
    ],

    'ad-not-verify' => [
        'title' => 'Your ad is hide',
        'letter-theme' => 'To inform you that you have created an ad not been tested and is no longer available for viewing by users.',
        'link-about' => 'Following the link below you can view, edit and improve your ads. After editing, all users will again be able to see your ad.',
        'btn' => 'Edit',
        'mail-about' => 'This letter was sent to you in order to publicize the changed status of previously created by you and / or edited ads',
    ],

);