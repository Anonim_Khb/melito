<?php

//ENGLISH

return [
    'in' => '\i\n',
    'error-page-title' => 'Error',
    'language' => 'Язык',
    'language-flag' => '/melito_img/flags/en-1.png',
    'log-in' => 'Login',
    'ads' => 'Ads',
    'shop' => 'Shop',
    'logout-header' => 'Logout',
    'fast-search' => 'Fast search',
    'rented-room' => 'Rented rooms',
    'looking-room' => 'Looking rooms',
    'oops' => 'Oops!',
    'something-went-wrong' => 'Something went wrong....',
    'support-link' => 'Support',
    'password-reset-btn' => 'Reset password',
    'register-fields' => 'Register form',
    'my-account' => 'My account',
    'support' => 'Support',
    'header-no-notifications' => 'You have no unread notifications',
    'hide-all-notifications' => 'Hide all',
    'notifications-links' => 'Read more',

    'footer' => [
        'home' => 'Main',
        'support' => 'Help',
        'need-room' => 'Need room',
        'offer-room' => 'Offer room',
        'shop' => 'Shop',
        'rules' => 'Rules',
        'log-in' => 'Login',
        'sign-in' => 'Register',
        'personal-office' => 'Personal office',
        'my-ads' => 'My ads',
        'my-donate' => 'My donate',
        'my-transactions' => 'My sales',
        'my-bookmarks' => 'My bookmarks',
        'my-help' => 'My questions',
        'my-settings' => 'My settings',
        'lang-ru' => 'Русский язык',
        'lang-en' => 'English language',
        'description' => 'Melito - the website for those who donate or want to rent a room or an apartment on favorable terms',
    ],

    'menu' => [
        'support' => 'Support',
        'rules' => 'Rules',
        'faq' => 'Answers & Questions',
        'sell-house' => 'Sell housing',
        'buy-house' => 'Buy housing',
        'want-rent' => 'Want rent',
        'need-rent' => 'Need rent',
        'my-ads-room' => 'My ads',
    ],

    'search' => [
        'city' => [
            'title' => 'City',
            'popover' => 'The city in which you want to rent or buy a home'
        ],
        'area' => [
            'title' => 'Area',
            'popover' => 'Specify minimum and maximum values of the area to search for housing',
            'min' => 'min',
            'max' => 'max',
        ],
        'price' => [
            'title' => 'Price',
            'popover' => 'Specify minimum and maximum values of the rental or sale of housing search',
            'min' => 'min',
            'max' => 'max',
        ],
        'house-type' => [
            'title' => 'Type of room',
            'popover' => 'Type of housing , such as in high-rise apartment or private house',
            'any' => 'Any',
            'apartment' => 'Apartment',
            'house' => 'House',
            'studio' => 'Studio',
        ],
        'deal-type' => [
            'title' => 'Deal type',
            'popover' => 'Specify the type of transaction that you are interested in , that is, whether you want to rent a house or buy',
            'any' => 'Any',
            'rent' => 'Rent',
            'sale' => 'Sale',
        ],
        'without-prepayment' => [
            'title' => 'Prepayment',
            'popover' => 'Search property for sale / lease that does not require advance payments and/or prepayment',
            'text' => 'Without prepayment',
        ],
        'with-image' => [
            'title' => 'Images',
            'popover' => 'Search only those ads , the description of which includes photographs',
            'text' => 'With images',
        ],
        'gender' => [
            'title' => 'Your gender',
            'popover' => 'The filter includes a landlord wishes to take shelter to man, given his gender',
            'any' => 'Not specified',
            'male' => 'Male',
            'female' => 'Female',
        ],
        'occupation' => [
            'title' => 'Your occupation',
            'popover' => 'The filter includes a landlord wishes to take shelter to man, given his occupation',
            'any' => 'Not specified',
            'student' => 'Student',
            'professional' => 'Working',
        ],
        'age' => [
            'title' => 'Age',
            'popover' => 'The filter includes a landlord wishes to take shelter man , given his age',
            'any' => 'Not specified',
        ],

        'entry-date' => [
            'title' => 'Entry date',
            'popover' => 'Specifies the date , not later than that available housing',
            'placeholder' => 'Not specified',
        ],
        'communal' => [
            'title' => 'Neighborhood',
            'popover' => 'It refers to rental housing . Would you like to take a room in one , or agree to the shares',
            'any' => 'Not specified',
            'single' => 'For one',
            'several' => 'Possible neighbors',
        ],
        'search-btn' => 'SEARCH',
    ],

    'performance' => [
        'housemates_1' => 'Find the perfect neighbor',
        'thumbs_up_1' => 'Excellent service',
        'gives_key_1' => 'Take a quick deal',
        'multiscreens_1' => 'Access to all devices',
        'golden_key_1' => 'The result of today',
        'houses_around_1' => 'Great choice',
        'coffee_cup_1' => 'Comfort and convenience',
        'house_search_1' => 'Individual selection',
    ],

];