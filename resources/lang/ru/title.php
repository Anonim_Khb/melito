<?php

//RUSSIAN

return [
    'login' => 'Вход',
    'register' => 'Регистрация',
    'register-complete' => 'Регистрация завершена',
    'register-not-confirm' => 'Регистрация не подтверждена',
    'password-remind' => 'Сброс пароля',
    'new-offer' => 'Сдать комнату',
    'new-need' => 'Снять комнату',
    'offer-open-watch' => 'Объявление',
    'user-profile' => 'Профиль',
    'shop-about' => 'Магазин',
    'ad-edit' => 'Редактирование',
    'payment-success' => 'Система оплаты',
    'payment-fail' => 'Система оплаты',
    'support-general' => 'Помощь',
    'admin-general' => 'Управление',
    'admin-support' => 'Техподдержка',
    'admin-notification' => 'Оповещения',
    'admin-gift' => 'Подарки',
    'admin-ads-validate' => 'Проверка объявлений',
    'admin-shop' => 'Магазин',
    'rules' => 'Правила',
    'faq' => 'Помощь',
];