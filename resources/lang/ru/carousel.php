<?php

//RUSSIAN

return [

    'home' => [
        '0' => [
            'title' => 'Testing Title 1',
            'text' => 'Testing text 1 Testing text 1',
            'image' => '/melito_img/carousel/1111.png',
            'active' => true,
        ],
        '1' => [
            'title' => 'Testing Title 2',
            'text' => 'Testing text 2 Testing text 2',
            'image' => '/melito_img/carousel/2222.png',
            'active' => false,
        ],
        '2' => [
            'title' => 'Testing Title 3',
            'text' => 'Testing text 3 Testing text 3',
            'image' => '/melito_img/carousel/3333.png',
            'active' => false,
        ],
        '3' => [
            'title' => 'Testing Title 4',
            'text' => 'Testing text 4 Testing text 4',
            'image' => '/melito_img/carousel/4444.png',
            'active' => false,
        ],
    ],

];