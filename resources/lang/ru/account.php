<?php

//RUSSIAN

return array(

    //For function in helpers.php
    'user-status' => [
        'online' => 'Онлайн',
        'offline' => 'Не в сети',
        'now-offline' => 'Был в сети: ',
        'in' => ' \в ',
    ],

    //Links && Titles
    'menu' => [
        'link-personal-info' => 'Анкета',
        'link-message' => 'Сообщения',
        'link-ads' => 'Объявления',
        'link-share' => 'Закладки',
        'link-setting' => 'Настройки',
        'link-help' => 'Помощь',
        'link-donate' => 'Донат',
    ],
    //Personal
    'pr-part-general' => [
        'title' => 'Общая информация',
        'avatar' => 'Аватар',
        'avatar-validation' => 'Файл должен быть изображением (форматы: jpeg, png, bmp, gif) не более 10 Мб',
        'avatar-change' => 'Сменить аватар',
        'avatar-remove' => 'Удалить аватар',
        'name' => [
            'title' => 'Имя',
            'placeholder' => 'Ваше имя или псевдоним',
        ],
        'gender' => [
            'title' => 'Пол',
            'male' => 'Мужской',
            'female' => 'Женский',
            'not_select' => 'Не выбрано',
        ],
        'birthday' => [
            'title' => 'Дата рождения',
            'placeholder' => 'гггг-мм-дд',
        ],
        'avatar-delete' => [
            'title' => 'Удаление аватара',
            'text' => '<p>Удаление аватара - необратимый процесс. Вам будет назначен стандартный аватар.</p>
                <p>Вы уверены, что хотите удалить аватар?</p>',
            'btn-cancel' => 'Отмена',
            'btn-confirm' => 'Удалить',
        ],
        'avatar-crop' => [
            'title' => 'Редактирование аватара',
            'avatar-crop-info' => 'Выделите область изображения и нажмите кнопку «Сохранить»',
            'avatar-crop-btn' => 'Сохранить',
        ],
    ],
    'pr-part-personal' => [
        'title' => 'Личное',
        'smoke' => [
            'title' => 'Отношение к курению',
            'no' => 'Не курю',
            'yes' => 'Курю',
        ],
        'alcohol' => [
            'title' => 'Отношение к алкоголю',
            'yes' => 'Выпиваю',
            'sometime' => 'Иногда',
            'no' => 'Не пью',
        ],
        'criminal' => [
            'title' => 'Судимость',
            'no' => 'Не имею',
            'yes' => 'Есть',
        ],
        'work' => [
            'title' => 'О работе',
            'yes' => 'Работаю',
            'student' => 'Студент',
            'business' => 'Свой бизнес',
            'no' => 'Безработный',
        ],
        'additional' => [
            'title' => 'Дополнительно',
            'placeholder' => 'Любая другая информация о вас',
        ],
    ],
    'pr-part-contact' => [
        'title' => 'Контакты',
        'email' => 'Эл. адрес',
        'phone' => [
            'title' => 'Телефон',
            'placeholder' => 'Контактный номер',
        ],
        'skype' => [
            'title' => 'Скайп',
            'placeholder' => 'Логин в Скайп',
        ],
        'contacts' => 'Доступно для связи',
        'available' => 'Другое',
        'another-contact' => [
            'title' => 'Другое',
            'placeholder' => 'Другой способ связи',
        ],
    ],
    //Buttons
    'save' => 'Сохранить',

    'ads' => [
        'ads-count' => 'Объявления',
        'watch' => 'Подробнее',
        'create' => 'Создать',
        'none-ads' => 'У Вас пока нет объявлений',
        'timestamp' => [
            'created' => 'Создано',
            'updated' => 'Поднято',
        ],
        'square-meters' => 'м<sup>2</sup>',
        'street-abbr' => 'ул.',
        'view-count' => 'Просмотров',
        'deal-type' => [
            'title' => 'Тип сделки',
            'rent' => 'Сдается за',
            'sale' => 'Продается за',
        ],
        'btn' => [
            'title' => 'Действия',
            'crop' => 'Обложка',
            'premium' => 'Премиум',
            'edit' => 'Редактировать',
            'up' => [
                'title' => 'Поднять',
                'free' => 'Бесплатно',
                'premium' => 'Премиум',
            ],
            'hide' => 'Скрыть',
            'show' => 'Опубликовать',
        ],
        'ad-cover-modal' => [
            'title' => 'Обложка',
            'text' => 'Обратите внимание, что вы редактируете только обложку объявления, а не само фото. Удачное представление обложки может способствовать популярности объявления.',
        ],
    ],

    'donate' => [
        'balance' => [
            'title' => 'Ваш баланс'
        ],
        'goods' => [
            'title' => 'Ваши услуги'
        ],
        'privileges' => [
            'title' => 'Ваши заказы',
            'btn-add' => 'Активировать услугу',
            'expired' => 'Истекло',
            'active' => 'Активно',
        ],
        'transactions' => [
            'title' => 'Транзакции',
            'columns' => [
                'type' => 'Тип',
                'ad-id' => 'Объявление',
                'name' => 'Название',
                'number' => 'Количество',
                'sum' => 'Сумма',
                'date' => 'Дата',
            ],
        ],
    ],

    'bookmark' => [
        'delete-all' => 'Удалить все закладки',
        'delete-one-bookmark' => 'Удалить закладку',
        'ad-watch' => 'Посмотреть',
        'none-bookmarks' => 'У вас пока нет закладок',
        'modal-bad-title' => 'Подтверждение удаления',
        'modal-bad-text' => 'Удаление будет необратимым. Вы уверены, что хотите удалить все свои закладки?',
    ],

    'settings' => [
        'change-password' => [
            'title' => 'Сменить пароль',
            'old-password' => 'Старый пароль',
            'new-password' => 'Новый пароль',
            'repeat-password' => 'Повторите пароль',
            'save-btn' => 'Сохранить',
        ],
    ],

    'another' => [
        'not-exists' => 'не указано',
        'exists' => 'есть',
        'not-ads' => 'У пользователя пока нет созданных объявлений',
    ],

);