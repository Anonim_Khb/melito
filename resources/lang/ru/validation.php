<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute должен быть принят.',
    'active_url' => 'Поле :attribute не является URL адресом.',
    'after' => 'Поле :attribute должно быть более поздней датой, чем :date.',
    'alpha' => 'Поле :attribute может содержать только буквы.',
    'alpha_dash' => 'Поле :attribute может содержать только алфавитные символы, цифры, знаки подчёркивания и дефисы',
    'alpha_num' => 'Поле :attribute может содержать только буквы и цифры.',
    'array' => 'Поле :attribute должно быть массивом',
    'before' => 'Поле :attribute должно быть более ранней датой, чем :date.',
    'between' => [
        'numeric' => 'Значение поля :attribute должно быть между :min и :max.',
        'file' => 'Значение поля :attribute должно быть между :min и :max килобайт.',
        'string' => 'Значение поля :attribute должно быть от :min до :max символов.',
        'array' => 'Значение поля :attribute должно быть между :min и :max items.',
    ],
    'boolean' => 'Значение поля :attribute должно быть True или False.',
    'confirmed' => 'Поля :attribute и его повтор должны быть одинаковыми',
    'date' => 'Значение :attribute должно быть датой.',
    'date_format' => 'Значение :attribute не соответсвует формату :format.',
    'different' => 'Значения :attribute и :other должны отличаться.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'Значение поля :attribute должно быть от :min до :max цифр.',
    'email' => 'Значение поля :attribute должно быть адресом эл.почты.',
    'filled' => 'The :attribute field is required.',
    'exists' => 'Такого :attribute не существует в нашей системе.',
    'image' => 'Содержимое :attribute должно быть изображением.',
    'in' => 'Значение поля :attribute неверно.',
    'integer' => 'Значение :attribute должно быть простым значением.',
    'ip' => 'The :attribute must be a valid IP address.',
    'max' => [
        'numeric' => 'Поле :attribute не должно быть не более :max.',
        'file' => 'Содержимое :attribute должно быть не более :max килобайт.',
        'string' => 'Поле :attribute не должно быть длиннее :max символов.',
        'array' => 'The :attribute may not have more than :max слов.',
    ],
    'mimes' => 'Содержимое :attribute должно быть одним из форматов: :values.',
    'min' => [
        'numeric' => 'Поле :attribute не должно быть менее :min.',
        'file' => 'Содержимое :attribute должно быть не менее :min килобайт.',
        'string' => 'Поле :attribute не должно быть короче :min символов.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'Значение поля :attribute некорректно или запрещено',
    'numeric' => 'Поле :attribute должно быть числом.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'Поле :attribute обязательно для заполнения.',
    'required_if' => 'Поле :attribute обязательно для заполения, если активно поле :other.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'Значения :attribute и :other должны совпадать.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'Такой :attribute уже используется кем-то на нашем сайте.',
    'url' => 'The :attribute format is invalid.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'g-recaptcha-response' => [
            'captcha' => 'Проверка "Я не робот" не пройдена.',
        ],
        'password' => [
            'confirmed' => 'Значения полей "Пароль" и "Повтор пароля" должны совпадать.',
        ],
        'new_password' => [
            'confirmed' => 'Значения полей "Новый пароль" и "Повтор пароля" должны совпадать.',
        ],
        'pr_token' => [
            'required' => 'Ошибка безопасности, пожалуйста, повторите попытку снова.',
            'exists' => 'Ошибка безопасности. Возможно Вы ввели неверный Email. Пожалуйста, повторите попытку снова.',
        ],
        'rules' => [
            'required' => 'Вы должны ознакомиться и принять правила',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'email' => '"Эл. адрес"',
        'password' => '"Пароль"',
        'g-recaptcha-response' => '"Каптча"',
        'new_password' => '"Новый пароль"',

        //Account
        'name' => '"Имя"',
        'gender' => '"Пол"',
        'birthday' => '"Дата рождения"',
        'phone' => '"Телефон"',
        'skype' => '"Skype"',
        'whatsapp' => '"Whatsapp"',
        'viber' => '"Viber"',
        'messenger' => '"Messenger"',
        'contact_another' => '"Другой способ связи"',
        'alcohol' => '"Отношение к алкоголю"',
        'smoke' => '"Отношение к курению"',
        'criminal' => '"Судимость"',
        'work' => '"О работе"',
        'additional' => '"Дополнительно"',
        'text_msg' => '"Текст сообщения"',
        'old_password' => '"Старый пароль"',
        'new_password_1' => '"Новый пароль"',
        'new_password_2' => '"Повторение пароля"',

        //Ads Offer
        'city' => '"Город"',
        'street_name' => '"Улица"',
        'house_number' => '"Номер дома"',
        'house_type' => '"Тип помещения"',
        'communal' => '"Тип комнаты"',
        'sanitary_ware' => '"Санузел"',
        'wifi' => '"Wi-Fi"',
        'tv' => '"ТВ"',
        'parking' => '"Парковка"',
        'balcony' => '"Балкон"',
        'garden' => '"Сад"',
        'security' => '"Охрана"',
        'elevator' => '"Лифт"',
        'jacuzzi' => '"Джакуззи"',
        'boiler' => '"Бойлер"',
        'gim' => '"Тренажерный зал"',
        'furniture' => '"Мебель"',
        'appliances' => '"Бытовая техника"',
        'rent_price' => '"Цена аренды"',
        'add_pay' => '"Дополнительная плата"',
        'room_meters' => '"Площадь (кв.м.)"',
        'available_from' => '"Дата заселения"',
        'prepayment' => '"Предоплата"',
        'gender' => '"Пол"',
        'occupation' => '"Род занятий"',
        'age_from' => '"Возраст ОТ"',
        'age_to' => '"Возраст ДО"',
        'children' => '"С детьми"',
        'couples' => '"Пары"',
        'guests' => '"Гости"',
        'pets' => '"Животные"',
        'smoking' => '"Курение"',
        'ad_text' => '"Текст объявления"',
        'floor' => 'Этаж',

        //Ads NEED
        'price_from' => '"Цена ОТ"',
        'price_to' => '"Цена ДО"',
        'advanced-search' => '"Расширенный поиск"',
        'room_meters_from' => '"Площадь комнаты ОТ"',
        'room_meters_to' => '"Площадь комнаты ДО"',
        'entry' => '"Дата въезда"',
        'age' => '"Возраст"',

        //Payment
        'sum' => '"Сумма"',

        //Support
        'category' => '"Категория"',
        'text' => '"Текст"',
        'dialog_id' => '"Диалог"',
        'dialog' => '"Диалог"',

        //Admin
        'good_name' => '"Имя подарка"',
    ],

];