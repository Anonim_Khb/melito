<?php

//RUSSIAN

return array(

    'ad-status' => [
        'title' => 'Статус',
        'not-confirm' => 'Регистрация не подтверждена',
        'active' => 'Активно',
        'wait' => 'Активно, не проверено',
        'closed' => 'Скрыто',
        'for-correct' => 'Нужны исправления',
        'corrected' => 'Ожидает проверку',
    ],
    'ad-info' => [
        'info' => '<ul>
            <li><i class="fa fa-map-marker">&nbsp;</i> :city</li>
            <li>Просмотров: :views</li>
            <li>Создано: :create</li>
            <li>Поднято: :update</li>
        </ul>'
    ],
    'new-offer' => [
        'not-select' => 'Не выбрано',
        'title' => 'Сдача комнаты',
        'under-title' => 'Подавайте объявления БЕСПЛАТНО, быстро и легко!',
        'locality' => [
            'title' => 'Местоположение',
            'city' => 'Город',
            'select-city' => 'Выберите город',
        ],
        'street-name' => 'Улица',
        'house-number' => 'Номер дома',
        'deal-type' => [
            'title' => 'Тип сделки',
            'rent' => 'Аренда',
            'sale' => 'Продажа',
        ],
        'house-type' => [
            'title' => 'Тип помещения',
            'apartment' => 'Квартира',
            'house' => 'Дом',
            'studio' => 'Студия',
        ],
        'communal' => [
            'title' => 'Тип аренды',
            'single' => 'Для одного',
            'several' => 'С подселением',
        ],
        'sanitary_ware' => [
            'title' => 'Санузел',
            'home' => 'В доме',
            'street' => 'На улице',
        ],
        'amenities' => [
            'title' => 'Удобства',
            'wifi' => 'Wi-Fi',
            'tv' => 'ТВ',
            'parking' => 'Парковка',
            'balcony' => 'Балкон',
            'garden' => 'Сад',
            'security' => 'Охрана',
            'elevator' => 'Лифт',
            'jacuzzi' => 'Джакузи',
            'gim' => 'Тренажерный зал',
            'furniture' => 'Мебель',
            'appliances' => 'Бытовая техника',
            'boiler' => 'Бойлер',
        ],
        'rent-price' => [
            'title' => 'Стоимость',
            'rub' => 'Рубль',
        ],
        'additional-payment' => 'Дополнительная плата',
        'room-meters' => 'Площадь',
        'available-from' => 'Доступна с',
        'prepayment' => 'Предоплата (месяцев)',
        'preferred' => [
            'title' => 'Предпочтительно',
            'gender' => [
                'title' => 'Предпочтительные пол',
                'male' => 'Мужчина',
                'female' => 'Женщина',
                'not-matter' => 'Не важно',
            ],
            'occupation' => [
                'title' => 'Предпочтительное занятие',
                'student' => 'Студент',
                'professional' => 'Работающий',
                'not-matter' => 'Не важно',
            ],
            'rules' => [
                'title' => 'Правила',
                'children' => 'Семьи и дети принимаются',
                'couples' => 'Пары принимаются',
                'guests' => 'Гости',
                'pets' => 'Животные разрешены',
                'smoking' => 'Курение разрешено',
            ],
            'age' => [
                'title' => 'Предпочтительный возраст',
                'from' => 'От',
                'to' => 'До',
            ]
        ],
        'ad-present' => [
            'title' => 'Ваше объявление',
            'ad-text' => [
                'title' => 'Текст объявления'
            ],
        ],
        'images' => [
            'button' => 'Выбрать фото'
        ],
        'form-button' => 'Создать',
        'ad-floor' => 'Этаж'
    ],

    'new-need' => [
        'title' => 'Найти комнату',
        'rent-price' => [
            'title' => 'Цена',
            'from' => 'От',
            'to' => 'До',
        ],
        'room-meters' => [
            'title' => 'Метраж комнаты',
            'from' => 'От',
            'to' => 'До'
        ],
        'advanced-search' => 'Расширенный поиск',
        'form-button' => 'Искать',
        'additional-payment' => 'Возможна дополнительная плата',
        'entry' => 'Дата въезда',
        'prepayment' => 'Возможна предоплата',
        'gender' => [
            'title' => 'Ваш пол'
        ],
        'occupation' => [
            'title' => 'Род занятий'
        ],
        'age' => [
            'title' => 'Ваш возраст',
            'value' => 'лет',
        ],
        'with-images' => 'Фотография обязательна'
    ],

    'ad-watch' => [
        'city' => 'Город: ',
        'views' => 'Просмотры: ',
        'reg-not-confirm' => 'E-Mail не подтвержден',
        'reg-confirm' => 'E-Mail подтвержден',
        'null-data' => 'Не указано',
        'ad-up' => 'Поднято: ',
        'ad-edit' => 'Редактировать',
        'profile' => [
            'name' => 'Имя',
            'gender' => [
                'title' => 'Пол',
                'male' => 'Мужской',
                'female' => 'Женский',
            ],
            'age' => 'Возраст',
            'href' => 'Профиль юзера',
            'write-letter' => 'Написать автору',
        ],
        'send-email' => [
            'title' => 'Написать автору',
            'disabled-send' => 'Для отправки сообщения пользователю необходима регистрация',
        ],
        'street-name' => 'Улица',
        'house-number' => 'Номер дома',
    ],
    'ad-edit' => [
        'save-button' => 'Сохранить',
    ],

);