<?php

//RUSSIAN

return [

    'agent-number' => 'Вы агент #',

    'button' => [
        'admin-panel' => 'Админ панель',
    ],

    'support' => [
        'link' => 'Техподдержка',
        'title' => 'Техподдержка',
        'title-warning' => 'Обратите внимание',
        'about' => 'Уважаемые агенты, обратите Ваше внимание, что открыв обращение пользователя, изменяется статус данного обращения. После смены статуса другие агенты, в случае Вашего отказа, смогут взять вопрос в работу не ранее, чем через 2 часа. Желательно отвечать на открытые обращения сразу',
        'about-warning' => 'Обратите Ваше внимание на вопросы ниже. Это вопросы оставленные без ответа. Вопросы уже могут принадлежать другим агентам, поэтому отвечать на них нужно предварительно ознакомившись с материалом',
        'category' => 'Категория',
        'status' => 'Статус',
        'agent' => 'Агент',
        'author' => 'Автор',
        'last-update' => 'Посл.изменение',
        'send-message' => 'Отправить',
        'unblock-sending' => 'Разблокировать',
        'placeholder-answer' => 'Введите ответ и нажмите кнопку "Отправить"',
        'set-close-question' => 'Закрыть вопрос',
    ],

    'notification' => [
        'link' => 'Создать оповещение',
        'title' => 'Создание оповещений',
        'about' => 'Уважаемые агенты, любое создание оповещений должно быть согласовано с руководством сайта. Для того, чтобы создать оповещение - выберите необходимое оповещение и нажмите на кнопку, которая активируется автоматически',
        'btn-create' => 'Создать',
        'active-general' => [
            'title' => 'Активные оповещения',
            'about' => 'Список активных оповещений для всех пользователей с возможностью их полного удаления',
            'type' => 'Оповещение',
            'created' => 'Создано',
            'delete' => 'Удалить',
        ],
    ],

    'gift' => [
        'link' => 'Подарки',
        'title' => 'Подарок для всех',
        'about' => 'Уважаемые агенты, создание подарочных акций должно быть согласовано с руководством сайта. Для того, чтобы создать акцию - выберите необходимую услугу и нажмите на кнопку, которая активируется автоматически',
        'btn-create' => 'Подарить',
        'general-placeholder' => 'Введите краткое пояснение-причину подарка',
        'days' => 'Дни',
        'personal' => [
            'title' => 'Подарок пользователю',
            'about' => 'Раздел предназначен для персональных подарков, то есть подарков конкретным пользователям',
            'good-name-placeholder' => 'Подарок',
            'sum-placeholder' => 'Кол-во',
            'user-placeholder' => 'Юзер',
        ],
    ],

    'ads' => [
        'link' => 'Объявления',
        'title' => 'Проверка объявлений',
        'about' => 'В данном разделе осуществляется проверка объявлений на правильность заполнения. Обратите внимание на статус объявления, оно может быть как активным, так и скрыто от пользователей, но ожидающее проверки',
        'image' => 'Обложка',
        'status' => 'Статус',
        'comments' => 'Комментарии',
        'created' => 'Создано',
        'updated' => 'Изменено',
        'agent' => 'Агент',
        'comment' => 'Комментарий',
        'watch' => 'Посмотреть',
        'comment-placeholder' => 'Если объявление нуждается в корректировке, то обязательно укажите это здесь и установите соответствующий статус',
        'comment-header' => 'Администратор',
        'privilege' => 'Подключенные услуги',
        'delete-ad' => [
            'modal-call-btn' => 'Удалить',
            'title' => 'Удаление объявления',
            'body' => 'Обратите внимание, что удаляя объявление оно удаляется из базы данных безвозвратно. Также ведется учет лиц, удаляющих объявления пользователей.',
            'body-question' => 'Вы уверены, что хотите удалить данное объявление?',
            'btn-success' => 'Удалить',
            'btn-cancel' => 'Отмена',
            'comment-placeholder' => 'Укажите причину удаления',
        ],
    ],

    'delete-transaction' => [
        'title' => 'Удаленные объявления',
        'about' => 'Список удаленных объявлений с указанием комментариев удаливших их администраторов',
        'ad-id' => 'Объявление',
        'agent-id' => 'Агент',
        'comment' => 'Комментарий',
        'date' => 'Дата',
    ],

    'shop' => [
        'link' => 'Магазин',
        'title' => 'Управление магазином',
        'about' => 'Здесь Вы можете изменять цены как на отдельные услуги, так и на пакеты. При этом Вы будете получать автоматически расчитанные цены и видеть установленные скидки',
        'real-price' => 'Сумма',
        'selling-price' => 'Продажа',
    ],

    'transaction' => [
        'about' => 'Информация по всем послежним операциям юзеров, связанных с покупкой, тратой или активацией услуг',
        'user' => 'Юзер',
    ],

];