<div class="list-group">

    <a class="list-group-item @if($menuActive == 'login-password') active @endif" href="{{ route('faq-login-password') }}">
        {{ trans('faq.menu.login-password') }}
    </a>

    <a class="list-group-item @if($menuActive == 'ads-work') active @endif" href="{{ route('faq-ads-work') }}">
        {{ trans('faq.menu.ads-work') }}
    </a>

    <a class="list-group-item @if($menuActive == 'placing-ads') active @endif" href="{{ route('faq-placing-ads') }}">
        {{ trans('faq.menu.placing-ads') }}
    </a>

    <a class="list-group-item @if($menuActive == 'ad-edit') active @endif" href="{{ route('faq-ad-edit') }}">
        {{ trans('faq.menu.ad-edit') }}
    </a>

</div>

<div class="list-group second-menu">

    <a class="list-group-item" data-toggle="modal" data-target="#modalSupportSendMessage">
        {{ trans('home.menu.support') }}
    </a>

    <a class="list-group-item" href="{{ route('rules-general') }}">
        {{ trans('home.menu.rules') }}
    </a>

</div>