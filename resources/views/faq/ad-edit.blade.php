@extends('faq.main')
@section('body')

    <h2 class="support-title">{{ trans('faq.ad-edit.title') }}</h2>

    @foreach($questions as $key => $value)

        <p>

            <a role="button" data-toggle="collapse" href="#{{ $key }}" aria-expanded="false"
               aria-controls="{{ $key }}">
                {{ $value['question'] }}
            </a>

            <div class="collapse" id="{{ $key }}">
                <div class="well">
                    {!! $value['answer'] !!}
                </div>
            </div>

        </p>

    @endforeach


@endsection


@section('js-bottom')

@endsection

