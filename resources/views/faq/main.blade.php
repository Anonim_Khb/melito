<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    <script src="/myjs/support.js"></script>

    <link href='/css/melito_css/support.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-header">
        <div class="col-md-24 section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row row-center faq-rules-menu-row general-menu-collapse-1">
        <div class="col-xs-24 col-sm-7 col-md-6 col-lg-6">
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#faq-page-navbar-collapse" aria-expanded="false">
                            <i class="collapse-btn fa fa-bars"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="faq-page-navbar-collapse">
                        @include('faq.menu')
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-xs-24 col-sm-15 col-md-15 col-lg-14">
            @include('partials.page-notification')
            @yield('body')
            @yield('js-bottom')
            @include('partials.packages-js')
            @include('modals.support-send-message')
        </div>
    </div>

    <div class="row row-footer">
        <div class="col-md-24 section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>