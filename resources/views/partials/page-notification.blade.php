<div class="alert alert-dismissible alert-warning global-alert-error @If(Notifications::byType('danger')->count() <= 0) hidden @endif">
    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>
    <p class="alert-error-title">{{ trans('home.something-went-wrong') }}</p>
    <ul>
        @foreach(Notifications::byType('danger')->get() as $a_error)
            <li>{{$a_error['message']}}</li>
        @endforeach
    </ul>
</div>