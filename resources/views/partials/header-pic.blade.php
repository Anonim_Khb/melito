<div id="headerCarousel">

    <div id="carousel-header" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            @foreach($carousels as $key => $value)
                <li data-target="#carousel-header" data-slide-to="{{ $key }}"
                    @if($value['active']) class="active" @endif></li>
            @endforeach
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($carousels as $key => $value)
                <div class="item @if($value['active']) active @endif">
                    <img style="background: url('{{ $value['image'] }}') center center;background-size: cover;"
                         src="" alt="">

                    <div class="carousel-caption">
                        <h1 class="carousel-title">{{ $value['title'] }}</h1>

                        <p class="carousel-text">{{ $value['text'] }}</p>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-header" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-header" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>