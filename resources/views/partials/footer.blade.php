<div>

    <div class=" row footer-logo-name text-center">
        <img src="{{ asset('/melito_img/logo/mf-4.png') }}">
    </div>

    <div class="row">

        <div class="footer-logo-img hidden-xs">
            <img src="{{ asset('/melito_img/logo/melito-l8.png') }}">
        </div>

        <div class="footer-list">
            <ul class="list-inline">
                <li>
                    <a href="{{ route('home') }}">{{ trans('home.footer.home') }}</a>
                </li>
                <li>
                    <a href="{{ route('faq-login-password') }}">{{ trans('home.footer.support') }}</a>
                </li>
                <li>
                    <a href="{{ route('shop-page') }}">{{ trans('home.footer.shop') }}</a>
                </li>
                <li>
                    <a href="{{ route('rules-general') }}">{{ trans('home.footer.rules') }}</a>
                </li>
                <li>
                    <a href="{{ route('ads-new-need') }}">{{ trans('home.footer.need-room') }}</a>
                </li>
                <li>
                    <a href="{{ route('ads-new-offer') }}">{{ trans('home.footer.offer-room') }}</a>
                </li>
            </ul>

            <ul class="list-inline">
                @if(Auth::check())
                    <li>
                        <a href="{{ route('account-personal') }}">{{ trans('home.footer.personal-office') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-ads') }}">{{ trans('home.footer.my-ads') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-donate') }}">{{ trans('home.footer.my-donate') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-transactions') }}">{{ trans('home.footer.my-transactions') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-bookmarks') }}">{{ trans('home.footer.my-bookmarks') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-support') }}">{{ trans('home.footer.my-help') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('account-my-settings') }}">{{ trans('home.footer.my-settings') }}</a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('login') }}">{{ trans('home.footer.log-in') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">{{ trans('home.footer.sign-in') }}</a>
                    </li>
                @endif
            </ul>

            <ul class="list-inline">
                <li>
                    <a href="{{ route('lang-select', 'ru') }}">{{ trans('home.footer.lang-ru') }}</a>
                </li>
                <li>
                    <a href="{{ route('lang-select', 'en') }}">{{ trans('home.footer.lang-en') }}</a>
                </li>
            </ul>
        </div>

    </div>

    <div class="row text-center">
        @include('partials.share-buttons', [
            $url = route('home'),
            $image = null,
            $description = trans('home.footer.description')
        ])
    </div>

    <div class="row text-center footer-privacy">
        Copyright &copy; {{ Jenssegers\Date\Date::now()->year }}
        <br>
        Melito &trade; Inc.
    </div>

</div>