<div id="myHeader">
    <div class="navbar navbar-info navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="menuCollapseBtn" type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>
                <a href="{{ route('home') }}" class="navbar-brand">
                    <img src="{{ asset('/' . config('image.headerLogoForColorful')) }}">
                </a>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle header-link" data-toggle="dropdown">
                            <img id="localeFlag" src="{{ trans('home.language-flag') }}" alt="flag">
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('lang-select', 'ru') }}">
                                    <img src="{{ asset('/melito_img/flags/ru-1.png') }}" alt="ru">&nbsp;
                                    Русский
                                    @if(userLocale() == 'ru') <i class="fa fa-check locale-check"></i> @endif
                                </a>
                            </li>
                            <li><a href="{{ route('lang-select', 'en') }}">
                                    <img src="{{ asset('/melito_img/flags/en-1.png') }}" alt="en">&nbsp;
                                    English
                                    @if(userLocale() == 'en') <i class="fa fa-check locale-check"></i> @endif
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- NOTIFICATION -->
                    <li class="dropdown header-notifications">
                        <a class="dropdown-toggle header-link" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            @if(count($new_notifications) > 0)
                                <span id="headerNotificationCount" class="badge">
                                        {{ count($new_notifications) }}
                                    </span>
                            @endif
                        </a>
                        <ul id="headerNotificationDropdown"
                            class="dropdown-menu">

                            @if(count($new_notifications) > 0)

                                @foreach($new_notifications as $notification)

                                    <li class="list-notifications">
                                        <div class="media header-notification-block" data-id="{{ $notification->id }}">
                                            <div class="media-left">
                                                <img class="media-object"
                                                     src="/{{ config('image.icons_path') . $notification->icon_src }}"
                                                     alt="icon" data-holder-rendered="true">
                                            </div>
                                            <div class="media-body"><h5
                                                    class="media-heading">{{ trans($notification->text) }}</h5>
                                                @if($notification->url != null)
                                                    <p>
                                                        <a data-href="{{ route($notification->url) }}"
                                                           class="header-notification-hide notification-url">
                                                            {{ trans('home.notifications-links') }}
                                                        </a>
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="media-right">
                                                <i class="fa fa-close notification-delete header-notification-hide"></i>
                                            </div>
                                        </div>
                                    </li>

                                @endforeach

                                <li id="hideAllHeaderNotifications" class="text-center">
                                    <a>{{ trans('home.hide-all-notifications') }}</a>
                                </li>

                            @endif

                            <li id="headers-none-notifications" class="text-center"
                                @if(count($new_notifications) > 0) style="display: none" @endif>
                                {{ trans('home.header-no-notifications') }}
                            </li>

                        </ul>
                    </li>

                    <!-- SHOP -->
                    <li>
                        <a href="{{ route('shop-page') }}" class="header-link">{{ trans('home.shop') }} </a>
                    </li>

                    <!-- ADS -->
                    <li class="dropdown">
                        <a class="dropdown-toggle header-link" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            {{ trans('home.ads') }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('ads-new-need') }}">
                                    {{ trans('home.menu.need-rent') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('ads-new-need') . '?deal_type=sale' }}">
                                    {{ trans('home.menu.buy-house') }}
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('ads-new-offer') }}">
                                    {{ trans('home.menu.want-rent') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('ads-new-offer') . '?deal_type=sale' }}">
                                    {{ trans('home.menu.sell-house') }}
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('account-my-ads') }}">
                                    {{ trans('home.menu.my-ads-room') }}
                                </a></li>
                        </ul>
                    </li>

                    <!-- LOGIN or USER -->
                    @if(Auth::check())
                        <li class="dropdown">
                            <a class="dropdown-toggle header-link" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">{{ str_limit(Auth::user()->email, $limit = 8, $end = '~') }}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('account-personal') }}">
                                        {{ trans('home.my-account') }}
                                    </a></li>
                                <li><a href="{{ route('faq-login-password') }}">
                                        {{ trans('home.support') }}
                                    </a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ route('logout') }}" class="header-sign-out">
                                        {{ trans('home.logout-header') }}
                                    </a></li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('login') }}" class="header-link">{{ trans('home.log-in') }}</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>