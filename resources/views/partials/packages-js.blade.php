<!--JAVASCRIPT-->
<script src='/packages/jquery/jquery.js'></script>
<script src='/packages/bootstrap/js/bootstrap.js'></script>
<script src="/packages/bootstrap-material-design/js/material.min.js"></script>
<script src="/packages/bootstrap-material-design/js/ripples.min.js"></script>
<script src='/myjs/main.js'></script>


<!--NoUiSlider-->
{{--<script src="/packages/bootstrap-material-design/noiusliderjs/nouislider.min.js"></script>--}}

<!--MATERIAL ICONS-->
{{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons"--}}
{{--rel="stylesheet">--}}

<!--CLICK CAROUSEL-->
{{--<link href='/packages/slick/slick/slick.css' rel='stylesheet' type='text/css'>--}}
{{--<link href='/packages/slick/slick/slick-theme.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/slick/slick/slick.min.js'></script>--}}

{{--<!--AWESOME BOOTSTRAP CHECKBOX-->--}}
{{--<link href='/packages/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css' rel='stylesheet' type='text/css'>--}}

{{--<!--RECAPTCHA-->--}}
{{--<script src='https://www.google.com/recaptcha/api.js'></script>--}}

{{--<!--OWL CAROUSEL-->--}}
{{--<script src='/packages/owl-carousel/owl.carousel.min.js'></script>--}}
{{--<link href='/packages/owl-carousel/assets/owl.carousel.min.css' rel='stylesheet' type='text/css'>--}}
{{--<link href='/packages/owl-carousel/assets/owl.theme.default.min.css' rel='stylesheet' type='text/css'>--}}

{{--<!--FANCYBOX-->--}}
{{--<link href='/packages/fancyBox/source/jquery.fancybox.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/fancyBox/source/jquery.fancybox.js'></script>--}}

{{--<!--JQUERY TOGGLES-->--}}
{{--<link href='/packages/jquery-toggles/css/toggles.css' rel='stylesheet' type='text/css'>--}}
{{--<link href='/packages/jquery-toggles/css/themes/toggles-iphone.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/jquery-toggles/toggles.min.js'></script>--}}

{{--<!--SELECT 2-->--}}
{{--<link href='/packages/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/select2/dist/js/select2.min.js'></script>--}}

{{--<!--VUE JS-->--}}
{{--<script src='/packages/vue/vue-full.js'></script>--}}
{{--<script src='/packages/vue/vue-resource.js'></script>--}}

{{--<!--RUBAXA SORTABLE-->--}}
{{--<script src='/packages/rubaxa-sortable/Sortable.min.js'></script>--}}