<div class="form-group label-floating">
    <label class="control-label" for="focusedInput1">
        {{ trans('auth.register.email') }}
    </label>
    <input id="focusedInput1" class="form-control input-lg" type="email" name="email" value="{{ old('email') }}"
           required="required">
    <p class="help-block">{{ trans('auth.register.email-placeholder') }}</p>
</div>

<div class="form-group label-floating">
    <label class="control-label" for="focusedInput2">
        {{ trans('auth.register.password') }}
    </label>
    <input id="focusedInput2" class="form-control input-lg" type="password" name="password"
           required="required">
    <p class="help-block">{{ trans('auth.register.password-placeholder') }}</p>
</div>

<div class="form-group label-floating">
    <label class="control-label" for="focusedInput3">
        {{ trans('auth.register.password-confirm') }}
    </label>
    <input id="focusedInput3" class="form-control input-lg" type="password" name="password_confirmation"
           required="required">
    <p class="help-block">{{ trans('auth.register.password-confirm-placeholder') }}</p>
</div>

<div class="auth-checkbox checkbox">
    <label>
        <input type="checkbox" name="rules" checked="checked">
        <span>{!! trans('auth.register.rules') !!}</span>
    </label>
</div>