<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
<div data-size="{{ $size or 'm' }}"
     data-url="{{ $url or null }}"
     data-lang="{{ Request::cookie('locale') }}"
     data-image="{{ $image or null }}"
     data-description="{{ $description or null }}"
     class="ya-share2"
     data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp">
</div>
