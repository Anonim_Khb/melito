<div id="paymentRobokassa">

    <h4 class="payment-title">{{ trans('shop.payment.title') }}</h4>

    {!! Form::open( [ 'route' => 'payment-send', 'method' => 'post' ] ) !!}

    <div class="form-group">

        <div class="input-group">
            <input id="paymentSum" class="form-control" type="number" data-minimal="{{ config('shop.minimal-pay') }}"
                   name="sum" value="{{ old('sum', config('shop.default-pay')) }}">
            <button id="paymentSaleBtn" type="submit" class="btn btn-fab btn-fab-mini">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    {!! Form::close() !!}

</div>