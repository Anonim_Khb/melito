<!--CSS-->
<link href='/packages/bootstrap/css/bootstrap.css' rel='stylesheet' type='text/css'>
<link href='/packages/bootstrap/css/bootstrap-theme.css' rel='stylesheet' type='text/css'>
<link href="/packages/bootstrap-material-design/css/bootstrap-material-design.css" rel="stylesheet">
<link href="/packages/bootstrap-material-design/css/ripples.min.css" rel="stylesheet">
<link href='/css/melito_css/main.css' rel='stylesheet' type='text/css'>
<link href='/css/melito_css/modal.css' rel='stylesheet' type='text/css'>

<!--FONTS-->
<link href='/packages/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

<link
    href='https://fonts.googleapis.com/css?family=Exo+2:400,600,900|Philosopher:400,700|Open+Sans:400,800,600|Comfortaa:400,900|Marmelad|Ubuntu+Condensed:400,900|Andika&subset=latin,cyrillic'
    rel='stylesheet' type='text/css'>

<link href='/packages/my-icons/one/style.css' rel='stylesheet' type='text/css'>



<!--NoUiSlider-->
{{--<script src="/packages/bootstrap-material-design/noiusliderjs/nouislider.min.js"></script>--}}

<!--MATERIAL ICONS-->
{{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons"--}}
      {{--rel="stylesheet">--}}

{{--<!--AWESOME BOOTSTRAP CHECKBOX-->--}}
{{--<link href='/packages/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css' rel='stylesheet' type='text/css'>--}}

{{--<!--RECAPTCHA-->--}}
{{--<script src='https://www.google.com/recaptcha/api.js'></script>--}}

{{--<!--OWL CAROUSEL-->--}}
{{--<script src='/packages/owl-carousel/owl.carousel.min.js'></script>--}}
{{--<link href='/packages/owl-carousel/assets/owl.carousel.min.css' rel='stylesheet' type='text/css'>--}}
{{--<link href='/packages/owl-carousel/assets/owl.theme.default.min.css' rel='stylesheet' type='text/css'>--}}

{{--<!--FANCYBOX-->--}}
{{--<link href='/packages/fancyBox/source/jquery.fancybox.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/fancyBox/source/jquery.fancybox.js'></script>--}}

{{--<!--JQUERY TOGGLES-->--}}
{{--<link href='/packages/jquery-toggles/css/toggles.css' rel='stylesheet' type='text/css'>--}}
{{--<link href='/packages/jquery-toggles/css/themes/toggles-iphone.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/jquery-toggles/toggles.min.js'></script>--}}

{{--<!--SELECT 2-->--}}
{{--<link href='/packages/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>--}}
{{--<script src='/packages/select2/dist/js/select2.min.js'></script>--}}

{{--<!--VUE JS-->--}}
{{--<script src='/packages/vue/vue-full.js'></script>--}}
{{--<script src='/packages/vue/vue-resource.js'></script>--}}

{{--<!--RUBAXA SORTABLE-->--}}
{{--<script src='/packages/rubaxa-sortable/Sortable.min.js'></script>--}}