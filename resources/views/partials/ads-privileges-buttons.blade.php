<a>
    <i class="fa fa-plus-square fa-fw"></i>
    <span>{{ trans('account.ads.btn.premium') }}</span>
    <i class="fa fa-caret-right fa-fw dd-submenu-arrow"></i>
</a>

<ul class="dd-submenu-list">
    <li class="ads-priv-btns" data-priv-type="vip">
        <a>
            <i class="fa fa-angle-right fa-fw"></i>
            {{ trans('shop.privileges.vip.title') }}
        </a>
    </li>
    <li class="ads-priv-btns" data-priv-type="top">
        <a>
            <i class="fa fa-angle-right fa-fw"></i>
            {{ trans('shop.privileges.top.title') }}
        </a>
    </li>
    <li class="ads-priv-btns" data-priv-type="auto_up">
        <a>
            <i class="fa fa-angle-right fa-fw"></i>
            {{ trans('shop.privileges.auto_up.title') }}
        </a>
    </li>
    <li class="ads-priv-btns" data-priv-type="color">
        <a>
            <i class="fa fa-angle-right fa-fw"></i>
            {{ trans('shop.privileges.color.title') }}
        </a>
    </li>
    <i class="ad-id-for-modal" class="hidden" data-adid="{{ $ad_id }}"></i>
</ul>