<div id="loadingProcessGif">

    <div id="darkBackground"></div>

    <img id="gifLoading" src="/{{ config('image.LoadingDefaultImage') }}"
         alt="Loading">

</div>