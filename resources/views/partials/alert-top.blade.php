<div class="alerts-top">

    @if (Notifications::byType('success')->count() > 0)
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p class="text-center">{!! Notifications::byType('success')->first()['message'] !!}</p>
        </div>
    @endif

    @if (Notifications::byType('error')->count() > 0)
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p class="text-center">{!! Notifications::byType('error')->first()['message'] !!}</p>
        </div>
    @endif

</div>

<div class="all-light-box"></div>