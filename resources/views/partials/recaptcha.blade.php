@if(config('captcha.CaptchaEnabled'))

    <div class="google-recaptcha">
        {!! app('captcha')->display() !!}
    </div>

@endif