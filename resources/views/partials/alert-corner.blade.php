<div id="adUpError">

    <span id="alertCornerClose" class="">&times;</span>

    <p id="alertCornerText"></p>

</div>

<script>

    $(document).ready(function () {

        $('#alertCornerClose').on('click', function () {
            $(this).parent('div').fadeOut();
        });

        var timer;

        $.AlertCorner = function (data) {

            $('#adUpError').show('normal').css('background', data.color)
                .children('#alertCornerText').text(data.message);

            clearInterval(timer);

            timer = setInterval(function () {

                $('#adUpError').fadeOut('slow');

            }, 5500);
        }

    });

</script>