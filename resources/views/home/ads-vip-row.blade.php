<div id="indexVipAds">

    @foreach($adsVips as $adsVip)

        <div class="ad-body">

            <a href="{{ route('offer-watch-open', $adsVip->parentAds->id) }}">

                <div class="ad-img" style="background: url('/{{ isset($adsVip->parentAds->image->filename) ?
                        config('image.AdsOfferImagesPathMini') . $adsVip->parentAds->image->filename :
                        config('image.AdsOfferDefaultImage') }}') center center no-repeat;background-size: cover;">
                </div>

                <div class="ad-text">

                    <p class="ad-city">
                        <i class="fa fa-map-marker fa-fw"></i>
                        {{ userLocale() == 'ru' ?
                        $adsVip->parentAds->cityName->name_ru : $adsVip->parentAds->cityName->name_en }}
                    </p>

                    <p class="ad-street">
                        {{ trans('account.ads.street-abbr') . str_limit($adsVip->parentAds->street_name, 20, '...') }}
                    </p>

                    <p class="ad-address">
                        {{ trans('ads.new-offer.house-type.' . $adsVip->parentAds->house_type . '') }},
                        {!! $adsVip->parentAds->room_meters . trans('account.ads.square-meters') !!}{{ $adsVip->parentAds->rooms ?
                        ', ' . $adsVip->parentAds->rooms . ' rooms' : '' }}
                    </p>

                    <p class="ad-price">
                        <span class="deal-type">
                            {{ trans('account.ads.deal-type.' . $adsVip->parentAds->deal_type . '') }}
                        </span>
                        <span class="ad-price-sum">
                            {{ $adsVip->parentAds->rent_price }}
                            <i class="fa fa-ruble"></i>
                        </span>
                    </p>

                </div>

            </a>

        </div>

    @endforeach

</div>