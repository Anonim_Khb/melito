@extends('home.main')

@section('include-links')

    <link href='/packages/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
    <link href='/packages/select2/themes/city-index.css' rel='stylesheet' type='text/css'>

    <link href='/packages/slick/slick/slick.css' rel='stylesheet' type='text/css'>
    <link href='/packages/slick/slick/slick-theme.css' rel='stylesheet' type='text/css'>

    <link href='/packages/datetimepicker/css/bootstrap-datepicker.min.css' rel='stylesheet' type='text/css'>

    <link href='/css/melito_css/index.css' rel='stylesheet' type='text/css'>
    <link href='/css/melito_css/media.css' rel='stylesheet' type='text/css'>

@endsection

@section('body')

    <div class="row row-header-pic">
        <div class="section-header-pic">
            @include ('partials.header-pic')
        </div>
    </div>

    <div class="row index-search-form">
        @include('home.search-form')
    </div>

    <div class="row index-ads-vip">
        @include('home.ads-vip-row')
    </div>

    <div class="row index-ads-last">
        @include('home.ads-last-row')
    </div>

    <div class="row index-performance">
        @include('home.performance-row')
    </div>

@endsection

@section('js-bottom')

    <script src='/packages/select2/dist/js/select2.min.js'></script>

    <script src="/packages/bootstrap-material-design/noiusliderjs/nouislider.min.js"></script>

    <script src='/packages/slick/slick/slick.min.js'></script>

    <script src='/packages/datetimepicker/js/bootstrap-datepicker.min.js'></script>
    <script src='/packages/datetimepicker/locales/bootstrap-datepicker.ru.js'></script>

    <script src="/myjs/index-page.js"></script>

    <script src="/myjs/select2-cities-index.js"></script>

@endsection