<div id="indexLastAds">

    @foreach($ads as $ad)

        <div class="ad-body">

            <a href="{{ route('offer-watch-open', $ad->id) }}">

                <div class="ad-img" style="background: url('/{{ isset($ad->image->filename) ?
                        config('image.AdsOfferImagesPathMini') . $ad->image->filename :
                        config('image.AdsOfferDefaultImage') }}') center center no-repeat;background-size: cover;">
                </div>

                <div class="ad-text">

                    <p class="ad-city">
                        <i class="fa fa-map-marker fa-fw"></i>
                        {{ userLocale() == 'ru' ?
                        $ad->cityName->name_ru : $ad->cityName->name_en }}
                    </p>

                    <p class="ad-street">
                        {{ trans('account.ads.street-abbr') . str_limit($ad->street_name, 20, '...') }}
                    </p>

                    <p class="ad-address">
                        {{ trans('ads.new-offer.house-type.' . $ad->house_type . '') }},
                        {!! $ad->room_meters . trans('account.ads.square-meters') !!}{{ $ad->rooms ?
                        ', ' . $ad->rooms . ' rooms' : '' }}
                    </p>

                    <p class="ad-price">
                        <span class="deal-type">
                            {{ trans('account.ads.deal-type.' . $ad->deal_type . '') }}
                        </span>
                        <span class="ad-price-sum">
                            {{ $ad->rent_price }}
                            <i class="fa fa-ruble"></i>
                        </span>
                    </p>

                </div>

            </a>

        </div>

    @endforeach

</div>