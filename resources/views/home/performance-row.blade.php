<div id="indexPerformance">

    @foreach($performances as $performance)

        <div class="index-performance">
            <img data-lazy="/{{ config('image.performance_path') . $performance['src'] }}" style="background-color:{{ $performance['color']}}" class="img-circle" alt="performance">
            <div class="performance-text text-center">
                {{ trans('home.performance.' . $performance['name'] .'') }}
            </div>
        </div>

    @endforeach

</div>