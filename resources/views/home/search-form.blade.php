<div id="searchFormIndex">

    <div id="searchBtnHeader" class="list-inline">
        <a id="searchLink" class="btn btn-raised">
            <span id="searchIcon" class="glyphicon glyphicon-search" aria-hidden="true"></span>
            <span id="searchText">{{ trans('home.fast-search') }}</span>
        </a>
    </div>

    <div id="formFields">

        {!! Form::open( [ 'route' => 'ads-new-need', 'method' => 'get', 'id' => 'SendCityHeaderForm' ] ) !!}

        <div class="row">

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-1">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.city.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.city.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select id="select0001" class="form-control select-city-js" name="city" required="required">
                        <option value="{{ $userCity->id }}" selected="selected">{{ $userCity->text }}</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.deal-type.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.deal-type.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control" name="deal_type">
                        @foreach($fData['deal_type'] as $deal_type)
                            <option value="{{ $deal_type['name'] }}" @if($deal_type['name'] == 'rent') selected @endif>
                                {{ trans('home.search.deal-type.' . $deal_type['name']. '') }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.house-type.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.house-type.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control" name="house_type">
                        <option value="">{{ trans('home.search.house-type.any') }}</option>
                        @foreach($fData['house_type'] as $house_type)
                            <option value="{{ $house_type['name'] }}">
                                {{ trans('home.search.house-type.' . $house_type['name']. '') }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-lg-4 col-lg-offset-1">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3 text-center">
                            <span class="label label-success">{{ trans('home.search.area.min') }}</span>

                            <input id="areaMin" type="number" name="rm_from" class="form-control" readonly>
                        </div>

                        <div class="col-xs-6 text-center">
                            <span class="label label-info">{{ trans('home.search.area.title') }}
                                <a data-container="body" data-toggle="popover" data-placement="top"
                                   data-content="{{ trans('home.search.area.popover') }}"
                                   class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                            </span>

                            <div class="my-slider">
                                <div id="area" class="slider shor slider-info"></div>
                            </div>
                        </div>

                        <span id="areaMinMax" class="hidden"
                              data-area-min="{{ Cache::get('areaMin', 0) }}"
                              data-area-max="{{ Cache::get('areaMax', 5000) }}"></span>

                        <div class="col-xs-3 text-center">
                            <span class="label label-primary">{{ trans('home.search.area.max') }}</span>

                            <input id="areaMax" type="number" name="rm_to" class="form-control" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-2 text-center">
                            <span class="label label-success">{{ trans('home.search.price.min') }}</span>

                            <input id="rubleMin" type="number" name="price_from" class="form-control" readonly>
                        </div>

                        <div class="col-xs-6 text-center">
                            <span class="label label-info">{{ trans('home.search.price.title') }}
                                <a data-container="body" data-toggle="popover" data-placement="top"
                                   data-content="{{ trans('home.search.price.popover') }}"
                                   class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                            </span>

                            <div class="my-slider">
                                <div id="ruble" class="slider shor slider-info"></div>
                            </div>
                        </div>

                        <span id="priceMinMax" class="hidden"
                              data-rent-min="{{ Cache::get('rentPriceMin', 0) }}"
                              data-rent-max="{{ Cache::get('rentPriceMax', 1000000) }}"
                              data-sale-min="{{ Cache::get('salePriceMin', 0) }}"
                              data-sale-max="{{ Cache::get('salePriceMax', 10000000) }}"></span>

                        <div class="col-xs-4 text-center">
                            <span class="label label-primary">{{ trans('home.search.price.max') }}</span>

                            <input id="rubleMax" type="number" name="price_to" class="form-control" readonly>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-3 col-lg-offset-1">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.communal.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.communal.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control only-rent" name="communal">
                        <option value="">{{ trans('home.search.communal.any') }}</option>
                        @foreach($fData['communal'] as $communal)
                            <option value="{{ $communal['name'] }}">
                                {{ trans('home.search.communal.' . $communal['name']. '') }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-2">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.gender.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.communal.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control only-rent" name="gender">
                        <option value="">{{ trans('home.search.gender.any') }}</option>
                        @foreach($fData['gender'] as $gender)
                            <option value="{{ $gender['name'] }}">
                                {{ trans('home.search.gender.' . $gender['name']. '') }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.occupation.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.occupation.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control only-rent" name="occupation">
                        <option value="">{{ trans('home.search.occupation.any') }}</option>
                        @foreach($fData['occupation'] as $occupation)
                            <option value="{{ $occupation['name'] }}">
                                {{ trans('home.search.occupation.' . $occupation['name']. '') }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-2">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.age.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.age.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <select class="form-control only-rent" name="age">
                        <option value="">{{ trans('home.search.age.any') }}</option>
                        @for($i = 18; $i<100; $i++)
                            <option value="{{ $i }}">
                                {{ $i }}
                            </option>
                        @endfor
                    </select>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-3 col-lg-2 col-lg-offset-1">
                <div class="form-group">
                <span class="label label-info">{{ trans('home.search.entry-date.title') }}
                    <a data-container="body" data-toggle="popover" data-placement="top"
                       data-content="{{ trans('home.search.entry-date.popover') }}"
                       class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                </span>


                    <div class="input-group date entry-date">
                        <input id="entryDate" type="text" class="form-control" name="entry"
                               data-start="{{ Jenssegers\Date\Date::now()->format('Y-m-d') }}"
                               data-locale="{{ Request::cookie('locale') }}"
                               placeholder="{{ trans('home.search.entry-date.placeholder') }}">
                        <span class="input-group-addon"></span>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-offset-1">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.without-prepayment.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.prepayment.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="prepayment" value="1">
                            {{ trans('home.search.without-prepayment.text') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-2">
                <div class="form-group">
                    <span class="label label-info">{{ trans('home.search.with-image.title') }}
                        <a data-container="body" data-toggle="popover" data-placement="top"
                           data-content="{{ trans('home.search.with-image.popover') }}"
                           class="glyphicon glyphicon-question-sign popover-help text-danger"></a>
                    </span>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="wIm" value="1">
                            {{ trans('home.search.with-image.text') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 form-group">
                <button id="searchFormIndexSubmit" type="submit" href="javascript:void(0)"
                        class="btn btn-raised btn-lg btn-warning">
                    <b>{{ trans('home.search.search-btn') }}</b>
                </button>
            </div>

        </div>

        {{--<button type="button" id="AdOfferFromHeader" data-href="{{ route('ads-new-offer') }}">--}}
        {{--<span class="btn">{{ trans('home.offer-room') }}</span>--}}
        {{--</button>--}}

        {!! Form::close() !!}

    </div>

</div>

