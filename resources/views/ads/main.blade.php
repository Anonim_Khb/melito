<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    @yield('include-links')

    <link href='/css/melito_css/ads.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-header">
        <div class="section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row row-center">
        <div class="section-content">
            @yield('body')
            @include('partials.packages-js')
            @yield('js-bottom')
        </div>
    </div>


    <div class="row row-footer">
        <div class="section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>