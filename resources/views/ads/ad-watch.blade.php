@extends('ads.main')

@section('body')

    OPEN AD

    <div id="adsWatch">

        <div class="row">

            <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>

            <div class="col-xs-24 col-sm-20 col-md-20 col-lg-18">

                <div class="row">

                    <div class="col-xs-24 col-sm-16 col-md-16 col-lg-17">

                        <div>
                            <i id="bookmarkStarEl" class="fa fa-star ad-bookmark {{ $adsOffers->bookmark != null ? 'bookmark-star-true' : 'bookmark-star-false' }}"
                               data-id="{{ $adsOffers->id }}"></i>
                        </div>

                        <div>#{{ $adsOffers->id }}</div>

                        <div>
                            {{ trans('ads.ad-watch.city') }}
                            <a href="{{ route('ads-new-need', ['city' => $adsOffers->city]) }}">
                                <?php $adCity = userLocale() == 'ru' ?  $adsOffers->cityName->name_ru : $adsOffers->cityName->name_en; ?>
                                {{ $adCity }}
                            </a>
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.views') }}
                            {{ $adsOffers->count }}
                        </div>

                        <div>
                            {{ trans('ads.ad-status.title') }}
                            {{ trans('ads.ad-status.' . $adsOffers->status . '') }}
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.ad-up') }}
                            {{ $adsOffers->updated_at }}
                        </div>

                        @if(isset($user) && ($user->id == $adsOffers->author->id || in_array($user->group_id, config('app.userGroup.redactor'), true)))
                            <div>
                                <a href="{{ route('offer-edit', $adsOffers->id) }}">
                                    {{ trans('ads.ad-watch.ad-edit') }}
                                </a>
                            </div>
                        @endif

                        <a v-if="model.images.length != 0" v-for="image in model.images"
                           v-bind:href="model.pathToImages + image.filename"
                           class="fancybox" rel="gallery"
                           title="{{ config('app.site_name_ru') }}">
                            <img v-if="image.filename == model.firstImage"
                                 style="width:100%;margin-bottom: 1em;"
                                 v-bind:src="model.pathToImages + model.firstImage"/>
                        </a>

                        <a v-if="model.images.length == 0">
                            <img style="width:100%;margin-bottom: 1em;"
                                 v-bind:src="model.pathToDefaultImages + model.defaultImage">
                        </a>

                        <div id="imageLine" class="owl-carousel owl-theme" style="width:39em;">

                            @foreach($adsOffers->images as $image)

                                <div class="item">

                                    <img style="height:6.6em;" image-attr="{{ $image->filename }}"
                                         src="/{{ config('image.AdsOfferImagesPathMini') }}{{ $image->filename }}"/>

                                </div>

                            @endforeach

                        </div>

                        <div>
                            {{ trans('ads.new-offer.rent-price.title') }}
                            {{ $adsOffers->rent_price }}
                            {{ trans('ads.new-offer.rent-price.rub') }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.additional-payment') }}
                            {{ $adsOffers->add_pay != 0 ?
                            $adsOffers->add_pay . ' ' .trans('ads.new-offer.rent-price.rub') : '-' }}
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.street-name') }}
                            {{ $adsOffers->street_name }}
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.house-number') }}
                            {{ $adsOffers->house_number }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.house-type.title') }}
                            @foreach($adsOffers->fData['house_type'] as $house_type)
                                <button
                                    class="{{ $adsOffers->house_type == $house_type['name'] ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ trans('ads.new-offer.house-type.' . $house_type['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.communal.title') }}
                            @foreach($adsOffers->fData['communal'] as $communal)
                                <button
                                    class="{{ $adsOffers->communal == $communal['name'] ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ trans('ads.new-offer.communal.' . $communal['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.ad-floor') }}
                            {{ $adsOffers->floor }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.room-meters') }}
                            {{ $adsOffers->room_meters }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.available-from') }}
                            {{ dateFormatJFY($adsOffers->available_from) }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.sanitary_ware.title') }}
                            @foreach($adsOffers->fData['sanitary_ware'] as $sanitary_ware)
                                <button
                                    class="{{ $adsOffers->sanitary_ware == $sanitary_ware['name'] ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ trans('ads.new-offer.sanitary_ware.' . $sanitary_ware['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.amenities.title') }}
                            @foreach($adsOffers->fData['amenities'] as $amenities)
                                <?php $myTemp = $amenities['name']; ?>
                                <button class="{{ $adsOffers->$myTemp ? 'text-success' : 'text-default' }}"
                                        disabled>
                                    {{ trans('ads.new-offer.amenities.' . $amenities['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.prepayment') }}
                            @foreach($adsOffers->fData['prepayment'] as $prepayment)
                                <button
                                    class="{{ $adsOffers->prepayment == $prepayment ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ $prepayment }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.preferred.gender.title') }}
                            @foreach($adsOffers->fData['gender'] as $gender)
                                <button
                                    class="{{ $adsOffers->gender == $gender['name'] ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ trans('ads.new-offer.preferred.gender.' . $gender['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.preferred.occupation.title') }}
                            @foreach($adsOffers->fData['occupation'] as $occupation)
                                <button
                                    class="{{ $adsOffers->occupation == $occupation['name'] ? 'text-success' : 'text-default' }}"
                                    disabled>
                                    {{ trans('ads.new-offer.preferred.occupation.' . $occupation['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.preferred.age.title') }}
                            {{ trans('ads.new-offer.preferred.age.from') }}
                            {{ $adsOffers->age_from }}
                            {{ trans('ads.new-offer.preferred.age.to') }}
                            {{ $adsOffers->age_to }}
                        </div>

                        <div>
                            {{ trans('ads.new-offer.preferred.rules.title') }}
                            @foreach($adsOffers->fData['rules'] as $rules)
                                <?php $myTemp = $rules['name']; ?>
                                <button class="{{ $adsOffers->$myTemp ? 'text-success' : 'text-default' }}"
                                        disabled>
                                    {{ trans('ads.new-offer.preferred.rules.' . $rules['name'] . '') }}
                                </button>
                            @endforeach
                        </div>

                        <div>
                            {{ trans('ads.new-offer.ad-present.ad-text.title') }}
                            {{ $adsOffers->ad_text }}
                        </div>


                        <div>
                            @include('partials.share-buttons', [
                                $url = route('offer-watch-open', 1),
                                $image = null,
                                $description = trans('ads.ad-watch.city') . $adCity . ', ' . trans('ads.new-offer.rent-price.title') . ': ' . $adsOffers->rent_price . ', ' . trans('ads.new-offer.available-from') . ': ' . dateFormatJFY($adsOffers->available_from)
                                ])
                        </div>

                    </div>

                    <div class="hidden-xs col-sm-8 col-md-8 col-lg-7">
                        <img
                            src="{{ userAvatarOrDefaultById($adsOffers->author->id) }}"
                            style="width: 100%">

                        <div>
                            {{ trans('ads.ad-watch.profile.name') }}:
                            {{ !empty($adsOffers->author->profile->name) ? str_limit($adsOffers->author->profile->name, 10, '....') : trans('ads.ad-watch.null-data') }}
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.profile.gender.title') }}:
                            {{ !empty($adsOffers->author->profile->gender) ? trans('ads.ad-watch.profile.gender.' . $adsOffers->author->profile->gender . '') : trans('ads.ad-watch.null-data') }}
                        </div>

                        <div>
                            {{ trans('ads.ad-watch.profile.age') }}:
                            {{ !empty($adsOffers->age) ? $adsOffers->age : trans('ads.ad-watch.null-data') }}
                        </div>

                        @if((isset($user) && $user->id != $adsOffers->author->id) || (!$user))
                            <div>
                                <a data-toggle="modal" data-target="#modalSendEmail">
                                    {{ trans('ads.ad-watch.profile.write-letter') }}
                                </a>
                            </div>
                        @endif

                        <div>
                            <a href="{{ route('user-profile', ['id' => $adsOffers->author->id]) }}">
                                {{ trans('ads.ad-watch.profile.href') }}
                            </a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>

        </div>

    </div>

    <script>
        adImages = '{!! json_encode($adsOffers->images) !!}';
    </script>

@endsection

@section('js-bottom')

    @include('partials.alert-corner')

    <script src="/myjs/ad-bookmarks.js"></script>

    <script src="/myjs/ads-watch-vue.js"></script>

    @include('modals.send-message-from-ad-watch')

@endsection