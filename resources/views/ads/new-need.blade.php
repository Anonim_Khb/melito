@extends('ads.main')

@section('include-links')

    <link href='/packages/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>

@endsection

@section('body')

    AAAA

    {{ trans('ads.new-need.title') }}

    @include('partials.page-notification')

    <div class="row" id="adsNeed">

        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">

            <div class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#AdSearchFormCollapse-1" aria-expanded="false">
                            <i class="fa fa-filter"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="AdSearchFormCollapse-1">

                        {!! Form::open( [ 'route' => 'ads-new-need', 'method' => 'get', 'id' => 'adsNeedForm' ] ) !!}

                        {{ trans('ads.new-offer.locality.title') }}
                        <div id="adsCity">
                            <select v-on:input="changeInputValue('adsCity')" class="select-city-js" name="city"
                                    style="width: 26em;" required>
                                <option value="{{ $userCity->id }}" selected="selected">{{ $userCity->text }}</option>
                            </select>
                        </div>

                        <div id="dealType">
                            {{ trans('ads.new-offer.deal-type.title') }}
                            @foreach($fData['deal_type'] as $deal_type)
                                <label class="radio-inline">
                                    <input type="radio"
                                           name="deal_type"
                                           value="{{ $deal_type['name'] }}"
                                           @if(Input::has('deal_type'))
                                           {{ Input::get('deal_type') == $deal_type['name'] ? 'checked' : '' }}
                                           @else
                                           {{ $deal_type['name'] == 'rent' ? 'checked' : '' }}
                                           @endif
                                           v-on:click="changeInputValue('dealType')">
                                    {{ trans('ads.new-offer.deal-type.' . $deal_type['name']. '') }}
                                </label>
                            @endforeach
                        </div>

                        <div id="withImage">
                            <input v-on:click="changeInputValue('withImage')"
                                   type="checkbox"
                                   name="wIm"
                                {{ Input::has('wIm') ? 'checked' : '' }}>
                            {{ trans('ads.new-need.with-images') }}
                        </div>

                        <div>НЕ МЕНЕЕ
                            {{ trans('ads.new-need.rooms.title') }}
                            <div id="rooms">
                                <input v-on:input="changeInputValue('rooms')"
                                       type="number" name="rooms"
                                       value="{{ Input::get('rooms') }}"
                                       placeholder="">
                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldValue('rooms')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>

                        <div>
                            {{ trans('ads.new-need.rent-price.title') }}
                            <div id="priceFrom">
                                {{ trans('ads.new-need.rent-price.from') }}
                                <input v-on:input="changeInputValue('priceFrom')"
                                       type="number" name="price_from"
                                       value="{{ Input::get('price_from') }}"
                                       placeholder="">

                                {{ trans('ads.new-need.rent-price.to') }}
                                <input v-on:input="changeInputValue('priceFrom')"
                                       type="number"
                                       name="price_to"
                                       value="{{ Input::get('price_to') }}"
                                       placeholder="">
                                {{ trans('ads.new-offer.rent-price.rub') }}
                            </div>
                        </div>

                        <div id="houseType">
                            {{ trans('ads.new-offer.house-type.title') }}
                            @foreach($fData['house_type'] as $house_type)
                                <label class="radio-inline">
                                    <input type="radio"
                                           name="house_type"
                                           value="{{ $house_type['name'] }}"
                                           {{ Input::get('house_type') == $house_type['name'] ? 'checked' : '' }}
                                           v-on:click="changeInputValue('houseType')">
                                    {{ trans('ads.new-offer.house-type.' . $house_type['name']. '') }}
                                </label>
                            @endforeach
                            <button type="button" class="btn btn-danger"
                                    v-on:click="cleanFieldRadioCheckbox('houseType')">
                                <i class="fa fa-close"></i>
                            </button>
                        </div>

                        <div id="sanitaryWare">
                            {{ trans('ads.new-offer.sanitary_ware.title') }}
                            @foreach($fData['sanitary_ware'] as $sanitary_ware)
                                <label class="radio-inline">
                                    <input type="radio"
                                           name="sanitary_ware"
                                           value="{{ $sanitary_ware['name'] }}"
                                           {{ Input::get('sanitary_ware') == $sanitary_ware['name'] ? 'checked' : '' }}
                                           v-on:click="changeInputValue('sanitaryWare')">
                                    {{ trans('ads.new-offer.sanitary_ware.' . $sanitary_ware['name'] . '') }}
                                </label>
                            @endforeach
                            <button type="button" class="btn btn-danger"
                                    v-on:click="cleanFieldRadioCheckbox('sanitaryWare')">
                                <i class="fa fa-close"></i>
                            </button>
                        </div>

                        <div id="amenities">
                            {{ trans('ads.new-offer.amenities.title') }}
                            @foreach($fData['amenities'] as $amenities)
                                <label class="checkbox-inline">
                                    <input type="checkbox"
                                           name="{{ $amenities['name'] }}"
                                           value="1"
                                           {{ Input::get($amenities['name']) == 1 ? 'checked' : '' }}
                                           v-on:click="changeInputValue('amenities')">
                                    {{ trans('ads.new-offer.amenities.'.$amenities['name'].'') }}
                                </label>
                            @endforeach
                            <button type="button" class="btn btn-danger"
                                    v-on:click="cleanFieldRadioCheckbox('amenities')">
                                <i class="fa fa-close"></i>
                            </button>
                        </div>

                        <div>
                            {{ trans('ads.new-need.room-meters.title') }}
                            <div id="adMeters">
                                {{ trans('ads.new-need.room-meters.from') }}
                                <input type="number"
                                       name="rm_from"
                                       value="{{ Input::get('rm_from') }}"
                                       placeholder=""
                                       v-on:input="changeInputValue('adMeters')">

                                {{ trans('ads.new-need.room-meters.to') }}
                                <input type="number"
                                       name="rm_to"
                                       value="{{ Input::get('rm_to') }}"
                                       placeholder=""
                                       v-on:input="changeInputValue('adMeters')">

                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldValue('adMeters')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>

                        <div>
                            {{ trans('ads.new-need.entry') }}
                            <div id="entryDate">
                                НЕ ПОЗДНЕЕ
                                <input type="text"
                                       name="entry"
                                       value="{{ Input::get('entry') }}"
                                       v-on:keyup="changeInputValue('entryDate')">

                                <button type="button" class="btn btn-danger" v-on:click="cleanFieldValue('entryDate')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>

                        <div id="prepayment">
                            <input type="checkbox"
                                   name="prepayment"
                                   value="1"
                                   {{ Input::get('prepayment') == 1 ? 'checked' : '' }}
                                   v-on:click="changeInputValue('prepayment')">
                            NO {{ trans('ads.new-need.prepayment') }}
                        </div>

                        <!--ONLY RENT-->
                        <div v-if="model.deal_type == 'rent'">
                            <div id="communal">
                                {{ trans('ads.new-offer.communal.title') }}
                                @foreach($fData['communal'] as $communal)
                                    <label class="radio-inline">
                                        <input type="radio"
                                               name="communal"
                                               value="{{ $communal['name'] }}"
                                               {{ Input::get('communal') == $communal['name'] ? 'checked' : '' }}
                                               :disabled="model.deal_type != 'rent'"
                                               v-on:click="changeInputValue('communal')">
                                        {{ trans('ads.new-offer.communal.' . $communal['name'] . '') }}
                                    </label>
                                @endforeach
                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldRadioCheckbox('communal')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>

                            <div id="addPay">
                                <input type="checkbox"
                                       name="add_pay"
                                       value="1"
                                       {{ Input::get('add_pay') == 1 ? 'checked' : '' }}
                                       :disabled="model.deal_type != 'rent'"
                                       v-on:click="changeInputValue('addPay')">
                                {{ trans('ads.new-need.additional-payment') }}
                            </div>

                            <div id="gender">
                                {{ trans('ads.new-need.gender.title') }}
                                @foreach($fData['gender'] as $gender)
                                    <label class="radio-inline">
                                        <input type="radio"
                                               name="gender"
                                               value="{{ $gender['name'] }}"
                                               {{ Input::get('gender') == $gender['name'] ? 'checked' : '' }}
                                               :disabled="model.deal_type != 'rent'"
                                               v-on:click="changeInputValue('gender')">
                                        {{ trans('ads.new-offer.preferred.gender.'.$gender['name'].'') }}
                                    </label>
                                @endforeach
                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldRadioCheckbox('gender')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>

                            <div id="occupation">
                                {{ trans('ads.new-need.occupation.title') }}
                                @foreach($fData['occupation'] as $occupation)
                                    <label class="radio-inline">
                                        <input type="radio"
                                               name="occupation"
                                               value="{{ $occupation['name'] }}"
                                               {{ Input::get('occupation') == $occupation['name'] ? 'checked' : '' }}
                                               :disabled="model.deal_type != 'rent'"
                                               v-on:click="changeInputValue('occupation')">
                                        {{ trans('ads.new-offer.preferred.occupation.'.$occupation['name'].'') }}
                                    </label>
                                @endforeach
                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldRadioCheckbox('occupation')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>

                            <div>
                                <div id="ageNumber">
                                    {{ trans('ads.new-need.age.title') }}
                                    <input type="number"
                                           name="age"
                                           value="{{ Input::get('age') }}"
                                           v-on:keyup="changeInputValue('ageNumber')">
                                    {{ trans('ads.new-need.age.value') }}

                                    <button type="button" class="btn btn-danger" v-on:click="cleanFieldValue('ageNumber')">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </div>
                            </div>

                            <div id="adRules">
                                {{ trans('ads.new-offer.preferred.rules.title') }}
                                @foreach($fData['rules'] as $rules)
                                    <label class="checkbox-inline">
                                        <input type="checkbox"
                                               name="{{ $rules['name'] }}"
                                               value="1"
                                               {{ Input::get($rules['name']) == 1 ? 'checked' : '' }}
                                               :disabled="model.deal_type != 'rent'"
                                               v-on:click="changeInputValue('adRules')">
                                        {{ trans('ads.new-offer.preferred.rules.'.$rules['name'].'') }}
                                    </label>
                                @endforeach
                                <button type="button" class="btn btn-danger"
                                        v-on:click="cleanFieldRadioCheckbox('adRules')">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>
                        <!--END Only Rent-->

                        <div id="SearchAdsFormButton" style="position: absolute;right:0;display:none;">
                            <button name="submit_1" type="submit">AAAA2</button>
                        </div>

                        <div>
                            <button name="submit_2" type="submit">AAAA</button>
                        </div>

                        <div id="SearchAdsInfoBlock"
                             style="position: fixed;bottom:2em;left:2em;background: #8fc3ff;width:20em;height:6em;display:none;">
                            @{{ model.reloadPage || 'AAAA' }}
                            <span v-on:click="clearTimeoutAndInterval" class="fa fa-close"></span>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

            <div>
                @if(count($adsPrivilegesVip))
                    EXIST VIP
                    @foreach($adsPrivilegesVip as $adsVip)
                        <br>
                        <div class="bg-warning">
                            <img
                                src="@if($adsVip->parentAds->image) /{{ config('image.AdsOfferImagesPathMini') . $adsVip->parentAds->image->filename }} @else /{{ config('image.AdsOfferDefaultImage') }} @endif"
                                alt="Ad Title">
                            <p>{{ $adsVip->parentAds->rent_price }} {{ trans('ads.new-offer.rent-price.rub') }}</p>
                            <p>{{ $adsVip->parentAds->room_meters }}</p>
                            <p>{{ $adsVip->parentAds->ad_text }}</p>
                            <p>{{ getTimeAgo($adsVip->parentAds->updated_at) }}</p>
                            <p>{{ $adsVip->parentAds->house_type }}</p>
                            <p>{{ $adsVip->parentAds->communal }}</p>
                            <a href="/offer/{{ $adsVip->parentAds->id }}">READ MORE</a>


                            <div>
                                <i data-id="{{ $adsVip->parentAds->id }}"
                                   class="fa fa-star ad-bookmark {{ $adsVip->parentAds->bookmark != null ? 'bookmark-star-true' : 'bookmark-star-false' }}"></i>
                            </div>
                        </div>
                    @endforeach
                @else
                    NOT EXIST VIP
                @endif
            </div>

        </div>

        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">

            <div>
                @if(count($adsPrivilegesTop) > 0)
                    EXIST TOP
                    @foreach($adsPrivilegesTop as $adsTop)
                        <br>
                        <div class="bg-danger">
                            <img
                                src="@if($adsTop->parentAds->image) /{{ config('image.AdsOfferImagesPathMini') . $adsTop->parentAds->image->filename }} @else /{{ config('image.AdsOfferDefaultImage') }} @endif"
                                alt="Ad Title">
                            <p>{{ $adsTop->parentAds->rent_price }} {{ trans('ads.new-offer.rent-price.rub') }}</p>
                            <p>{{ $adsTop->parentAds->room_meters }}</p>
                            <p>{{ $adsTop->parentAds->ad_text }}</p>
                            <p>{{ getTimeAgo($adsTop->parentAds->updated_at) }}</p>
                            <p>{{ $adsTop->parentAds->house_type }}</p>
                            <p>{{ $adsTop->parentAds->communal }}</p>
                            <a href="/offer/{{ $adsTop->parentAds->id }}">READ MORE</a>


                            <div>
                                <i data-id="{{ $adsTop->parentAds->id }}"
                                   class="fa fa-star ad-bookmark {{ $adsTop->parentAds->bookmark != null ? 'bookmark-star-true' : 'bookmark-star-false' }}"></i>
                            </div>
                        </div>

                    @endforeach
                @else
                    NOT EXIST TOP
                @endif
            </div>

            <div>

                @if(count($adsOffers) > 0)

                    @foreach($adsOffers as $adsOffer)
                        <br>
                        <div class="bg-info"
                             @if($adsOffer->privileges) style="background: {{ $adsOffer->privileges->color_selected }} @endif">
                            <img style="float:left;"
                                src="@if($adsOffer->image) /{{ config('image.AdsOfferImagesPathMini') . $adsOffer->image->filename }} @else /{{ config('image.AdsOfferDefaultImage') }} @endif"
                                alt="Ad Title">
                            <div>
                                <p>{{ $adsOffer->rent_price }} {{ trans('ads.new-offer.rent-price.rub') }}</p>
                                <p>{{ $adsOffer->room_meters }}</p>
                                <p>{{ getTimeAgo($adsOffer->updated_at) }}</p>
                                <p>{{ $adsOffer->house_type }}</p>
                                <p>{{ $adsOffer->communal }}</p>
                                <a href="{{ route('offer-watch-open', $adsOffer->id) }}">READ MORE</a>
                            </div>

                            <div>
                                <i data-id="{{ $adsOffer->id }}"
                                   class="fa fa-star ad-bookmark {{ $adsOffer->bookmark != null ? 'bookmark-star-true' : 'bookmark-star-false' }}"></i>
                            </div>
                        </div>

                    @endforeach

                @else
                    NOTHING FOR SEARCH
                @endif

            </div>

            {!! $adsOffers->appends(Input::except('_token'))->render() !!}

        </div>

    </div>

@endsection

@section('js-bottom')

    @include('partials.alert-corner')

    <script src='/packages/select2/dist/js/select2.min.js'></script>

    <script src='/packages/vue/vue-full.js'></script>
    <script src='/packages/vue/vue-resource.js'></script>

    <script src="/myjs/ad-bookmarks.js"></script>

    <script src="/myjs/ads-need-vue.js"></script>

    <script src="/myjs/select2-cities-index.js"></script>

@endsection