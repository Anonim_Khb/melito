@extends('ads.main')

@section('body')

    <div id="adsEdit">

        <div id="validationErrors">
            <ul>
                <li v-for="error in model.errors" track-by="$index">@{{ error }}</li>
            </ul>
        </div>

        {!! Form::open( [ 'url' => 'javascript:void(0);', 'method' => 'post', 'id' => 'adsEditForm' ] ) !!}

        <div>#{{ $adsOffer->id }}</div>

        {{ trans('ads.new-offer.locality.title') }}

        <input type="hidden" id="deletedAdId" data-redirect="{{ route('offer-watch-open', $adsOffer->id) }}" name="ad_id" value="{{ $adsOffer->id }}">

        <div>
            <select class="select-city-js" name="city" style="width: 26em;">
                <option value="{{ $adsOffer->cityName->id }}">{{ $adsOffer->cityName->name }}</option>
            </select>
        </div>

        <div>
            {{ trans('ads.new-offer.street-name') }}
            <input type="text" name="street_name" value="{{ $adsOffer->street_name }}" placeholder="">
        </div>

        <div>
            {{ trans('ads.new-offer.house-number') }}
            <input type="text" name="house_number" value="{{ $adsOffer->house_number }}" placeholder="">
        </div>

        <div>
            {{ trans('ads.new-offer.ad-floor') }}
            <input type="number" name="floor" value="{{ $adsOffer->floor }}" placeholder="">
        </div>

        <div>
            {{ trans('ads.new-offer.house-type.title') }}
            @foreach($fData['house_type'] as $house_type)
                <label class="radio-inline">
                    <input type="radio"
                           name="house_type"
                           value="{{ $house_type['name'] }}"
                        {{ $adsOffer->house_type == $house_type['name'] ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.house-type.' . $house_type['name'] . '') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.communal.title') }}
            @foreach($fData['communal'] as $communal)
                <label class="radio-inline">
                    <input type="radio"
                           name="communal"
                           value="{{ $communal['name'] }}"
                        {{ $adsOffer->communal == $communal['name'] ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.communal.' . $communal['name'] . '') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.sanitary_ware.title') }}
            @foreach($fData['sanitary_ware'] as $sanitary_ware)
                <label class="radio-inline">
                    <input type="radio"
                           name="sanitary_ware"
                           value="{{ $sanitary_ware['name'] }}"
                        {{ $adsOffer->sanitary_ware == $sanitary_ware['name'] ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.sanitary_ware.' . $sanitary_ware['name'] . '') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.amenities.title') }}
            @foreach($fData['amenities'] as $amenities)
                <label class="checkbox-inline">
                    <input type="checkbox"
                           name="{{ $amenities['name'] }}"
                           value="1"
                        {{ $adsOffer->$amenities['name'] == 1 ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.amenities.'.$amenities['name'].'') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.rent-price.title') }}
            <input type="number" name="rent_price" value="{{ $adsOffer->rent_price }}" placeholder="">
            {{ trans('ads.new-offer.rent-price.rub') }}
        </div>

        <div>
            <input type="checkbox"
                   name="add_pay_check"
                   value="1"
                   v-model="model.add_pay"
                {{ $adsOffer->add_pay ? 'checked' : '' }}>
            {{ trans('ads.new-offer.additional-payment') }}
        </div>

        <div v-if="model.add_pay == true">
            <input type="number" name="add_pay" value="{{ $adsOffer->add_pay }}" placeholder="">
            {{ trans('ads.new-offer.rent-price.rub') }}
        </div>

        <div>
            {{ trans('ads.new-offer.room-meters') }}
            <div class="input-group">
                <input type="number" name="room_meters" value="{{ $adsOffer->room_meters }}" class="form-control" placeholder="">
                <span class="input-group-addon">M2</span>
            </div>
        </div>

        <div>
            {{ trans('ads.new-offer.available-from') }}
            <input type="text" name="available_from" value="{{ $adsOffer->available_from }}" data-mask="9999-99-99">
        </div>

        <div>
            {{ trans('ads.new-offer.prepayment') }}
            @foreach($fData['prepayment'] as $prepayment)
                <label class="radio-inline">
                    <input type="radio"
                           name="prepayment"
                           value="{{ $prepayment }}"
                        {{ $adsOffer->prepayment == $prepayment ? 'checked' : '' }}>
                    {{ $prepayment }}
                </label>
            @endforeach
        </div>

        {{ trans('ads.new-offer.preferred.title') }}

        <div>
            {{ trans('ads.new-offer.preferred.gender.title') }}
            @foreach($fData['gender'] as $gender)
                <label class="radio-inline">
                    <input type="radio"
                           name="gender"
                           value="{{ $gender['name'] }}"
                        {{ $adsOffer->gender == $gender['name'] ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.preferred.gender.'.$gender['name'].'') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.preferred.occupation.title') }}
            @foreach($fData['occupation'] as $occupation)
                <label class="radio-inline">
                    <input type="radio"
                           name="occupation"
                           value="{{ $occupation['name'] }}"
                        {{ $adsOffer->occupation == $occupation['name'] ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.preferred.occupation.'.$occupation['name'].'') }}
                </label>
            @endforeach
        </div>

        <div>
            {{ trans('ads.new-offer.preferred.age.title') }}
            <br>
            {{ trans('ads.new-offer.preferred.age.from') }}
            <select name="age_from">
                @for($i = 18; $i<100; $i++)
                    <option value="{{ $i }}"
                    {{ $adsOffer->age_from == $i ? 'selected' : '' }}>
                        {{ $i }}
                    </option>
                @endfor
            </select>

            {{ trans('ads.new-offer.preferred.age.to') }}
            <select name="age_to">
                @for($i = 99; $i>17; $i--)
                    <option value="{{ $i }}"
                        {{ $adsOffer->age_to == $i ? 'selected' : '' }}>
                        {{ $i }}
                    </option>
                @endfor
            </select>
        </div>

        <div>
            {{ trans('ads.new-offer.preferred.rules.title') }}
            @foreach($fData['rules'] as $rules)
                <label class="checkbox-inline">
                    <input type="checkbox"
                           name="{{ $rules['name'] }}"
                           value="1"
                        {{ $adsOffer->$rules['name'] == 1 ? 'checked' : '' }}>
                    {{ trans('ads.new-offer.preferred.rules.'.$rules['name'].'') }}
                </label>
            @endforeach
        </div>

        {{ trans('ads.new-offer.ad-present.title') }}

        <div>
            {{ trans('ads.new-offer.ad-present.ad-text.title') }}
            <textarea name="ad_text" class="form-control" placeholder="" rows="6">
                {{ $adsOffer->ad_text }}
            </textarea>
        </div>

        <div>
            <!-- The fileinput-button span is used to style the file input field as button -->
            <div>
                <span class="btn btn-success fileinput-button" :disabled="model.btnDisable == true"
                      v-on:click="imagesUpload">
                    <i class="fa fa-image">&nbsp;</i>
                    <span>{{ trans('ads.new-offer.images.button') }}</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileUpload" type="file" name="files[]" multiple>
                </span>
            </div>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
            <!-- The container for the uploaded files -->
            <input type="hidden" name="imagesTitle" v-model="model.imagesTitle">

            <div id="files" class="files">
                <ul v-if="model.photos != ''" class="list-unstyled">
                    <li v-for="photo in model.photos" :class="photo.status">
                        <span v-on:click="selectImagesTitle(photo)">
                            
                            <span class="adsSortedText">
                                <img v-if="photo.status == 'text-danger'" class="img-rounded"
                                     src="/melito_img/users_img/ads_default/warning.png"
                                     style="width:3.1em;">
                                <img v-else class="img-rounded"
                                     v-bind:src="model.urlForImagesMini + photo.filename"
                                     style="width:3.1em;">
                                @{{ photo.name || photo.filename }}
                            </span>
                            <span v-if="photo.status != 'text-danger'">
                                <i v-if="model.imagesTitle == photo.filename" class="fa fa-check-square-o"></i>
                                <i v-else class="fa fa-image"></i>
                            </span>

                        </span>

                        <span v-if="photo.filename"
                              v-on:click="deleteImage(photo)"
                              class="fa fa-close pull-right">
                        </span>
                    </li>
                </ul>
            </div>
        </div>

        @if(in_array(Auth::user()->group_id, config('app.userGroup.redactor'), true))

            @include('admin.for-ad-check')

        @endif

        <div>
            <input type="hidden" name="hash" value="{{ $hash }}" id="adHash">
        </div>

        <div>
            <button type="button" class="btn btn-primary" id="NewAdOfferForm" v-on:click="sendForm"
                    :disabled="model.btnDisable == true">
                {{ trans('ads.ad-edit.save-button') }}
            </button>
        </div>

        {!! Form::close() !!}

    </div>

    <script>
        photos = '{!! json_encode($adsOffer->images) !!}';
    </script>

@endsection

@section('js-bottom')

    <script src="/myjs/ads-edit-vue.js"></script>

    <script src="/myjs/select2-cities-index.js"></script>

@endsection



