<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ trans('home.error-page-title') }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    <link href='/css/error.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-center">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-content">
            @yield('body')
            @include('partials.packages-js')
        </div>
    </div>

    @yield('js-bottom')

</div>

</body>
</html>