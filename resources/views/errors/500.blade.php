@extends('errors.main')
@section('body')
    <div class="errors-boat-logo">
        <img src="/public/assets/rf_img/logo/logo-rf-3.png">
    </div>
    <div class="errors-boat-text text-center"><i class="fa fa-warning"></i>&nbsp;<span>ОШИБКА</span></div>
    <div class="errors-boat-text2 text-center">ERROR 500</div>
    <div class="errors-boat-button text-center">
        <a href="{{URL::previous()}}">
            <button class="btn btn-lg btn-primary">Вернуться назад</button>
        </a>
    </div>
@endsection