<div class="modal fade" id="modalConfrimShopBuy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open( [ 'route' => 'post-shop-buy', 'method' => 'post' ] ) !!}

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('shop.modal-confirm.title') }}
                </h4>

            </div>

            <div class="modal-body">

                <p>
                    {{ trans('shop.modal-confirm.text') }}
                </p>

                <span id="purchaseName">@{{ model.purchaseName }}</span>

                <input type="number" name="number" v-model="model.purchaseNumber" min="1" data-mask="99" required>

                <input id="goodName" type="hidden" name="good" v-model="model.purchaseValue" required>

                <p>
                    {{ trans('shop.modal-confirm.total') }}
                    @{{ model.purchaseNumber * model.purchasePrice }}
                    <i class="fa fa-ruble"></i>
                </p>

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('shop.buttons.close') }}</a>

                <button type="submit" class="btn btn-primary">{{ trans('shop.buttons.confirm') }}</button>

            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>