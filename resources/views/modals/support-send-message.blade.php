<div class="modal fade my-modals" id="modalSupportSendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            {!! Form::open( [ 'route' => 'post-support-dialog', 'method' => 'post' ] ) !!}

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('support.modal.help-message.title') }}
                </h4>

            </div>

            <div class="modal-body">

                <div>
                    <span class="input-title">{{ trans('support.modal.help-message.category') }}</span>
                    <select name="category" class="form-control" required="required">
                        @foreach(\App\Http\MyClass\MyArrays::supportCategories() as $category)
                            <option value="{{ $category }}">{{ trans('support.category.' . $category . '') }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="for-padding-top">
                    <span class="input-title">{{ trans('support.modal.help-message.text') }}</span>
                    <textarea class="form-control input-lg" rows="4" cols="50" name="text" placeholder="{{ trans('support.modal.help-message.placeholder') }}"  required="required">{{ old('text') }}</textarea>
                </div>

                <div class="for-padding-top">
                    @include('partials.recaptcha')
                </div>

                <div class="for-padding-top modal-text">
                    @if(Auth::check())

                        {{ trans('support.modal.help-message.info-auth') }}

                    @else

                        {{ trans('support.modal.help-message.info-guest') }}

                    @endif
                </div>

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('support.modal.help-message.close-btn') }}</a>

                <button type="submit" class="btn btn-primary">{{ trans('support.modal.help-message.send-btn') }}</button>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>