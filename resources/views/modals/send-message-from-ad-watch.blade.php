<div class="modal fade" id="modalSendEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open( [ 'route' => [ 'user-send-email-post', $adsOffers->id ], 'method' => 'post' ] ) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    @if($adsOffers->author->status == 'pending')
                        {{ trans('ads.ad-watch.reg-not-confirm') }}
                    @else
                        {{ trans('ads.ad-watch.reg-confirm') }}
                    @endif
                </h4>

            </div>
            <div class="modal-body">

                <div>
                    {{ trans('account.users.text-msg') }}
                    <textarea name="text_msg" placeholder="BlaBla">{{ old('text_msg') }}</textarea>
                </div>

                @if(!$user)
                    {{ trans('ads.ad-watch.send-email.disabled-send') }}
                @endif

            </div>
            <div class="modal-footer">
                @if(!$user)
                    <a href="{{ route('register') }}">{{ trans('home.registration-btn') }}</a>
                @endif

                <button @if(!$user) disabled @endif type="submit">{{ trans('account.users.send-msg') }}</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>