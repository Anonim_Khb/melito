<div class="modal fade my-modals" id="avastarDeleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('account.pr-part-general.avatar-delete.title') }}
                </h4>

            </div>

            <div class="modal-body">

                {!! trans('account.pr-part-general.avatar-delete.text') !!}

            </div>

            <div class="modal-footer">

                <a class="btn btn-success" data-dismiss="modal">{{ trans('account.pr-part-general.avatar-delete.btn-cancel') }}</a>

                <button id="fileDeleteAvatar" data-default-avatar="{{ config('image.UserAvatarsDefaultImage') }}" @if($profile->avatar == null) disabled="disabled" @endif data-dismiss="modal" type="button" class="btn btn-danger">{{ trans('account.pr-part-general.avatar-delete.btn-confirm') }}</button>

            </div>

        </div>

    </div>

</div>