<div class="modal fade my-modals" id="modalPrivilegeActivate" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            {!! Form::open( [ 'url' => 'javascript:void(0);', 'method' => 'post', 'id' => 'AdPrivModalForm' ] ) !!}

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('shop.privileges.modal.title') }}
                </h4>

            </div>

            <div class="modal-body" id="AdPrivModalBody">

                <div class="priv-modal-block" id="AdPrivModalSelect_vip" style="display: none;">
                    <p>
                        {{ trans('shop.privileges.modal.description.vip') }}
                    </p>
                </div>

                <div class="priv-modal-block" id="AdPrivModalSelect_top" style="display: none;">
                    <p>
                        {{ trans('shop.privileges.modal.description.top') }}
                    </p>
                </div>

                <div class="priv-modal-block" id="AdPrivModalSelect_auto_up">
                    <p>
                        {{ trans('shop.privileges.modal.description.auto_up') }}
                    </p>
                </div>

                <div class="priv-modal-block" id="AdPrivModalSelect_color">

                    <p>
                        {{ trans('shop.privileges.modal.description.color') }}
                    </p>

                    <div id="selectColorTitle">
                        {{ trans('shop.privileges.color.select') }}
                    </div>

                    <div class="btn-group priv-colors" data-toggle="buttons">

                        @foreach($colors as $color)

                            <label class="color-label active btn fa fa-paint-brush" style="background: {{ $color->color }}">
                                <input type="radio" name="color" value="{{ $color->name }}" id="option1"
                                       autocomplete="off" checked>
                            </label>

                        @endforeach

                    </div>

                </div>

                <div class="form-inline priv-sum">
                    <div class="form-group">
                        <label for="exampleInputName2">{{ trans('shop.privileges.modal.sum') }}</label>
                        <input type="number" name="sum" value="1" class="form-control" id="exampleInputName2"
                               placeholder="{{ trans('shop.privileges.modal.sum') }}">
                    </div>
                </div>

                <input id="PrivAdModalId" type="hidden" name="ad_id" value="">

                <input id="PrivAdModalGoodName" type="hidden" name="good_name" value="">

                <p class="priv-comment">
                    {!!  trans('shop.privileges.modal.description.rules') !!}
                </p>

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('shop.buttons.close') }}</a>

                <button id="AdPrivModalSendForm" data-dismiss="modal" type="button"
                        class="btn btn-success">{{ trans('shop.privileges.modal.btn') }}</button>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>