<div class="modal fade" id="modalConfrimSupportCloseQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            {!! Form::open( [ 'route' => 'account-my-support-close-dialog', 'method' => 'post' ] ) !!}

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('support.modal.close-question.title') }}
                </h4>

            </div>

            <div class="modal-body">

                <input type="hidden" name="dialog" value="{{ $dialog->id }}">

                {{ trans('support.modal.close-question.body-text') }}

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('support.modal.close-question.btn-cancel') }}</a>

                <button type="submit" class="btn btn-primary">{{ trans('support.modal.close-question.btn-confirm') }}</button>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>