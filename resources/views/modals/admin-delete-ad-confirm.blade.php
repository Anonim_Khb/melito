<div class="modal fade" id="modalAdminDeleteAdConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('admin.ads.delete-ad.title') . ' #' .  $adsOffer->id }}
                </h4>

            </div>
            <div class="modal-body">

                <p>{{ trans('admin.ads.delete-ad.body') }}</p>

                <p>{{ trans('admin.ads.delete-ad.body-question') }}</p>

                <textarea id="adminDeleteComment" class="form-control" placeholder="{{ trans('admin.ads.delete-ad.comment-placeholder') }}"></textarea>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">{{ trans('admin.ads.delete-ad.btn-cancel') }}</button>
                <button type="button" id="adminAdDeleteConfirm" class="btn btn-danger">
                    {{ trans('admin.ads.delete-ad.btn-success') }}
                </button>
            </div>

        </div>
    </div>
</div>

<script src="/myjs/admin.js"></script>