<div class="modal fade my-modals crop-modal" id="adCoverCropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('account.ads.ad-cover-modal.title') }}
                </h4>

            </div>

            <div class="modal-body">

                <div>

                    <p class="help-block text-center">{{ trans('account.pr-part-general.avatar-crop.avatar-crop-info') }}</p>

                    <p id="modalCoverText">{{ trans('account.ads.ad-cover-modal.text') }}</p>

                    <div class="avatar-crop-img">
                        <img id="forCropAdImage" data-path="{{ config('image.AdsOfferImagesPathMini') }}">
                    </div>

                </div>

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('account.pr-part-general.avatar-delete.btn-cancel') }}</a>

                <button id="uploadAdCoverAfterCrop" data-dismiss="modal" type="button" class="btn btn-success">{{ trans('account.pr-part-general.avatar-crop.avatar-crop-btn') }}</button>

            </div>

        </div>

    </div>

</div>