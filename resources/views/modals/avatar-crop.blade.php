<div class="modal fade my-modals crop-modal" id="avatarCropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('account.pr-part-general.avatar-crop.title') }}
                </h4>

            </div>

            <div class="modal-body">

                <div class="text-center avatar-crop">

                    <p class="help-block">{{ trans('account.pr-part-general.avatar-crop.avatar-crop-info') }}</p>

                    <div class="avatar-crop-img">
                        <img id="forCropImage" data-path="{{ config('image.UserAvatarsTempFolder') }}">
                    </div>

                </div>

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('account.pr-part-general.avatar-delete.btn-cancel') }}</a>

                <button id="uploadAvatarAfterCrop" data-dismiss="modal" type="button" class="btn btn-success">{{ trans('account.pr-part-general.avatar-crop.avatar-crop-btn') }}</button>

            </div>

        </div>

    </div>

</div>