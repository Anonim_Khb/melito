<div class="modal fade my-modals" id="modalBookmarkDeleteAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('account.bookmark.modal-bad-title') }}
                </h4>

            </div>

            <div class="modal-body">

                {{ trans('account.bookmark.modal-bad-text') }}

            </div>

            <div class="modal-footer">

                <a class="btn btn-default" data-dismiss="modal">{{ trans('shop.buttons.close') }}</a>

                <button id="accountAllBookmarkDelete" data-dismiss="modal" class="btn btn-primary">{{ trans('shop.buttons.confirm') }}</button>

            </div>

        </div>

    </div>

</div>