<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    <link href='/css/error.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-center">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-content">
            @include('partials.page-notification')
            @include('partials.packages-js')
            @yield('body')
        </div>
    </div>

    @yield('js-bottom')

</div>

</body>
</html>