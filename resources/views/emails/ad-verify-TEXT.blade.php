<div>
    <p>
        {{ trans('email.ad-verify.letter-theme') }}
    </p>
    <p>
        {{ trans('email.ad-verify.link-about') }}
    </p>
    <p>
        <a href="{{ route('offer-watch-open', $adId) }}">
            <b>{{ trans('email.ad-verify.btn') }}</b>
        </a>
    </p>
    <br><hr/>
    <p>
        {!! trans('email.ad-verify.mail-about') !!}
    </p>
</div>
