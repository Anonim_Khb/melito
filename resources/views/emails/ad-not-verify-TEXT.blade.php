<div>
    <p>
        {{ trans('email.ad-not-verify.letter-theme') }}
    </p>
    <p>
        {{ trans('email.ad-not-verify.link-about') }}
    </p>
    <p>
        <a href="{{ route('offer-watch-open', $adId) }}">
            <b>{{ trans('email.ad-not-verify.btn') }}</b>
        </a>
    </p>
    <br><hr/>
    <p>
        {!! trans('email.ad-not-verify.mail-about') !!}
    </p>
</div>
