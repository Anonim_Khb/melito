<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body style="background: #e9eaed;">
<br>
<table style="margin: 0 5.2% 0 5.2%;">
    <tr>
        <td>
            <img src="{{ $message->embed(public_path(config('image.email-logo'))) }}"
                 alt="{{ config('app.site_name_ru') }} logo" style="width: 3.1em;margin: 0 .8em">
        </td>
        <td>
            <h2 style="color: #5779a4;font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;overflow:hidden;">
                {{ trans('email.support-response.welcome') }}
            </h2>
        </td>
    </tr>
</table>
<div style="margin: .4em 5.2% 2.6em 5.2%;
			background: #ffffff;
			padding: 2% 2.6% .8em 2.6%;
			color: #192332;
			font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
			font-size: 1.05em;">
    <p style="margin:.31em;">
        {{ trans('email.support-response.letter-theme') }}
        <a href="{{ route('home') }}" style="color:#526983;">{{ config('app.site_name_ru') }}</a>
    </p>

    <p style="margin:.31em;">
        {{ trans('email.support-response.link-about') }}
    </p>

    <p style="text-align:center;margin-top:1.75em;">
        <a href="{{ route('account-my-support') }}" style="cursor: pointer;">
            <button style="
				background: #157cff;
				color: #ffffff;
				font-size: 1.1em;
				font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
				border-radius: 6px;
				border: 1px solid #5779a4;
				cursor: pointer;
				padding:.52em 2em;">
                <b>{{ trans('email.support-response.btn') }}</b>
            </button>
        </a>
    </p>
    <br>
    <hr/>
    <p style="
			color: #666666;
			font-size: .75em;
			font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
			padding:.52em 2em;">
        {{ trans('email.support-response.mail-about') }}
    </p>
</div>
<br>
</body>
</html>