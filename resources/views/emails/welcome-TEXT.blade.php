<div>
  <p>
  {{ trans('email.welcome-email.glad-to-see') }}
  <a href="{{ route('home') }}/">{{ config('app.site_name_ru') }}</a>
  </p>
  <p>
    {{ trans('email.welcome-email.link-about') }}
  </p>
  <p>
    <a href="{{ route('register-confirm-email', $token) }}">
        <b>{{ trans('email.welcome-email.btn-confirm') }}</b>
    </a>
  </p>
  <br><hr/>
  <p>
    {!! trans('email.welcome-email.mail-about') !!}
  </p>
</div>
