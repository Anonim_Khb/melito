<div>
  <p>
  {{ trans('email.remind-password-email.letter-theme') }}<a href="{{ route('home') }}">{{ config('app.site_name_ru') }}</a>
  </p>
  <p>
    {{ trans('email.remind-password-email.link-about') }}
  </p>
  <p>
    {{ trans('email.remind-password-email.link-time-life') }}
  </p>
  <p>
    <a href="{{ route('password-reset', $token) }}">
      <b>{{ trans('email.remind-password-email.btn-reset') }}</b>
    </a>
  </p>
  <br><hr/>
  <p>
    {!! trans('email.welcome-email.mail-about') !!}
  </p>
</div>
