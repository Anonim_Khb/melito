<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body style="background: #e9eaed;">
<br>
<table style="margin: 0 5.2% 0 5.2%;">
    <tr>
        <td>
            <img src="{{ $message->embed(public_path(config('image.email-logo'))) }}"
                 alt="{{ config('app.site_name_ru') }} logo" style="width: 3.1em;margin: 0 .8em">
        </td>
        <td>
            <h2 style="color: #5779a4;font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;overflow:hidden;">
                {{ trans('email.message.title') }}
            </h2>
        </td>
    </tr>
</table>
<div style="margin: .4em 5.2% 2.6em 5.2%;
                    background: #ffffff;
                    padding: 2% 2.6% .8em 2.6%;
                    color: #192332;
                    font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
                    font-size: 1.05em;">
    <p style="margin:.31em;">
        {{ trans('email.message.theme') }}
        <a href="{{ route('offer-watch-open', $ad_id) }}"
           style="color:#526983;">{{ trans('email.message.theme-link') }}
        </a>
    </p>

    <div style="position: relative;bottom: 1em">
        <img src="{{ $message->embed(public_path($avatar_path)) }}" alt="user avatar"
             style="width: 5.2em;
                         border-radius: 5em;
                         border: 0.1em solid #dbdcdf;
                         position: relative;
                         top: 3.9em;
                         left: 45%;
                         ">

        <div style="margin:0 .31em .31em .31em; background: #edf6ff;border: 0.1em ridge #dbdcdf;font-size: 1em;">
            <p style="padding: 3.9em 1.3em 1em 1.3em;font-size: 1em;color: #2a4a73;text-indent: 1.3em;">
                {{ $text }}
            </p>
        </div>
    </div>

    <p style="text-align:center;margin-top:1.75em;">
        <a href="{{ route('user-profile', $from) }}" style="cursor: pointer;">
            <button style="
                        background: #157cff;
                        color: #ffffff;
                        font-size: 1.1em;
                        font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
                        border-radius: 6px;
                        border: 1px solid #5779a4;
                        cursor: pointer;
                        padding:.52em 2em;">
                <b>{{ trans('email.message.user-profile') }}</b></button>
        </a>
    </p>
    <br>
    <hr/>
    <p style="
                    color: #666666;
                    font-size: .75em;
                    font-family: Verdana, Arial, Helvetica, Lucida, Tahoma, Lucida Grande, Sans serif;
                    padding:.52em 2em;">
        {{ trans('email.message.mail-about') }} {{ config('app.url') }}.
    </p>
</div>
<br>
</body>
</html>