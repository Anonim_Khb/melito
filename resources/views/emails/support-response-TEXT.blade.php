<div>
    <p>
        {{ trans('email.support-response.letter-theme') }}
        <a href="{{ route('home') }}">{{ config('app.site_name_ru') }}</a>
    </p>
    <p>
        {{ trans('email.support-response.link-about') }}
    </p>
    <p>
        <a href="{{ route('account-my-support') }}">
            <b>{{ trans('email.support-response.btn') }}</b>
        </a>
    </p>
    <br><hr/>
    <p>
        {{ trans('email.support-response.mail-about') }}
    </p>
</div>
