<div>
    <p>
        {{ trans('email.message.theme') }}<a href="{{ route('offer-watch-open', $ad_id) }}">{{ trans('email.message.theme-link') }}</a>
    </p>

    <p>
        <a href="{{ route('user-profile', $from) }}">
            <b>{{ trans('email.message.user-profile') }}</b>
        </a>
    </p>
    <br>
    <hr/>
    <p>
        {{ trans('email.message.mail-about') }}
    </p>
</div>
