@extends('admin.main')

@section('body')

    <div class="row">

        {!! Form::open( [ 'route' => 'admin-notification-post', 'method' => 'post' ] ) !!}

        <div class="panel panel-info">

            <div class="panel-heading">
                <h4>{{ trans('admin.notification.title') }}</h4>
            </div>

            <div class="panel-body">
                <p>{{ trans('admin.notification.about') }}</p>

                @foreach($templates as $key => $value)

                    <div class="col-xs-24 col-sm-12 col-md-8 col-lg-8">

                        <table class="table table-hover admin-notification" data-name="{{ $key }}"
                               style="border: .1em solid #d6d6d6;">
                            <tr>
                                <td>
                                    <img style="width:4em;"
                                         src="/{{ config('image.icons_path') . $value['icon_src'] }}" alt="icon">
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                {{ trans($value['text']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="{{ route($value['url']) }}">
                                                    {{ trans('home.notifications-links') }}
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <i class="fa fa-close"></i>
                                </td>
                            </tr>
                        </table>

                    </div>

                @endforeach

                <input id="notificationName" type="hidden" name="notification">

            </div>
            <div class="panel-footer form-inline">

                <div class="input-group">
                    <select name="days" class="form-control" required>
                        @for($i=1;$i<8;$i++)
                            <option value="{{ $i }}" @if($i == old('days')) selected @endif>{{ $i }}</option>
                        @endfor
                    </select>
                    <div class="input-group-addon">
                        {{ trans('admin.gift.days') }}
                    </div>
                </div>

                <button id="adminNotificationActivated" class="btn btn-primary" disabled>
                    {{ trans('admin.notification.btn-create') }}
                </button>

            </div>

        </div>

        {!! Form::close() !!}

        <br>

        <div class="panel panel-success">

            <div class="panel-heading">
                <h4>{{ trans('admin.notification.active-general.title') }}</h4>
            </div>
            <div class="panel-body">
                <p>{{ trans('admin.notification.active-general.about') }}</p>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ trans('admin.notification.active-general.type') }}</th>
                        <th>{{ trans('admin.notification.active-general.created') }}</th>
                        <th>{{ trans('admin.notification.active-general.delete') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notifications as $notification)

                        <tr class="admin-active-notification">
                            <th scope="row">{{ $notification->id }}</th>
                            <td>
                                <table style="border: .1em solid #d6d6d6;width: 32em;">
                                    <tr>
                                        <td>
                                            <img style="width:4em;"
                                                 src="/{{ config('image.icons_path') . $notification->icon_src }}" alt="icon">
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ trans($notification->text) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="{{ route($notification->url) }}">
                                                            {{ trans('home.notifications-links') }}
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <i class="fa fa-close"></i>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>{{ getTimeAgo($notification->updated_at) }}</td>
                            <td>
                                <i class="fa fa-close admin-delete-notification"
                                   data-id="{{ $notification->id }}"
                                   style="font-size:1.3em;color:red;">
                                </i>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                {!! $notifications->render() !!}
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    @include('partials.alert-corner')

@endsection