@extends('admin.main')

@section('body')

    <div class="row">

        @if($dialog->warning)
            <div>
                {{ trans('notification.support-status-warning') }}
            </div>
        @endif

        <div class="col-xs-24 col-sm-8 col-md-7 col-lg-7">

            <div class="well well-lg">

                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        <td>
                            №: {{ $dialog->id }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('admin.support.category') }}
                            : {{ trans('support.category.' . $dialog->category . '') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('admin.support.status') }}
                            : {{ trans('support.dialog-status.' . $dialog->status . '') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('admin.support.author') }}:
                            @if($dialog->user_id)
                                <a href="{{ route('user-profile', $dialog->user_id) }}">#{{ $dialog->user_id }}</a>
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('admin.support.agent') }}:
                            @if($dialog->agent_id)
                                <a href="{{ route('user-profile', $dialog->agent_id) }}">#{{ $dialog->agent_id }}</a>
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('admin.support.last-update') }}: {{ getTimeAgo($dialog->updated_at) }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="col-xs-24 col-sm-14 col-md-14 col-lg-13">

            <div id="adminPanelDialogMessages" style="width: 100%;height: 50vh;background:#f4f4f4;overflow-y:auto;">

                <table class="table">

                    @foreach($dialog->messages as $message)

                        <tr>
                            <td>
                                <img class="img-circle" src="{{ $message->avatar }}" alt="avatar"
                                     style="height:4.4em;padding:.25em;">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            {{ $message->author }} {{ $message->author_id ? $message->author_id : null }}
                                            {{ getTimeAgo($message->created_at) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ $message->text }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endforeach

                </table>

            </div>

            <div>
                {!! Form::open( [ 'route' => 'post-admin-support-answer', 'method' => 'post' ] ) !!}

                <input type="hidden" name="dialog_id" value="{{ $dialog->id }}">

                <textarea rows="2" @if($dialog->warning) disabled @endif style="width:100%;" name="text" required
                          placeholder="{{ trans('admin.support.placeholder-answer') }}"
                          class="admin-support-for-unblock">{{ old('text', null) }}</textarea>

                <div>
                    <input type="checkbox" name="status" value="1" checked @if(!$dialog->user_id) disabled @endif>
                    {{ trans('admin.support.set-close-question') }}
                </div>

                @if($dialog->warning)
                    <a id="adminSupportUnblockTextarea" class="btn btn-danger">
                        {{ trans('admin.support.unblock-sending') }}
                    </a>
                @endif

                <button type="submit" class="btn btn-primary admin-support-for-unblock"
                        @if($dialog->warning) disabled @endif>{{ trans('admin.support.send-message') }}
                </button>

                {!! Form::close() !!}
            </div>

        </div>

        <div class="hidden-xs col-sm-2 col-md-3 col-lg-4"></div>

    </div>

@endsection

@section('js-bottom')

    <script src="/myjs/admin.js"></script>

@endsection