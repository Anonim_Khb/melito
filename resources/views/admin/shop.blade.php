@extends('admin.main')

@section('body')

    <div id="adminShopPage" class="row">

        <div class="panel panel-success">

            <div class="panel-heading">
                <h4>{{ trans('admin.shop.title') }}</h4>
            </div>
            <div class="panel-body">
                <p>{{ trans('admin.shop.about') }}</p>

                <div>
                    <a href="{{ route('admin-notification') }}">
                        {{ trans('admin.notification.link') }}
                    </a>
                </div>

                <div class="row shop-packages">

                    @foreach($shopPackages as $shopPackage)

                        {!! Form::open( [ 'url' => 'javascript:void(0);', 'method' => 'post', 'id' => $shopPackage->name, 'data-action' => route('admin-shop-package-update', $shopPackage->name) ] ) !!}

                        <div class="col-xs-24 col-sm-12 col-md-8 col-lg-6 {{ $shopPackage->name }}">

                            <div class="thumbnail">

                                <h4>{{ trans('shop.packages.titles.' . $shopPackage->name . '') }}</h4>

                                <ul class="list-unstyled">
                                    @foreach($shopServices as $shopService)
                                        <?php $serviceName = $shopService->name; ?>

                                        <li class="form-inline">
                                            <span>{{ trans('shop.goods.' . $serviceName . '.title') }}</span>
                                            <input style="width:6em;" type="number" name="{{ $serviceName }}"
                                                   class="form-control"
                                                   value="{{ $shopPackage->$serviceName }}">
                                        </li>
                                    @endforeach
                                </ul>

                                <div class="input-group">
                                    <div class="input-group-addon">{{ trans('admin.shop.real-price') }}</div>

                                    <input type="number" name="real_price" class="form-control real-price" disabled>

                                    <div class="input-group-addon"><i class="fa fa-ruble"></i></div>
                                </div>

                                <div class="input-group">
                                    <div class="input-group-addon">{{ trans('admin.shop.selling-price') }}</div>

                                    <input type="number" name="price" class="form-control"
                                           value="{{ $shopPackage->price }}">

                                    <div class="input-group-addon"><i class="fa fa-ruble"></i></div>
                                </div>

                                <div class="input-group">
                                    <div class="input-group-addon">{{ trans('shop.about.discount') }}</div>

                                    <input type="number" name="discount" class="form-control package-discount"
                                           value="{{ $shopPackage->discount }}" readonly>

                                    <div class="input-group-addon"><i class="fa fa-percent"></i></div>
                                </div>

                                <div>
                                    <button type="button" v-on:click="updatePackage('{{ $shopPackage->name }}')"
                                            class="btn btn-primary">
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>

                            </div>

                        </div>

                        {!! Form::close() !!}

                    @endforeach

                </div>

                <br><br><br><br>

                <div class="row">

                    {{ trans('shop.about.services-title') }}

                    <div class="shop-services">

                        <ul class="list-unstyled">
                            @foreach($shopServices as $shopService)

                                <li class="form-inline">
                                    {{ trans('shop.goods.' . $shopService->name . '.title') }}
                                    <input style="width:6em;" v-model="model.{{ $shopService->name }}" type="number"
                                           class="form-control" value="{{ $shopService->price }}">
                                    <i class="fa fa-ruble"></i>
                                    <button
                                        v-on:click="updateService('{{ $shopService->name }}', model.{{ $shopService->name }})"
                                        class="btn btn-primary">
                                        <i class="fa fa-save"></i>
                                    </button>
                                </li>
                            @endforeach
                        </ul>

                    </div>

                </div>

                <div>
                    {!! Form::open( [ 'url' => 'javascript:void(0);', 'method' => 'post', 'id' => 'adminColorUpdate' ] ) !!}

                    @foreach($colors as $color)
                        <input type="color" name="{{ $color->name }}" value="{{ $color->color }}">
                    @endforeach

                    <button type="button" v-on:click="updateColor()" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                    </button>

                    {!! Form::close() !!}
                </div>

            </div>

            <div class="panel-footer"></div>

        </div>


        <div class="panel panel-info">

            <div class="panel-heading">
                <h4>{{ trans('account.donate.transactions.title') }}</h4>
            </div>

            <div class="panel-body">

                <p>{{ trans('admin.transaction.about') }}</p>

                <table class="table">
                    <thead>
                    <tr class="active">
                        <th>№</th>
                        <th>{{ trans('admin.transaction.user') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.ad-id') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.type') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.name') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.number') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.sum') }}</th>
                        <th>{{ trans('account.donate.transactions.columns.date') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($transactions as $transaction)

                        <tr>
                            <th scope="row">{{ $transaction->id }}</th>

                            <td>
                                <a href="{{ route('user-profile', $transaction->user_id) }}">#{{ $transaction->user_id }}</a>
                            </td>

                            @if($transaction->ad_id)
                                <td>
                                    <a href="{{ route('offer-watch-open', $transaction->ad_id) }}">#{{ $transaction->ad_id }}</a>
                                </td>
                            @else
                                <td>--</td>
                            @endif

                            <td>{{ trans('shop.transaction.' . $transaction->description . '') }}</td>

                            @if (Lang::has('shop.all-gp.' . $transaction->good_name . ''))
                                <td>{{ trans('shop.all-gp.' . $transaction->good_name . '') }}</td>
                            @else
                                <td>--</td>
                            @endif

                            <td>{{ $transaction->number or '--' }}</td>
                            <td>{{ $transaction->sum or '--' }}</td>
                            <td>{{ dateFormatJFY($transaction->created_at) }}</td>
                        </tr>

                    @endforeach

                    </tbody>

                </table>

            </div>

            <div class="panel-footer">
                {!! $transactions->render() !!}
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    @include('partials.alert-corner')

    <script src="/myjs/admin-shop-vue.js"></script>

@endsection