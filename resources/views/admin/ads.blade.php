@extends('admin.main')

@section('body')

    <div class="row">

        <div class="panel panel-success">

            <div class="panel-heading">
                <h4>{{ trans('admin.ads.title') }}</h4>
            </div>
            <div class="panel-body">
                <p>{{ trans('admin.ads.about') }}</p>

                @foreach($ads as $ad)

                    <div class="media thumbnail">
                        <div class="media-left">
                            <img style="width:9em;"
                                 src="@if($ad->image) /{{ config('image.AdsOfferImagesPathMini') . $ad->image->filename }} @else /{{ config('image.AdsOfferDefaultImage') }} @endif"
                                 alt="Ad Title">
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading">
                                #{{ $ad->id }}
                                {{ trans('ads.ad-status.' . $ad->status . '') }}
                                <span style="float:right;">
                                    <a class="btn admin-ads-show-comments" data-id="{{ $ad->id }}"
                                       @if(count($ad->verifyComments) == 0) disabled @endif>{{ trans('admin.ads.comments') . ': '. count($ad->verifyComments) }}
                                    </a>
                                </span>
                            </h5>

                            <p>
                                {{ trans('admin.ads.created') }}
                                {{ dateFormatJFYHI($ad->created_at) }}
                            </p>

                            <p>
                                {{ trans('admin.ads.updated') }}
                                {{ dateFormatJFYHI($ad->updated_at) }}
                            </p>

                            @if($ad->privileges)
                                <p>
                                    {{ trans('admin.ads.privilege') }}
                                </p>
                            @endif

                            <a target="_blank" href="{{ route('offer-edit', $ad->id) }}">
                                {{ trans('admin.ads.watch') }}
                            </a>

                            @if(count($ad->verifyComments) >0)

                                @foreach($ad->verifyComments as $comment)

                                    <div class="media thumbnail admin-ads-comments" data-id="{{ $ad->id }}"
                                         style="display:none;">
                                        <div class="media-body">
                                            <h5 class="media-heading">
                                                {{ trans('admin.ads.comment') }} #{{ $comment->id }}
                                                {{ trans('admin.ads.created') }}{{ dateFormatJFYHI($comment->created_at) }}
                                            </h5>
                                            {{ trans('admin.ads.agent') }}
                                            {{ $comment->agent_id }}
                                            {{ $comment->comment }}
                                        </div>
                                    </div>

                                @endforeach

                            @endif
                        </div>
                    </div>

                @endforeach

            </div>
            <div class="panel-footer">
                {!! $ads->render() !!}
            </div>

        </div>

        <div class="panel panel-default">

            <div class="panel-heading">
                <h4>{{ trans('admin.delete-transaction.title') }}</h4>
            </div>
            <div class="panel-body">
                <p>{{ trans('admin.delete-transaction.about') }}</p>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ trans('admin.delete-transaction.ad-id') }}</th>
                        <th>{{ trans('admin.delete-transaction.agent-id') }}</th>
                        <th>{{ trans('admin.delete-transaction.comment') }}</th>
                        <th>{{ trans('admin.delete-transaction.date') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($deleteTransactions as $transaction)

                        <tr>
                            <th scope="row">{{ $transaction->id }}</th>
                            <td>{{ $transaction->ad_id }}</td>
                            <td>
                                <a href="{{ route('user-profile', $transaction->agent_id) }}">{{ '#' . $transaction->agent_id }}</a>
                            </td>
                            <td>{{ $transaction->comment }}</td>
                            <td>{{ dateFormatJFYHI($transaction->created_at) }}</td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                {!! $deleteTransactions->render() !!}
            </div>
        </div>

    </div>

@endsection

@section('js-bottom')

    @include('partials.alert-corner')

@endsection

