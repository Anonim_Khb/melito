<div>
    <a href="{{ route('admin-general') }}">
        {{ trans('admin.button.admin-panel') }}
    </a>

    {{ trans('admin.agent-number') . Auth::user()->id }}
</div>