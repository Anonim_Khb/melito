@extends('admin.main')

@section('body')

    <div class="row">

        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4>{{ trans('admin.support.title') }}</h4>
                </div>
                <div class="panel-body">
                    <p>{{ trans('admin.support.about') }}</p>

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{ trans('admin.support.category') }}</th>
                            <th>{{ trans('admin.support.status') }}</th>
                            <th>{{ trans('admin.support.agent') }}</th>
                            <th>{{ trans('admin.support.last-update') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dialogs as $dialog)

                            <tr class="support-row-link" data-href="{{ route('admin-support-answer', $dialog->id) }}">
                                <th scope="row">{{ $dialog->id }}</th>
                                <td>{{ trans('support.category.' . $dialog->category . '') }}</td>
                                <td>{{ trans('support.dialog-status.' . $dialog->status . '') }}</td>
                                <td>@if($dialog->agent_id) <a href="{{ route('user-profile', $dialog->agent_id) }}">#{{ $dialog->agent_id }}</a> @else -- @endif</td>
                                <td>{{ getTimeAgo($dialog->updated_at) }}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    {!! $dialogs->render() !!}
                </div>
            </div>

            <div class="panel panel-warning">

                <div class="panel-heading">
                    <h4>{{ trans('admin.support.title-warning') }}</h4>
                </div>
                <div class="panel-body">
                    <p>{{ trans('admin.support.about-warning') }}</p>

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{ trans('admin.support.category') }}</th>
                            <th>{{ trans('admin.support.status') }}</th>
                            <th>{{ trans('admin.support.agent') }}</th>
                            <th>{{ trans('admin.support.last-update') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($otherDialogs as $otherDialog)

                            <tr class="support-row-link" data-href="{{ route('admin-support-answer', $otherDialog->id) }}">
                                <th scope="row">{{ $otherDialog->id }}</th>
                                <td>{{ trans('support.category.' . $otherDialog->category . '') }}</td>
                                <td>{{ trans('support.dialog-status.' . $otherDialog->status . '') }}</td>
                                <td>@if($otherDialog->agent_id) <a href="{{ route('user-profile', $otherDialog->agent_id) }}">#{{ $otherDialog->agent_id }}</a> @else -- @endif</td>
                                <td>{{ getTimeAgo($otherDialog->updated_at) }}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="panel-footer">
                    {!! $otherDialogs->render() !!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src="/myjs/admin.js"></script>

@endsection