@extends('admin.main')

@section('body')

    @include('partials.page-notification')

    <div>
        <a href="{{ route('admin-support') }}">
            {{ trans('admin.support.link') }}
        </a>
    </div>

    <div>
        <a href="{{ route('admin-notification') }}">
            {{ trans('admin.notification.link') }}
        </a>
    </div>

    <div>
        <a href="{{ route('admin-gift') }}">
            {{ trans('admin.gift.link') }}
        </a>
    </div>

    <div>
        <a href="{{ route('admin-ads') }}">
            {{ trans('admin.ads.link') }}
        </a>
    </div>

    <div>
        <a href="{{ route('admin-shop') }}">
            {{ trans('admin.shop.link') }}
        </a>
    </div>


@endsection

@section('js-bottom')



@endsection