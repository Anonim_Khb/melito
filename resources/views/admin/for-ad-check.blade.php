<div class="bg-primary" style="border:.5em groove #31009d;padding:2em;">

    <h2>
        {{ trans('admin.ads.comment-header') }}

        @if(in_array(Auth::user()->group_id, config('app.userGroup.admin'), true))
            <a class="btn btn-danger"
               data-toggle="modal"
               data-target="#modalAdminDeleteAdConfirm"
               style="float:right;">
                {{ trans('admin.ads.delete-ad.modal-call-btn') }}
            </a>
        @endif
    </h2>

    <h4>ID: #{{ $adsOffer->id }}</h4>

    <h4>{{ trans('ads.ad-status.title') . ': ' . trans('ads.ad-status.' . $adsOffer->status . '') }}</h4>

    <h5>{{ trans('admin.ads.comment-placeholder') }}</h5>

    <textarea name="admin_comment" rows="6" placeholder="{{ trans('admin.ads.comment-placeholder') }}"
              style="width:100%;">{{ old('admin_comment') }}</textarea>

    <select class="form-control" name="admin_status">
        @foreach(config('ad.admin-status') as $adminStatus)
            <option value="{{ $adminStatus }}" @if($adsOffer->status == $adminStatus) selected @endif>{{ trans('ads.ad-status.' . $adminStatus . '') }}</option>
        @endforeach
    </select>

</div>

@include('modals.admin-delete-ad-confirm')