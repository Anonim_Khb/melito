@extends('admin.main')

@section('body')

    <div class="row">

        {!! Form::open( [ 'route' => 'admin-gift-personal', 'method' => 'post', 'class' => 'form-inline' ] ) !!}

        <div class="panel panel-info">

            <div class="panel-heading">
                <h4>{{ trans('admin.gift.personal.title') }}</h4>
            </div>

            <div class="panel-body">
                <p>{{ trans('admin.gift.personal.about') }}</p>

                <div class="form-group">
                    <select name="good_name" class="form-control" required>
                        <option>{{ trans('admin.gift.personal.good-name-placeholder') }}</option>
                        @foreach($goods as $key => $value)
                            <option value="{{ $key }}" @if($key == old('$key')) selected @endif>{{ $value['title'] }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select name="sum" class="form-control" required>
                        <option>{{ trans('admin.gift.personal.sum-placeholder') }}</option>
                        @for($i=1;$i<6;$i++)
                            <option value="{{ $i }}" @if($i == old('sum')) selected @endif>{{ $i }}</option>
                        @endfor
                    </select>
                </div>

                <div class="form-group">
                    <select class="form-control select2-users-js" name="user" style="width:18em;" required></select>
                </div>

            </div>

            <div class="panel-footer">
                <button class="btn btn-primary">
                    {{ trans('admin.gift.btn-create') }}
                </button>
            </div>

        </div>

        {!! Form::close() !!}



        {!! Form::open( [ 'route' => 'admin-gift-general', 'method' => 'post' ] ) !!}

        <div class="panel panel-warning">

            <div class="panel-heading">
                <h4>{{ trans('admin.gift.title') }}</h4>
            </div>

            <div class="panel-body">
                <p>{{ trans('admin.gift.about') }}</p>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

                    <div class="thumbnail admin-general-gifts" data-name="auto_up">
                        <img alt="icon" style="height: 200px; width: 100%; display: block;"
                             src="/{{ config('image.icons_path') . 'auto_up.png' }}">
                        <div class="caption"><h3>{{ trans('shop.goods.auto_up.title') }}</h3>
                            <p>{{ trans('shop.goods.auto_up.description') }}</p>
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

                    <div class="thumbnail admin-general-gifts" data-name="color">
                        <img alt="icon" style="height: 200px; width: 100%; display: block;"
                             src="/{{ config('image.icons_path') . 'color.png' }}">
                        <div class="caption"><h3>{{ trans('shop.goods.color.title') }}</h3>
                            <p>{{ trans('shop.goods.color.description') }}</p>
                        </div>
                    </div>

                </div>

                <input id="generalGiftName" type="hidden" name="good_name">

                <input type="text" name="text" class="form-control" value="{{ old('text') }}" placeholder="{{ trans('admin.gift.general-placeholder') }}">

            </div>

            <div class="panel-footer form-inline">
                <div class="input-group">
                    <select name="days" class="form-control" required>
                        @for($i=1;$i<8;$i++)
                            <option value="{{ $i }}" @if($i == old('days')) selected @endif>{{ $i }}</option>
                        @endfor
                    </select>
                    <div class="input-group-addon">
                        {{ trans('admin.gift.days') }}
                    </div>
                </div>

                <button id="adminGiftActivated" class="btn btn-primary" disabled>
                    {{ trans('admin.gift.btn-create') }}
                </button>
            </div>

        </div>

        {!! Form::close() !!}

    </div>



@endsection

@section('js-bottom')

    <script src="/myjs/select2-users.js"></script>

    @include('partials.alert-corner')

@endsection