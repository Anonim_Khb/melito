<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    @yield('include-links')

    <link href='/css/melito_css/auth.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid">

    <div class="row row-header">
        <div class="col-md-12 section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row row-center">
        <div class="col-md-12 section-content auth-main-content">
            @include('partials.page-notification')
            @yield('body')
            @include('partials.packages-js')
            @yield('js-bottom')
        </div>
    </div>

    <div class="row row-footer">
        <div class="col-md-12 section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>