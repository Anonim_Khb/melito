<div>
    <ul class="list-inline text-center email-list">
        <li><a data-blank="true" data-toggle="tooltip" data-placement="bottom" title="{{ trans('auth.registered.email-tooltip', ['mail' => 'Gmail']) }}" href="https://mail.google.com/mail">Gmail</a></li>
        <li><a data-blank="true" data-toggle="tooltip" data-placement="bottom" title="{{ trans('auth.registered.email-tooltip', ['mail' => 'Mail.ru']) }}" href="https://mail.ru/">Mail.ru</a></li>
        <li><a data-blank="true" data-toggle="tooltip" data-placement="bottom" title="{{ trans('auth.registered.email-tooltip', ['mail' => 'Yandex']) }}" href="https://mail.yandex.ru/">Yandex</a></li>
        <li><a data-blank="true" data-toggle="tooltip" data-placement="bottom" title="{{ trans('auth.registered.email-tooltip', ['mail' => 'Rambler']) }}" href="https://mail.rambler.ru/">Rambler</a></li>
        <li><a data-blank="true" data-toggle="tooltip" data-placement="bottom" title="{{ trans('auth.registered.email-tooltip', ['mail' => 'Yahoo']) }}" href="https://www.yahoo.com/">Yahoo</a></li>
    </ul>
</div>