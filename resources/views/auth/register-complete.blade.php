@extends('auth.main')

@section('body')

    <div class="row register-complete">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

            <div class="page-title text-center">{{ trans('auth.registered.title') }}</div>

            <div class="page-text jumbotron">
                {!!  trans('auth.registered.text', ['email' => Auth::user()->email]) !!}
            </div>

            @include('auth.mail-services')

            <div class="page-list jumbotron">
                {!! trans('auth.registered.why-need') !!}
            </div>

        </div>

    </div>

@endsection