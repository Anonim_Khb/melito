@extends('auth.main')

@section('include-links')


@endsection

@section('body')

    <div class="row auth-row">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <div class="auth-form">

                <div class="auth-title text-center">
                    <i class="icon-login-2"></i>
                    {{ trans('auth.login.title') }}
                </div>

                {!! Form::open( [ 'route' => 'login-post', 'method' => 'post' ] ) !!}

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput2">
                        {{ trans('auth.login.email') }}
                    </label>
                    <input class="form-control input-lg" id="focusedInput2" type="email" name="email" value="{{ old('email') }}" required="required">
                    <p class="help-block">{{ trans('auth.login.email-placeholder') }}</p>
                </div>

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput3">
                        {{ trans('auth.login.password') }}
                    </label>
                    <input class="form-control input-lg" id="focusedInput3" type="password" name="password" value="{{ old('password') }}" required="required">
                    <p class="help-block">{{ trans('auth.login.password-placeholder') }}</p>
                </div>

                <div class="auth-checkbox checkbox">
                    <label>
                        <input type="checkbox" name="remember" checked="checked">
                        <span>{{ trans('auth.login.remember-me') }}</span>
                    </label>
                </div>

                @if (Session::get('wrong_login') > 2)

                    @include('partials.recaptcha')

                @endif

                <div class="auth-btn">
                    <button class="btn btn-raised btn-success btn-lg" type="submit"
                            id="loginForm"><b>{{ trans('auth.login.btn') }}</b></button>
                </div>

                {!! Form::close() !!}

            </div>

            <div class="auth-bottom-links">
                <a href="{{ route('password-remind') }}">{{ trans('auth.login.forgot-password') }}</a>

                <a class="pull-right" href="{{ route('register') }}">{{ trans('auth.login.registration-btn') }}</a>
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src='https://www.google.com/recaptcha/api.js'></script>

@endsection