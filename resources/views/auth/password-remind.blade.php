@extends('auth.main')

@section('body')

    <div class="row auth-row remind-password">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <p id="showPasswordRecoveryInstruction" class="text-center">
                <a>{{ trans('auth.password-remind.show-text') }}<i class="fa fa-mouse-pointer fa-fw fa-lg"></i></a>
            </p>

            <div id="PasswordRecoveryInstruction" class="remind-text jumbotron">
                {!! trans('auth.password-remind.text') !!}
            </div>

            <div class="auth-form">

                <div class="auth-title text-center">
                    <i class="fa fa-unlock-alt"></i>
                    {{ trans('auth.password-remind.title') }}
                </div>

                {!! Form::open( [ 'route' => 'password-remind-post', 'method' => 'post' ] ) !!}

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput1">
                        {{ trans('auth.password-remind.email') }}
                    </label>
                    <input class="form-control input-lg" id="focusedInput1" type="email" name="email"
                           value="{{ old('email') }}" required="required">
                    <p class="help-block">{{ trans('auth.password-remind.email-placeholder') }}</p>
                </div>

                @include('partials.recaptcha')

                <div class="auth-btn">
                    <button class="btn btn-raised btn-success btn-lg" type="submit"
                            id="PassRemindForm">{{ trans('auth.password-remind.btn') }}</button>
                </div>

                {!! Form::close() !!}

            </div>

            <div class="auth-bottom-links">
                <a href="{{ route('faq-login-password') }}">{{ trans('auth.password-remind.support') }}</a>

                <a class="pull-right" href="{{ route('register') }}">{{ trans('auth.login.registration-btn') }}</a>
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script src="/myjs/auth.js"></script>

@endsection