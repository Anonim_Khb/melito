@extends('auth.main')

@section('include-links')

@endsection

@section('body')

    <div class="row auth-row">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <div class="auth-form">

                <div class="auth-title text-center">
                    <i class="icon-register-2"></i>
                    {{ trans('auth.register.title') }}
                </div>

                {!! Form::open( [ 'route' => 'register-post', 'method' => 'post' ] ) !!}

                @include('partials.register-fields')

                @include('partials.recaptcha')

                <div id="registerBtn" class="auth-btn">
                    <button class="btn btn-success btn-raised btn-lg" type="submit"
                            id="loginForm">{{ trans('auth.register.btn') }}</button>
                </div>

                {!! Form::close() !!}

            </div>

            <div class="auth-bottom-links">
                <a class="pull-right" href="{{ route('login') }}">{{ trans('auth.register.have-account') }}</a>
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script src="/myjs/auth.js"></script>

@endsection

