@extends('auth.main')

@section('body')

    <div class="row auth-row password-reset">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <div class="auth-form">

                <div class="auth-title text-center">
                    <i class="icon-login-1"></i>
                    {{ trans('auth.password-reset.title') }}
                </div>

                {!! Form::open( [ 'route' => 'password-reset-post', 'method' => 'post' ] ) !!}

                <input type="hidden" name="pr_token" value="{{ $pr_token }}">

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput1">{{ trans('auth.password-reset.email') }}</label>
                    <input id="focusedInput1" class="form-control input-lg" type="email" name="email"
                           value="{{ old('email') }}" required="required">
                    <p class="help-block">{{ trans('auth.password-reset.email-placeholder') }}</p>
                </div>

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput2">{{ trans('auth.password-reset.new-password') }}</label>
                    <input id="focusedInput2" class="form-control input-lg" type="password" name="new_password"
                           value="{{ old('new_password') }}" required="required">
                    <p class="help-block">{{ trans('auth.password-reset.new-password-placeholder') }}</p>
                </div>

                <div class="form-group label-floating">
                    <label class="control-label" for="focusedInput3">{{ trans('auth.password-reset.new-password-confirm') }}</label>
                    <input id="focusedInput3" class="form-control input-lg" type="password" name="new_password_confirmation"
                           value="{{ old('new_password_confirmation') }}" required="required">
                    <p class="help-block">{{ trans('auth.password-reset.new-password-confirm-placeholder') }}</p>
                </div>

                <div class="auth-btn reset-btn">
                    <button class="btn btn-raised btn-success btn-lg" type="submit"
                            id="PassReset">{{ trans('auth.password-reset.btn') }}</button>
                </div>

                {!! Form::close() !!}

            </div>

            <div class="auth-bottom-links">
                <a class="pull-right" href="{{ route('faq-login-password') }}">{{ trans('auth.password-remind.support') }}</a>
            </div>

        </div>

    </div>

@endsection