@extends('auth.main')

@section('body')


    <div class="row register-not-complete">

        <div
            class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

            <h2 class="page-title text-center">{{ trans('auth.not-confirm.title') }}</h2>

            <div class="page-text jumbotron">
                {!!  trans('auth.not-confirm.text', ['email' => Auth::user()->email]) !!}
            </div>

            @include('auth.mail-services')

            <div class="page-text">
                {!! trans('auth.not-confirm.re-send') !!}
            </div>

            <p id="showReSendForm" class="show-form">
                <a>{{ trans('auth.not-confirm.show-form') }}</a>
            </p>

            <div id="reSendForm">

                {!! Form::open( [ 'route' => 'registration-no-confirm-post', 'method' => 'post' ] ) !!}

                @include('partials.recaptcha')

                <div class="auth-btn">
                    <button class="btn btn-raised btn-success btn-lg" type="submit" id="ResendConfirmEmail">
                        {{ trans('auth.not-confirm.send-btn') }}
                    </button>
                </div>

                {!! Form::close() !!}
            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script src="/myjs/auth.js"></script>

@endsection