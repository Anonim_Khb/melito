@extends('account.main')
@section('body')

    <div class="row">

        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24">

            <div id="userDialogMessages" style="height:55vh;overflow-y:auto;background:#f4f4f4;">

                <table class="table">

                    @foreach($dialog->messages as $message)

                        <tr>
                            <td>
                                <img class="img-circle"
                                     src="@if($message->author_id == $userId)
                                     {{ userAvatarOrDefaultById($userId, 1) }}
                                     @else
                                     {{ '/' . config('image.UserAvatarsDefaultPathMini') . 'support-1.png' }}
                                     @endif" alt="avatar"
                                     style="height:3.1em;">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            @if($message->author_id == $userId)
                                                {{ trans('support.author.you') }}
                                            @else
                                                {{ trans('support.author.agent') . ' #' . $message->author_id }}
                                            @endif
                                            {{ dateFormatJFYHI($message->created_at) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ $message->text }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    @endforeach

                </table>

            </div>

            <div>

                {!! Form::open( [ 'route' => 'account-my-support-send-message', 'method' => 'post' ] ) !!}

                <textarea id="messageText" name="text" rows="2" required style="width:100%;"
                          placeholder="{{ trans('admin.support.placeholder-answer') }}"
                          @if($dialog->status == 'closed') disabled @endif
                >{{ old('text') }}</textarea>

                <input type="hidden" name="dialog" value="{{ $dialog->id }}">

                @if($dialog->status != 'closed')
                    <a data-toggle="modal"
                       data-target="#modalConfrimSupportCloseQuestion">
                        {{ trans('support.modal.close-question.call-bth') }}
                    </a>
                @endif

                <button type="submit"
                        class="btn btn-primary"
                        @if($dialog->status == 'closed') disabled @endif>
                    {{ trans('admin.support.send-message') }}
                </button>

                {!! Form::close() !!}

            </div>

        </div>

    </div>

@endsection

@section('js-bottom')

    <script src="/myjs/account-support.js"></script>

    @include('modals.support-close-question-confirm')

@endsection

