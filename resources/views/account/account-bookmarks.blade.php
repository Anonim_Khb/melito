@extends('account.main')

@section('include-links')

    <link href='/css/melito_css/modal.css' rel='stylesheet' type='text/css'>

    <link href='/css/melito_css/media.css' rel='stylesheet' type='text/css'>

@endsection

@section('body')

    <div id="accountBookmarks">

        @if(count($bookmarks) > 0)

            <div>
                <a id="accountDeleteAllBookmarks" data-toggle="modal" data-target="#modalBookmarkDeleteAll"
                   class="btn btn-sm btn-raised btn-danger">
                    {{ trans('account.bookmark.delete-all') }}
                </a>
            </div>

            <div class="ad-blocks">

            @foreach($bookmarks as $bookmark)

                <div class="ad-block">

                    <div class="ad-image">
                        <a href="{{ route('offer-watch-open', $bookmark->myAds->id) }}">
                            <img class="ad-img" data-adid="{{ $bookmark->myAds->id }}"
                                 style="background: url('/{{ isset($bookmark->myAds->image->filename) ?
                                                    config('image.AdsOfferImagesPathMini') . $bookmark->myAds->image->filename :
                                                    config('image.AdsOfferDefaultImage') }}') center center no-repeat; background-size: cover;">
                        </a>
                    </div>

                    <div class="ad-body">

                        <div class="ad-id-status">
                            <span class="ad-info ad-status-line">
                                <i class="fa fa-hashtag fa-fw"></i><span class="">{{ $bookmark->myAds->id }}</span>
                            </span>
                            <span class="ad-info pull-right">
                                <i class="fa fa-adjust fa-fw ad-status-line" data-container="body" data-toggle="popover"
                                   data-placement="top"
                                   data-content="{{ trans('ads.ad-status.title') . ': ' . trans('ads.ad-status.' . $bookmark->myAds->status . '') }}"></i>
                                <span class="dropup ad-status-line" id="adStatus">
                                    <i class="fa fa-info-circle fa-fw" class="dropdown-toggle"
                                       type="button" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false"></i>
                                    <span class="dropdown-menu dropdown-menu-right">
                                        {!! trans('ads.ad-info.info',
                                           ['views' => $bookmark->myAds->count, 'create' => dateFormatJFYHI($bookmark->myAds->created_at),
                                           'update' => dateFormatJFYHI($bookmark->myAds->updated_at),
                                           'city' => userLocale() == 'ru' ? $bookmark->myAds->cityName->name_ru : $bookmark->myAds->cityName->name_en]) !!}
                                    </span>
                                </span>
                            </span>
                        </div>

                        <a href="{{ route('offer-watch-open', $bookmark->myAds->id) }}">
                            <p class="ad-street">
                                {{ trans('account.ads.street-abbr') . str_limit($bookmark->myAds->street_name, 20, '...') }}
                            </p>

                            <p class="ad-address">
                                {{ trans('ads.new-offer.house-type.' . $bookmark->myAds->house_type . '') }},
                                {!! $bookmark->myAds->room_meters . trans('account.ads.square-meters') !!}{{ $bookmark->myAds->rooms ?
                                                ', ' . $bookmark->myAds->rooms . ' rooms' : '' }}
                            </p>

                            <p class="ad-price">
                                <span
                                        class="deal-type">{{ trans('account.ads.deal-type.' . $bookmark->myAds->deal_type . '') }}</span>
                                <span class="ad-price-sum">{{ $bookmark->myAds->rent_price }} <i
                                            class="fa fa-ruble"></i></span>
                            </p>

                        </a>

                    </div>

                </div>

            @endforeach

                <div class="ad-block"></div>
                <div class="ad-block"></div>
                <div class="ad-block"></div>
                <div class="ad-block"></div>

            </div>

        @endif

        <p id="noneBookmarks" class="text-center"
           @if(count($bookmarks) > 0) style="display:none;" @endif>
            {{ trans('account.bookmark.none-bookmarks') }}
        </p>

        {!! $bookmarks->render() !!}

    </div>

@endsection


@section('js-bottom')

    @include('partials.alert-corner')

    @include('modals.bookmarks-delete-confirm')

    <script src="/myjs/ad-bookmarks.js"></script>

@endsection

