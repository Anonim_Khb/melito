<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    <link href='/css/melito_css/account.css' rel='stylesheet' type='text/css'>

    @yield('include-links')

</head>
<body>

@include('partials.alert-top')

@include('partials.loading-process')

<div class="container-fluid all-home-tmp">

    <div class="row row-header">
        <div class="col-md-12 section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row account-row section-content">
        <div class="col-xs-12 col-sm-3 nopadding">
            <nav class="navbar my-menu-list-1">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#my-account-collapse" aria-expanded="false">
                            <i class="fa fa-bars collapse-btn"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="my-account-collapse">
                        @include('account.menu')
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-xs-12 col-sm-9">
            @include('partials.page-notification')
            @yield('body')
            @include('partials.packages-js')
            @yield('js-bottom')
        </div>
    </div>

    <div class="row row-footer">
        <div class="col-md-12 section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>