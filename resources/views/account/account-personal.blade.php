@extends('account.main')

@section('include-links')

    <link rel="stylesheet" href="/packages/jquery-file-upload/css/jquery.fileupload.css">

    <link rel="stylesheet" href="/packages/tapmodo-Jcrop/css/jquery.Jcrop.css" type="text/css"/>

    <link href='/packages/datetimepicker/css/bootstrap-datepicker.min.css' rel='stylesheet' type='text/css'>

@endsection

@section('body')

    <div class="account-personal">

        <div class="text-center user-avatar">

            <div class="user-avatar-img center-block">
                <img class="img-thumbnail" rel="gallery" id="user_avatar"
                     src="{{ userAvatarOrDefaultById($profile->user_id) }}"
                     alt="User avatar"/>

                <div id="avatarControlButtons">
                    <p class="fileinput-button" id="fileUploadAvatar">
                        <a class="avatar-control-buttons">
                            <i class="fa fa-image fa-fw">&nbsp;</i>
                            {{ trans('account.pr-part-general.avatar-change') }}
                        </a>
                        <input id="fileUpload" type="file" name="file">
                    </p>

                    <p id="deleteAvatarBtn" @if($profile->avatar != null) data-toggle="modal"
                       @endif data-target="#avastarDeleteConfirmModal">
                        <a class="avatar-control-buttons">
                            <i class="fa fa-reply fa-fw"></i>
                            {{ trans('account.pr-part-general.avatar-remove') }}
                        </a>
                    </p>

                </div>
            </div>

            <p class="help-block small">{{ trans('account.pr-part-general.avatar-validation') }}</p>

        </div>

        {!! Form::open( [ 'route' => 'account-personal-post', 'method' => 'post' ] ) !!}

        <div class="account-block-1">

            <h4 class="block-title">{{ trans('account.pr-part-general.title') }}</h4>

            <div class="form-group label-floating my-input-width">
                <label for="focusedInput1"
                       class="control-label input-title-label">{{ trans('account.pr-part-general.name.title') }}</label>
                <input id="focusedInput1" class="form-control input-lg" type="text" name="name"
                       value="{{ old('name', $profile->name) }}">
                <p class="help-block">{{ trans('account.pr-part-general.name.placeholder') }}</p>
            </div>

            <div class="form-group my-input-width">
                <label
                    class="control-label input-title-label">{{ trans('account.pr-part-general.gender.title') }}</label>
                <select name="gender" class="form-control input-lg">
                    <option value="">{{ trans('account.pr-part-general.gender.not_select') }}</option>
                    <option
                        {{ old('gender', $profile->gender) == 'male' ? 'selected' : '' }} value="male">{{ trans('account.pr-part-general.gender.male') }}</option>
                    <option
                        {{ old('gender', $profile->gender) == 'female' ? 'selected' : '' }} value="female">{{ trans('account.pr-part-general.gender.female') }}</option>
                </select>
            </div>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label"
                       for="userBirthday">{{ trans('account.pr-part-general.birthday.title') }}</label>
                <div class="input-group date">
                    <input id="userBirthday" type="text" class="form-control input-lg" name="birthday"
                           value="{{ old('birthday', $profile->birthday) }}"
                           data-end="{{ $minBirthdayDate }}"
                           data-locale="{{ Request::cookie('locale') }}"
                           placeholder="{{ trans('account.pr-part-general.birthday.placeholder') }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>

        </div>


        <div class="account-block-2">

            <h4 class="block-title">{{ trans('account.pr-part-contact.title') }}</h4>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label">{{ trans('account.pr-part-contact.email') }}</label>
                <input class="form-control input-lg" type="text" placeholder="{{ $user->email }}"
                       disabled="disabled">
            </div>

            <div class="form-group label-floating my-input-width">
                <label for="focusedInput2"
                       class="control-label input-title-label">{{ trans('account.pr-part-contact.phone.title') }}</label>
                <input id="focusedInput2" class="form-control input-lg" type="text" name="phone"
                       value="{{ old('phone', $profile->phone) }}">
                <p class="help-block">{{ trans('account.pr-part-contact.phone.placeholder') }}</p>
            </div>

            <div class="form-group label-floating my-input-width">
                <label for="focusedInput3"
                       class="control-label input-title-label">{{ trans('account.pr-part-contact.skype.title') }}</label>
                <input id="focusedInput3" class="form-control input-lg" type="text" name="skype"
                       value="{{ old('skype', $profile->skype) }}">
                <p class="help-block">{{ trans('account.pr-part-contact.skype.placeholder') }}</p>
            </div>

            <div class="form-group">
                <label class="control-label input-title-label">{{ trans('account.pr-part-contact.contacts') }}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="whatsapp"
                               value="1" {{ old('whatsapp', $profile->whatsapp) == '1' ? 'checked' : '' }}>
                        <span class="label label-success">WhatsApp</span>
                    </label>
                    <label>
                        <input type="checkbox" name="viber" value="1"
                            {{ old('viber', $profile->viber) == '1' ? 'checked' : '' }}>
                        <span class="label label-primary">Viber</span>
                    </label>
                    <label>
                        <input type="checkbox" name="messenger" value="1"
                            {{ old('messenger', $profile->messenger) == '1' ? 'checked' : '' }}>
                        <span class="label label-info">Messenger</span>
                    </label>
                </div>
            </div>

            <div class="form-group label-floating my-input-width">
                <label for="focusedInput4"
                       class="control-label input-title-label">{{ trans('account.pr-part-contact.another-contact.title') }}</label>
                <input id="focusedInput4" class="form-control input-lg" type="text" name="contact_another"
                       value="{{ old('contact_another', $profile->contact_another) }}">
                <p class="help-block">{{ trans('account.pr-part-contact.another-contact.placeholder') }}</p>
            </div>

        </div>


        <div class="account-block-3">

            <h4 class="block-title">{{ trans('account.pr-part-personal.title') }}</h4>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label">{{ trans('account.pr-part-personal.alcohol.title') }}</label>
                <select name="alcohol" class="form-control input-lg">
                    <option value="">{{ trans('account.pr-part-general.gender.not_select') }}</option>
                    <option value="yes"
                        {{ old('alcohol', $profile->alcohol) == 'yes' ? 'selected' : '' }}
                    >{{ trans('account.pr-part-personal.alcohol.yes') }}</option>
                    <option value="sometime"
                        {{ old('alcohol', $profile->alcohol) == 'sometime' ? 'selected' : '' }}
                    >{{ trans('account.pr-part-personal.alcohol.sometime') }}</option>
                    <option value="no"
                        {{ old('alcohol', $profile->alcohol) == 'no' ? 'selected' : '' }}
                    >{{ trans('account.pr-part-personal.alcohol.no') }}</option>
                </select>
            </div>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label">{{ trans('account.pr-part-personal.smoke.title') }}</label>
                <select name="smoke" class="form-control input-lg">
                    <option value="">{{ trans('account.pr-part-general.gender.not_select') }}</option>
                    <option value="yes" {{ old('smoke', $profile->smoke) == 'yes' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.smoke.yes') }}</option>
                    <option value="no" {{ old('smoke', $profile->smoke) == 'no' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.smoke.no') }}</option>
                </select>
            </div>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label">{{ trans('account.pr-part-personal.criminal.title') }}</label>
                <select name="criminal" class="form-control input-lg">
                    <option value="">{{ trans('account.pr-part-general.gender.not_select') }}</option>
                    <option value="no" {{ old('criminal', $profile->criminal) == 'no' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.criminal.no') }}</option>
                    <option value="yes" {{ old('criminal', $profile->criminal) == 'yes' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.criminal.yes') }}</option>
                </select>
            </div>

            <div class="form-group my-input-width">
                <label class="control-label input-title-label">{{ trans('account.pr-part-personal.work.title') }}</label>
                <select name="work" class="form-control input-lg">
                    <option value="">{{ trans('account.pr-part-general.gender.not_select') }}</option>
                    <option value="yes" {{ old('work', $profile->work) == 'yes' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.work.yes') }}</option>
                    <option value="student" {{ old('work', $profile->work) == 'student' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.work.student') }}</option>
                    <option value="business" {{ old('work', $profile->work) == 'business' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.work.business') }}</option>
                    <option value="no" {{ old('work', $profile->work) == 'no' ? 'selected' : '' }}>
                        {{ trans('account.pr-part-personal.work.no') }}</option>
                </select>
            </div>

            <div class="form-group label-floating my-input-width">
                <label for="focusedInput5" class="control-label input-title-label">{{ trans('account.pr-part-personal.additional.title') }}</label>
                <textarea id="focusedInput5" name="additional"
                          class="form-control input-lg" rows="3">{{ old('additional', $profile->additional) }}</textarea>
                <p class="help-block">{{ trans('account.pr-part-personal.additional.placeholder') }}</p>
            </div>

        </div>

        <div class="save-btn">
            <button class="btn btn-raised btn-success" type="submit" id="AccountQuestionnaire">{{ trans('account.save') }}</button>
        </div>

        {!! Form::close() !!}

    </div>

@endsection


@section('js-bottom')

    @include('partials.alert-corner')

    @include('modals.avatar-delete-confirm')

    @include('modals.avatar-crop')

    <script src='/packages/datetimepicker/js/bootstrap-datepicker.min.js'></script>
    <script src='/packages/datetimepicker/locales/bootstrap-datepicker.ru.js'></script>

    <script src="/packages/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="/packages/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <script src="/packages/jquery-file-upload/js/jquery.fileupload.js"></script>

    <script src="/packages/tapmodo-Jcrop/js/jquery.Jcrop.min.js"></script>

    <script src="/myjs/account.js"></script>

@endsection

