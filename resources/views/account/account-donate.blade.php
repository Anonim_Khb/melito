@extends('account.main')

@section('include-links')

    <link href='/css/melito_css/media.css' rel='stylesheet' type='text/css'>

@endsection

@section('body')

    <div id="accountDonate">

        <div class="row block-shadow-1">

            <div class="col-xs-12 col-sm-6">
                <h4 class="donate-title">
                    {{ trans('account.donate.balance.title') }}
                </h4>

                <p id="donateBalance">
                    {{ $donate->balance }} <i class="fa fa-ruble"></i>
                </p>
            </div>

            <div class="col-xs-12 col-sm-6">

                @include('partials.robokassa-HTML')

            </div>

        </div>

        <div class="row block-shadow-1">

            <div class="col-xs-12">
                <h4 class="donate-title">
                    {{ trans('account.donate.goods.title') }}
                </h4>

                <table class="table goods-table">

                    <tr>
                        <td>{{ trans('shop.goods.auto_up.title') }}</td>
                        <td class="good-sum">{{ $donate->goods->auto_up or 0 }}</td>
                    </tr>

                    <tr>
                        <td>{{ trans('shop.goods.color.title') }}</td>
                        <td class="good-sum">{{ $donate->goods->color or 0 }}</td>
                    </tr>

                    <tr>
                        <td>{{ trans('shop.goods.ad_limit.title') }}</td>
                        <td class="good-sum">{{ $donate->goods->ad_limit or 0 }}</td>
                    </tr>

                    <tr>
                        <td>{{ trans('shop.goods.top.title') }}</td>
                        <td class="good-sum">{{ $donate->goods->top or 0 }}</td>
                    </tr>

                    <tr>
                        <td>{{ trans('shop.goods.vip.title') }}</td>
                        <td class="good-sum">{{ $donate->goods->vip or 0 }}</td>
                    </tr>

                </table>

                <a id="goodsActivateBtn" class="btn btn-raised btn-info" href="{{ route('account-my-ads') }}">
                    <b>{{ trans('account.donate.privileges.btn-add') }}</b>
                </a>
            </div>

        </div>

        <div class="row block-shadow-1">
            <div class="col-xs-12">
                <h4 class="donate-title">
                    {{ trans('account.donate.privileges.title') }}
                </h4>

                <fieldset class='table-responsive'>
                    <table class="table table-responsive">
                        <thead>
                        <tr class="active">
                            <th>№</th>
                            <th>{{ trans('account.donate.transactions.columns.ad-id') }}</th>
                            <th>{{ trans('shop.all-gp.auto_up') }}</th>
                            <th>{{ trans('shop.all-gp.color') }}</th>
                            <th>{{ trans('shop.all-gp.top') }}</th>
                            <th>{{ trans('shop.all-gp.vip') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($privileges as $privilege)

                            <tr>
                                <th scope="row">{{ $privilege->id }}</th>
                                <td><a href="{{ route('offer-watch-open', $privilege->ad_id) }}">#{{ $privilege->ad_id }}</a></td>
                                @if($privilege->auto_up)
                                    <td>
                                        <span class="{{ $dateNow < $privilege->auto_up ? 'text-success' : 'text-danger' }}">{{ dateFormatJFYHI($privilege->auto_up) }}</span>
                                    </td>
                                @else
                                    <td>--</td>
                                @endif
                                @if($privilege->color)
                                    <td>
                                        <i class="fa fa-circle" style="font-size:1.3em;color:{{ $privilege->color_selected }}"></i>
                                        <span class="{{ $dateNow < $privilege->color ? 'text-success' : 'text-danger' }}">{{ dateFormatJFYHI($privilege->color) }}</span>
                                    </td>
                                @else
                                    <td>--</td>
                                @endif

                                @if($privilege->top)
                                    <td>
                                        <span class="{{ $dateNow < $privilege->top ? 'text-success' : 'text-danger' }}">{{ dateFormatJFYHI($privilege->top) }}</span>
                                    </td>
                                @else
                                    <td>--</td>
                                @endif

                                @if($privilege->vip)
                                    <td>
                                        <span class="{{ $dateNow < $privilege->vip ? 'text-success' : 'text-danger' }}">{{ dateFormatJFYHI($privilege->vip) }}</span>
                                    </td>
                                @else
                                    <td>--</td>
                                @endif
                            </tr>

                        @endforeach

                        </tbody>

                    </table>
                </fieldset>

                {!! $privileges->render() !!}

            </div>

        </div>

        <div class="row block-shadow-1">

            <div class="col-xs-12">
                <h4 class="donate-title">
                    {{ trans('account.donate.transactions.title') }}
                </h4>

                <fieldset class='table-responsive'>
                    <table class="table">
                        <thead>
                            <tr class="active">
                                <th>№</th>
                                <th>{{ trans('account.donate.transactions.columns.ad-id') }}</th>
                                <th>{{ trans('account.donate.transactions.columns.type') }}</th>
                                <th>{{ trans('account.donate.transactions.columns.name') }}</th>
                                <th>{{ trans('account.donate.transactions.columns.number') }}</th>
                                <th>{{ trans('account.donate.transactions.columns.sum') }}</th>
                                <th>{{ trans('account.donate.transactions.columns.date') }}</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($transactions as $transaction)

                            <tr>
                                <th scope="row">{{ $transaction->id }}</th>

                                @if($transaction->ad_id)
                                    <td>
                                        <a href="{{ route('offer-watch-open', $transaction->ad_id) }}">#{{ $transaction->ad_id }}</a>
                                    </td>
                                @else
                                    <td>--</td>
                                @endif

                                <td>{{ trans('shop.transaction.' . $transaction->description . '') }}</td>

                                @if (Lang::has('shop.all-gp.' . $transaction->good_name . ''))
                                    <td>{{ trans('shop.all-gp.' . $transaction->good_name . '') }}</td>
                                @else
                                    <td>--</td>
                                @endif

                                <td class="text-center">{{ $transaction->number or '--' }}</td>
                                <td>{{ $transaction->sum or '--' }}</td>
                                <td>{{ dateFormatJFYHI($transaction->created_at) }}</td>
                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </fieldset>

                {!! $transactions->render() !!}

            </div>

        </div>

    </div>

@endsection


@section('js-bottom')

    <script src="/myjs/robokassa-JS.js"></script>

@endsection

