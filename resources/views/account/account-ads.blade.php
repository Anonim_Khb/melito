@extends('account.main')

@section('include-links')

    <link href='/css/melito_css/media.css' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/packages/tapmodo-Jcrop/css/jquery.Jcrop.css" type="text/css"/>

@endsection

@section('body')

    <div id="accountAds">

        <div id="topInfoLine" class="row">

            <ul class="list-inline">
                <li id="adsCount">
                    {{ trans('account.ads.ads-count') }}:
                    <span id="countNumber">{{ count($adsOffers) }}</span>
                </li>

                <li id="createNew" class="pull-right">
                    <a href="{{ route('ads-new-offer') }}" class="btn btn-raised btn-sm btn-success">
                        {{ trans('account.ads.create') }}&nbsp;<i class="fa fa-plus fa-fw"></i>
                    </a>
                </li>
            </ul>

        </div>

            <div class="ad-blocks">

                @if(count($adsOffers) > 0)

                    @foreach($adsOffers as $adsOffer)

                        <div class="ad-block">

                            <div class="ad-image">
                                <a href="{{ route('offer-watch-open', $adsOffer->id) }}">
                                    <img class="ad-img" data-adid="{{ $adsOffer->id }}"
                                         style="background: url('/{{ isset($adsOffer->image->filename) ?
                                                    config('image.AdsOfferImagesPathMini') . $adsOffer->image->filename :
                                                    config('image.AdsOfferDefaultImage') }}') center center no-repeat; background-size: cover;">
                                </a>
                            </div>

                            <div class="ad-body">

                                <div class="ad-id-status">
                                    <span class="ad-info ad-status-line">
                                        <i class="fa fa-hashtag fa-fw"></i><span class="">{{ $adsOffer->id }}</span>
                                    </span>
                                    <span class="ad-info pull-right">
                                        <i class="fa fa-adjust fa-fw ad-status-line" data-container="body" data-toggle="popover"
                                           data-placement="top"
                                           data-content="{{ trans('ads.ad-status.title') . ': ' . trans('ads.ad-status.' . $adsOffer->status . '') }}"></i>
                                        <span class="dropup ad-status-line" id="adStatus">
                                            <i class="fa fa-info-circle fa-fw" class="dropdown-toggle"
                                               type="button" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false"></i>
                                            <span class="dropdown-menu dropdown-menu-right">
                                                {!! trans('ads.ad-info.info',
                                                   ['views' => $adsOffer->count, 'create' => dateFormatJFYHI($adsOffer->created_at),
                                                   'update' => dateFormatJFYHI($adsOffer->updated_at),
                                                   'city' => userLocale() == 'ru' ? $adsOffer->cityName->name_ru : $adsOffer->cityName->name_en]) !!}
                                            </span>
                                        </span>
                                    </span>
                                </div>

                                <a href="{{ route('offer-watch-open', $adsOffer->id) }}">
                                    <p class="ad-street">
                                        {{ trans('account.ads.street-abbr') . str_limit($adsOffer->street_name, 20, '...') }}
                                    </p>

                                    <p class="ad-address">
                                        {{ trans('ads.new-offer.house-type.' . $adsOffer->house_type . '') }},
                                        {!! $adsOffer->room_meters . trans('account.ads.square-meters') !!}{{ $adsOffer->rooms ?
                                                ', ' . $adsOffer->rooms . ' rooms' : '' }}
                                    </p>

                                    <p class="ad-price">
                                        <span
                                            class="deal-type">{{ trans('account.ads.deal-type.' . $adsOffer->deal_type . '') }}</span>
                                        <span class="ad-price-sum">{{ $adsOffer->rent_price }} <i
                                                    class="fa fa-ruble"></i></span>
                                    </p>

                                </a>

                                <div class="ad-footer">

                                    <div class="dropup footer-dd">
                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-bars fa-fw"></i>
                                            {{ trans('account.ads.btn.title') }}
                                            <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu" data-shid="{{ $adsOffer->id }}">
                                            <li v-show="{{ $adsOffer->status != 'closed' }}">
                                                <a v-on:click="adCoverImageCrop({{ $adsOffer->id }})">
                                                    <i class="fa fa-image fa-fw"></i>
                                                    <span>{{ trans('account.ads.btn.crop') }}</span>
                                                </a>
                                            </li>
                                            <li v-show="{{ $adsOffer->status != 'closed' }}" class="dd-submenu">
                                                @include('partials.ads-privileges-buttons', ['ad_id' => $adsOffer->id])
                                            </li>

                                            <li v-show="{{ $adsOffer->status != 'closed' }}">
                                                <a href="{{ route('offer-edit', $adsOffer->id) }}">
                                                    <i class="fa fa-edit fa-fw"></i>
                                                    <span>{{ trans('account.ads.btn.edit') }}</span>
                                                </a>
                                            </li>
                                            <li class="dd-submenu" v-show="{{ $adsOffer->status != 'closed' }}">
                                                <a>
                                                    <i class="fa fa-arrow-up fa-fw"></i>
                                                    <span>{{ trans('account.ads.btn.up.title') }}</span>
                                                    <i class="fa fa-caret-right fa-fw dd-submenu-arrow"></i>
                                                </a>
                                                <ul class="dd-submenu-list">
                                                    <li v-on:click="upAdFree({{ $adsOffer->id }})">
                                                        <a>
                                                            <i class="fa fa-angle-right fa-fw"></i>
                                                            {{ trans('account.ads.btn.up.free') }}
                                                        </a>
                                                    </li>
                                                    <li v-on:click="upAdPremium({{ $adsOffer->id }})">
                                                        <a>
                                                            <i class="fa fa-angle-right fa-fw"></i>
                                                            {{ trans('account.ads.btn.up.premium') }}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li v-show="{{ $adsOffer->status != 'closed' }}">
                                                <a v-on:click="hideOrShowAd({{ $adsOffer->id }})">
                                                    <i class="fa fa-eye-slash fa-fw"></i>
                                                    <span>{{ trans('account.ads.btn.hide') }}</span>
                                                </a>
                                            <li data-condition="closed"
                                                v-show="{{ $adsOffer->status == 'closed' }}">
                                                <a v-on:click="hideOrShowAd({{ $adsOffer->id }})">
                                                    <i class="fa fa-eye fa-fw"></i>
                                                    <span>{{ trans('account.ads.btn.show') }}</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                            </div>

                        </div>

                    @endforeach

                    <div class="ad-block"></div>
                    <div class="ad-block"></div>
                    <div class="ad-block"></div>
                    <div class="ad-block"></div>

            </div>

            @else

                <p id="noneAds" class="text-center">
                    {{ trans('account.ads.none-ads') }}
                </p>

            @endif

        {!! $adsOffers->render() !!}

    </div>

@endsection


@section('js-bottom')

    @include('partials.alert-corner')

    @include('modals.ads-privilege-activate')

    @include('modals.ad-cover-crop')

    <script src='/packages/vue/vue-full.js'></script>
    <script src='/packages/vue/vue-resource.js'></script>

    <script src="/packages/tapmodo-Jcrop/js/jquery.Jcrop.min.js"></script>

    <script src="/myjs/account-vue.js"></script>

    <script src="/myjs/ad-privileges.js"></script>

@endsection