<div class="list-group">

    <a class="list-group-item @if($menuActive == 'personal') active @endif" href="{{ route('account-personal') }}">
        <i class="fa fa-file-text fa-fw"></i>
        {{ trans('account.menu.link-personal-info') }}
    </a>

    <a class="list-group-item @if($menuActive == 'ads') active @endif" href="{{ route('account-my-ads') }}">
        <i class="fa fa-building-o fa-fw"></i>
        {{ trans('account.menu.link-ads') }}
    </a>

    <a class="list-group-item @if($menuActive == 'donate') active @endif" href="{{ route('account-my-donate') }}">
        <i class="fa fa-level-up fa-fw"></i>
        {{ trans('account.menu.link-donate') }}
    </a>

    <a class="list-group-item @if($menuActive == 'bookmarks') active @endif" href="{{ route('account-my-bookmarks') }}">
        <i class="fa fa-bookmark-o fa-fw"></i>
        {{ trans('account.menu.link-share') }}
    </a>

    <a class="list-group-item @if($menuActive == 'help') active @endif" href="{{ route('account-my-support') }}">
        <i class="fa fa-support fa-fw"></i>
        {{ trans('account.menu.link-help') }}
    </a>

    <a class="list-group-item @if($menuActive == 'settings') active @endif" href="{{ route('account-my-settings') }}">
        <i class="fa fa-cog fa-fw"></i>
        {{ trans('account.menu.link-setting') }}
    </a>

</div>