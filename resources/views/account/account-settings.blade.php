@extends('account.main')
@section('body')

    <h4>{{ trans('account.settings.change-password.title') }}</h4>

    {!! Form::open( [ 'route' => 'settings-change-password', 'method' => 'post' ] ) !!}

    <div>
        {{ trans('account.settings.change-password.old-password') }}
        <input type="password" name="old_password" value="{{ old('old_password') }}" required>
    </div>

    <div>
        {{ trans('account.settings.change-password.new-password') }}
        <input type="password" name="new_password_1" value="{{ old('new_password_1') }}" required>
    </div>

    <div>
        {{ trans('account.settings.change-password.repeat-password') }}
        <input type="password" name="new_password_2" value="{{ old('new_password_2') }}" required>
    </div>

    <div>
        <button type="submit" id="changePassword">{{ trans('account.settings.change-password.save-btn') }}</button>
    </div>

    {!! Form::close() !!}

@endsection


@section('js-bottom')

@endsection

