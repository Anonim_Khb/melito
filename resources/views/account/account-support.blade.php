@extends('account.main')
@section('body')

    <div>

        @if(count($dialogs) > 0)

            <div style="height:50vh;overflow-y:auto;">

                {{ trans('support.names.dialog-count') . count($dialogs) }}

                <table class="table table-hover">

                    @foreach($dialogs as $dialog)

                        <tr class="support-row-link"
                            data-href="{{ route('account-my-support', 'dialog=' . $dialog->id) }}">

                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            {{ trans('support.names.category') }}:
                                            {{ trans('support.category.' . $dialog->category . '') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ trans('support.names.from') }}
                                            {{ dateFormatJFY($dialog->created_at) }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                @if($dialog->watch == 'no')
                                    <i class="fa fa-circle" style="font-size: .7em;color:green;"></i>
                                @endif
                            </td>
                            <td>
                                <i class="{{ trans('support.fa-status.' . $dialog->status . '.class') }}"
                                   style="color:{{ trans('support.fa-status.' . $dialog->status . '.color') }}"
                                   data-toggle="tooltip" data-placement="bottom"
                                   title="{{ trans('support.dialog-status.' . $dialog->status . '') }}"></i>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <img class="img-circle"
                                                 src="@if($dialog->message['author_id'] == $userId)
                                                 {{ userAvatarOrDefaultById($userId, 1) }}
                                                 @else
                                                 {{ '/' . config('image.UserAvatarsDefaultPathMini') . 'support-1.png' }}
                                                 @endif" alt="avatar"
                                                 style="height:3.1em;">
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        @if($dialog->message['author_id'] == $userId)
                                                            {{ trans('support.author.you') }}
                                                        @else
                                                            {{ trans('support.author.agent') . ' #' . $dialog->message['author_id'] }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {{ dateFormatJFYHI($dialog->message['created_at']) }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    @endforeach

                </table>

            </div>

        @else

            svsvsvs

        @endif

        {!! $dialogs->render() !!}

    </div>

@endsection


@section('js-bottom')

    <script src="/myjs/account-support.js"></script>

@endsection

