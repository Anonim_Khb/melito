<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

    <script src="/myjs/account.js"></script>

    <link href='/css/melito_css/account.css' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-header">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 navbar-fixed-top section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row row-center">
        <div class="col-xs-24 col-sm-7 col-md-6 col-lg-6 section-content">
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        @include('another-user.menu')
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-xs-24 col-sm-17 col-md-18 col-lg-18 section-content">
            @include('partials.page-notification')
            @yield('body')
            @yield('js-bottom')
        </div>
    </div>

    <div class="row row-footer">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>