@extends('another-user.main')
@section('body')

        <div>
            <img style="width: 13em" id="user_avatar" src="{{ userAvatarOrDefaultById($user->profile->user_id) }}" alt="Your avatar" />
        </div>

        <div>
            {{ trans('account.pr-part-general.name') }}
            {{ $user->profile->name or $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-general.gender.title') }}
            {{ $user->profile->gender or $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-general.birthday') }}
            {{ $user->profile->birthday or $notExists }}
        </div>

        <br>
        {{ trans('account.pr-part-contact.title') }}

        <div>
            {{ trans('account.pr-part-contact.email') }}
            {{ $user->email }}
        </div>

        <div>
            {{ trans('account.pr-part-contact.phone') }}
            {{ $user->profile->phone or $notExists }}
        </div>

        <div>
            Skype
            {{ $user->profile->skype or $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-contact.contacts') }}
            <p>
                WhatsApp
                {{ $user->profile->whatsapp == 1 ? $exists : $notExists }}
            </p>
            <p>
                Viber
                {{ $user->profile->viber == 1 ? $exists : $notExists }}
            </p>
            <p>
                Messenger
                {{ $user->profile->messenger == 1 ? $exists : $notExists }}
            </p>
        </div>


        <div>
            {{ trans('account.pr-part-contact.another-contact') }}
            {{ $user->profile->contact_another or $notExists }}
        </div>

        <br>
        {{ trans('account.pr-part-personal.title') }}

        <div>
            {{ trans('account.pr-part-personal.alcohol.title') }}
            {{ $user->profile->alcohol ? trans('account.pr-part-personal.alcohol.' . $user->profile->alcohol . '') : $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-personal.smoke.title') }}
            {{ $user->profile->smoke ? trans('account.pr-part-personal.smoke.' . $user->profile->smoke . '') : $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-personal.criminal.title') }}
            {{ $user->profile->criminal ? trans('account.pr-part-general.gender.' . $user->profile->criminal . '') : $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-personal.work.title') }}
            {{ $user->profile->work ? trans('account.pr-part-general.gender.' . $user->profile->work . '') : $notExists }}
        </div>

        <div>
            {{ trans('account.pr-part-personal.additional') }}
            {{ $user->profile->additional or $notExists }}
        </div>

@endsection


@section('js-bottom')

@endsection

