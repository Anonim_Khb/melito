<div class="list-group">

    <a class="list-group-item @if($menuActive == 'personal') active @endif" href="{{ route('user-profile', Route::getCurrentRoute()->getParameter('id')) }}">
        {{ trans('account.menu.link-personal-info') }}
    </a>

    <a class="list-group-item @if($menuActive == 'ads') active @endif" href="{{ route('user-profile-ads', Route::getCurrentRoute()->getParameter('id')) }}">
        {{ trans('account.menu.link-ads') }}
    </a>

</div>