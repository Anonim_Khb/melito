@extends('another-user.main')
@section('body')

    @if(count($adsOffers) > 0)

        @foreach($adsOffers as $adsOffer)

            <div style="border:1px solid grey; margin: 1em 0">

                <div>
                    <i id="bookmarkStarEl" class="fa fa-star ad-bookmark {{ $adsOffer->bookmark != null ? 'bookmark-star-true' : 'bookmark-star-false' }}"
                       data-id="{{ $adsOffer->id }}"></i>
                </div>

                <img src="/{{ isset($adsOffer->image->filename) ?
                                            config('image.AdsOfferImagesPathMini') . $adsOffer->image->filename :
                                            config('image.AdsOfferDefaultImage') }}">

                <div>
                    {{ trans('ads.ad-status.title') }}
                    {{ trans('ads.ad-status.' . $adsOffer->status . '') }}
                </div>

                <div>
                    {{ userLocale() == 'ru' ?  $adsOffer->cityName->name_ru : $adsOffer->cityName->name_en }}
                </div>

                <div>
                    {{ $adsOffer->rent_price }}
                </div>

                <div>
                    {{ str_limit($adsOffer->ad_text, 100, '....') }}
                </div>

                <div>
                    {{ trans('account.ads.timestamp.created') }}
                    {{ dateFormatJFY($adsOffer->created_at) }}
                </div>

                <div>
                    {{ trans('account.ads.timestamp.updated') }}
                    {{ $adsOffer->updated_at }}
                </div>

                <a href="{{ route('offer-watch-open', $adsOffer->id) }}">WATCH</a>

            </div>

        @endforeach

    @else

        {!!  trans('account.another.not-ads') !!}

    @endif
@endsection


@section('js-bottom')

    @include('partials.alert-corner')

    <script src="/myjs/ad-bookmarks.js"></script>

@endsection

