@extends('shop.main')

@section('body')

    <div id="shopPage">

        <div>
            {{ trans('shop.about.shop-title') }}

            @if(isset($balance))
                {{ trans('shop.about.balance') }}
                {{ $balance }}
                <i class="fa fa-ruble"></i>

                <a href="{{ route('account-my-donate') }}"
                   @If(Notifications::byType('danger')->count() > 0) style="font-size: 1.5em;color:red;" @endif>
                    {{ trans('shop.about.add-funds') }}
                </a>
            @endif

        </div>

        <div class="row">

            @foreach($gifts as $gift)

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

                    <div class="thumbnail" data-name="auto_up">
                        <img alt="icon" style="height: 200px; width: 100%; display: block;"
                             src="/{{ config('image.icons_path') . $gift->good_name . '.png' }}">

                        <div class="caption"><h3>{{ trans('shop.about.have-gift') }}</h3>

                            <p><h4>{{ trans('shop.goods.' . $gift->good_name . '.title') }}</h4></p>
                            <p>{{ $gift->text }}</p>

                            <p><a href="{{ route('pick-up-gift-post', $gift->id) }}" class="btn btn-primary"
                                  role="button">
                                    {{ trans('shop.about.give-gift-btn') }}
                                </a></p>
                        </div>
                    </div>

                </div>

            @endforeach

        </div>

        <div class="row">

            @foreach($shopPackages as $shopPackage)

                <div class="col-xs-24 col-sm-12 col-md-8 col-lg-6">

                    <div class="thumbnail">

                        <h4>{{ trans('shop.packages.titles.' . $shopPackage->name . '') }}</h4>

                        <ul class="list-unstyled">
                            @foreach($shopServices as $shopService)
                                <?php $serviceName = $shopService->name; ?>

                                <li>
                                    {{ trans('shop.goods.' . $serviceName . '.title') }}
                                    {{ $shopPackage->$serviceName }}
                                </li>
                            @endforeach
                        </ul>

                        <div>
                            {{ $shopPackage->price }}
                            <i class="fa fa-ruble"></i>
                        </div>

                        <div>
                            {{ trans('shop.about.discount') }}
                            {{ $shopPackage->discount }}
                            <i class="fa fa-percent"></i>
                        </div>

                        <div>
                            <a class="btn btn-primary"
                                v-on:click="openModal('{{ $shopPackage->name }}','{{ trans('shop.packages.titles.' . $shopPackage->name . '') }}','{{ $shopPackage->price }}')"
                                data-toggle="modal"
                                data-target="#modalConfrimShopBuy">
                                {{ trans('shop.buttons.buy') }}
                            </a>
                        </div>

                    </div>

                </div>

            @endforeach

        </div>

        <br><br><br><br>

        <div class="row">

            {{ trans('shop.about.services-title') }}

            <div>

                <ul class="list-unstyled">
                    @foreach($shopServices as $shopService)

                        <li>
                            {{ trans('shop.goods.' . $shopService->name . '.title') }}
                            {{ $shopService->price }}
                            <i class="fa fa-ruble"></i>
                            <button
                                v-on:click="openModal('{{ $shopService->name }}','{{ trans('shop.goods.' . $shopService->name . '.title') }}','{{ $shopService->price }}')"
                                data-toggle="modal"
                                data-target="#modalConfrimShopBuy">
                                {{ trans('shop.buttons.buy') }}
                            </button>

                            <i class="fa fa-question-circle show-services-description"></i>

                            <div class="services-description" style="display: none;">
                                {{ trans('shop.goods.' . $shopService->name . '.description') }}
                            </div>
                        </li>
                    @endforeach
                </ul>

            </div>

        </div>

        @include('modals.shop-confirm-buy')

    </div>

@endsection

@section('js-bottom')

    <script src="/myjs/shop-vue.js"></script>

@endsection