<!DOCTYPE html>
<html lang="ru">
<head>

    @include('partials.meta')

    <title>{{ $title or '' }} | {{ config('app.site_name') }}</title>

    @include('partials.packages-css')

</head>
<body>

@include('partials.alert-top')

<div class="container-fluid all-home-tmp">

    <div class="row row-header">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-header">
            @include('partials.header')
        </div>
    </div>

    <div class="row row-center">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-content">
            @include('partials.page-notification')
            @yield('body')
            @include('partials.packages-js')
            @yield('js-bottom')
        </div>
    </div>


    <div class="row row-footer">
        <div class="col-xs-24 col-sm-24 col-md-24 col-lg-24 section-footer">
            @include('partials.footer')
        </div>
    </div>

</div>

</body>
</html>