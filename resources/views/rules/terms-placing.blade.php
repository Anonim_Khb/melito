@extends('rules.main')
@section('body')

    <h2 class="support-title">{{ trans('rules.terms-placing.title') }}</h2>

    <div>
        {!! trans('rules.terms-placing.foreword') !!}
    </div>

    <div>
        <h3>{{ trans('rules.terms-placing.provisions.title') }}</h3>
        {!! trans('rules.terms-placing.provisions.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.terms-placing.user-obligations.title') }}</h3>
        {!! trans('rules.terms-placing.user-obligations.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.terms-placing.provide-information.title') }}</h3>
        {!! trans('rules.terms-placing.provide-information.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.terms-placing.compliance.title') }}</h3>
        {!! trans('rules.terms-placing.compliance.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.terms-placing.guarantees.title') }}</h3>
        {!! trans('rules.terms-placing.guarantees.text') !!}
    </div>

@endsection


@section('js-bottom')

@endsection

