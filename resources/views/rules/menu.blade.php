<div class="list-group">

    <a class="list-group-item @if($menuActive == 'general') active @endif" href="{{ route('rules-general') }}">
        {{ trans('rules.menu.general') }}
    </a>

    <a class="list-group-item @if($menuActive == 'terms-placing') active @endif" href="{{ route('rules-terms-placing') }}">
        {{ trans('rules.menu.terms-placing') }}
    </a>

    <a class="list-group-item @if($menuActive == 'ads-requirements') active @endif" href="{{ route('rules-ads-requirements') }}">
        {{ trans('rules.menu.ads-requirements') }}
    </a>

    <a class="list-group-item @if($menuActive == 'paid-services') active @endif" href="{{ route('rules-paid-services') }}">
        {{ trans('rules.menu.paid-services') }}
    </a>

</div>

<div class="list-group second-menu">

    <a class="list-group-item" data-toggle="modal" data-target="#modalSupportSendMessage">
        {{ trans('home.menu.support') }}
    </a>

    <a class="list-group-item" href="{{ route('faq-login-password') }}">
        {{ trans('home.menu.faq') }}
    </a>

</div>