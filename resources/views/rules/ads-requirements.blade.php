@extends('rules.main')
@section('body')

    <h2 class="support-title">{{ trans('rules.ads-requirements.title') }}</h2>

    <div>
        {!! trans('rules.ads-requirements.foreword') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.relevance.title') }}</h3>
        {!! trans('rules.ads-requirements.relevance.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.unique.title') }}</h3>
        {!! trans('rules.ads-requirements.unique.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.region.title') }}</h3>
        {!! trans('rules.ads-requirements.region.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.details.title') }}</h3>
        {!! trans('rules.ads-requirements.details.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.price.title') }}</h3>
        {!! trans('rules.ads-requirements.price.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.photos.title') }}</h3>
        {!! trans('rules.ads-requirements.photos.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.ads-requirements.third-person.title') }}</h3>
        {!! trans('rules.ads-requirements.third-person.text') !!}
    </div>

@endsection


@section('js-bottom')

@endsection

