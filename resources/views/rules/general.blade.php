@extends('rules.main')
@section('body')

    <h2 class="support-title">{{ trans('rules.general.title') }}</h2>

    <div>
        {!! trans('rules.general.foreword') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.terms.title') }}</h3>
        {!! trans('rules.general.terms.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.conditions.title') }}</h3>
        {!! trans('rules.general.conditions.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.register.title') }}</h3>
        {!! trans('rules.general.register.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.ads.title') }}</h3>
        {!! trans('rules.general.ads.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.liability.title') }}</h3>
        {!! trans('rules.general.liability.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.information.title') }}</h3>
        {!! trans('rules.general.information.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.intellectual.title') }}</h3>
        {!! trans('rules.general.intellectual.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.use-information.title') }}</h3>
        {!! trans('rules.general.use-information.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.messages.title') }}</h3>
        {!! trans('rules.general.messages.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.validity.title') }}</h3>
        {!! trans('rules.general.validity.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.assignment.title') }}</h3>
        {!! trans('rules.general.assignment.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.general.disputes.title') }}</h3>
        {!! trans('rules.general.disputes.text') !!}
    </div>

@endsection


@section('js-bottom')

@endsection

