@extends('rules.main')
@section('body')

    <h2 class="support-title">{{ trans('rules.paid-services.title') }}</h2>

    <div>
        {!! trans('rules.paid-services.foreword') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.terms.title') }}</h3>
        {!! trans('rules.paid-services.terms.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.general.title') }}</h3>
        {!! trans('rules.paid-services.general.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.obligations.title') }}</h3>
        {!! trans('rules.paid-services.obligations.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.settlement.title') }}</h3>
        {!! trans('rules.paid-services.settlement.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.responsibility.title') }}</h3>
        {!! trans('rules.paid-services.responsibility.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.claims.title') }}</h3>
        {!! trans('rules.paid-services.claims.text') !!}
    </div>

    <div>
        <h3>{{ trans('rules.paid-services.agreement.title') }}</h3>
        {!! trans('rules.paid-services.agreement.text') !!}
    </div>

@endsection


@section('js-bottom')

@endsection

