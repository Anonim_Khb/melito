<?php

namespace App\Console;

use App\Models\AdDeleteTransaction;
use App\Models\AdsOffer;
use App\Models\AdsOfferTmpImage;
use App\Models\AdsPrivilege;
use App\Models\AvatarTemp;
use App\Models\EmailMessage;
use App\Models\Gift;
use App\Models\Notification;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use App\Models\UserPayment;
use App\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use File;
use Jenssegers\Date\Date;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];


    protected function schedule(Schedule $schedule)
    {
        //Clean Zombie
        $schedule->call(function () {
            User::where('status', 'pending')->where('updated_at', '<', Date::now()->subDays(7))
                ->delete();
        })->dailyAt('00:30');


        //Clear Email Messages
        $schedule->call(function () {
            EmailMessage::where('created_at', '<', Date::now()->subMonth(1))
                ->delete();
        })->monthly();


        //Clear Ads Photos Tmp
        $schedule->call(function () {
            $images = AdsOfferTmpImage::where('created_at', '<', Date::now()->subHours(2))
                ->get();

            foreach ($images as $image) {
                File::delete(public_path(config('image.AdsOfferImagesPath') . $image->filename));
                AdsOfferTmpImage::where('id', $image->id)->delete();
            }

        })->dailyAt('10:30');


        //Clear Avatars Photos Tmp
        $schedule->call(function () {
            $images = AvatarTemp::where('created_at', '<', Date::now()->subHours(2));

            foreach ($images->get() as $image) {
                File::delete(public_path(config('image.UserAvatarsTempFolder') . $image->name));
            }
            $images->delete();

        })->dailyAt('11:30');


        //Clear Users Payments
        $schedule->call(function () {
            UserPayment::where('created_at', '<', Date::now()->subDays(7))
                ->delete();
        })->dailyAt('01:30');


        //Auto Up Automatically
        $schedule->call(function () {
            $adsId = AdsPrivilege::where('auto_up', '>', Date::now())
                ->whereHas('parentAds', function($query){
                    $query->where('updated_at', '<', Date::now()->sub('' . config('shop.autoUpInterval') . ' Days'));
                })->lists('ad_id');

            AdsOffer::whereIn('id', $adsId)->update(['updated_at' => Date::now()]);
        })->hourly();


        //Clear Support Dialogs and Messages
        $schedule->call(function () {
            $dialogs = SupportDialog::where('updated_at', '<', Date::now()->sub('6 Month'));

            $dialogsIds = $dialogs->lists('id');

            $dialogs->delete();

            SupportDialogMessage::whereIn('dialog_id', $dialogsIds)->delete();
        })->weekly();


        //Clear Notifications
        $schedule->call(function () {

            Notification::whereNotNull('user_id')
                ->where('created_at', '<', Date::now()->sub('1 Month'))
                ->orWhere(function ($query) {
                    $query->whereNull('user_id')
                        ->where('valid_to', '<', Date::now());
                })->delete();

        })->weekly();


        //Delete Not Corrected Ads
        $schedule->call(function () {

            $adIds = AdsOffer::where('status', 'for-correct')
                ->where('updated_at', '<', Date::now()->sub('1 Month'))
                ->lists('id');

            $adsOffer = new AdsOffer();
            $adsOffer->AdDeleteAndAllRecords($adIds);

        })->monthly();


        //Ad Delete Transactions
        $schedule->call(function () {

            AdDeleteTransaction::where('created_at', '<', Date::now()->sub('6 Month'))
                ->delete();

        })->monthly();

        //Gifts Delete
        $schedule->call(function () {

            Gift::where('valid_to', '<', Date::now())
                ->delete();

        })->weekly();

    }

}
