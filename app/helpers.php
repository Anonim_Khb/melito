<?php

use \Jenssegers\Date\Date;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Session;

//    checkUserOnline
//    getUserAge
//    dateFormatJFY
//    dateFormatJFYHI
//    randomString
//    messageToAlertCorner
//    userAvatarOrDefaultById
//    redirectToPreviousURLorDefault
//    userLocale
//    getTimeAgo


function dateFormatJFY($available)
{
    $date = Date::parse($available)->format('j F Y');

    return $date;
}


function getUserAge($birthday)
{
    if (!$birthday || $birthday == '0000-00-00' || $birthday == null) {
        return null;
    }

    $age = Date::now()->format('Y') - Date::parse($birthday)->format('Y');

    return $age;
}


function getTimeAgo($date)
{
    $timeAgo = Date::parse($date)->ago();

    return $timeAgo;
}


function dateFormatJFYHI($date)
{
    return Date::parse($date)->format('j M Y ' . trans('home.in') . ' H:i');
}


function randomString()
{
    return uniqid(str_random());
}


function messageToAlertCorner($status, $message, $color)
{
    return ['status' => $status, 'message' => $message, 'color' => $color];
}


function userAvatarOrDefaultById($user_id, $mini = null)
{
    $profile = UserProfile::where('user_id', $user_id)
        ->select('avatar')
        ->first();

    if ($mini == null) {

        return isset($profile->avatar) ?
            '/' . config('image.UserAvatarsPath') . $profile->avatar :
            '/' . config('image.UserAvatarsDefaultImage');

    } else {

        return isset($profile->avatar) ?
            '/' . config('image.UserAvatarsPathMini') . $profile->avatar :
            '/' . config('image.UserAvatarsDefaultImageMini');

    }

}


function redirectToPreviousURLorDefault($anotherPath = null)
{
    if (Session::has('previous')) {

        return redirect()->guest(Session::pull('previous'));

    } else {

        return !empty($anotherPath) ? redirect($anotherPath) : redirect()->route('home');

    }
}


function userLocale()
{
    $locale = in_array(\Request::cookie('locale'), config('app.locales'), true) ? \Request::cookie('locale') : config('app.locale');

    return $locale;
}




//function checkUserOnline($updated_at)
//{
//    $date24 = new Date('-24 hours');
//    $onlineLimit = new Date('-16 minutes');
//
//    if ($onlineLimit > $updated_at) {
//        if ($updated_at >= $date24) {
//            return
//                trans('account.user-status.now-offline') .
//                Date::parse($updated_at)->ago();
//        } else {
//            return
//                trans('account.user-status.now-offline') .
//                Date::parse($updated_at)->format(
//                    'j M' .
//                    trans('account.user-status.in') .
//                    ' H:i');
//        }
//    } else {
//        return trans('account.user-status.online');
//    }
//
//}