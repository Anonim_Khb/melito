<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Auth;
use Hash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
        'group_id',
        'status',
        'confirm_register',
        'password_reset',
        'hash',
        'remember_token'
    ];

    protected $hidden = ['password', 'remember_token'];


    public function profile()
    {
        return $this->hasOne('App\Models\UserProfile', 'user_id', 'id');
    }


    public function ads()
    {
        return $this->hasMany('App\Models\AdsOffer', 'user_id', 'id');
    }


    public static function emailCheckExist($email)
    {
        $check = User::where('email', '=', $email)->first();

        return $check;
    }


    public static function login($data, $remember)
    {
        if (\Auth::attempt(['email' => $data['email'], 'password' => $data['password']], $remember)) {
            return \Auth::user();
        } else {
            return false;
        }
    }


    public static function registrationSimple($data, $random_str)
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'hash' => $random_str,
        ]);

        return $user;
    }


    public static function getUserByValue($field, $value)
    {
        $user = User::where($field, $value)->first();

        return $user;
    }


    public static function updateField($user_id, $field, $value)
    {
        $update = User::where('id', $user_id)
            ->update([$field => $value]);

        return $update;
    }


}