<?php
/*===========================================================
  ONLY AUTH
===========================================================*/
Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'account'], function () {

        Route::get('personal', [
            'as' => 'account-personal',
            'uses' => 'AccountController@getMyAccount',
        ]);

        Route::get('', function () {
            return redirect()->route('account-personal');
        });

        Route::post('personal', [
            'as' => 'account-personal-post',
            'uses' => 'AccountController@postMyAccount',
        ]);

        Route::get('ads', [
            'as' => 'account-my-ads',
            'uses' => 'AccountController@getMyAds',
        ]);

        Route::get('donate', [
            'as' => 'account-my-donate',
            'uses' => 'AccountController@getMyDonate',
        ]);

        Route::get('transactions', [
            'as' => 'account-my-transactions',
            'uses' => 'AccountController@getMyTransactions',
        ]);

        Route::get('bookmarks', [
            'as' => 'account-my-bookmarks',
            'uses' => 'AccountController@getMyBookmarks',
        ]);

        Route::get('support', [
            'as' => 'account-my-support',
            'uses' => 'AccountController@getMySupport',
        ]);

        Route::post('support/dialog/send', [
            'as' => 'account-my-support-send-message',
            'uses' => 'AccountController@mySupportSendMessage',
        ]);

        Route::post('support/dialog/close', [
            'as' => 'account-my-support-close-dialog',
            'uses' => 'AccountController@mySupportDialogClose',
        ]);

        Route::get('settings', [
            'as' => 'account-my-settings',
            'uses' => 'AccountController@getMySettings',
        ]);

        Route::post('settings/change-password', [
            'as' => 'settings-change-password',
            'uses' => 'Auth\PasswordController@postChangePassword',
        ]);

    });

    Route::get('offer/edit/{id}', [
        'as' => 'offer-edit',
        'uses' => 'AdsController@adEdit',
    ]);

    Route::post('shop/buy', [
        'as' => 'post-shop-buy',
        'uses' => 'ShopController@postShopBuy',
    ]);

    Route::get('shop/pick-up-gift/{gift_id}', [
        'as' => 'pick-up-gift-post',
        'uses' => 'ShopController@pickUpGift',
    ]);


    Route::group(['prefix' => 'payment'], function () {

        Route::post('send', [
            'as' => 'payment-send',
            'uses' => 'PaymentController@sendToRobokassa',
        ]);

        Route::post('result', [
            'as' => 'payment-result',
            'uses' => 'PaymentController@resultRobokassa',
        ]);

        Route::get('success', [
            'as' => 'payment-success',
            'uses' => 'PaymentController@successRobokassa',
        ]);

        Route::get('fail', [
            'as' => 'payment-fail',
            'uses' => 'PaymentController@failRobokassa',
        ]);

    });

});

/*===========================================================
  EASY AUTH
===========================================================*/
Route::group(['middleware' => 'auth-easy'], function () {

    Route::post('send/email/ad{id}/', [
        'as' => 'user-send-email-post',
        'uses' => 'AccountController@userSendEmail',
    ]);

});

/*===========================================================
  ONLY GUEST
===========================================================*/
Route::group(['middleware' => 'guest'], function () {
    // Authentication routes...
    Route::get('login', [
        'as' => 'login',
        'uses' => 'Auth\AuthController@getLogin',
    ]);

    Route::post('login', [
        'as' => 'login-post',
        'uses' => 'Auth\AuthController@postLogin',
    ]);

    // Registration routes...
    Route::get('register', [
        'as' => 'register',
        'uses' => 'Auth\AuthController@getRegister',
    ]);

    Route::post('register', [
        'as' => 'register-post',
        'uses' => 'Auth\AuthController@postRegister',
    ]);

    // Password REMIND
    Route::get('password/remind', [
        'as' => 'password-remind',
        'uses' => 'Auth\PasswordController@getPasswordRemind',
    ]);

    Route::post('password/remind', [
        'as' => 'password-remind-post',
        'uses' => 'Auth\PasswordController@postPasswordRemind',
    ]);

    // Password REMIND - RESET
    Route::get('password/reset/{token}', [
        'as' => 'password-reset',
        'uses' => 'Auth\PasswordController@getResetPassword'
    ]);

    Route::get('password/reset', function () {
        return redirect()->route('password-reset');
    });

    Route::post('password/reset', [
        'as' => 'password-reset-post',
        'uses' => 'Auth\PasswordController@postResetPassword',
    ]);

});

/*===========================================================
  NOT FILTER
===========================================================*/
// Home page
Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@index',
]);

//Logout
Route::get('logout', [
    'as' => 'logout',
    'uses' => 'Auth\AuthController@getLogout',
]);

//Change Language
Route::get('language/{lang}', [
    'as' => 'lang-select',
    'uses' => 'LanguageController@setLanguage',
]);

//NOT Confirm Register
Route::get('register/problem', [
    'as' => 'registration-no-confirm',
    'uses' => 'Auth\AuthController@getRegisterNotConfirm',
]);

Route::post('register/problem', [
    'as' => 'registration-no-confirm-post',
    'uses' => 'Auth\AuthController@postRegisterNotConfirm',
]);

//Register Complete
Route::get('register/complete', [
    'as' => 'registration-complete',
    'uses' => 'Auth\AuthController@registerComplete',
]);

//Confirm Register
Route::get('register/confirm/{token}', [
    'as' => 'register-confirm-email',
    'uses' => 'Auth\AuthController@registerConfirm',
]);

Route::group(['prefix' => 'ads'], function () {

    //Ads
    Route::get('new', [
        'as' => 'ads-new-offer',
        'uses' => 'AdsController@adsNewOffer',
    ]);

    Route::get('search', [
        'as' => 'ads-new-need',
        'uses' => 'AdsController@newNeedRoom',
    ]);

    Route::get('offer/finish', [
        'as' => 'ads-offer-finish',
        'uses' => 'AdsController@adsOfferFinishGroupOne',
    ]);

});

Route::get('offer/{offer_id}', [
    'as' => 'offer-watch-open',
    'uses' => 'AdsController@getAdOfferWatch',
]);

Route::get('user/{id}/account', [
    'as' => 'user-profile',
    'uses' => 'AccountController@userProfile',
]);

Route::get('user/{id}/ads', [
    'as' => 'user-profile-ads',
    'uses' => 'AccountController@userAds',
]);

//VIP && SHOP
Route::get('shop', [
    'as' => 'shop-page',
    'uses' => 'ShopController@getAboutShop',
]);

//SUPPORT
Route::group(['prefix' => 'support'], function () {

    Route::get('', function(){
        return redirect()->route('faq-login-password');
    });

    Route::post('new-dialog', [
        'as' => 'post-support-dialog',
        'uses' => 'SupportController@postNewSupportDialog',
    ]);

    Route::group(['prefix' => 'rules'], function () {

        Route::get('', function(){
            return redirect()->route('rules-general');
        });

        Route::get('general', [
            'as' => 'rules-general',
            'uses' => 'SupportController@rulesGeneral',
        ]);

        Route::get('ads-placing', [
            'as' => 'rules-terms-placing',
            'uses' => 'SupportController@rulesTermsPlacing',
        ]);

        Route::get('ads-requirements', [
            'as' => 'rules-ads-requirements',
            'uses' => 'SupportController@rulesAdsRequirements',
        ]);

        Route::get('paid-services', [
            'as' => 'rules-paid-services',
            'uses' => 'SupportController@rulesPaidServices',
        ]);

    });

    Route::group(['prefix' => 'faq'], function () {

        Route::get('', function(){
            return redirect()->route('faq-login-password');
        });

        Route::get('login-password', [
            'as' => 'faq-login-password',
            'uses' => 'SupportController@faqLoginAndPassword',
        ]);

        Route::get('ads-work', [
            'as' => 'faq-ads-work',
            'uses' => 'SupportController@faqAdsWork',
        ]);

        Route::get('placing-ads', [
            'as' => 'faq-placing-ads',
            'uses' => 'SupportController@faqPlacingAds',
        ]);

        Route::get('ad-edit', [
            'as' => 'faq-ad-edit',
            'uses' => 'SupportController@faqAdEdit',
        ]);

    });

});

/*===========================================================
  AJAX
===========================================================*/
Route::group(['middleware' => 'ajax'], function () {

    Route::post(
        'ads/offer/images',
        'FilesController@adsOfferImages'
    );

    Route::post(
        'ads/edit/images',
        'FilesController@adsOfferEditImages'
    );

    Route::post(
        'ads/delete-image',
        'FilesController@deleteImageAjax'
    );

    Route::post('ads/new/offerForm', [
        'uses' => 'AdsController@postAdsNewOffer',
    ]);

    Route::post('ads/edit/form', [
        'uses' => 'AdsController@adsEditAjax',
    ]);

    Route::get(
        'ads/cities',
        'AdsController@getCitiesForAds'
    );

    Route::post(
        'ads/need/search',
        'AdsController@AddNeedSearchAjax'
    );

    Route::post(
        'ads/add-bookmark',
        'AdsController@AddToBookmark'
    );

    Route::post(
        'ads/delete-one-bookmark',
        'AdsController@DeleteOneBookmark'
    );

    Route::post(
        'ads/delete-all-bookmarks',
        'AdsController@DeleteAllBookmarks'
    );

    Route::post(
        'account/ads/up',
        'AdsController@adUpFreeAjax'
    );
    Route::post(
        'account/ads/up-premium',
        'AdsController@adUpPremiumAjax'
    );

    Route::post(
        'account/ads/show-hide',
        'AdsController@adHideOrShowStatusAjax'
    );

    Route::post('ads/add-privileges', [
        'uses' => 'ShopController@postAddPrivilege',
    ]);

    Route::post('notification/hide', [
        'uses' => 'PagesController@hideNotification',
    ]);

    Route::post('notification/hide/all', [
        'uses' => 'PagesController@hideAllNotifications',
    ]);

    Route::post('account/avatar/upload-temp', [
        'uses' => 'FilesController@avatarForTempFolder',
    ]);

    Route::post('account/avatar/upload', [
        'uses' => 'FilesController@avatarUploadAjax',
    ]);

    Route::post('account/avatar/delete', [
        'uses' => 'FilesController@avatarDeleteAjax',
    ]);

    Route::post('ads/image/cover-edit', [
        'uses' => 'AdsController@adCoverImageNameById',
    ]);

    Route::post('ads/image/cover-crop', [
        'uses' => 'FilesController@adCoverImageCrop',
    ]);

});

/*===========================================================
  ADMINISTRATION
===========================================================*/
Route::group(['prefix' => 'admin'], function () {

    Route::group(['middleware' => 'admin'], function () {

        Route::get('', [
            'as' => 'admin-general',
            'uses' => 'AdminController@getAdminGeneral',
        ]);

        Route::get('support', [
            'as' => 'admin-support',
            'uses' => 'AdminController@getAdminSupport',
        ]);

        Route::get('support/{dialog_id}', [
            'as' => 'admin-support-answer',
            'uses' => 'AdminController@adminSupportAnswer',
        ]);

        Route::post('support/answer', [
            'as' => 'post-admin-support-answer',
            'uses' => 'AdminController@postAdminSupportAnswer',
        ]);

        Route::get('find/user', [
            'uses' => 'AdminController@findUserSelect2Ajax',
        ]);

        Route::get('ads', [
            'as' => 'admin-ads',
            'uses' => 'AdminController@getAdsValidate',
        ]);

    });

    Route::group(['middleware' => 'super-admin'], function () {

        Route::get('notification/create', [
            'as' => 'admin-notification',
            'uses' => 'AdminController@getNotificationPage',
        ]);

        Route::post('notification/create/new', [
            'as' => 'admin-notification-post',
            'uses' => 'AdminController@createNewNotificationPost',
        ]);

        Route::post('notification/delete', [
            'uses' => 'AdminController@deleteGeneralNotificationAjax',
        ]);

        Route::get('gift/create', [
            'as' => 'admin-gift',
            'uses' => 'AdminController@makeGiftAction',
        ]);

        Route::post('gift/create/general', [
            'as' => 'admin-gift-general',
            'uses' => 'AdminController@giveGeneralGiftPost',
        ]);

        Route::post('gift/create/personal', [
            'as' => 'admin-gift-personal',
            'uses' => 'AdminController@givePersonalGiftPost',
        ]);

        Route::post('ad/delete', [
            'uses' => 'AdminController@deleteAdAjax',
        ]);

        Route::get('shop', [
            'as' => 'admin-shop',
            'uses' => 'AdminController@getShop',
        ]);

        Route::post('shop/update/service', [
            'uses' => 'AdminController@shopUpdateService',
        ]);

        Route::post('shop/update/package/{name}', [
            'as' => 'admin-shop-package-update',
            'uses' => 'AdminController@shopUpdatePackage',
        ]);

        Route::post('shop/update/colors', [
            'uses' => 'AdminController@updateColorsAjax',
        ]);

    });

});