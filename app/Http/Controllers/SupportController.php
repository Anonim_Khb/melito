<?php namespace App\Http\Controllers;

use App\Http\MyClass\MyArrays;
use App\Http\Requests\DialogMessagesAjaxRequest;
use App\Http\Requests\supportNewDialogPostRequest;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use Request;
use Notifications;
use Illuminate\Support\Facades\Auth;
use Response;


class SupportController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function postNewSupportDialog(
        supportNewDialogPostRequest $errors,
        Request $request,
        SupportDialog $dialog,
        SupportDialogMessage $dialogMessage
    ) {
        $user_id = $this->user ? $this->user->id : null;

        $dialog = $dialog->create([
            'user_id' => $user_id,
            'status' => $this->user ? 'wait' : 'guest',
            'category' => $request::get('category'),
        ]);

        $dialogMessage->create([
            'dialog_id' => $dialog->id,
            'author_id' => $user_id,
            'text' => e($request::get('text')),
        ]);

        Notifications::add(trans('notification.support-new-dialog'), 'success');

        return redirect()->back();
    }


    public function rulesGeneral()
    {
        $data = [
            'title' => trans('title.rules'),
            'menuActive' => 'general'
        ];

        return view('rules.general', $data);
    }


    public function rulesTermsPlacing()
    {
        $data = [
            'title' => trans('title.rules'),
            'menuActive' => 'terms-placing'
        ];

        return view('rules.terms-placing', $data);
    }


    public function rulesAdsRequirements()
    {
        $data = [
            'title' => trans('title.rules'),
            'menuActive' => 'ads-requirements'
        ];

        return view('rules.ads-requirements', $data);
    }


    public function rulesPaidServices()
    {
        $data = [
            'title' => trans('title.rules'),
            'menuActive' => 'paid-services'
        ];

        return view('rules.paid-services', $data);
    }


    public function faqLoginAndPassword()
    {
        $data = [
            'title' => trans('title.faq'),
            'questions' => trans('faq.login-password.body'),
            'menuActive' => 'login-password'
        ];

        return view('faq.login-password', $data);
    }


    public function faqAdsWork()
    {
        $data = [
            'title' => trans('title.faq'),
            'questions' => trans('faq.ads-work.body'),
            'menuActive' => 'ads-work'
        ];

        return view('faq.ads-work', $data);
    }


    public function faqPlacingAds()
    {
        $data = [
            'title' => trans('title.faq'),
            'questions' => trans('faq.placing-ads.body'),
            'menuActive' => 'placing-ads'
        ];

        return view('faq.placing-ads', $data);
    }


    public function faqAdEdit()
    {
        $data = [
            'title' => trans('title.faq'),
            'questions' => trans('faq.ad-edit.body'),
            'menuActive' => 'ad-edit'
        ];

        return view('faq.ad-edit', $data);
    }

}