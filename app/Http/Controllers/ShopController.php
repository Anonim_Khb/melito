<?php namespace App\Http\Controllers;

use App\Http\Requests\PrivilegesActivateAjaxRequest;
use App\Http\Requests\ShopBuyingRequest;
use App\Models\AdsOffer;
use App\Models\AdsPrivilege;
use App\Models\Gift;
use App\Models\ReceivedGift;
use App\Models\ShopColor;
use App\Models\ShopPackage;
use App\Models\ShopPrice;
use App\Models\ShopService;
use App\Models\UserGood;
use App\Models\UserProfile;
use App\Models\UserTransaction;
use Request;
use Notifications;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;


class ShopController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();

        $this->middleware('auth', ['only' => ['purchaseService']]);
    }


    public function getAboutShop(Gift $gift, ShopService $shopService, ShopPackage $shopPackage)
    {
        $gifts = $gift->where('valid_to', '>', Date::now());

        if ($this->user && $this->user->status == 'active') {

            $balance = $this->user->profile->balance;

            $gifts = $gifts->with([
                'receivedGifts' => function ($query) {
                    $query->where('user_id', $this->user->id);
                }
            ])->get();

            foreach ($gifts as $key => $value) {

                $value->receivedGifts != null ? $gifts->forget($key) : '';

            }

        } else {

            $balance = null;

            $gifts = $gifts->get();

        }

        $data = [
            'title' => trans('title.shop-about'),
            'balance' => $balance,
            'gifts' => $gifts,
            'shopPackages' => $shopPackage->get(),
            'shopServices' => $shopService->get()
        ];

        return view('shop.shop', $data);
    }


    public function postShopBuy(
        ShopBuyingRequest $errors,
        Request $request,
        ShopPackage $shopPackage,
        ShopService $shopService
    ) {
        $good = $request::get('good');
        $number = $request::get('number');
        $balance = $this->user->profile->balance;

        $shopService = $shopService->where('name', $good)->first();

        if ($shopService) {

            $sum = $shopService->price * $number;

        } else {

            $shopPackage = $shopPackage->where('name', $good)->first();

            $sum = $shopPackage->price * $number;

        }

        if ($sum > $balance) {

            Notifications::add(trans('notification.tight-money'), 'danger');

            return redirect()->back();

        }

        UserProfile::updateBalance($this->user->id, $balance, $sum);

        UserGood::addGoods($this->user->id, $good, $number);

        UserTransaction::addTransaction($this->user->id, null, $good, $number, $sum, 'buy');

        Notifications::add(trans('notification.buying-complete'), 'success');

        return redirect()->route('shop-page');
    }


    public function postAddPrivilege(
        PrivilegesActivateAjaxRequest $errors,
        Request $request,
        AdsOffer $adsOffer,
        UserGood $userGood
    ) {
        $adCheckAuthor = $adsOffer->where('id', $request::get('ad_id'))
            ->where('user_id', $this->user->id)
            ->first();

        if (!$adCheckAuthor) {

            return messageToAlertCorner(null, trans('notification.corner.error'),
                config('notification.color.error'));

        } elseif ($adCheckAuthor->status == 'closed') {

            return messageToAlertCorner(null, trans('notification.corner.ad-status-closed'),
                config('notification.color.warning'));

        }

        $goodName = $request::get('good_name');
        $sum = $request::get('sum');
        $ad_id = $request::get('ad_id');
        $color = $request::get('color');

        $good = $userGood->where('user_id', $this->user->id);

        $goodExists = $good->first();

        if ($goodExists && $goodExists->$goodName >= $sum) {

            $good->update([
                $goodName => $goodExists->$goodName - $sum
            ]);

            AdsPrivilege::serviceActivated($this->user->id, $ad_id, $goodName, $sum, $color);

            UserTransaction::addTransaction($this->user->id, $ad_id, $goodName, $sum, null, 'spent');

            return messageToAlertCorner(null, trans('notification.corner.ad-privilege-activated'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner(null, trans('notification.corner.premium-error'),
                config('notification.color.warning'));

        }

    }


    public function pickUpGift(
        $gift_id,
        Gift $gift,
        ReceivedGift $receivedGift,
        UserGood $userGood,
        UserTransaction $userTransaction
    ) {
        $giftCheck = $gift->where('id', $gift_id)
            ->where('valid_to', '>', Date::now())
            ->with([
                'receivedGifts' => function ($query) {
                    $query->where('user_id', $this->user->id);
                }
            ])
            ->first();

        if ($giftCheck && $giftCheck->receivedGifts == null) {

            $receivedGift->create(['user_id' => $this->user->id, 'gift_id' => $giftCheck->id]);

            $userGood->addGoods($this->user->id, $giftCheck->good_name, 1);

            $userTransaction->addTransaction($this->user->id, null, $giftCheck->good_name, 1, null,
                'admin-gift-action');

            Notifications::add(trans('notification.pick-up-gift-success'), 'success');

            return redirect()->route('account-my-donate');

        } else {

            Notifications::add(trans('notification.pick-up-gift-error'), 'error');

            return redirect()->back();

        }


    }


}