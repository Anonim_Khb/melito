<?php namespace App\Http\Controllers;

class LanguageController extends Controller
{


    public function __construct()
    {
        //
    }


    public function setLanguage($lang)
    {
        in_array($lang, config('app.locales'), true) ? \Cookie::queue('locale', $lang, 2628000) : '';

        return back();
    }


}