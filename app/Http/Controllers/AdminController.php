<?php namespace App\Http\Controllers;

use App\Http\MyClass\MailerClass;
use App\Http\Requests\AdminGiftGeneralPostRequest;
use App\Http\Requests\AdminGiftPersonalPostRequest;
use App\Http\Requests\AdminNewNotificationPostRequest;
use App\Http\Requests\AdminSupportAnswerPostRequest;
use App\Http\Requests\ColorUpdateAdminAjaxRequest;
use App\Models\AdBookmark;
use App\Models\AdDeleteTransaction;
use App\Models\AdsOffer;
use App\Models\AdsOfferImage;
use App\Models\AdsPrivilege;
use App\Models\Gift;
use App\Models\Notification;
use App\Models\ShopColor;
use App\Models\ShopPackage;
use App\Models\ShopPrice;
use App\Models\ShopService;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use App\Models\UserGood;
use App\Models\UserTransaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Request;
use Jenssegers\Date\Date;
use Notifications;
use Validator;
use Response;

class AdminController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function getAdminGeneral()
    {
        $data = [
            'title' => trans('title.admin-general')
        ];

        return view('admin.general', $data);
    }


    public function getAdminSupport(SupportDialog $supportDialog)
    {
        $dialogs = $supportDialog->whereNull('agent_id')
            ->orWhere(function ($query) {
                $query->where('agent_id', $this->user->id);
            })
            ->whereNotIn('status', ['closed', 'agent'])
            ->orderBy('updated_at', 'asc')
            ->paginate(20);

        $otherDialogs = $supportDialog->where(function ($query) {
            $query->where('status', 'in-work')
                ->where('updated_at', '<', Date::now()->sub('2 Hours'));
        })
            ->orWhere(function ($query) {
                $query->where('status', 'wait')
                    ->where('updated_at', '<', Date::now()->sub('12 Hours'));
            })
            ->orderBy('updated_at', 'asc')
            ->paginate(10);

        $data = [
            'title' => trans('title.admin-support'),
            'dialogs' => $dialogs,
            'otherDialogs' => $otherDialogs
        ];

        return view('admin.support', $data);
    }


    public function adminSupportAnswer($dialog_id, SupportDialog $supportDialog)
    {
        $selectDialog = $supportDialog->where('id', $dialog_id);

        $dialog = $selectDialog->with('messages')->first();

        if (($dialog->agent_id && $dialog->agent_id != $this->user->id) ||
            in_array($dialog->status, ['closed', 'guest', 'agent'])
        ) {

            if ($dialog->status == 'guest') {
                SupportDialog::updateDialog($dialog_id, ['status' => 'closed', 'agent_id' => $this->user->id]);
            }

            $dialog = array_add($dialog, 'warning', true);

        } else {
            SupportDialog::updateDialog($dialog_id, ['status' => 'in-work', 'agent_id' => $this->user->id]);
        }

        $dialog = self::avatarAndAuthorForHuman($dialog);

        $data = [
            'title' => trans('title.admin-support'),
            'dialog' => $dialog
        ];

        return view('admin.support-answer', $data);
    }


    public function postAdminSupportAnswer(
        AdminSupportAnswerPostRequest $errors,
        Request $request,
        SupportDialog $dialog
    ) {
        $text = $request::get('text');
        $dialog_id = $request::get('dialog_id');
        $status = $request::get('status');

        $selectDialog = $dialog->where('id', $dialog_id);

        $dialog = $selectDialog->first();

        if (!$dialog->user_id) {

            Notifications::add(trans('notification.admin-support-error'), 'error');

            return redirect()->back();

        }

        SupportDialog::updateDialog($dialog_id, [
            'status' => $status ? 'closed' : 'agent',
            'agent_id' => $this->user->id,
            'watch' => 'no'
        ]);

        SupportDialogMessage::createNewMessage([
            'dialog_id' => $dialog_id,
            'author_id' => $this->user->id,
            'text' => e($text)
        ]);

        MailerClass::newSupportResponse(User::find($dialog->user_id)->email);

        Notification::createNotification(config('notification.support-answer'), $dialog->user_id, null);

        Notifications::add(trans('notification.admin-support-success'), 'success');

        return redirect()->route('admin-support');
    }


    public static function avatarAndAuthorForHuman($dialog)
    {
        foreach ($dialog->messages as $message) {

            if (!$message->author_id) {
                $avatar = '/' . config('image.UserAvatarsDefaultPathMini') . 'guest-1.png';
                $author = trans('support.author.guest');
            } elseif ($message->author_id == $dialog->user_id) {
                $avatar = userAvatarOrDefaultById($dialog->user_id, 1);
                $author = trans('support.author.user');
            } else {
                $avatar = '/' . config('image.UserAvatarsDefaultPathMini') . 'support-1.png';
                $author = trans('support.author.agent');
            }

            array_add($message, 'avatar', $avatar);
            array_add($message, 'author', $author);
        }

        return $dialog;
    }


    public function getNotificationPage(Notification $notification)
    {
        $templates = config('notification.for-all');

        $notifications = $notification->getActiveGeneralNotifications();

        $data = [
            'title' => trans('title.admin-notification'),
            'templates' => $templates,
            'notifications' => $notifications
        ];

        return view('admin.notification', $data);
    }


    public function createNewNotificationPost(AdminNewNotificationPostRequest $errors, Request $request)
    {
        $notification = config('notification.for-all.' . $request::get('notification') . '');

        Notification::createNotification($notification, null, $request::get('days'));

        Notifications::add(trans('notification.corner.admin-notification-create-success'), 'success');

        return redirect()->back();
    }


    public function deleteGeneralNotificationAjax(Request $request, Notification $notification)
    {
        $notification = $notification->where('id', $request::get('notification_id'))
            ->whereNull('user_id');

        if ($notification->first()) {

            $notification->delete();

            return messageToAlertCorner(null, trans('notification.corner.admin-notification-delete-success'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner(null, trans('notification.corner.admin-notification-delete-error'),
                config('notification.color.error'));

        }
    }


    public function makeGiftAction()
    {
        $goods = trans('shop.goods');

        $data = [
            'title' => trans('title.admin-gift'),
            'goods' => $goods
        ];

        return view('admin.gift', $data);
    }


    public function giveGeneralGiftPost(AdminGiftGeneralPostRequest $errors, Request $request, Gift $gift)
    {
        $gift->create([
            'creator_id' => $this->user->id,
            'good_name' => $request::get('good_name'),
            'valid_to' => Date::now()->add('' . $request::get('days') . ' Days'),
            'text' => $request::get('text')
        ]);

        Notification::createNotification(config('notification.for-all.gift-take'), null, $request::get('days'));

        Notifications::add(trans('notification.admin-gift-create-success'), 'success');

        return redirect()->back();
    }


    public function findUserSelect2Ajax(Request $request, User $user)
    {
        if ($request::has('myQuery')) {

            $query = $request::get('myQuery');

            if (is_numeric($query)) {

                $users = $user->where('id', 'LIKE', $query . '%')
                    ->select('id', 'email as text')
                    ->get();

            } else {

                $users = $user->where('email', 'LIKE', $query . '%')
                    ->select('id', 'email as text')
                    ->get();

            }

            return Response::json($users);
        }
    }


    public function givePersonalGiftPost(
        AdminGiftPersonalPostRequest $errors,
        Request $request,
        UserGood $userGood,
        UserTransaction $transaction
    ) {
        $goodName = $request::get('good_name');
        $sum = $request::get('sum');
        $user_id = $request::get('user');

        $userGood->addGoods($user_id, $goodName, $sum);

        $transaction->addTransaction($user_id, null, $goodName, $sum, null, 'admin-gift');

        Notification::createNotification(config('notification.admin-personal-gift'), $user_id, null);

        Notifications::add(trans('notification.admin-gift-create-success'), 'success');

        return redirect()->back();
    }


    public function getAdsValidate(AdsOffer $adsOffer, AdDeleteTransaction $deleteTransaction)
    {
        $ads = $adsOffer->whereIn('status', config('ad.verify-status'))
            ->with([
                'privileges',
                'verifyComments' => function ($query) {
                    $query->orderBy('created_at', 'desc');
                },
                'image' => function ($query) {
                    $query->where('title', 1);
                }
            ])->orderBy('updated_at', 'asc')->paginate(10);

        foreach ($ads as $ad) {

            $ad->privileges = $ad->privileges ? true : false;

        }

        $deleteTransactions = $deleteTransaction->paginate(10);

        $data = [
            'title' => trans('title.admin-ads-validate'),
            'ads' => $ads,
            'deleteTransactions' => $deleteTransactions
        ];

        return view('admin.ads', $data);
    }


    public function deleteAdAjax(
        Request $request,
        AdsOffer $adsOffer,
        AdDeleteTransaction $deleteTransaction
    ) {
        if (in_array($this->user->group_id, config('app.userGroup.admin'), true) && $request::has('comment')) {

            $adId = $request::get('ad_id');
            $comment = $request::get('comment');

            $adsOffer->AdDeleteAndAllRecords($adId);

            $deleteTransaction->create(['ad_id' => $adId, 'agent_id' => $this->user->id, 'comment' => $comment]);

            Notifications::add(trans('notification.admin-ad-delete-success'), 'success');

            return $adId;

        }

        return 'ACCESS ERROR';
    }


    public function getShop(
        ShopService $shopService,
        ShopPackage $shopPackage,
        UserTransaction $transaction,
        ShopColor $shopColor
    ) {
        $transactions = $transaction->orderBy('created_at', 'desc')
            ->paginate('20');

        $data = [
            'title' => trans('title.admin-shop'),
            'shopPackages' => $shopPackage->get(),
            'shopServices' => $shopService->get(),
            'transactions' => $transactions,
            'colors' => $shopColor->get()
        ];

        return view('admin.shop', $data);
    }


    public function shopUpdateService(Request $request, ShopService $shopService)
    {
        if (!is_numeric($request::get('price'))) {

            return messageToAlertCorner('ERROR', trans('notification.corner.admin-shop-update-error'),
                config('notification.color.error'));

        }

        $update = $shopService->where('name', $request::get('service_name'))
            ->update(['price' => $request::get('price')]);

        if ($update) {

            return messageToAlertCorner('OK', trans('notification.corner.admin-shop-update-success'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner('ERROR', trans('notification.corner.admin-shop-update-error'),
                config('notification.color.error'));

        };
    }


    public function shopUpdatePackage($name, Request $request, ShopPackage $shopPackage)
    {
        $update = $shopPackage->where('name', $name)
            ->update([
                'ad_limit' => $request::get('ad_limit'),
                'color' => $request::get('color'),
                'auto_up' => $request::get('auto_up'),
                'top' => $request::get('top'),
                'vip' => $request::get('vip'),
                'price' => $request::get('price'),
                'discount' => $request::get('discount')
            ]);

        if ($update) {

            return messageToAlertCorner('OK', trans('notification.corner.admin-shop-update-success'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner('ERROR', trans('notification.corner.admin-shop-update-error'),
                config('notification.color.error'));

        };
    }


    public function updateColorsAjax(Request $request, ShopColor $shopColor)
    {
        for($i = 1; $i < 7; $i++){

            $shopColor->where('name', 'color_' . $i)
                ->update(['color' => $request::get('color_' . $i)]);

        }

        return messageToAlertCorner('OK', trans('notification.corner.admin-shop-update-success'),
            config('notification.color.success'));
    }

}