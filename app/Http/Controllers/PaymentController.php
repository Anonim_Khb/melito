<?php namespace App\Http\Controllers;

use App\Http\Requests\PaymentSumPostRequest;
use App\Models\UserPayment;
use App\Models\UserProfile;
use App\Models\UserTransaction;
use Validator;
use Request;
use Illuminate\Support\Facades\Auth;
use Notifications;

class PaymentController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function sendToRobokassa(PaymentSumPostRequest $errors, Request $request, UserPayment $payment)
    {
        $userSum = $request::get('sum');

        $newPayment = $payment->create([
            'user_id' => $this->user->id,
            'sum' => $userSum
        ]);

        $mrh_login = env('ROBOKASSA_LOGIN');
        $mrh_pass1 = env('ROBOKASSA_PASS_1');
        $inv_id = $newPayment->id;
        $inv_desc = trans('shop.robokassa.inv_desc');
        $culture = userLocale();
        $encoding = "utf-8";
        $out_summ = $userSum;
        $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");

        $payment->where('id', $newPayment->id)->update([
            'signature' => $crc,
            'hash' => md5("$crc:$userSum")
        ]);

        return redirect()->away('https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=' . $mrh_login . '&Description=' . $inv_desc . '&OutSum=' . $out_summ . '&InvoiceID=' . $inv_id . '&Signature=' . $crc . '&Encoding=' . $encoding . '&Culture=' . $culture . '&IsTest=' . env('IS_TEST') . '');
    }


    public function resultRobokassa(Request $request, UserPayment $payment, UserProfile $profile)
    {
        $SignatureValue = $request::get('SignatureValue');
        $OutSum = $request::get('OutSum');
        $InvId = $request::get('InvId');

        $result = $payment->where('id', $InvId)
            ->where('signature', $SignatureValue)
            ->first();

        $hash = md5("$SignatureValue:$result->sum");

        if ($result && $OutSum == $result->sum && $result->hash == $hash) {

            $userProfile = $profile->where('user_id', $result->user_id)->first();

            $profile->where('id', $userProfile->id)->update([

                'balance' => $userProfile->balance + $result->sum

            ]);

            $payment->where('id', $InvId)->update([

                'status' => 'done'

            ]);

            UserTransaction::addTransaction($result->user_id, null, null, null, $result->sum, 'put');

            return 'OK' . $request::get('InvId');

        }

    }


    public function successRobokassa()
    {
        $data = [
            'title' => trans('title.payment-success')
        ];

        return view('payment.success', $data);
    }


    public function failRobokassa()
    {
        $data = [
            'title' => trans('title.payment-fail')
        ];

        return view('payment.fail', $data);
    }


}