<?php namespace App\Http\Controllers;

use App\Http\MyClass\MailerClass;
use App\Http\Requests\accountAvatarUploadAjaxRequest;
use App\Http\Requests\AccountPersonal;
use App\Http\Requests\DialogMessagesGetRequest;
use App\Models\AdBookmark;
use App\Models\AdsOffer;
use App\Models\AdsPrivilege;
use App\Models\EmailMessage;
use App\Models\Notification;
use App\Models\ShopColor;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use App\Models\UserTransaction;
use Illuminate\Support\Facades\Input;
use Validator;
use App\User;
use App\Http\MyClass\HelperClass;
use App\Models\UserProfile;
use App\Http\Requests\sendMessageFromAccount;
use Request;
use Illuminate\Support\Facades\Auth;
use Notifications;
use \Jenssegers\Date\Date;

class AccountController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function getMyAccount()
    {
        $data = [
            'title' => trans('title.user-profile') . ' | ' . $this->user->id,
            'user' => $this->user,
            'profile' => $this->user->profile,
            'menuActive' => 'personal',
            'minBirthdayDate' => Date::now()->sub('18 Years')->format('Y-m-d'),
        ];

        return view('account.account-personal', $data);
    }


    public function postMyAccount(AccountPersonal $errors, Request $request)
    {
        $data = HelperClass::changeNotCheckedFieldOnOurValue($request::all(), ['whatsapp', 'messenger', 'viber']);

        UserProfile::updateProfile($this->user->id, $data);

        Notifications::add(trans('notification.account-personal-update'), 'success');

        return redirect()->route('account-personal');
    }


    public function userProfile($id, User $user)
    {
        if ($this->user && $id == $this->user->id) {
            return redirect()->route('account-personal');
        }

        $user = $user->where('id', $id)
            ->with('profile')->first();

        $data = [
            'title' => trans('title.user-profile') . ' | ' . $id,
            'user' => $user,
            'menuActive' => 'personal',
            'notExists' => trans('account.another.not-exists'),
            'exists' => trans('account.another.exists')
        ];

        return $user ? view('another-user.user-profile', $data) : abort(404);
    }


    public function userAds($id, AdsOffer $adsOffer)
    {
        $user = User::find($id);

        if (!$user) {

            abort(404);

        }

        if ($this->user && $id == $this->user->id) {
            return redirect()->route('account-personal');
        }

        $adsOffers = $adsOffer->where('user_id', $id)
            ->with([
                'image' => function ($query) {
                    $query->where('title', 1);
                }
            ], 'cityName', 'bookmark')
            ->orderBy('updated_at', 'desc')
            ->get();

        $data = [
            'title' => trans('title.user-profile') . ' | ' . $id,
            'adsOffers' => $adsOffers,
            'menuActive' => 'ads'
        ];

        return view('another-user.user-ads', $data);
    }


    public function userSendEmail(
        $ad_id,
        sendMessageFromAccount $errors,
        Request $request,
        AdsOffer $adsOffer,
        EmailMessage $emailMessage
    ) {

        $ad = $adsOffer->where('id', $ad_id)->with([
            'author' => function ($query) {
                $query->select('id', 'email');
            }
        ])->select('user_id')->first();

        if ($this->user->id == $ad->user_id) {

            Notifications::add(trans('notification.message-send-himself'), 'error');

            return redirect()->back()->withInput();

        }

        $exist = $emailMessage->where('user_id', $this->user->id)
            ->where('ad_id', $ad_id)->first();

        if ($exist) {

            Notifications::add(trans('notification.message-already-exist'), 'error');

            return redirect()->back()->withInput();

        }

        $adresat = $ad->author->email;

        $avatar_path = userAvatarOrDefaultById($this->user->id, true);

        MailerClass::userSendMessage(e($request::get('text_msg')), $this->user->id, $adresat, $avatar_path, $ad_id);

        $emailMessage->create([
            'user_id' => $this->user->id,
            'ad_id' => $ad_id
        ]);

        Notification::createNotification(config('notification.send-email-for-user-ad'), $ad->user_id, null);

        Notifications::add(trans('notification.message-send'), 'success');

        return redirect()->back();
    }


    public function getMyAds(AdsOffer $adsOffers, ShopColor $colors)
    {
        $adsOffers = $adsOffers->where('user_id', $this->user->id)
            ->with([
                'image' => function ($query) {
                    $query->where('title', 1);
                }
            ], 'cityName', 'author.profile')
            ->orderBy('updated_at', 'desc')
            ->paginate(12);

        $data = [
            'title' => trans('title.user-profile') . ' | ' . $this->user->id,
            'adsOffers' => $adsOffers,
            'menuActive' => 'ads',
            'colors' => $colors->get(),
        ];

        return view('account.account-ads', $data);
    }


    public function getMyDonate(UserProfile $profile)
    {
        $donate = $profile->where('user_id', $this->user->id)
            ->with('goods')
            ->first();

        $privileges = AdsPrivilege::where('user_id', $this->user->id)
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        $transactions = UserTransaction::where('user_id', $this->user->id)
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        $data = [
            'title' => trans('title.user-profile') . ' | ' . $this->user->id,
            'donate' => $donate,
            'transactions' => $transactions,
            'privileges' => $privileges,
            'dateNow' => Date::now(),
            'menuActive' => 'donate',
        ];

        return view('account.account-donate', $data);
    }


    public function getMyBookmarks(AdBookmark $bookmark)
    {
        $bookmarks = $bookmark->where('user_id', $this->user->id)
            ->with([
                'myAds.image' => function ($query) {
                    $query->where('title', 1);
                },
                'myAds.cityName'
            ])
            ->orderBy('updated_at', 'desc')
            ->paginate(12);

        $data = [
            'title' => trans('title.user-profile') . ' | ' . $this->user->id,
            'bookmarks' => $bookmarks,
            'menuActive' => 'bookmarks',
        ];

        return view('account.account-bookmarks', $data);
    }


    public function getMySupport(DialogMessagesGetRequest $errors, SupportDialog $dialogs, Request $request)
    {
        if ($request::has('dialog')) {

            $selectDialog = $dialogs->where('id', $request::get('dialog'));

            $dialog = $selectDialog->with('messages')->first();

            $dialog->watch == 'no' ? $selectDialog->update(['watch' => null]) : '';

            $data = [
                'title' => trans('title.user-profile') . ' | ' . $this->user->id,
                'dialog' => $dialog,
                'userId' => $this->user->id,
                'menuActive' => 'help',
            ];

            return view('account.account-support-dialog', $data);

        } else {

            $dialogs = $dialogs->where('user_id', $this->user->id)
                ->with('message')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);;

            $data = [
                'title' => trans('title.user-profile') . ' | ' . $this->user->id,
                'dialogs' => $dialogs,
                'userId' => $this->user->id,
                'menuActive' => 'help',
            ];

            return view('account.account-support', $data);

        }
    }


    public function mySupportSendMessage(DialogMessagesGetRequest $errors, Request $request, SupportDialog $dialog)
    {
        $dialog_id = $request::get('dialog');
        $text = $request::get('text');

        $selectDialog = $dialog->where('id', $dialog_id)->first();

        if (!$selectDialog || $selectDialog->user_id != $this->user->id || $selectDialog->status == 'closed') {

            Notifications::add(trans('notification.support-access-error'), 'error');

            return redirect()->back();

        }

        SupportDialog::updateDialog($dialog_id, [
            'status' => 'wait'
        ]);

        SupportDialogMessage::createNewMessage([
            'dialog_id' => $dialog_id,
            'author_id' => $this->user->id,
            'text' => e($text)
        ]);

        Notifications::add(trans('notification.send-support-message-success'), 'success');

        return redirect()->route('account-my-support');
    }


    public function mySupportDialogClose(DialogMessagesGetRequest $errors, Request $request, SupportDialog $dialog)
    {
        $dialog_id = $request::get('dialog');

        $selectDialog = $dialog->where('id', $dialog_id)->first();

        if (!$selectDialog || $selectDialog->user_id != $this->user->id || $selectDialog->status == 'closed') {

            Notifications::add(trans('notification.support-access-error'), 'error');

            return redirect()->back();

        }

        SupportDialog::updateDialog($dialog_id, [
            'status' => 'closed'
        ]);

        Notifications::add(trans('notification.close-support-dialog-success'), 'success');

        return redirect()->route('account-my-support');
    }


    public function getMySettings()
    {
        $data = [
            'title' => trans('title.user-profile') . ' | ' . $this->user->id,
            'menuActive' => 'settings',
        ];

        return view('account.account-settings', $data);
    }


}