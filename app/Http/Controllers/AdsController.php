<?php namespace App\Http\Controllers;

use App\Http\MyClass\HelperClass;
use App\Http\MyClass\LocationClass;
use App\Http\MyClass\MailerClass;
use App\Http\MyClass\MyArrays;
use App\Http\Requests\AdsNeedRoom;
use App\Http\Requests\AdsOfferRoom;
use App\Http\Requests\AdsSearchGetRequest;
use App\Models\AdBookmark;
use App\Models\AdsOffer;
use App\Models\AdsOfferImage;
use App\Models\AdsOfferTmpImage;
use App\Models\AdsPrivilege;
use App\Models\Notification;
use App\Models\SxgeoCity;
use App\Models\UserGood;
use App\Models\UserTransaction;
use App\Models\VerifyAd;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Events\RegisteredAndAuth;
use Illuminate\Support\Facades\Event;
use Request;
use Jenssegers\Date\Date;
use Response;
use Notifications;

class AdsController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();

        $this->middleware('auth-easy',
            ['only' => ['adHideOrShowStatusAjax', 'adUpAjax', 'postAdsEdit', 'adEdit', 'adCoverImagePathById']]);

        $this->middleware('ads-limit', ['only' => ['adsNewOffer', 'postAdsNewOffer', 'adHideOrShowStatusAjax']]);
    }


    public function adsNewOffer(Request $request)
    {
        $fData = MyArrays::adsOffer();

        $cityForSearch = $request::has('city') ? $request::get('city') : LocationClass::getUserLocationCityId();

        $userCity = SxgeoCity::getUserLocationCityRegionCountry($cityForSearch);

        $data = [
            'fData' => $fData,
            'hash' => randomString() . time(),
            'userCity' => $userCity,
            'title' => trans('title.new-offer'),
        ];

        return view('ads.new-offer', $data);
    }


    public function postAdsNewOffer(
        Request $request,
        AdsOfferRoom $errors,
        AdsOfferImage $adsOfferImage,
        AdsOfferTmpImage $adsOfferTmpImage
    ) {
        if (!Auth::check()) {
            $dataRegister = [
                'email' => $request::get('email'),
                'password' => $request::get('password')
            ];

            $user = User::registrationSimple($dataRegister, randomString());

            Event::fire(new RegisteredAndAuth($user));

            $this->user = $user;
        }

        $data = HelperClass::changeNotCheckedFieldOnOurValue($request::all(), [
            'wifi',
            'tv',
            'parking',
            'balcony',
            'garden',
            'security',
            'elevator',
            'jacuzzi',
            'boiler',
            'gim',
            'furniture',
            'appliances',
            'children',
            'couples',
            'guests',
            'pets',
            'smoking',
            'add_pay',
        ]);

        $status = $this->user->status == 'active' ? 'wait' : 'not-confirm';

        $ad_id = AdsOffer::createAdOffer($this->user->id, $data, $status);

        $images = $adsOfferTmpImage->where('hash', $request::get('hash'))->get();

        if ($images) {

            foreach ($images as $image) {

                $adsOfferImage->insert([
                    ['ad_id' => $ad_id, 'filename' => $image->filename,]
                ]);

            }

            $adsOfferTmpImage->where('hash', $request::get('hash'))->delete();

            if ($request::has('imagesTitle') && $request::get('imagesTitle') != '') {

                AdsOfferImage::setAdImageTitle($ad_id, $request::get('imagesTitle'));

            }

        }

        return $ad_id;
    }


    public function newNeedRoom(AdsNeedRoom $errors, Request $request, AdsOffer $adsOffers, AdsPrivilege $privilege)
    {
        $fData = MyArrays::adsNeed();

        HelperClass::adsMaxSumAreaCache();

        $cityForSearch = $request::has('city') ? $request::get('city') : LocationClass::getUserLocationCityId();

        $userCity = SxgeoCity::getUserLocationCityRegionCountry($cityForSearch);

        $adsPrivilegesTop = $privilege::getRandomPrivilegesAds($cityForSearch, 'top',
            $this->user ? $this->user->id : null, 3);

        $adsPrivilegesVip = $privilege::getRandomPrivilegesAds($cityForSearch, 'vip',
            $this->user ? $this->user->id : null, 3);

        $fieldsToCheck = [
            'deal_type',
            'house_type',
            'communal',
            'sanitary_ware',
            'wifi',
            'tv',
            'parking',
            'balcony',
            'garden',
            'security',
            'elevator',
            'jacuzzi',
            'boiler',
            'gim',
            'furniture',
            'appliances',
            'add_pay',
            'gender',
            'occupation',
            'children',
            'couples',
            'guests',
            'pets',
            'smoking'
        ];

        $adsOffers = $adsOffers->where('city', $cityForSearch)
            ->whereIn('status', config('ad.open-status'))
            ->with([
                'image' => function ($query) {
                    $query->where('title', 1);
                },
                'privileges' => function ($query) {
                    $query->where('color', '>', Date::now());
                },
            ]);

        foreach ($fieldsToCheck as $field) {
            if ($request::has($field)) {
                $adsOffers = $adsOffers->where($field, $request::get($field));
            }
        }

        $fieldsToGroupCheck = [
            ['price_from', 'price_to', 'rent_price'],
            ['rm_from', 'rm_to', 'room_meters']
        ];

        foreach ($fieldsToGroupCheck as $groups) {
            if ($request::has($groups[0]) && $request::has($groups[1])) {
                $adsOffers->whereBetween($groups[2], [$request::get($groups[0]), $request::get($groups[1])]);
            } elseif ($request::has($groups[0])) {
                $adsOffers->where($groups[2], '>=', $request::get($groups[0]));
            } elseif ($request::has($groups[1])) {
                $adsOffers->where($groups[2], '<=', $request::get($groups[1]));
            }
        }

        if ($request::has('prepayment')) {
            $adsOffers->where('prepayment', 0);
        }

        if ($request::has('rooms')) {
            $adsOffers->where('rooms', '>=', $request::get('rooms'));
        }

        if ($request::has('entry')) {
            $adsOffers->where('available_from', '<=', $request::get('entry'));
        }

        if ($request::has('age')) {
            $adsOffers = $adsOffers->where('age_from', '<=', $request::get('age'))
                ->where('age_to', '>=', $request::get('age'));
        }

        if ($request::has('wIm')) {
            $adsOffers = $adsOffers->has('image');
        }

        if ($this->user) {
            $adsOffers = $adsOffers->with([
                'bookmark' => function ($query) {
                    $query->where('user_id', $this->user->id);
                }
            ]);
        }

        $adsOffers = $adsOffers->orderBy('updated_at', 'desc')
            ->paginate(10);

        $data = [
            'fData' => $fData,
            'userCity' => $userCity,
            'adsOffers' => $adsOffers,
            'adsPrivilegesTop' => $adsPrivilegesTop,
            'adsPrivilegesVip' => $adsPrivilegesVip,
            'title' => trans('title.new-need')
        ];

        return view('ads.new-need', $data);
    }


    public function getCitiesForAds(Request $request)
    {
        if ($request::get('myQuery') == '') {

            $cities = MyArrays::popularCities();

            $popularCities = SxgeoCity::getCityRegionCountryById($cities, userLocale());

            return Response::json($popularCities);

        } else {

            $result = SxgeoCity::searchCities($request::get('myQuery'), userLocale());

            $result = SxgeoCity::getCityRegionCountryById($result, userLocale());

            return Response::json($result);
        }
    }


    public function getAdOfferWatch($offer_id, AdsOffer $adsOffers)
    {
        $adsOffers->timestamps = false;

        $adsOffers->where('id', $offer_id)->increment('count');

        $adsOffers = $adsOffers->with('images', 'cityName', 'author.profile')
            ->where('id', $offer_id)
            ->first();

        if (!$adsOffers) {
            return abort(404);
        }

        if (!empty($adsOffers->author->profile->birthday)) {
            $adsOffers = array_add($adsOffers, 'age', $userAge = getUserAge($adsOffers->author->profile->birthday));
        }

        $adsOffers = array_add($adsOffers, 'fData', MyArrays::adsOffer());

        $data = [
            'title' => trans('title.offer-open-watch') . ' | ' . $offer_id,
            'adsOffers' => $adsOffers,
            'user' => $this->user
        ];

        return view('ads.ad-watch', $data);
    }


    public function adHideOrShowStatusAjax(Request $request, AdsOffer $adsOffer)
    {
        $adsOffer = $adsOffer->where('id', $request::get('id'))->first();

        $adsOffer->timestamps = false;

        if ($adsOffer->status == 'active') {

            $adsOffer->update(['status' => 'closed']);

            return messageToAlertCorner('closed', trans('notification.corner.hide'),
                config('notification.color.warning'));

        } elseif ($adsOffer->status == 'closed') {

            $adsOffer->update(['status' => 'active']);

            return messageToAlertCorner('active', trans('notification.corner.show'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner(null, trans('notification.corner.ad-not-yet-verify'),
                config('notification.color.warning'));

        }
    }


    public function adUpFreeAjax(Request $request, AdsOffer $adsOffer)
    {
        $adsOffer = $adsOffer->where('id', $request::get('id'))->first();

        if (in_array($adsOffer->status, config('ad.open-status'), true)) {

            $checkLastUpdate = $adsOffer->updated_at >= Date::now()->subHours(24) ? false : true;

            if (!$checkLastUpdate) {

                return messageToAlertCorner(null, trans('notification.corner.early'), config('notification.color.warning'));

            } else {

                $adsOffer->touch();

                return messageToAlertCorner(null, trans('notification.corner.success'), config('notification.color.success'));

            }

        } else {

            return messageToAlertCorner(null, trans('notification.corner.error'), config('notification.color.error'));

        }
    }


    public function adUpPremiumAjax(Request $request, AdsOffer $adsOffer, UserGood $userGood)
    {
        $adsOffer = $adsOffer->where('id', $request::get('id'))->first();

        if (in_array($adsOffer->status, config('ad.open-status'), true)) {

            $autoUp = $userGood->where('user_id', $this->user->id)->select('auto_up')->first();

            if ($autoUp && $autoUp->auto_up > 0) {

                $userGood->where('user_id', $this->user->id)->update([

                    'auto_up' => $autoUp->auto_up - 1

                ]);

                UserTransaction::addTransaction($this->user->id, $request::get('id'), 'auto_up', 1, null, 'spent');

                $adsOffer->touch();

                return messageToAlertCorner(null, trans('notification.corner.success'), config('notification.color.success'));

            } else {

                return messageToAlertCorner(null, trans('notification.corner.premium-error'),
                    config('notification.color.warning'));

            }

        } else {

            return messageToAlertCorner(null, trans('notification.corner.error'), config('notification.color.error'));

        }
    }


    public function adEdit($id, AdsOffer $adsOffer)
    {
        $adsOffer = $adsOffer->where('id', $id)
            ->with([
                'images',
                'cityName' => function ($query) {
                    $query->select('id', 'name_' . userLocale() . ' as name');
                }
            ])->first();

        if (!$adsOffer || ($adsOffer->user_id != $this->user->id && !in_array($this->user->group_id,
                    config('app.userGroup.redactor'), true))
        ) {

            return abort(401);

        } elseif (in_array($adsOffer->status, config('ad.edit-forbidden'), true)) {

            Notifications::add(trans('notification.ad-edit-forbidden'), 'error');

            return redirect()->back();

        } elseif ($adsOffer) {

            $fData = MyArrays::adsOffer();

            $data = [
                'hash' => randomString() . time(),
                'adsOffer' => $adsOffer,
                'fData' => $fData,
                'title' => trans('title.ad-edit') . ' | ' . $id
            ];

            return view('ads.ad-edit', $data);
        }
    }


    public function adsEditAjax(
        AdsOfferRoom $errors,
        Request $request,
        AdsOffer $adsOffer,
        AdsOfferImage $adsOfferImage,
        AdsOfferTmpImage $adsOfferTmpImage,
        VerifyAd $verifyAd
    ) {
        $adEdit = $adsOffer->where('id', $request::get('ad_id'))
            ->first();

        if (!$adEdit || $adEdit->user_id != $this->user->id && !in_array($this->user->group_id,
                config('app.userGroup.redactor'), true)
        ) {

            return [trans('notification.ad-edit-permission')];

        }

        $data = HelperClass::changeNotCheckedFieldOnOurValue($request::all(), [
            'wifi',
            'tv',
            'parking',
            'balcony',
            'garden',
            'security',
            'elevator',
            'jacuzzi',
            'boiler',
            'gim',
            'furniture',
            'appliances',
            'children',
            'couples',
            'guests',
            'pets',
            'smoking',
            'add_pay',
        ]);

        $adsOffer->timestamps = false;

        $status = $this->calculateAdEditStatus($adEdit->status,
            $request::has('admin_status') ? $request::get('admin_status') : null);

        $update = $adsOffer->updateAd($adEdit->id, $data, $status);

        if (!$update) {
            return [trans('notification.ad-edit-error')];
        }

        if (in_array($this->user->group_id, config('app.userGroup.redactor'), true)) {

            if ($request::has('admin_comment')) {

                $verifyAd->create([
                    'ad_id' => $adEdit->id,
                    'agent_id' => $this->user->id,
                    'comment' => e($request::get('admin_comment')),
                ]);

            }

            if ($status == 'for-correct') {

                Notification::createNotification(config('notification.ad-verify-error'), $adEdit->user_id, null);

                MailerClass::adVerifyCheckError(User::find($adEdit->user_id)->email, $adEdit->id);

            } elseif ($status == 'active') {

                Notification::createNotification(config('notification.ad-verify-success'), $adEdit->user_id, null);

                MailerClass::adVerifyCheckSuccess(User::find($adEdit->user_id)->email, $adEdit->id);

            }

        }

        $images = $adsOfferTmpImage->where('hash', $request::get('hash'))->get();

        if ($images) {

            foreach ($images as $image) {

                $adsOfferImage->insert([
                    ['ad_id' => $adEdit->id, 'filename' => $image->filename,]
                ]);

            }

            $adsOfferTmpImage->where('hash', $request::get('hash'))->delete();

            if ($request::has('imagesTitle') && $request::get('imagesTitle') != '') {

                $titles = $adsOfferImage->where('ad_id', $adEdit->id);

                $titles->update(['title' => 0]);

                $titles->where('filename', $request::get('imagesTitle'))
                    ->update(['title' => 1]);

            }

        }

        return $adEdit->id;
    }


    public function calculateAdEditStatus($oldStatus, $adminStatus)
    {
        if (in_array($this->user->group_id, config('app.userGroup.redactor'), true)) {

            $newStatus = $adminStatus != null ? $adminStatus : '';

        } else {

            if (in_array($oldStatus, config('ad.open-status'), true)) {
                $newStatus = 'wait';
            } elseif (in_array($oldStatus, config('ad.correct-status'), true)) {
                $newStatus = 'corrected';
            }

        }

        return $newStatus;
    }


    public function AddToBookmark(Request $request, AdBookmark $adBookmark, AdsOffer $adsOffer)
    {
        ;
        if (!$this->user) {
            return messageToAlertCorner(null, trans('notification.corner.only-auth'), config('notification.color.warning'));
        }

        $adsOffer = $adsOffer->where('id', $request::get('id'))
            ->select('user_id')
            ->first();

        if ($adsOffer && $adsOffer->user_id == $this->user->id) {

            return messageToAlertCorner('delete', trans('notification.corner.bookmark-ad-owner'),
                config('notification.color.warning'));

        }

        $result = $adBookmark->where('ad_id', $request::get('id'));

        if ($result->first()) {

            $result->delete();

            return messageToAlertCorner('delete', trans('notification.corner.bookmark-delete'),
                config('notification.color.warning'));

        } else {

            $adBookmark->create([
                'user_id' => $this->user->id,
                'ad_id' => $request::get('id')
            ]);

            return messageToAlertCorner('added', trans('notification.corner.bookmark-added'),
                config('notification.color.success'));

        }
    }


    public function DeleteOneBookmark(Request $request, AdBookmark $adBookmark)
    {
        $userBookmark = $adBookmark->where('id', $request::get('id'));

        $check = $userBookmark->first();

        if (!$check || $check->user_id != $this->user->id) {

            return messageToAlertCorner('delete', trans('notification.corner.error'),
                config('notification.color.error'));

        }

        $userBookmark->delete();

        return messageToAlertCorner('delete', trans('notification.corner.bookmark-delete'),
            config('notification.color.success'));

    }


    public function DeleteAllBookmarks(AdBookmark $adBookmark)
    {
        $bookmarks = $adBookmark->where('user_id', $this->user->id)->delete();

        if ($bookmarks || $bookmarks > 0) {

            return messageToAlertCorner('delete', trans('notification.corner.bookmark-all-delete'),
                config('notification.color.success'));

        } else {

            return messageToAlertCorner('delete', trans('notification.corner.bookmark-all-delete-early'),
                config('notification.color.warning'));

        }
    }


    public function adCoverImageNameById(Request $request, AdsOffer $adsOffer)
    {
        if ($request::has('adId') && is_numeric($request::get('adId'))) {

            $cover = $adsOffer->where('id', $request::get('adId'))
                ->where('user_id', $this->user->id)
                ->with([
                    'image' => function ($query) {
                        $query->where('title', 1);
                    }
                ])->first();

            if ($cover && $cover->image != null) {

                return messageToAlertCorner('ok', $cover->image->filename, null);

            } else {

                return messageToAlertCorner('error', trans('notification.corner.account-ad-cover-crop-absent'),
                    config('notification.color.warning'));

            }
        }
    }


}