<?php namespace App\Http\Controllers;

use App\Http\MyClass\HelperClass;
use App\Http\MyClass\LocationClass;
use App\Http\MyClass\MyArrays;
use App\Models\AdsOffer;
use App\Models\AdsPrivilege;
use App\Models\Notification;
use Request;
use App\Models\SxgeoCity;
use Response;
use Illuminate\Support\Facades\Auth;
use Cookie;

class PagesController extends Controller
{

    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function index(AdsPrivilege $privilege, AdsOffer $adsOffer)
    {
        $city = LocationClass::getUserLocationCityId();

        $userCity = SxgeoCity::getUserLocationCityRegionCountry($city);

        HelperClass::adsMaxSumAreaCache();

        $adsPrivilegesVip = $privilege::getRandomPrivilegesAds($city, 'vip', null, 5);

        $ads = $adsOffer->where('city', $city)
            ->with([
                'image' => function ($query) {
                    $query->where('title', 1);
                }
            ])
            ->whereIn('status', config('ad.open-status'))
            ->orderBy('updated_at', 'desc')
            ->take(10)
            ->get();

        $data = [
            'title' => config('app.site_name'),
            'carousels' => trans('carousel.home'),
            'userCity' => $userCity,
            'fData' => MyArrays::adsNeed(),
            'performances' => MyArrays::indexPerformances(),
            'ads' => $ads,
            'adsVips' => $adsPrivilegesVip
        ];

        return view('home.index', $data);
    }


    public function hideNotification(Request $request, Notification $notification)
    {
        $select = $notification->where('id', $request::get('notification_id'));

        $notification = $select->first();

        if ($notification && $notification->user_id == null) {

            Cookie::queue($notification->id, true, 2880);

        } elseif ($notification && $notification->user_id == $this->user->id) {

            $select->delete();

        } else {

            return null;

        }

        return 'OK';
    }


    public function hideAllNotifications(Notification $notification)
    {
        $notifications = $notification->getUserNotifications();

        foreach ($notifications as $key => $value) {

            if ($this->user && $value->user_id == $this->user->id) {

                $notificationsIds[] = $value->id;

            }

            if ($value->user_id == null) {

                Cookie::queue($value->id, true, 2880);

            }

        }

        isset($notificationsIds) ? $notification->whereIn('id', $notificationsIds)->delete() : '';

        return 'OK';
    }

}