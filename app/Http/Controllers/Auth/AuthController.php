<?php

namespace App\Http\Controllers\Auth;

use App\Http\MyClass\HelperClass;
use App\Http\MyClass\MailerClass;
use App\Http\Requests\CheckCaptha;
use App\Http\Requests\LoginUser;
use App\Http\Requests\RegisterUser;
use App\Models\AdsOffer;
use App\Models\Notification;
use App\Models\UserProfile;
use App\Models\UserSetting;
use Notifications;
use Illuminate\Support\Facades\Event;
use Validator;
use App\Http\Controllers\Controller;
use Jenssegers\Date\Date;
use App\User;
use App\Events\RegisteredAndAuth;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{

    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();

        $this->middleware('auth-easy', [
            'only' => [
                'getRegisterNotConfirm',
                'postRegisterNotConfirm',
                'registerComplete',
                'registerNotConfirm',
                'logout'
            ]
        ]);

        $this->middleware('auth-pending',
            ['only' => ['registerConfirm', 'getRegisterNotConfirm', 'postRegisterNotConfirm']]);
    }


    public function getLogin()
    {
        $data = [
            'title' => trans('title.login')
        ];

        return view('auth.login', $data);
    }


    public function postLogin(LoginUser $errors, Request $request)
    {
        $data = $request::all();

        $remember = isset($data['remember']) ? true : false;

        $user = User::login($data, $remember);

        if (!$user) {

            HelperClass::wrongLoginSession();

            Notifications::add(trans('notification.login-error'), 'danger');

            return redirect()->back()->withInput();

        } else {

            Session::forget('wrong_login');

            Notifications::add(trans('notification.login-success'), 'success');

            return redirectToPreviousURLorDefault();

        }
    }


    public function getRegister()
    {
        $data = [
            'title' => trans('title.register')
        ];

        return view('auth.register', $data);
    }


    public function postRegister(RegisterUser $errors, Request $request)
    {
        $data = $request::all();

        $user = User::registrationSimple($data, randomString());

        if (!$user instanceof Model) {

            Event::fire(new RegisteredAndAuth($user));

            Notification::createNotification(config('notification.send-email-for-register-confirm'), $user->id, null);

            return redirect()->route('registration-complete');

        } else {

            Notifications::add(trans('notification.register-error'), 'error');

            return redirect()->back()->withInput();

        }
    }


    public function registerConfirm($token, User $user, AdsOffer $adsOffer)
    {
        $user = $user->where('hash', $token)->first();

        $user->update(['status' => 'active', 'hash' => null]);

        $adsOffer->where('user_id', $user->id)
            ->where('status', 'not-confirm')->update(['status' => 'wait']);

        UserProfile::createNew($user->id);
        UserSetting::createNew($user->id);

        if (!$this->user) {

            Auth::loginUsingId($user->id);

        }

        Notifications::add(trans('notification.register-confirm'), 'success');

        return redirect()->route('home');
    }


    public function registerComplete()
    {
        $data = [
            'title' => trans('title.register-complete')
        ];

        return view('auth.register-complete', $data);
    }


    public function getRegisterNotConfirm()
    {
        $data = [
            'title' => trans('title.register-not-confirm')
        ];

        return view('auth.register-not-confirm', $data);
    }


    public function postRegisterNotConfirm(CheckCaptha $errors)
    {
        $checkLastHelp = $this->user->confirm_register >= Date::now()->subHours(24) ? false : true;

        if (!$checkLastHelp) {
            Notifications::add(trans('notification.confirm-email-Not-send'), 'danger');

            return redirect()->back();
        }

        $this->user->update([
            'confirm_register' => Date::now(),
            'hash' => randomString()
        ]);

        MailerClass::welcome($this->user);

        Notifications::add(trans('notification.confirm-email-send'), 'success');

        return redirect()->route('home');
    }


    public function getLogout()
    {
        Auth::logout();

        return redirect()->route('home');
    }


}
