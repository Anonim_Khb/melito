<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\MyClass\MailerClass;
use App\Http\Requests\ChangePasswordPostRequest;
use App\Http\Requests\RemindPassword;
use App\Http\Requests\ResetPassword;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Jenssegers\Date\Date;
use Request;
use Notifications;

class PasswordController extends Controller
{


    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function getPasswordRemind()
    {
        $data = [
            'title' => trans('title.password-remind')
        ];

        return view('auth.password-remind', $data);
    }


    public function postPasswordRemind(RemindPassword $errors, Request $request)
    {
        $email = $request::get('email');

        $user = User::getUserByValue('email', $email);

        $checkLastHelp = $user->password_reset >= Date::now()->subHours(24) ? false : true;

        if (!$checkLastHelp) {

            Notifications::add(trans('notification.password-remind-email-error'), 'danger');

            return redirect()->back();

        }

        $user->update([
            'password_reset' => Date::now(),
            'hash' => randomString()
        ]);

        MailerClass::remindPassword($user);

        Notifications::add(trans('notification.password-remind-email-success'), 'success');

        return redirect()->route('home');
    }


    public function getResetPassword($token)
    {
        $user = User::getUserByValue('hash', $token);

        if (!$user) {
            return abort('404');
        }

        $timeCheck = $user->password_reset <= Date::now()->subHours(1) ? false : true;

        if (!$timeCheck) {
            return abort('404');
        }

        $data = [
            'title' => trans('title.password-remind'),
            'pr_token' => $token
        ];

        return view('auth.password-reset', $data);
    }


    public function postResetPassword(ResetPassword $errors, Request $request)
    {
        $token = $request::get('pr_token');

        $user = User::getUserByValue('hash', $token);

        User::updateField($user->id, 'hash', null);

        User::updateField($user->id, 'password', bcrypt($request::get('new_password')));

        Notifications::add(trans('notification.password-reset-success'), 'success');

        return redirect()->route('login');
    }


    public function postChangePassword(ChangePasswordPostRequest $errors, Request $request)
    {
        if (!Hash::check($request::get('old_password'), $this->user->password)) {

            Notifications::add(trans('notification.password-reset-wrong-old'), 'danger');

            return redirect()->back()->withInput();

        }

        $update = User::updateField($this->user->id, 'password', bcrypt($request::get('new_password_1')));

        if ($update) {

            Notifications::add(trans('notification.password-reset-success'), 'success');

            return redirect()->back();

        }
    }


}
