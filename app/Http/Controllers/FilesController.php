<?php namespace App\Http\Controllers;

use App\Http\MyClass\FilesClass;
use App\Http\Requests\avatarCropTempImageAjaxRequest;
use App\Models\AdsOffer;
use App\Models\AdsOfferImage;
use App\Models\AvatarTemp;
use App\Models\UserProfile;
use Validator;
use App\Models\AdsOfferTmpImage;
use Response;
use File;
use Request;
use Illuminate\Support\Facades\Auth;

class FilesController extends Controller
{

    protected $maxImages = 10;

    protected $user;


    public function __construct()
    {
        $this->user = Auth::user();

        $this->middleware('auth-easy', ['only' => ['adCoverImageCrop']]);

    }


    public function adsOfferImages(Request $request, AdsOfferTmpImage $adsOfferTmpImage)
    {
        $files = $request::file('files');

        $hash = $request::get('hash');

        $json = ['files' => []];

        $imagesNow = $adsOfferTmpImage->where('hash', $hash)->count();

        foreach ($files as $file) {

            if ($imagesNow < $this->maxImages) {

                $validator = Validator::make(
                    ['file' => $file],
                    ['file' => 'mimes:jpg,jpeg,png,gif,bmp|min:5|max:10240']
                );

                if ($validator->fails()) {
                    $json['files'][] = [
                        'name' => $file->getClientOriginalName(),
                        'status' => 'text-danger'
                    ];
                } else {
                    $filename = randomString() . time() . '.' . $file->getClientOriginalExtension();

                    AdsOfferTmpImage::createNew($hash, $filename);

                    $imagesNow++;

                    $json['files'][] = [
                        'name' => $file->getClientOriginalName(),
                        'size' => $file->getSize(),
                        'type' => $file->getMimeType(),
                        'filename' => $filename,
                        'status' => 'text-success'
                    ];

                    FilesClass::resizeAndSaveImage($file, config('image.defaultSize.adImage'), config('image.defaultSize.adImage'),
                        config('image.AdsOfferImagesPath'), $filename, true);

                    FilesClass::resizeAndSaveImageMini($file, config('image.defaultSize.adImageMini'), config('image.defaultSize.adImageMini'),
                        config('image.AdsOfferImagesPathMini'), $filename, true);
                }

            } else {

                $json['files'][] = [
                    'name' => $file->getClientOriginalName(),
                    'status' => 'text-danger'
                ];

            }
        }

        return Response::json($json);
    }


    public function adsOfferEditImages(
        Request $request,
        AdsOfferImage $adsOfferImage,
        AdsOfferTmpImage $adsOfferTmpImage
    ) {
        $files = $request::file('files');

        $hash = $request::get('hash');

        $ad_id = $request::get('ad_id');

        if (!$files || !$hash || !$ad_id) {
            return null;
        }

        $json = ['files' => []];

        $imageCount = $adsOfferImage->where('ad_id', $ad_id)->count();
        $tmpImageCount = $adsOfferTmpImage->where('hash', $hash)->count();

        $imagesNow = $imageCount + $tmpImageCount;

        foreach ($files as $file) {

            if ($imagesNow < $this->maxImages) {

                $validator = Validator::make(
                    ['file' => $file],
                    ['file' => 'mimes:jpg,jpeg,png,gif,bmp|min:5|max:10240']
                );

                if ($validator->fails()) {
                    $json['files'][] = [
                        'name' => $file->getClientOriginalName(),
                        'status' => 'text-danger'
                    ];
                } else {
                    $filename = randomString() . time() . '.' . $file->getClientOriginalExtension();

                    AdsOfferTmpImage::createNew($hash, $filename);

                    $imagesNow++;

                    $json['files'][] = [
                        'name' => $file->getClientOriginalName(),
                        'size' => $file->getSize(),
                        'type' => $file->getMimeType(),
                        'filename' => $filename,
                        'status' => 'text-success'
                    ];

                    FilesClass::resizeAndSaveImage($file, config('image.defaultSize.adImage'), config('image.defaultSize.adImage'),
                        config('image.AdsOfferImagesPath'), $filename, true);

                    FilesClass::resizeAndSaveImageMini($file, config('image.defaultSize.adImageMini'), config('image.defaultSize.adImageMini'),
                        config('image.AdsOfferImagesPathMini'), $filename, true);
                }

            } else {

                $json['files'][] = [
                    'name' => $file->getClientOriginalName(),
                    'status' => 'text-danger'
                ];

            }
        }

        return Response::json($json);
    }


    public function deleteImageAjax(Request $request, AdsOfferImage $adsOfferImage, AdsOfferTmpImage $adsOfferTmpImage)
    {
        if ($request::has('image')) {

            $imageName = $request::get('image');

            $adsOfferImage = $adsOfferImage->where('filename', $imageName)
                ->with('adOffer')->first();

            if ($adsOfferImage->adOffer->user_id != $this->user->id && !in_array($this->user->group_id,
                    config('app.userGroup.redactor'), true)
            ) {
                return 'ACCESS ERROR';
            }

            if ($adsOfferImage && $adsOfferImage->title == 1) {

                $adsOfferImage->delete();

                AdsOfferImage::newAdImageTitle($adsOfferImage->ad_id);

            } elseif ($adsOfferImage) {

                $adsOfferImage->delete();

            }

            $adsOfferTmpImage->where('filename', $imageName)->delete();

            self::deleteImageFile($imageName);

            return 'OK';
        }
    }


    public static function deleteImageFile($imageName)
    {
        File::delete(public_path(config('image.AdsOfferImagesPath') . $imageName));
        File::delete(public_path(config('image.AdsOfferImagesPathMini') . $imageName));
    }


    public function avatarForTempFolder(Request $request, AvatarTemp $avatarTemp)
    {
        if ($request::file('file')) {

            $file = $request::file('file');

            $validator = Validator::make(
                ['file' => $file],
                ['file' => 'mimes:jpg,jpeg,png,gif,bmp|min:5|max:10240']
            );
            if ($validator->fails()) {
                return messageToAlertCorner('error', trans('notification.corner.account-avatar-update-error'),
                    config('notification.color.error'));
            }

            $filename = randomString() . time() . '.' . $file->getClientOriginalExtension();

            $saveFile = FilesClass::resizeAndSaveImage($file, config('image.defaultSize.avatar'), config('image.defaultSize.avatar'),
                config('image.UserAvatarsTempFolder'), $filename, false);

            if ($saveFile) {

                $avatarTemp->create([
                    'name' => $filename
                ]);

                return messageToAlertCorner('path', $filename,
                    null);

            } else {

                return messageToAlertCorner('error', trans('notification.corner.account-avatar-update-error'),
                    config('notification.color.error'));

            }
        }
    }


    public function avatarUploadAjax(avatarCropTempImageAjaxRequest $errors, Request $request)
    {
        if (File::exists(public_path(config('image.UserAvatarsTempFolder') . $request::get('imageName')))) {

            $extension = File::extension(public_path(config('image.UserAvatarsTempFolder') . $request::get('imageName')));

            $path = config('image.UserAvatarsPath');
            $pathMini = config('image.UserAvatarsPathMini');

            $file_name = 'avatar_user' . $this->user->id . '.' . $extension;

            FilesClass::deleteFile($path . $this->user->profile->avatar);
            FilesClass::deleteFile($pathMini . $this->user->profile->avatar);

            FilesClass::cropImage(public_path(config('image.UserAvatarsTempFolder') . $request::get('imageName')),
                config('image.defaultSize.avatarMini'), config('image.defaultSize.avatarMini'), $pathMini, $file_name, false,
                $request::get('cordW'), $request::get('cordH'), $request::get('cordX'), $request::get('cordY'));
            FilesClass::cropImage(public_path(config('image.UserAvatarsTempFolder') . $request::get('imageName')),
                config('image.defaultSize.avatar'), config('image.defaultSize.avatar'), $path, $file_name, false,
                $request::get('cordW'), $request::get('cordH'), $request::get('cordX'), $request::get('cordY'));

            UserProfile::updateAvatar($this->user->id, $file_name);

            return messageToAlertCorner(config('image.UserAvatarsPath') . $file_name,
                trans('notification.corner.account-avatar-update-success'),
                config('notification.color.success'));
        }

    }


    public function avatarDeleteAjax(UserProfile $profile)
    {
        if ($this->user->profile->avatar) {

            $profile->where('user_id', $this->user->id)
                ->update(['avatar' => null]);

            FilesClass::deleteFile(config('image.UserAvatarsPath') . $this->user->profile->avatar);
            FilesClass::deleteFile(config('image.UserAvatarsPathMini') . $this->user->profile->avatar);

            return messageToAlertCorner('success', trans('notification.corner.account-avatar-delete-success'),
                config('notification.color.success'));
        }
    }


    public function adCoverImageCrop(Request $request, AdsOfferImage $adsOfferImage)
    {
        $filename = $request::get('imageName');

        $adsOfferImage = $adsOfferImage->where('filename', $filename)
            ->with([
                'adOffer' => function ($query) {
                    $query->where('user_id', $this->user->id);
                }
            ])->first();

        if(!$adsOfferImage->adOffer){
            return messageToAlertCorner('error',
                trans('notification.corner.account-ad-cover-crop-error'),
                config('notification.color.error'));
        };

        $pathMini = config('image.AdsOfferImagesPathMini');

        FilesClass::cropImage(public_path($pathMini . $filename),
            config('image.defaultSize.adImageMini'), config('image.defaultSize.adImageMini'), $pathMini, $filename, false,
            $request::get('cordW'), $request::get('cordH'), $request::get('cordX'), $request::get('cordY'));

        return messageToAlertCorner($adsOfferImage->adOffer->id,
            trans('notification.corner.account-ad-cover-crop-success'),
            config('notification.color.success'));
    }


}