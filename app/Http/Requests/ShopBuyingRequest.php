<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\ShopPrice;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ShopBuyingRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        $rules = [
            'number' => 'required|numeric|min:1|max:9999',
            'good' => 'required|in:extra,professional,standard,mini,auto_up,color,ad_limit,top,vip'
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}