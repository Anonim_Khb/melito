<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;

class PrivilegesActivateAjaxRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'ad_id' => 'required|exists:ads_offers,id',
            'good_name' => 'required|in:top,color,auto_up,vip',
            'sum' => 'required|numeric|min:1|max:10000',
            'color' => 'required_if:good_name,color|exists:shop_colors,name',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return Response::json($errors);
    }


    public function formatErrors(Validator $validator)
    {
        return messageToAlertCorner('delete', trans('notification.corner.ad-privilege-error'),
            config('notification.color.error'));
    }

}

