<?php namespace App\Http\Requests;

use App\Http\MyClass\MyArrays;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class supportNewDialogPostRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $categories = implode(",", MyArrays::supportCategories());

        $rules = [
            'category' => 'required|in:'. $categories . '',
            'text' => 'required|min:1|max:3000',
        ];

        if (config('captcha.CaptchaEnabled')) {
            $rules = array_add($rules, 'g-recaptcha-response', 'required|captcha');
        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}

