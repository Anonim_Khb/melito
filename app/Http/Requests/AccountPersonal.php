<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class AccountPersonal extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'name' => 'min:1|max:50|not_in:admin,administrator,moder,moderator,melito,админ,администратор,модер,модератор',
            'gender' => 'in:male,female',
            'birthday' => 'date|date_format:Y-n-j',
            'phone' => 'min:6|max:50',
            'skype' => 'min:6|max:50',
            'whatsapp' => 'boolean',
            'viber' => 'boolean',
            'messenger' => 'boolean',
            'contact_another' => 'min:2|max:150',
            'alcohol' => 'in:yes,sometime,no',
            'smoke' => 'in:yes,no',
            'criminal' => 'in:no,yes',
            'work' => 'in:yes,student,business,no',
            'additional' => 'min:1|max:1500'
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}

