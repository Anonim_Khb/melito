<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ChangePasswordPostRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'old_password' => 'required',
            'new_password_1' => 'required|min:6|max:150',
            'new_password_2' => 'required|same:new_password_1',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}
