<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ResetPassword extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'email' => 'required|email',
            'pr_token' => 'required|exists:users,hash,email,' . \Input::get('email') . '',
            'new_password' => 'required|confirmed|min:6|max:300',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}
