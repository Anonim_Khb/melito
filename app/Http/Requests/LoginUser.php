<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class LoginUser extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        if (config('captcha.CaptchaEnabled') && Session::get('wrong_login') > 2) {
            $rules = array_add($rules, 'g-recaptcha-response', 'required|captcha');
        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}

