<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use \Jenssegers\Date\Date;

class sendMessageFromAccount extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'text_msg' => 'required|min:5|max:1500',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}

