<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class RemindPassword extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
        ];

        if (config('captcha.CaptchaEnabled')) {
            $rules = array_add($rules, 'g-recaptcha-response', 'required|captcha');
        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}


