<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;

class AdminGiftPersonalPostRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'good_name' => 'required|in:auto_up,color,ad_limit,top,vip',
            'sum' => 'required|min:1|max:5',
            'user' => 'required|exists:users,id'
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}

