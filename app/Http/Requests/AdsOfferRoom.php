<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;

class AdsOfferRoom extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'admin_status' => 'in:active,for-correct',
            'rooms' => 'required_if:deal_type,sale|numeric|min:1|max:100',
            'city' => 'required|exists:sxgeo_cities,id',
            'street_name' => 'required|min:1|max:100',
            'house_number' => 'required|min:1|max:20',
            'floor' => 'numeric',
            'deal_type' => 'required|in:rent,sale',
            'house_type' => 'required|in:apartment,house,studio',
            'communal' => 'required_if:deal_type,rent|in:single,several',
            'sanitary_ware' => 'in:home,street',
            'wifi' => 'boolean',
            'tv' => 'boolean',
            'parking' => 'boolean',
            'balcony' => 'boolean',
            'garden' => 'boolean',
            'security' => 'boolean',
            'elevator' => 'boolean',
            'jacuzzi' => 'boolean',
            'boiler' => 'boolean',
            'gim' => 'boolean',
            'furniture' => 'boolean',
            'appliances' => 'boolean',
            'rent_price' => 'required|numeric|min:0|max:999999',
            'add_pay' => 'required_if:add_pay_check,1|numeric|min:1|max:999999',
            'room_meters' => 'required|numeric|min:1|max:9999',
            'available_from' => 'required|date|date_format:Y-n-j',
            'prepayment' => 'required_if:deal_type,rent|in:0,1,2,3,6+',
            'gender' => 'required_if:deal_type,rent|in:male,female,not-matter',
            'occupation' => 'required_if:deal_type,rent|in:student,professional,not-matter',
            'age_from' => 'required_if:deal_type,rent|numeric|between:18,99',
            'age_to' => 'required_if:deal_type,rent|numeric|between:18,99',
            'children' => 'boolean',
            'couples' => 'boolean',
            'guests' => 'boolean',
            'pets' => 'boolean',
            'smoking' => 'boolean',
            'ad_text' => 'required|min:1|max:1500',
        ];

        if (!\Auth::check()) {

            $rules = array_add($rules, 'email', 'required|email|unique:users,email|min:6|max:300');
            $rules = array_add($rules, 'password', 'required|confirmed|min:6|max:300');

            if (config('captcha.CaptchaEnabled')) {
                $rules = array_add($rules, 'g-recaptcha-response', 'required|captcha');
            }

        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return Response::json($errors);
    }


    public function formatErrors(Validator $validator)
    {
        return $validator->errors()->toArray();
    }

}

