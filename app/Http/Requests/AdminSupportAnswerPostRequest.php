<?php namespace App\Http\Requests;

use App\Http\MyClass\MyArrays;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class AdminSupportAnswerPostRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'text' => 'required|min:1|max:3000',
            'dialog_id' => 'required|exists:support_dialogs,id',
            'status' => 'boolean'
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}

