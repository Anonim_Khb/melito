<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class RegisterUser extends Request
{


    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'email' => 'required|email|unique:users,email|min:6|max:300',
            'password' => 'required|confirmed|min:6|max:300',
            'rules' => 'required',
        ];

        if (config('captcha.CaptchaEnabled')) {
            $rules = array_add($rules, 'g-recaptcha-response', 'required|captcha');
        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }


}
