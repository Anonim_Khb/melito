<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;
use Illuminate\Support\Facades\Auth;

class DialogMessagesGetRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $user_id = Auth::user()->id;

        $rules = [
            'dialog' => 'exists:support_dialogs,id,user_id,' . $user_id . '',
            'text' => 'min:0|max:3000',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}


