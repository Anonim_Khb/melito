<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;

class avatarCropTempImageAjaxRequest extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'imageName' => 'required|exists:avatar_temps,name',
            'cordX' => 'required|numeric',
            'cordY' => 'required|numeric',
            'cordH' => 'required|numeric',
            'cordW' => 'required|numeric',
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return Response::json($errors);
    }


    public function formatErrors(Validator $validator)
    {
        return messageToAlertCorner('error', trans('notification.corner.account-avatar-update-error'),
            config('notification.color.error'));
    }

}

