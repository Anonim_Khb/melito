<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;
use Response;

class AdsNeedRoom extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'city' => 'exists:sxgeo_cities,id',
            'price_from' => 'numeric|min:0',
            'price_to' => 'numeric|min:0',
            'house_type' => 'in:apartment,house,studio',
            'deal_type' => 'in:rent,sale',
            'communal' => 'in:single,several',
            'sanitary_ware' => 'in:home,street',
            'wifi' => 'boolean',
            'tv' => 'boolean',
            'parking' => 'boolean',
            'balcony' => 'boolean',
            'garden' => 'boolean',
            'security' => 'boolean',
            'elevator' => 'boolean',
            'jacuzzi' => 'boolean',
            'boiler' => 'boolean',
            'gim' => 'boolean',
            'furniture' => 'boolean',
            'appliances' => 'boolean',
            'rm_to' => 'numeric|min:0',
            'rm_from' => 'numeric|min:0',
            'add_pay' => 'boolean',
            'entry' => 'date|date_format:Y-n-j',
            'prepayment' => 'boolean',
            'gender' => 'in:male,female,not-matter',
            'occupation' => 'in:student,professional,not-matter',
            'age' => 'numeric|between:18,99',
            'children' => 'boolean',
            'couples' => 'boolean',
            'guests' => 'boolean',
            'pets' => 'boolean',
            'smoking' => 'boolean'
        ];

        return $rules;
    }


    public function response(Array $errors)
    {
        return Response::json($errors);
    }


    public function formatErrors(Validator $validator)
    {
        return $validator->errors()->toArray();
    }

}

