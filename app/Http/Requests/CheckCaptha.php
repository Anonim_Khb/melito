<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class CheckCaptha extends Request
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        config('captcha.CaptchaEnabled') ?
            $rules = ['g-recaptcha-response' => 'required|captcha'] :
            $rules = [];

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        return $validator->errors()->getMessages();
    }

}
