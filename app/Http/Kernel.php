<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
//        \App\Http\Middleware\UserTimeUpdater::class,
        \App\Http\Middleware\UserLocale::class,
        \App\Http\Middleware\NotificationGlobalMiddleware::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'boat' => \App\Http\Middleware\FormLifeTime::class,
        'auth-easy' => \App\Http\Middleware\EasyAuthenticate::class,
        'auth-pending' => \App\Http\Middleware\pendingUsersOnlyMiddleware::class,
        'ajax' => \App\Http\Middleware\ajaxOnlyMiddleware::class,
        'ads-limit' => \App\Http\Middleware\adOfferLimitMiddleware::class,
        'admin' => \App\Http\Middleware\AdminMiddleware::class,
        'super-admin' => \App\Http\Middleware\SuperAdminMiddleware::class,
    ];
}
