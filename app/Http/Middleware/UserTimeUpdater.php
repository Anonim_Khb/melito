<?php

namespace App\Http\Middleware;

use Closure;

class UserTimeUpdater
{

    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            \Auth::user()->touch();
        }
        
        return $next($request);
    }

}
