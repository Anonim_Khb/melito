<?php

namespace App\Http\Middleware;

use App\Models\AdsOffer;
use App\Models\UserGood;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Notifications;

class adOfferLimitMiddleware
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if (!$this->auth->guest()) {

            $count = AdsOffer::where('user_id', $this->auth->user()->id)
                ->where('status', '!=', 'closed')
                ->count();

            $userGood = UserGood::where('user_id', $this->auth->user()->id)
                ->select('ad_limit')
                ->first();

            $userGoodLimit = $userGood ? $userGood->ad_limit : 0;

            $limit = config('app.adOfferLimit') + $userGoodLimit;

            if ($count >= $limit) {

                if ($request->ajax()) {

                    $status = AdsOffer::where('id', $request->id)
                        ->select('status')
                        ->first();

                    if ($status->status == 'closed') {

                        return [
                            'status' => 'error',
                            'message' => trans('account.ads.show-hide-alerts.limit'),
                            'color' => '#d7564b'
                        ];

                    }

                } else {

                    Notifications::add(trans('notification.ad_limit'), 'error');

                    return redirect()->route('shop-page');

                }

            }

        }

        return $next($request);
    }
}
