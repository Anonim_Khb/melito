<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Notifications;

class AdminMiddleware
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {

            if ($request->ajax()) {

                return response('ERROR.', 401);

            } else {

                Notifications::add(trans('notification.admin-access-error'), 'error');

                return abort(401);

            }

        }

        if (!in_array($this->auth->user()->group_id, config('app.userGroup.administration'), true)) {

            Notifications::add(trans('notification.admin-access-error'), 'error');

            return abort(401);

        }

        return $next($request);
    }
}
