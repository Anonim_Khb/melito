<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Notifications;

class SuperAdminMiddleware
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {

            if ($request->ajax()) {

                return response('ERROR.', 401);

            } else {

                Notifications::add(trans('notification.admin-access-error'), 'error');

                return abort(401);

            }

        }

        if ($this->auth->user()->group_id != 11) {

            Notifications::add(trans('notification.admin-access-error'), 'error');

            return abort(404);

        }

        return $next($request);
    }
}
