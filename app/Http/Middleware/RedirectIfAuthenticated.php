<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Notifications;

class RedirectIfAuthenticated
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {

            Notifications::add( trans('notification.guest-only'), 'error');

            return redirect()->route('home');

        }

        return $next($request);
    }
}
