<?php

namespace App\Http\Middleware;

use App\Models\Notification;
use Closure;
use Request;

class NotificationGlobalMiddleware
{

    public function handle($request, Closure $next)
    {
        $notification = new Notification();

        $notifications = $notification->getUserNotifications();

        view()->share('new_notifications', $notifications);

        return $next($request);

    }

}

