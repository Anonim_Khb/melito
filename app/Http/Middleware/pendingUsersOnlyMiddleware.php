<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Notifications;

class pendingUsersOnlyMiddleware
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {

            if ($request->ajax()) {

                return response('Unauthorized.', 401);

            } else {

                Notifications::add(trans('notification.auth-before'), 'error');

                return redirect()->guest('login');

            }

        }

        if ($this->auth->user()->status != 'pending') {

            Notifications::add(trans('notification.register-already-confirm'), 'error');

            return redirect()->back();

        }

        return $next($request);
    }
}


