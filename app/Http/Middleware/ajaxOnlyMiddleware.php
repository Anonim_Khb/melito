<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class ajaxOnlyMiddleware
{


    public function handle($request, Closure $next)
    {
        if (!$request->ajax()) {

            return abort(404);

        }

        return $next($request);
    }
}