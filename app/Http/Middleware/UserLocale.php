<?php

namespace App\Http\Middleware;

use Cookie;
use Closure;
use Jenssegers\Date\Date;
use Request;

class UserLocale
{

    public function handle($request, Closure $next)
    {
        !in_array(Request::cookie('locale'), config('app.locales'), true) ?
            Cookie::queue('locale', config('app.locale'), 2628000) : '';

        \App::setLocale(userLocale());

        Date::setLocale(userLocale());

        return $next($request);

    }

}
