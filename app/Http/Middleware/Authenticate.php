<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Notifications;

class Authenticate
{

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        Session::put('previous', URL::previous());
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {

            if ($request->ajax()) {

                return response('Unauthorized.', 401);

            } else {

                Notifications::add(trans('notification.auth-before'), 'error');

                return redirect()->guest('login');

            }

        }

        if ($this->auth->user()->status == 'pending') {

            Notifications::add(trans('notification.register-no-confirm'), 'error');

            return redirect()->route('registration-no-confirm');

        }

        return $next($request);
    }
}
