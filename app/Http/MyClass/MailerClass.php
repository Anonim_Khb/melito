<?php namespace App\Http\MyClass;

use Mail;
use DB;

class MailerClass
{


    public static function welcome($user)
    {
        $user_data = ['token' => $user->hash];

        $adresat = ['email' => $user->email];

        Mail::queue(['emails.welcome-HTML', 'emails.welcome-TEXT'], $user_data,
            function ($message) use ($adresat) {
                $message->to($adresat['email'])->subject(trans('email.welcome-email.title'));
            });
    }


    public static function remindPassword($user)
    {
        $user_data = ['token' => $user->hash];

        $adresat = ['email' => $user->email];

        Mail::queue(['emails.remind-password-HTML', 'emails.remind-password-TEXT'], $user_data,
            function ($message) use ($adresat) {
                $message->to($adresat['email'])->subject(trans('email.remind-password-email.title'));
            });
    }


    public static function userSendMessage($text, $user_id, $adresat, $avatar_path, $ad_id)
    {
        $user_data = [
            'text' => $text,
            'from' => $user_id,
            'avatar_path' => $avatar_path,
            'ad_id' => $ad_id
        ];

        Mail::queue(['emails.message-HTML', 'emails.message-TEXT'], $user_data,
            function ($message) use ($adresat) {
                $message->to($adresat)->subject(trans('email.message.title'));
            });
    }


    public static function newSupportResponse($email)
    {
        Mail::queue(['emails.support-response-HTML', 'emails.support-response-TEXT'], [],
            function ($message) use ($email) {
                $message->to($email)->subject(trans('email.support-response.title'));
            });
    }


    public static function adVerifyCheckSuccess($email, $adId)
    {
        $user_data = ['adId' => $adId];

        Mail::queue(['emails.ad-verify-HTML', 'emails.ad-verify-TEXT'], $user_data,
            function ($message) use ($email) {
                $message->to($email)->subject(trans('email.ad-verify.title'));
            });
    }


    public static function adVerifyCheckError($email, $adId)
    {
        $user_data = ['adId' => $adId];

        Mail::queue(['emails.ad-not-verify-HTML', 'emails.ad-not-verify-TEXT'], $user_data,
            function ($message) use ($email) {
                $message->to($email)->subject(trans('email.ad-not-verify.title'));
            });
    }


}
