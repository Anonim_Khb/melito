<?php namespace App\Http\MyClass;

use App\Models\AdsOffer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Request;

class HelperClass
{


    public static function wrongLoginSession()
    {
        Session::get('wrong_login') ?
            (Session::put('wrong_login', Session::get('wrong_login') + '1')) :
            (Session::put('wrong_login', '1'));
    }


    public static function changeNotCheckedFieldOnOurValue($data, $fields)
    {
        foreach ($fields as $field) {

            Input::has($field) ? '' : $data[$field] = Input::has($field);

        }

        return $data;
    }


    public static function adsMaxSumAreaCache()
    {
        /*Price*/
        if (!Cache::has('rentPriceMin')) {
            $rentPriceMin = AdsOffer::where('deal_type', 'rent')->min('rent_price');
            $rentPriceMin ? Cache::forever('rentPriceMin', $rentPriceMin) : Cache::forever('rentPriceMin', 0);
        }
        if (!Cache::has('rentPriceMax')) {
            $rentPriceMax = AdsOffer::where('deal_type', 'rent')->max('rent_price');
            $rentPriceMax ? Cache::forever('rentPriceMax', $rentPriceMax) : Cache::forever('rentPriceMax', 10000000);
        }
        if (!Cache::has('salePriceMin')) {
            $salePriceMin = AdsOffer::where('deal_type', 'sale')->min('rent_price');
            $salePriceMin ? Cache::forever('salePriceMin', $salePriceMin) : Cache::forever('salePriceMin', 0);
        }
        if (!Cache::has('salePriceMax')) {
            $salePriceMax = AdsOffer::where('deal_type', 'sale')->max('rent_price');
            $salePriceMax ? Cache::forever('salePriceMax', $salePriceMax) : Cache::forever('salePriceMax', 10000000);
        }

        /*Area*/
        if (!Cache::has('areaMin')) {
            $areaMin = AdsOffer::min('room_meters');
            $areaMin ? Cache::forever('areaMin', $areaMin) : Cache::forever('areaMin', 0);
        }
        if (!Cache::has('areaMax')) {
            $areaMax = AdsOffer::max('room_meters');
            $areaMax ? Cache::forever('areaMax', $areaMax) : Cache::forever('areaMax', 10000);
        }
    }


}