<?php namespace App\Http\MyClass;

use Mail;
use DB;

class MyArrays
{


    public static function adsOffer()
    {
        $adsOfferData = [
            'deal_type' => [
                ['name' => 'rent', 'pic' => 'wifi'],
                ['name' => 'sale', 'pic' => 'wifi']
            ],
            'house_type' => [
                ['name' => 'apartment', 'pic' => 'wifi'],
                ['name' => 'house', 'pic' => 'wifi'],
                ['name' => 'studio', 'pic' => 'wifi']
            ],
            'communal' => [
                ['name' => 'single', 'pic' => 'wifi'],
                ['name' => 'several', 'pic' => 'wifi']
            ],
            'sanitary_ware' => [
                ['name' => 'home', 'pic' => 'wifi'],
                ['name' => 'street', 'pic' => 'wifi']
            ],
            'amenities' => [
                ['name' => 'wifi', 'pic' => 'wifi'],
                ['name' => 'tv', 'pic' => 'tv'],
                ['name' => 'parking', 'pic' => 'car'],
                ['name' => 'balcony', 'pic' => 'exchange'],
                ['name' => 'garden', 'pic' => 'tree'],
                ['name' => 'security', 'pic' => 'lock'],
                ['name' => 'elevator', 'pic' => 'toggle-up'],
                ['name' => 'jacuzzi', 'pic' => 'tty'],
                ['name' => 'boiler', 'pic' => 'tty'],
                ['name' => 'gim', 'pic' => 'tty'],
                ['name' => 'furniture', 'pic' => 'tty'],
                ['name' => 'appliances', 'pic' => 'tty'],
            ],
            'prepayment' => [
                '0',
                '1',
                '2',
                '3',
                '6+'
            ],
            'gender' => [
                ['name' => 'male', 'pic' => 'wifi'],
                ['name' => 'female', 'pic' => 'wifi'],
                ['name' => 'not-matter', 'pic' => 'wifi'],
            ],
            'occupation' => [
                ['name' => 'student', 'pic' => 'wifi'],
                ['name' => 'professional', 'pic' => 'wifi'],
                ['name' => 'not-matter', 'pic' => 'wifi'],
            ],
            'rules' => [
                ['name' => 'children', 'pic' => 'wifi'],
                ['name' => 'couples', 'pic' => 'wifi'],
                ['name' => 'guests', 'pic' => 'wifi'],
                ['name' => 'pets', 'pic' => 'wifi'],
                ['name' => 'smoking', 'pic' => 'wifi'],
            ],
        ];

        return $adsOfferData;
    }


    public static function adsNeed()
    {
        $adsOfferData = [
            'deal_type' => [
                ['name' => 'rent', 'pic' => 'wifi'],
                ['name' => 'sale', 'pic' => 'wifi']
            ],
            'house_type' => [
                ['name' => 'apartment', 'pic' => 'wifi'],
                ['name' => 'house', 'pic' => 'wifi'],
                ['name' => 'studio', 'pic' => 'wifi']
            ],
            'communal' => [
                ['name' => 'single', 'pic' => 'wifi'],
                ['name' => 'several', 'pic' => 'wifi']
            ],
            'sanitary_ware' => [
                ['name' => 'home', 'pic' => 'wifi'],
                ['name' => 'street', 'pic' => 'wifi']
            ],
            'amenities' => [
                ['name' => 'wifi', 'pic' => 'wifi'],
                ['name' => 'tv', 'pic' => 'tv'],
                ['name' => 'parking', 'pic' => 'car'],
                ['name' => 'balcony', 'pic' => 'exchange'],
                ['name' => 'garden', 'pic' => 'tree'],
                ['name' => 'security', 'pic' => 'lock'],
                ['name' => 'elevator', 'pic' => 'toggle-up'],
                ['name' => 'jacuzzi', 'pic' => 'tty'],
                ['name' => 'boiler', 'pic' => 'tty'],
                ['name' => 'gim', 'pic' => 'tty'],
                ['name' => 'furniture', 'pic' => 'tty'],
                ['name' => 'appliances', 'pic' => 'tty'],
            ],
            'gender' => [
                ['name' => 'male', 'pic' => 'wifi'],
                ['name' => 'female', 'pic' => 'wifi'],
            ],
            'occupation' => [
                ['name' => 'student', 'pic' => 'wifi'],
                ['name' => 'professional', 'pic' => 'wifi'],
            ],
            'rules' => [
                ['name' => 'children', 'pic' => 'wifi'],
                ['name' => 'couples', 'pic' => 'wifi'],
                ['name' => 'guests', 'pic' => 'wifi'],
                ['name' => 'pets', 'pic' => 'wifi'],
                ['name' => 'smoking', 'pic' => 'wifi'],
            ],
        ];

        return $adsOfferData;
    }


    public static function popularCities()
    {
        $cities = [
            '' . LocationClass::getUserLocationCityId() . '',
            '524901',
            '498817',
            '1850147',
            '1816670',
            '4887398',
            '6058560',
            '5128581',
            '2988507',
            '3169070',
        ];

        return $cities;
    }


    public static function supportCategories()
    {
        return
            [
                'general',
                'technical',
                'pay',
                'donate',
                'wish-idea',
                'cooperation',
                'complaint'
            ];
    }


    public static function indexPerformances()
    {
        return [
            [
                'name' => 'housemates_1',
                'src' => 'housemates_1.png',
                'color' => '#96c8ed'
            ],
            [
                'name' => 'thumbs_up_1',
                'src' => 'thumbs_up_1.png',
                'color' => '#eddbb5'
            ],
            [
                'name' => 'gives_key_1',
                'src' => 'gives_key_1.png',
                'color' => '#a5a8ed'
            ],
            [
                'name' => 'multiscreens_1',
                'src' => 'multiscreens_1.png',
                'color' => '#a4edba'
            ],
            [
                'name' => 'golden_key_1',
                'src' => 'golden_key_1.png',
                'color' => '#edcea9'
            ],
            [
                'name' => 'houses_around_1',
                'src' => 'houses_around_1.png',
                'color' => '#d4edb2'
            ],
            [
                'name' => 'coffee_cup_1',
                'src' => 'coffee_cup_1.png',
                'color' => '#cdb4ed'
            ],
            [
                'name' => 'house_search_1',
                'src' => 'house_search_1.png',
                'color' => '#edadad'
            ],
        ];
    }


}