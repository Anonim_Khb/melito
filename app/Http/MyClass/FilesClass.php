<?php namespace App\Http\MyClass;

use App\Models\AdsOfferTmpImage;
use File;

class FilesClass
{


    public static function resizeAndSaveImageMini($image, $width, $height, $path, $file_name, $watermark = true)
    {
        $newImage = \Image::make($image)
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->orientate();

        $newImage->save(public_path($path . $file_name), 80);
    }


    public static function resizeAndSaveImage($image, $width, $height, $path, $file_name, $watermark = true)
    {
        $newImage = \Image::make($image)
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->orientate();;

        if ($watermark) {
            $newImage->insert(config('image.watermark'), 'bottom-right', 10, 10);
        }

        $newImage->save(public_path($path . $file_name), 95);

        return true;
    }


    public static function cropImage($imageName, $width, $height, $path, $file_name, $watermark = true, $coorW, $coorH, $coorX, $coorY)
    {
        $newImage = \Image::make($imageName)
            ->crop($coorW, $coorH, $coorX, $coorY)
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->orientate();

        if ($watermark) {
            $newImage->insert(config('image.watermark'), 'bottom-right', 10, 10);
        }

        $newImage->save(public_path($path . $file_name), 95);
    }


    public static function deleteFile($path)
    {
        File::delete(public_path($path));
    }


}