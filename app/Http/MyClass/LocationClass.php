<?php namespace App\Http\MyClass;

use SxGeo;
use Request;

class LocationClass
{


    public static function getUserLocationCityId()
    {
        include(public_path('/packages/SxGeo/SxGeo.php'));

        $SxGeo = new SxGeo(public_path('/packages/SxGeo/SxGeoCity.dat'), SXGEO_BATCH | SXGEO_MEMORY);

        $ip = config('app.defaultLocationIp') == null ? Request::getClientIp() : config('app.defaultLocationIp');

        $place = $SxGeo->getCityFull($ip);

        unset($SxGeo);

        return $place['city']['id'];
    }


    public static function getAllCountries()
    {
        userLocale() == 'ru' ?
            $countries = SxgeoCountry::orderBy('name_ru', 'asc')
                ->select('iso', 'name_ru as name')
                ->get() :
            $countries = SxgeoCountry::orderBy('name_en', 'asc')
                ->select('iso', 'name_en as name')
                ->get();

        return $countries;
    }


    public static function getLocation()
    {
        include(public_path('/packages/SxGeo/SxGeo.php'));

        $SxGeo = new SxGeo(public_path('/packages/SxGeo/SxGeoCity.dat'), SXGEO_BATCH | SXGEO_MEMORY);

        $ip = config('app.defaultLocationIp') == null ? Request::getClientIp() : config('app.defaultLocationIp');

        $place = $SxGeo->getCityFull($ip);

        userLocale() == 'ru' ?
            $location = [
                'id' => $place['city']['id'],
                'city' => $place['city']['name_ru'],
                'country' => $place['country']['name_ru'],
            ] :
            $location = [
                'id' => $place['city']['id'],
                'city' => $place['city']['name_en'],
                'country' => $place['country']['name_en'],
            ];

        unset($SxGeo);

        return $location;
    }


}