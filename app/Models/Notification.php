<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class Notification extends Model
{


    protected $table = 'notifications';


    protected $fillable = [
        'user_id',
        'text',
        'icon_src',
        'url',
        'valid_to'
    ];


    public $timestamps = true;


    public static function createNotification($template, $user_id = null, $validTo = null)
    {
        $notification = new Notification;

        $notification->user_id = $user_id;

        $validTo != null ? $notification->valid_to = Date::now()->add('' . $validTo . ' Days') : '';

        foreach ($template as $key => $value) {

            $notification->$key = $value;

        }

        $notification->save();
    }


    public function getUserNotifications()
    {
        $notifications = Notification::where(function ($query) {
            $query->whereNull('user_id')
                ->where('valid_to', '>', Date::now());
        });

        if (Auth::check()) {
            $notifications = $notifications->orWhere('user_id', Auth::user()->id);
        }

        $notifications = $notifications->orderBy('created_at', 'desc')->get();

        foreach ($notifications as $key => $value) {

            if (Request::cookie($value->id)) {

                $notifications->forget($key);

            }

        }

        return $notifications;
    }


    public function getActiveGeneralNotifications()
    {
        $notifications = Notification::where(function ($query) {
            $query->whereNull('user_id')
                ->where('valid_to', '>', Date::now());
        })->orderBy('created_at', 'desc')->paginate(10);

        return $notifications;
    }


}
