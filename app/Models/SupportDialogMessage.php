<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;

class SupportDialogMessage extends Model
{


    protected $table = 'support_dialog_messages';


    protected $fillable = [
        'dialog_id',
        'author_id',
        'recipient_id',
        'text'
    ];


    protected $touches = array('dialog');


    public function dialog()
    {
        return $this->belongsTo('App\Models\SupportDialog');
    }


    public static function createNewMessage($data)
    {
        SupportDialogMessage::create($data);
    }


}
