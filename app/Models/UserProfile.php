<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserProfile extends Model
{


    protected $table = 'user_profiles';


    protected $fillable = [
        'user_id',
        'name',
        'avatar',
        'gender',
        'birthday',
        'phone',
        'skype',
        'whatsapp',
        'viber',
        'messenger',
        'contact_another',
        'alcohol',
        'smoke',
        'criminal',
        'work',
        'additional'
    ];


    public function goods()
    {
        return $this->hasOne('App\Models\UserGood', 'user_id', 'user_id');
    }


    public function transactions()
    {
        return $this->hasMany('App\Models\UserTransaction', 'user_id', 'user_id');
    }


    public function privileges()
    {
        return $this->hasMany('App\Models\AdsPrivilege', 'user_id', 'user_id');
    }

    public static function createNew($user_id)
    {
        UserProfile::firstOrCreate([
            'user_id' => $user_id,
        ]);
    }


    public static function updateAvatar($user_id, $avatar)
    {
        UserProfile::where('user_id', $user_id)
            ->update([
                'avatar' => $avatar,
            ]);
    }


    public static function updateProfile($user_id, $data)
    {
        UserProfile::where('user_id', $user_id)
            ->update([
                'name' => $data['name'] ? e($data['name']) : null,
                'gender' => $data['gender'] ? $data['gender'] : null,
                'birthday' => $data['birthday'] ? $data['birthday'] : null,
                'phone' => $data['phone'] ? e($data['phone']) : null,
                'skype' => $data['skype'] ? e($data['skype']) : null,
                'whatsapp' => $data['whatsapp'] ? $data['whatsapp'] : 0,
                'viber' => $data['viber'] ? $data['viber'] : 0,
                'messenger' => $data['messenger'] ? $data['messenger'] : 0,
                'contact_another' => $data['contact_another'] ? e($data['contact_another']) : null,
                'alcohol' => $data['alcohol'] ? $data['alcohol'] : null,
                'smoke' => $data['smoke'] ? $data['smoke'] : null,
                'criminal' => $data['criminal'] ? $data['criminal'] : null,
                'work' => $data['work'] ? $data['work'] : null,
                'additional' => $data['additional'] ? e($data['additional']) : null
            ]);
    }


    public static function updateBalance($user_id, $balance, $sum)
    {
        UserProfile::where('user_id', $user_id)
            ->update(['balance' => $balance - $sum]);
    }


}
