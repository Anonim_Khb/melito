<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class AvatarTemp extends Model
{


    protected $table = 'avatar_temps';


    protected $fillable = [
        'name'
    ];


}
