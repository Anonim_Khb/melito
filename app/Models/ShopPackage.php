<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShopPackage extends Model
{


    protected $table = 'shop_packages';


    protected $fillable = [
        'name',
        'ad_limit',
        'color',
        'auto_up',
        'top',
        'vip',
        'price',
        'discount'
    ];


}
