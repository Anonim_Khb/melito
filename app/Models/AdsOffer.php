<?php namespace App\Models;

use App\Http\Controllers\FilesController;
use Illuminate\Database\Eloquent\Model;
use DB;
use Jenssegers\Date\Date;

class AdsOffer extends Model
{


    protected $table = 'ads_offers';


    protected $fillable = [
        'user_id',
        'status',
        'city',
        'street_name',
        'house_number',
        'deal_type',
        'rooms',
        'house_type',
        'communal',
        'floor',
        'sanitary_ware',
        'wifi',
        'tv',
        'parking',
        'balcony',
        'garden',
        'security',
        'elevator',
        'jacuzzi',
        'boiler',
        'gim',
        'furniture',
        'appliances',
        'rent_price',
        'add_pay',
        'room_meters',
        'available_from',
        'prepayment',
        'gender',
        'occupation',
        'age_from',
        'age_to',
        'children',
        'couples',
        'guests',
        'pets',
        'smoking',
        'ad_text',
        'count'
    ];


    public function images()
    {
        return $this->hasMany('App\Models\AdsOfferImage', 'ad_id');
    }


    public function image()
    {
        return $this->hasOne('App\Models\AdsOfferImage', 'ad_id');
    }


    public function privileges()
    {
        return $this->hasOne('App\Models\AdsPrivilege', 'ad_id');
    }


    public function author()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function cityName()
    {
        return $this->hasOne('App\Models\SxgeoCity', 'id', 'city');
    }


    public function bookmark()
    {
        return $this->hasOne('App\Models\AdBookmark', 'ad_id', 'id');
    }


    public function verifyComments()
    {
        return $this->hasMany('App\Models\VerifyAd', 'ad_id', 'id');
    }


    public static function createAdOffer($user_id, $data, $status)
    {
        $newAd = AdsOffer::create([
            'status' => $status,
            'user_id' => $user_id,
            'city' => $data['city'],
            'street_name' => e($data['street_name']),
            'house_number' => e($data['house_number']),
            'deal_type' => $data['deal_type'],
            'rooms' => $data['rooms'],
            'house_type' => $data['house_type'],
            'communal' => $data['deal_type'] == 'rent' ? $data['communal'] : null,
            'sanitary_ware' => $data['sanitary_ware'],
            'wifi' => $data['wifi'],
            'tv' => $data['tv'],
            'parking' => $data['parking'],
            'balcony' => $data['balcony'],
            'garden' => $data['garden'],
            'security' => $data['security'],
            'elevator' => $data['elevator'],
            'jacuzzi' => $data['jacuzzi'],
            'boiler' => $data['boiler'],
            'gim' => $data['gim'],
            'furniture' => $data['furniture'],
            'appliances' => $data['appliances'],
            'rent_price' => $data['rent_price'],
            'add_pay' => $data['add_pay'],
            'room_meters' => $data['room_meters'],
            'available_from' => $data['available_from'],
            'prepayment' => $data['deal_type'] == 'rent' ? $data['prepayment'] : null,
            'gender' => $data['deal_type'] == 'rent' ? $data['gender'] : null,
            'occupation' => $data['deal_type'] == 'rent' ? $data['occupation'] : null,
            'age_from' => $data['deal_type'] == 'rent' ? $data['age_from'] : null,
            'age_to' => $data['deal_type'] == 'rent' ? $data['age_to'] : null,
            'children' => $data['children'],
            'couples' => $data['couples'],
            'guests' => $data['guests'],
            'pets' => $data['pets'],
            'smoking' => $data['smoking'],
            'floor' => $data['floor'],
            'ad_text' => e($data['ad_text']),
        ]);

        return $newAd->id;
    }


    public function updateAd($id, $data, $status)
    {
        AdsOffer::where('id', $id)->update([
            'status' => $status,
            'city' => $data['city'],
            'street_name' => $data['street_name'],
            'rooms' => $data['rooms'],
            'house_number' => $data['house_number'],
            'house_type' => $data['house_type'],
            'communal' => $data['communal'],
            'sanitary_ware' => $data['sanitary_ware'],
            'wifi' => $data['wifi'],
            'tv' => $data['tv'],
            'parking' => $data['parking'],
            'balcony' => $data['balcony'],
            'garden' => $data['garden'],
            'security' => $data['security'],
            'elevator' => $data['elevator'],
            'jacuzzi' => $data['jacuzzi'],
            'boiler' => $data['boiler'],
            'gim' => $data['gim'],
            'furniture' => $data['furniture'],
            'appliances' => $data['appliances'],
            'rent_price' => $data['rent_price'],
            'add_pay' => $data['add_pay'],
            'room_meters' => $data['room_meters'],
            'available_from' => $data['available_from'],
            'prepayment' => $data['prepayment'],
            'gender' => $data['gender'],
            'occupation' => $data['occupation'],
            'age_from' => $data['age_from'],
            'age_to' => $data['age_to'],
            'children' => $data['children'],
            'couples' => $data['couples'],
            'guests' => $data['guests'],
            'pets' => $data['pets'],
            'smoking' => $data['smoking'],
            'floor' => $data['floor'],
            'ad_text' => $data['ad_text']
        ]);

        return true;
    }


    public function AdDeleteAndAllRecords($adId)
    {
        $adsOfferImages = is_numeric($adId) ?
            AdsOfferImage::where('ad_id', $adId)->get() :
            AdsOfferImage::whereIn('ad_id', $adId)->get();

        if ($adsOfferImages) {

            foreach ($adsOfferImages as $image) {
                FilesController::deleteImageFile($image->filename);
            }

        }

        is_numeric($adId) ? AdsOffer::destroy($adId) : AdsOffer::whereIn('id', $adId)->delete();
    }


}
