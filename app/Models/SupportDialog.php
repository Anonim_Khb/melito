<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;
use Jenssegers\Date\Date;

class SupportDialog extends Model
{


    protected $table = 'support_dialogs';


    protected $fillable = [
        'user_id',
        'category',
        'status',
        'agent_id',
        'watch'
    ];


    public function message()
    {
        return $this->hasOne('App\Models\SupportDialogMessage', 'dialog_id', 'id')->orderBy('created_at', 'desc');
    }


    public function messages()
    {
        return $this->hasMany('App\Models\SupportDialogMessage', 'dialog_id', 'id')->orderBy('created_at', 'asc');
    }


    public static function updateDialog($dialog_id, $array)
    {
        $dialog = SupportDialog::where('id', $dialog_id)->first();

        $dialog->update($array);
    }

}
