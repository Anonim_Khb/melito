<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class VerifyAd extends Model
{


    protected $table = 'verify_ads';


    protected $fillable = [
        'ad_id',
        'agent_id',
        'comment',
        'numbers'
    ];


}
