<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;

class SxgeoCountry extends Model
{


    protected $table = 'sxgeo_country';


    public static function getAllCountries()
    {
        userLocale() == 'ru' ?
            $countries = SxgeoCountry::orderBy('name_ru', 'asc')
                ->select('iso', 'name_ru as name')
                ->get() :
            $countries = SxgeoCountry::orderBy('name_en', 'asc')
                ->select('iso', 'name_en as name')
                ->get();

        return $countries;
    }


}
