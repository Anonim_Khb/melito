<?php namespace App\Models;

use App\Http\MyClass\LocationClass;
use Illuminate\Database\Eloquent\Model;
use DB;
use Request;

class SxgeoCity extends Model
{


    protected $table = 'sxgeo_cities';


    public function getRegion()
    {
        return $this->hasOne('\App\Models\SxgeoRegion', 'id', 'region_id');
    }


    public static function searchCities($query, $locale)
    {
        $result = SxgeoCity::where('name_' . $locale, 'LIKE', $query . '%')
            ->select('id')
            ->take(10)
            ->get();

        return $result;
    }


    public static function getCityRegionCountryById($cities, $locale)
    {
        $results = SxgeoCity::whereIn('id', $cities)
            ->with('getRegion.getCountry')
            ->take(10)
            ->get();

        foreach ($results as $result) {

            $popularCities[] = [

                'id' => $result['id'],
                'text' => $result['name_' . $locale],
                'country' => $result['getRegion']['getCountry']['name_' . $locale]

            ];

        }
        return isset($popularCities) ? $popularCities : null;
    }


    public static function getUserLocationCityRegionCountry($city)
    {
        $city = $city ? $city : '524901';

        $userCity = SxgeoCity::where('id', $city)
            ->select('id', 'name_' . userLocale() . ' as text')
            ->first();

        return $userCity;
    }


    public static function getCityNameById($cityId, $locale)
    {
        $cityName = SxgeoCity::where('id', $cityId)->pluck('name_' . $locale);

        return $cityName;
    }


    public static function getCitiesByRegionId($region_id)
    {
        userLocale() == 'ru' ?
            $cities = SxgeoCity::where('region_id', $region_id)
                ->orderBy('name_ru', 'asc')
                ->select('id', 'name_ru as name')
                ->get() :
            $cities = SxgeoCity::where('region_id', $region_id)
                ->orderBy('name_en', 'asc')
                ->select('id', 'name_en as name')
                ->get();

        return $cities;
    }


}
