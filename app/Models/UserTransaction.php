<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserTransaction extends Model
{


    protected $table = 'user_transactions';

    protected $fillable = [
        'user_id',
        'ad_id',
        'good_name',
        'number',
        'sum',
        'description'
    ];


    public static function addTransaction($user_id, $ad_id, $good, $number, $sum, $description)
    {
        UserTransaction::create([
            'user_id' => $user_id,
            'ad_id' => $ad_id,
            'good_name' => $good,
            'number' => $number,
            'sum' => $sum,
            'description' => $description,
        ]);
    }

}
