<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Input;

class AdsOfferTmpImage extends Model
{


    protected $table = 'ads_offer_tmp_images';

    protected $fillable = [
        'hash',
        'filename'
    ];


    public static function createNew($hash, $filename)
    {
        AdsOfferTmpImage::create([
            'hash' => $hash,
            'filename' => $filename,
        ]);
    }


}
