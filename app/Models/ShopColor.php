<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShopColor extends Model
{


    protected $table = 'shop_colors';


    protected $fillable = [
        'name',
        'color'
    ];


}
