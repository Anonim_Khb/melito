<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Jenssegers\Date\Date;

class Gift extends Model
{


    protected $table = 'gifts';


    protected $fillable = [
        'creator_id',
        'good_name',
        'valid_to',
        'text'
    ];


    public function receivedGifts()
    {
        return $this->hasOne('App\Models\ReceivedGift', 'gift_id', 'id');
    }

}
