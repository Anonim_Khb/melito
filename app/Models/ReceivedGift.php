<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Jenssegers\Date\Date;

class ReceivedGift extends Model
{


    protected $table = 'received_gifts';


    protected $fillable = [
        'gift_id',
        'user_id'
    ];

}
