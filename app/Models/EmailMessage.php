<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Jenssegers\Date\Date;

class EmailMessage extends Model
{


    protected $table = 'email_messages';

    protected $fillable = [
        'user_id',
        'ad_id',
    ];


}
