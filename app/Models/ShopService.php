<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShopService extends Model
{


    protected $table = 'shop_services';


    protected $fillable = [
        'name',
        'price'
    ];


}
