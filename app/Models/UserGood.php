<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserGood extends Model
{


    protected $table = 'user_goods';


    protected $fillable = [
        'user_id',
        'auto_up',
        'color',
        'ad_limit',
        'top'
    ];


    public static function addGoods($user_id, $good, $number)
    {

        $addGood = UserGood::firstOrNew(['user_id' => $user_id]);

        if (in_array($good, ['extra', 'professional', 'standard', 'mini'])) {

            $package = ShopPackage::where('name', $good)->first();

            $addGood->auto_up += $package->auto_up * $number;
            $addGood->color += $package->color * $number;
            $addGood->ad_limit += $package->ad_limit * $number;
            $addGood->top += $package->top * $number;
            $addGood->vip += $package->vip * $number;

        } else {

            $addGood->$good += $number;

        }

        $addGood->save();
    }


}
