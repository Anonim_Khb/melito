<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserSetting extends Model {


  protected $table = 'user_settings';

  protected $fillable = [
    'user_id'
  ];


  public static function createNew($user_id)
  {
    UserSetting::firstOrCreate([
      'user_id' => $user_id,
    ]);
  }

}
