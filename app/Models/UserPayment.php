<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserPayment extends Model
{


    protected $table = 'user_payments';


    protected $fillable = [
        'user_id',
        'signature',
        'sum',
        'status',
    ];


}
