<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdBookmark extends Model
{


    protected $table = 'ad_bookmarks';


    protected $fillable = [
        'user_id',
        'ad_id'
    ];


    public function myAds()
    {
        return $this->belongsTo('App\Models\AdsOffer', 'ad_id', 'id');
    }


}
