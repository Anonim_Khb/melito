<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdDeleteTransaction extends Model
{


    protected $table = 'ad_delete_transactions';


    protected $fillable = [
        'ad_id',
        'agent_id',
        'comment'
    ];


}
