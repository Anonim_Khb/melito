<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Jenssegers\Date\Date;

class AdsPrivilege extends Model
{


    protected $table = 'ads_privileges';


    protected $fillable = [
        'user_id',
        'ad_id',
        'auto_up',
        'last_auto_up',
        'color',
        'color_selected',
        'top',
        'vip'
    ];


    public function parentAds()
    {
        return $this->belongsTo('App\Models\AdsOffer', 'ad_id', 'id');
    }


    public static function getRandomPrivilegesAds($city, $privilegeName, $user_id, $sum)
    {
        $adIds = AdsPrivilege::where($privilegeName, '>', Date::now())
            ->with([
                'parentAds.bookmark' => function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                },
                'parentAds.image' => function ($query) {
                    $query->where('title', 1);
                },
            ])
            ->whereHas('parentAds', function ($query) use ($city) {
                $query->where('city', $city);
            })->orderByRaw('RAND()')->take($sum)->get();

        return $adIds;
    }


    public static function serviceActivated($user_id, $ad_id, $goodName, $sum, $color)
    {
        $addPriv = AdsPrivilege::firstOrNew(['ad_id' => $ad_id]);

        $addPriv->user_id = $user_id;

        if ($goodName == 'color') {

            $selectedColor = ShopColor::where('name', $color)->pluck('color');

            $addPriv->color_selected = $selectedColor;

        }

        if ($goodName == 'auto_up') {

            $sum = $sum * config('shop.autoUpInterval');

        }

        $addPriv->$goodName = $addPriv->$goodName < Date::now() ?
            Date::parse(null)->add('' . $sum . 'Days') :
            Date::parse($addPriv->$goodName)->add('' . $sum . 'Days');

        $addPriv->save();
    }


}
