<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Input;

class AdsOfferImage extends Model
{


    protected $table = 'ads_offer_images';

    protected $fillable = [
        'ad_id',
        'filename'
    ];


    public function adOffer()
    {
        return $this->belongsTo('App\Models\AdsOffer', 'ad_id', 'id');
    }


    public static function setAdImageTitle($ad_id, $image)
    {
        AdsOfferImage::where('ad_id', $ad_id)
            ->where('filename', $image)
            ->update(['title' => 1]);
    }


    public static function newAdImageTitle($adId)
    {
        $result = AdsOfferImage::where('ad_id', $adId)
            ->first();

        $result ? $result->increment('title') : '';
    }


}
