<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;

class SxgeoRegion extends Model
{


    protected $table = 'sxgeo_regions';


    public function getCountry()
    {
        return $this->hasOne('\App\Models\SxgeoCountry', 'iso', 'country');
    }


    public static function getRegionsByCountryIso($country_iso)
    {
        userLocale() == 'ru' ?
            $regions = SxgeoRegion::where('country', $country_iso)
                ->orderBy('name_ru', 'asc')
                ->select('id', 'name_ru as name')
                ->get() :
            $regions = SxgeoRegion::where('country', $country_iso)
                ->orderBy('name_en', 'asc')
                ->select('id', 'name_en as name')
                ->get();

            return $regions;
    }


}
