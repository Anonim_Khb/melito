<?php

namespace App\Listeners;

use App\Events\RegisteredAndAuth;
use App\Http\MyClass\MailerClass;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisteredAndSendEmail  $event
     * @return void
     */
    public function handle(RegisteredAndAuth $event)
    {
        MailerClass::welcome($event->user);
    }
}
