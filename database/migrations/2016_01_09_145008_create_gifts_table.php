<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsTable extends Migration
{
    public function up()
    {
        Schema::create('gifts', function ($table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->string('good_name');
            $table->string('text');
            $table->timestamp('valid_to');
            $table->timestamps();
            $table->index(['valid_to']);
        });
    }

    public function down()
    {
        Schema::drop('gifts');
    }
}