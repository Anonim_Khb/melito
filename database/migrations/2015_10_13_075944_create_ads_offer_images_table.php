<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsOfferImagesTable extends Migration
{

    public function up()
    {
        Schema::create('ads_offer_images', function ($table) {
            $table->increments('id');
            $table->integer('ad_id')->unsigned();
            $table->string('filename');
            $table->boolean('title')->default(false);
            $table->timestamps();

            $table->index(['ad_id']);
            $table->foreign('ad_id')->references('id')->on('ads_offers')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('ads_offer_images');
    }

}
