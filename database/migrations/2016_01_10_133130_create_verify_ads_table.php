<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifyAdsTable extends Migration
{
    public function up()
    {
        Schema::create('verify_ads', function ($table) {
            $table->increments('id');
            $table->integer('ad_id')->unsigned();
            $table->integer('agent_id');
            $table->string('comment');
            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads_offers')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('verify_ads');
    }
}
