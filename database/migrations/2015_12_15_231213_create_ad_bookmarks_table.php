<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdBookmarksTable extends Migration
{
    public function up()
    {
        Schema::create('ad_bookmarks', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ad_id')->unsigned();
            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads_offers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->index(['ad_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::drop('ad_bookmarks');
    }
}
