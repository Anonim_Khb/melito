<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('group_id')->default('1');
            $table->string('status')->default('pending');
            $table->timestamp('confirm_register')->nullable();
            $table->timestamp('password_reset')->nullable();
            $table->string('hash')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('users');
    }
}
