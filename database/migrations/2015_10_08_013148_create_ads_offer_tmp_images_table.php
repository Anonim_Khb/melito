<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsOfferTmpImagesTable extends Migration
{


    public function up()
    {
        Schema::create('ads_offer_tmp_images', function ($table) {
            $table->increments('id');
            $table->string('hash');
            $table->string('filename');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ads_offer_tmp_images');
    }


}
