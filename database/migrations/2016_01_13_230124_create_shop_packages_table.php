<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPackagesTable extends Migration
{
    public function up()
    {
        Schema::create('shop_packages', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('ad_limit');
            $table->integer('color');
            $table->integer('auto_up');
            $table->integer('top');
            $table->integer('vip');
            $table->integer('price');
            $table->integer('discount');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('shop_packages');
    }
}
