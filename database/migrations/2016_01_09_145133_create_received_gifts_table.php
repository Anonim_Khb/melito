<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedGiftsTable extends Migration
{
    public function up()
    {
        Schema::create('received_gifts', function ($table) {
            $table->increments('id');
            $table->integer('gift_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->index(['user_id', 'gift_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('gift_id')->references('id')->on('gifts')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('received_gifts');
    }
}
