<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportDialogMessagesTable extends Migration
{

    public function up()
    {
        Schema::create('support_dialog_messages', function ($table) {
            $table->increments('id');
            $table->integer('dialog_id')->unsigned();
            $table->integer('author_id')->nullable();
            $table->text('text');
            $table->timestamps();

            $table->foreign('dialog_id')->references('id')->on('support_dialogs')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('support_dialog_messages');
    }

}
