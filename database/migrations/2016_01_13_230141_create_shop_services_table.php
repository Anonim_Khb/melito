<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopServicesTable extends Migration
{
    public function up()
    {
        Schema::create('shop_services', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('shop_services');
    }
}
