<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('user_profiles', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unique()->unsigned();
            $table->string('name')->nullable();
            $table->string('avatar')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone')->nullable();
            $table->string('skype')->nullable();
            $table->boolean('whatsapp')->default(false);
            $table->boolean('viber')->default(false);
            $table->boolean('messenger')->default(false);
            $table->string('contact_another')->nullable();
            $table->string('alcohol')->nullable();
            $table->string('smoke')->nullable();
            $table->string('criminal')->nullable();
            $table->string('work')->nullable();
            $table->text('additional')->nullable();
            $table->integer('balance')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_profiles');
    }
}
