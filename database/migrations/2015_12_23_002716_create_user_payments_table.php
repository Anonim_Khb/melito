<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentsTable extends Migration
{

    public function up()
    {
        Schema::create('user_payments', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('signature');
            $table->string('hash');
            $table->integer('sum');
            $table->string('status')->default('wait');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_payments');
    }

}
