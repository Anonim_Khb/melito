<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopColorsTable extends Migration
{
    public function up()
    {
        Schema::create('shop_colors', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('shop_colors');
    }
}
