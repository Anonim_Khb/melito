<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGoodsTable extends Migration
{
    public function up()
    {
        Schema::create('user_goods', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unique()->unsigned();
            $table->integer('auto_up')->default(0);
            $table->integer('color')->default(0);
            $table->integer('ad_limit')->default(0);
            $table->integer('top')->default(0);
            $table->integer('vip')->default(0);
            $table->timestamps();

            $table->index(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_goods');
    }
}
