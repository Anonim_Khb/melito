<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportDialogsTable extends Migration
{

    public function up()
    {
        Schema::create('support_dialogs', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('category');
            $table->string('status')->default('wait');
            $table->integer('agent_id')->nullable();
            $table->string('watch')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('support_dialogs');
    }

}
