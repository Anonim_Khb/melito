<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{

    public function up()
    {
        Schema::create('notifications', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('text');
            $table->string('icon_src');
            $table->string('url')->nullable();
            $table->timestamp('valid_to')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'valid_to']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('notifications');
    }

}
