<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdDeleteTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('ad_delete_transactions', function ($table) {
            $table->increments('id');
            $table->integer('ad_id');
            $table->integer('agent_id');
            $table->string('comment');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ad_delete_transactions');
    }
}
