<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsOffersTable extends Migration
{
    public function up()
    {
        Schema::create('ads_offers', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('status')->default('wait');
            $table->integer('city');
            $table->string('street_name');
            $table->string('house_number');
            $table->string('house_type');
            $table->string('deal_type');
            $table->integer('rooms')->nullable();
            $table->string('communal')->nullable();
            $table->integer('floor')->nullable();
            $table->string('sanitary_ware')->nullable();
            $table->boolean('wifi')->default(false);
            $table->boolean('tv')->default(false);
            $table->boolean('parking')->default(false);
            $table->boolean('balcony')->default(false);
            $table->boolean('garden')->default(false);
            $table->boolean('security')->default(false);
            $table->boolean('elevator')->default(false);
            $table->boolean('jacuzzi')->default(false);
            $table->boolean('boiler')->default(false);
            $table->boolean('gim')->default(false);
            $table->boolean('furniture')->default(false);
            $table->boolean('appliances')->default(false);
            $table->integer('rent_price');
            $table->integer('add_pay');
            $table->integer('room_meters');
            $table->date('available_from');
            $table->string('prepayment')->nullable();
            $table->string('gender')->nullable();
            $table->string('occupation')->nullable();
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->boolean('children')->default(false);
            $table->boolean('couples')->default(false);
            $table->boolean('guests')->default(false);
            $table->boolean('pets')->default(false);
            $table->boolean('smoking')->default(false);
            $table->text('ad_text');
            $table->integer('count')->default(0);
            $table->timestamps();

            $table->index(['status', 'user_id', 'city']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('ads_offers');
    }
}