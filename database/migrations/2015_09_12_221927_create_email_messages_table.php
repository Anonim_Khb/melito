<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailMessagesTable extends Migration
{
    public function up()
    {
        Schema::create('email_messages', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ad_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('email_messages');
    }
}
