<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsPrivilegesTable extends Migration
{
    public function up()
    {
        Schema::create('ads_privileges', function ($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('ad_id')->unique()->unsigned();
            $table->timestamp('auto_up')->nullable();
            $table->timestamp('last_auto_up')->nullable();
            $table->timestamp('color')->nullable();
            $table->string('color_selected')->nullable();
            $table->timestamp('top')->nullable();
            $table->timestamp('vip')->nullable();
            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads_offers')->onDelete('cascade');
            $table->index(['ad_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::drop('ads_privileges');
    }
}
