<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('user_transactions', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ad_id')->nullable();
            $table->string('good_name')->nullable();
            $table->integer('number')->nullable();
            $table->integer('sum')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();

            $table->index(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_transactions');
    }
}