<?php

use Illuminate\Database\Seeder;

class ShopColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_colors')->insert([
            'name' => 'color_1',
            'color' => '#EAFC89',
        ]);
        DB::table('shop_colors')->insert([
            'name' => 'color_2',
            'color' => '#FFD0AB',
        ]);
        DB::table('shop_colors')->insert([
            'name' => 'color_3',
            'color' => '#B7FFAB',
        ]);
        DB::table('shop_colors')->insert([
            'name' => 'color_4',
            'color' => '#ABE1FF',
        ]);
        DB::table('shop_colors')->insert([
            'name' => 'color_5',
            'color' => '#B0ABFF',
        ]);
        DB::table('shop_colors')->insert([
            'name' => 'color_6',
            'color' => '#E2ABFF',
        ]);
    }
}
