<?php

use Illuminate\Database\Seeder;

class ShopPackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_packages')->insert([
            'name' => 'extra',
            'auto_up' => 50,
            'color' => 30,
            'ad_limit' => 20,
            'top' => 7,
            'vip' => 5,
            'price' => 1950,
            'discount' => 25,
        ]);
        DB::table('shop_packages')->insert([
            'name' => 'professional',
            'auto_up' => 35,
            'color' => 20,
            'ad_limit' => 15,
            'top' => 5,
            'vip' => 3,
            'price' => 1490,
            'discount' => 21,
        ]);
        DB::table('shop_packages')->insert([
            'name' => 'standard',
            'auto_up' => 20,
            'color' => 10,
            'ad_limit' => 7,
            'top' => 3,
            'vip' => 1,
            'price' => 745,
            'discount' => 18,
        ]);
        DB::table('shop_packages')->insert([
            'name' => 'mini',
            'auto_up' => 10,
            'color' => 5,
            'ad_limit' => 3,
            'top' => 1,
            'vip' => 0,
            'price' => 335,
            'discount' => 13,
        ]);
    }
}