<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ShopServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_services')->insert([
            'name' => 'auto_up',
            'price' => 15,
        ]);
        DB::table('shop_services')->insert([
            'name' => 'ad_limit',
            'price' => 20,
        ]);
        DB::table('shop_services')->insert([
            'name' => 'color',
            'price' => 25,
        ]);
        DB::table('shop_services')->insert([
            'name' => 'top',
            'price' => 50,
        ]);
        DB::table('shop_services')->insert([
            'name' => 'vip',
            'price' => 70,
        ]);
    }
}