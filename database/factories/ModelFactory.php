<?php

use Jenssegers\Date\Date;

$factory->define(App\User::class, function ($faker) {
    return [
        'email' => $faker->email,
        'password' => bcrypt('zzzzzz'),
        'hash' => str_random(10),
        'password_reset' => Date::now()->subHours(25),
        'group_id' => '1',
        'status' => 'pending',
        'confirm_register' => null,
        'remember_token' => null,
        'created_at' => Date::now(),
        'updated_at' => Date::now(),
    ];
});


$factory->define(\App\Models\UserSetting::class, function ($faker) {
    return [
        // ....
    ];
});


$factory->define(\App\Models\UserProfile::class, function ($faker) {
    return [
//        'user_id' => '',
        'name' => $faker->name,
        'avatar' => null,
        'gender' => 'male',
        'birthday' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'phone' => $faker->phoneNumber,
        'skype' => $faker->domainName,
        'whatsapp' => '1',
        'viber' => '1',
        'messenger' => '1',
        'contact_another' => $faker->address . $faker->phoneNumber,
        'alcohol' => 'no',
        'smoke' => 'no',
        'criminal' => 'no',
        'work' => 'yes',
        'additional' => $faker->text,
    ];
});


$factory->define(\App\Models\AdsOffer::class, function ($faker) {
    return [
        'user_id' => '999999',
        'status' => 'active',
        'city' => '2022890',
        'street_name' => 'Ким-Ю-Чена',
        'house_number' => 'дом 5 / корп 2',
        'house_type' => 'studio',
        'communal' => 'single',
        'sanitary_ware' => 'home',
        'wifi' => '0',
        'tv' => '0',
        'parking' => '1',
        'balcony' => '1',
        'garden' => '1',
        'floor' => '1',
        'security' => '1',
        'elevator' => '1',
        'jacuzzi' => '1',
        'boiler' => '1',
        'gim' => '1',
        'furniture' => '1',
        'appliances' => '1',
        'rent_price' => '5100',
        'add_pay' => '510',
        'room_meters' => '51',
        'available_from' => '2050-01-01',
        'prepayment' => '6+',
        'gender' => 'not-matter',
        'occupation' => 'professional',
        'age_from' => '25',
        'age_to' => '75',
        'children' => '1',
        'couples' => '1',
        'guests' => '1',
        'pets' => '1',
        'smoking' => '1',
        'ad_text' => $faker->text,
    ];
});