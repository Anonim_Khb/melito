var loc = new Vue({

    el: '#adsOffer',

    data: {
        model: {
            deal_type: '',
            add_pay: false,
            errors: [],
            btnDisable: false,
            photos: [],
            selectImagesTitle: '',
            imagesTitle: '',
            loading: false,
            urlForImagesMini: '/melito_img/users_img/ads_offer_mini/',
        },
    },
    methods: {
        deleteImage: function (photo) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/ads/delete-image',
                data: {image: photo.filename, _token: CSRF_TOKEN},
                cache: false,
                success: function (data) {
                    if (data == 'OK') {
                        loc.model.photos.$remove(photo);

                        if (loc.model.imagesTitle == photo.filename) {
                            if (loc.model.photos != '') {
                                loc.model.imagesTitle = loc.model.photos[0].filename;
                            } else {
                                loc.model.imagesTitle = '';
                            }
                        }
                    }
                }
            });
        },
        sendForm: function () {
            loc.model.loading = true;

            $.post('/ads/new/offerForm', $('#adsOfferForm').serialize(), function (data) {
                if (!$.isNumeric(data)) {
                    loc.model.loading = false;
                    $('html, body').animate({
                        scrollTop: $("#validationErrors").offset().top - 85
                    }, 1000);
                    loc.model.errors = data;
                } else {
                    window.location.replace('/offer/' + data);
                }
            });
        },
        imagesUpload: function () {
            var adHash = $('#adHash').val();
            $('#fileUpload').fileupload({
                singleFileUploads: true,
                //maxNumberOfFiles: 10, //NOT WORKING
                maxFileSize: 10000000, // 10 MB
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                url: '/ads/offer/images',
                dataType: 'json',
                autoUpload: true,
                add: function (e, data) {
                    $.each(data.files, function (index, file) {
                        if ($.inArray(file.type, ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/bmp']) != -1) {
                            data.submit();
                        } else {
                            alert('Формат файла или его вес не соответствуют требованиям\n\rFile Format or its weight does not comply')
                        }
                    });
                },
                start: function (e, data) {
                    loc.model.btnDisable = true;
                    loc.model.loading = true;
                },
                stop: function (e, data) {
                    loc.model.btnDisable = false;
                    loc.model.loading = false;
                },
                submit: function (e, data) {
                    data.formData = {
                        _token: CSRF_TOKEN,
                        hash: adHash
                    };
                },
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        loc.model.photos.push(file);
                        loc.selectDefaultImagesTitle(file);
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        },
        selectImagesTitle: function (photo) {
            if (photo.status != 'text-danger') {
                loc.model.imagesTitle = photo.filename;
            }
        },
        selectDefaultImagesTitle: function (file) {
            if (loc.model.imagesTitle == '' && file.status != 'text-danger') {
                loc.model.imagesTitle = file.filename;
            }
        }
    },
    ready: function () {
        $(document).ready(function () {

            $.SendFormEnterBtnDisabled('#adsOfferForm');

            changeDealType($('input[name="deal_type"]:checked').val());

            $('input[name="deal_type"]').change(function () {
                changeDealType($(this).val());
            })

            function changeDealType(value) {
                loc.model.deal_type = value;
            }

        });
    }
});