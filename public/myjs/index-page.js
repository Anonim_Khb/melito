$(document).ready(function () {

    /*SEARCH FORM*/
    /*Scroll*/
    $('#searchBtnHeader').on('click', function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top - $('#myHeader .navbar').height()
        }, 1000);
    });

/*-------------------------------------------------------------------->>>>>>*/
    /*Price Slider*/
    var sliderPrice = document.getElementById('ruble');

    var priceValues = [
        document.getElementById('rubleMin'),
        document.getElementById('rubleMax')
    ];

    var priceRentMin = $('#priceMinMax').data('rent-min');
    var priceRentMax = $('#priceMinMax').data('rent-max');

    noUiSlider.create(sliderPrice, {
        start: [priceRentMin, priceRentMax],
        behaviour: 'drag',
        connect: true,
        range: {
            'min': priceRentMin,
            'max': priceRentMax
        },
        step: 1000
    });

    sliderPrice.noUiSlider.on('update', function (values, handle) {
        priceValues[handle].value = Math.round(values[handle]);
    });


    /*Area Slider*/
    var sliderArea = document.getElementById('area');

    var areaValues = [
        document.getElementById('areaMin'),
        document.getElementById('areaMax')
    ];

    var areaMin = $('#areaMinMax').data('area-min');
    var areaMax = $('#areaMinMax').data('area-max');

    noUiSlider.create(sliderArea, {
        start: [areaMin, areaMax],
        behaviour: 'drag',
        connect: true,
        range: {
            'min': areaMin,
            'max': areaMax
        },
        step: 1
    });

    sliderArea.noUiSlider.on('update', function (values, handle) {
        areaValues[handle].value = Math.round(values[handle]);
    });


    /*Input Date - User Birthday*/
    $('.input-group.entry-date').datepicker({
        format: "yyyy-mm-dd",
        startDate: $('#entryDate').data('start'),
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        language: $('#entryDate').data('locale'),
        todayBtn: 'linked',
    });

    /*Change Rent or Sale*/
    $('select[name="deal_type"]').on('change', function () {

        if ($('option:selected', this).attr('value') == 'sale') {
            $('.only-rent').attr('disabled', 'disabled');

            var priceSaleMin = $('#priceMinMax').data('sale-min');
            var priceSaleMax = $('#priceMinMax').data('sale-max');

            sliderPrice.noUiSlider.updateOptions({
                range: {
                    'min': priceSaleMin,
                    'max': priceSaleMax
                }
            });
            sliderPrice.noUiSlider.set([priceSaleMin, priceSaleMax]);

        } else {
            $('.only-rent').removeAttr('disabled');

            sliderPrice.noUiSlider.updateOptions({
                range: {
                    'min': priceRentMin,
                    'max': priceRentMax
                }
            });
            sliderPrice.noUiSlider.set([priceRentMin, priceRentMax]);

        }

    });
    /*<<<<<<--------------------------------------------------------------------*/

    /*SLICK CAROUSEL*/
    $('#indexVipAds').slick({
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                }
            },
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('#indexLastAds').slick({
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                }
            },
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('#indexPerformance').slick({
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        infinite: true,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

});
