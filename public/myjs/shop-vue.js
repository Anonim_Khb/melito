var shop = new Vue({

    el: '#shopPage',

    data: {
        model: {
            purchaseValue: '',
            purchaseName: '',
            purchaseNumber: 1,
            purchasePrice: '',
            sum: '',
        },
    },
    methods: {
        openModal: function (value, name, price) {
            shop.model.sum = '';
            shop.model.purchaseNumber = 1;
            shop.model.purchaseValue = value;
            shop.model.purchaseName = name;
            shop.model.purchasePrice = price;
        }
    }
});


$(document).ready(function () {

    $('.show-services-description').on('click', function(){

        $(this).siblings('.services-description').toggle('normal');

    });

});