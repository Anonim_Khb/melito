var adWatch = new Vue({

    el: '#adsWatch',

    data: {
        model: {
            firstImage: '',
            images: '',
            pathToImages: '/melito_img/users_img/ads_offer/',
            pathToImagesMini: '/melito_img/users_img/ads_offer_mini/',
            pathToDefaultImages: '/melito_img/users_img/ads_default/',
            defaultImage: 'ads_default.png',
        },
    },
    methods: {},
    ready: function () {
        $(document).ready(function () {

            $('.fancybox').fancybox();

            $('.owl-carousel').owlCarousel({
                nav: true,
                margin: 10,
                items: 3,
                dots: false,
            });

            if(adImages == '[]'){
                adWatch.model.firstImage = adWatch.model.defaultImage;
            } else {
                adWatch.model.images = JSON.parse(adImages);
                adWatch.model.firstImage = adWatch.model.images[0]['filename'];
            }

        });
    }
});

$(document).ready(function () {

    $('#imageLine').on('click', 'img', function () {

        adWatch.model.firstImage = $(this).attr('image-attr');

    });

});