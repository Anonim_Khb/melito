var adminShop = new Vue({

    el: '#adminShopPage',

    data: {
        model: {
            auto_up: '',
            ad_limit: '',
            color: '',
            top: '',
            vip: '',
        },
    },
    methods: {
        updateService: function (serviceName, price) {
            $.post('/admin/shop/update/service', {
                price: price,
                service_name: serviceName,
                _token: CSRF_TOKEN
            }).then(function (data) {
                $.AlertCorner(data);
                if (data.status == 'OK') {
                    adminShop.packageSum(null);
                }
            }, function (data) {
                alert('SOMETHING ERROR');
            });
        },
        updatePackage: function (packageName) {
            var packageSum = adminShop.packageSum(packageName);

            var formAction = $('form[id=' + packageName + ']').data('action');

            $.post(formAction, $('#' + packageName + '').serialize(), function (data) {
                $('.' + packageName + ' .thumbnail').css('border', '.1em solid #d6d6d6');
                $.AlertCorner(data);
            });
        },
        updateColor: function () {
            $.post('/admin/shop/update/colors', $('#adminColorUpdate').serialize(), function (data) {
                $.AlertCorner(data);
            });
        },
        packageSum: function (packageName) {
            if (packageName != null) {
                adminShop.onePackageSum(packageName);
            } else {
                adminShop.onePackageSum('extra');
                adminShop.onePackageSum('professional');
                adminShop.onePackageSum('standard');
                adminShop.onePackageSum('mini');
            }
        },
        onePackageSum: function (packageName) {
            var color = $('.' + packageName + ' input[name="color"]').val();
            var auto_up = $('.' + packageName + ' input[name="auto_up"]').val();
            var ad_limit = $('.' + packageName + ' input[name="ad_limit"]').val();
            var vip = $('.' + packageName + ' input[name="vip"]').val();
            var top = $('.' + packageName + ' input[name="top"]').val();

            var sum = (color * adminShop.model.color) + (auto_up * adminShop.model.auto_up) + (ad_limit * adminShop.model.ad_limit) + (vip * adminShop.model.vip) + (top * adminShop.model.top);

            $('.' + packageName + ' .real-price').val(sum);
            adminShop.discountPercents(packageName, sum);
        },
        discountPercents: function (packageName, sum) {
            var priceSelling = $('.' + packageName + ' input[name="price"]').val();
            var discount = Math.round((sum - priceSelling) / (sum / 100));
            $('.' + packageName + ' .package-discount').val(discount);
        },
    }
});
$(document).ready(function () {

    adminShop.packageSum(null);

    /*Services*/
    $('.shop-services input').on('input', function () {
        $(this).siblings('button').css('color', 'red');
    });

    $('.shop-services button').on('click', function () {
        $(this).css('color', 'white');
    });

    /*Packages*/
    $('.shop-packages input').on('input', function () {
        $(this).closest('.thumbnail').css('border', '.1em solid red');
        var changePackage = $(this).closest('form').attr('id');
        adminShop.onePackageSum(changePackage);
    });

});