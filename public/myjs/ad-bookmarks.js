$(document).ready(function () {

    /*ADD NEW BOOKMARK*/
    $('.ad-bookmark').on('click', function () {

        var ad_id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "text",
            url: '/ads/add-bookmark',
            data: {id: ad_id, _token: CSRF_TOKEN},
            cache: false,
            success: function (data) {
                var MyData = JSON.parse(data);
                if(MyData.status == 'added'){
                    $('i[data-id=' + ad_id + ']').removeClass('bookmark-star-false').addClass('bookmark-star-true');
                } else {
                    $('i[data-id=' + ad_id + ']').removeClass('bookmark-star-true').addClass('bookmark-star-false');
                }
                $.AlertCorner(MyData);
            }
        });

    });


    //DELETE ONE BOOKMARK (ACCOUNT/BOOKMARK)
    $('.account-bookmark-delete').on('click', function () {

        $(this).closest('.ad-block').slideUp();

        var bookmarkId = $(this).attr('attr-id');

        $.ajax({
            type: "POST",
            dataType: "text",
            url: '/ads/delete-one-bookmark',
            data: {id: bookmarkId, _token: CSRF_TOKEN},
            cache: false,
            success: function (data) {
                var MyData = JSON.parse(data);
                $.AlertCorner(MyData);
            }
        });

    });

    //DELETE ALL BOOKMARKS (ACCOUNT/BOOKMARK)
    $('#accountAllBookmarkDelete').on('click', function () {

        $('.ad-block').slideUp();

        $.ajax({
            type: "POST",
            dataType: "text",
            url: '/ads/delete-all-bookmarks',
            data: {_token: CSRF_TOKEN},
            cache: false,
            success: function (data) {
                var MyData = JSON.parse(data);
                $.AlertCorner(MyData);
            }
        });

        $('#accountDeleteAllBookmarks').hide();

        $('#noneBookmarks').show();

    });

});