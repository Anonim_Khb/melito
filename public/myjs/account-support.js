$(document).ready(function () {

    //CLICK TABLE ROW AND REDIRECT
    $('.support-row-link').click(function () {
        window.document.location = $(this).data('href');
    });

    //SCROLL TO BOTTOM
    if(typeof $('#userDialogMessages')[0] !== "undefined"){
        $('#userDialogMessages').scrollTop(
            $('#userDialogMessages')[0].scrollHeight
        );
    }

});