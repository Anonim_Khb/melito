$(document).ready(function () {

    function formatResult(myResult) {
        var $user = $(
            '<option value="' + myResult.id + '">' + myResult.id + ' | ' + myResult.text + '</option>'
        );
        return $user;
    };

    function resultSelection(selectData) {
        return selectData.text;
    };

    $(".select2-users-js").select2({
        placeholder: "Select User",
        multiple: false,
        ajax: {
            url: "/admin/find/user",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    myQuery: params.term,
                    page: params.page,
                    token: CSRF_TOKEN
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    pagination: {
                        more: (params.page * 10) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1,
        templateResult: formatResult,
        templateSelection: resultSelection
    });

});