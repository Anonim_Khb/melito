$(document).ready(function () {

    /*CLICK TABLE ROW AND REDIRECT*/
    $('.support-row-link').click(function () {
        window.document.location = $(this).data('href');
    });

    /*SCROLL TO BOTTOM*/
    if (typeof $('#adminPanelDialogMessages')[0] !== "undefined") {
        $('#adminPanelDialogMessages').scrollTop(
            $('#adminPanelDialogMessages')[0].scrollHeight
        );
    }

    /*UNBLOCK SUPPORT MESSAGE SENDING*/
    $('#adminSupportUnblockTextarea').on('click', function () {
        $('.admin-support-for-unblock').removeAttr('disabled');
    });


    /*NOTIFICATION ACTIVATED*/
    $('.admin-notification').on('click', function () {
        $('.admin-notification').css({'border': '.1em solid #d6d6d6'});
        $(this).css({'border': '.15em solid red'});
        $('#notificationName').attr('value', $(this).data('name'));
        $('#adminNotificationActivated').removeAttr('disabled');
    });

    $('.admin-delete-notification').on('click', function () {
        var notificationId = $(this).data('id');
        $.post('/admin/notification/delete', {
            notification_id: notificationId,
            _token: CSRF_TOKEN
        }).then(function (data) {

            $.AlertCorner(data);

            $('[data-id="' + notificationId + '"]').closest('.admin-active-notification').fadeOut('slow');

        }, function (data) {
            alert('SOMETHING ERROR');
        });

    });

    /*GIFTS ACTIVATED*/
    $('.admin-general-gifts').on('click', function () {
        $('.admin-general-gifts').css({'border': '.1em solid #dddddd'});
        $(this).css({'border': '.15em solid red'});
        $('#generalGiftName').attr('value', $(this).data('name'));
        $('#adminGiftActivated').removeAttr('disabled');
    });


    /*ADS Comments Show*/
    $('.admin-ads-show-comments').on('click', function(){
        var adId = $(this).data('id');
        $('.admin-ads-comments[data-id=' + adId + ']').slideToggle();
    });


    /*DELETE AD*/
    $('#adminAdDeleteConfirm').on('click', function () {
        var adId = $('#deletedAdId').val();
        var comment = $('#adminDeleteComment').val();

        $.post('/admin/ad/delete', {
            ad_id: adId,
            comment: comment,
            _token: CSRF_TOKEN
        }).then(function (data) {
            if(data == adId){
                window.location.replace('/');
            } else {
                alert("CONTROLLER ERROR");
            }
        }, function (data) {
            alert('SOMETHING ERROR');
        });
    });

});