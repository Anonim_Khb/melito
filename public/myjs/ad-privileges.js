$(document).ready(function () {

    $.SendFormEnterBtnDisabled('#AdPrivModalForm');

    $('.ads-priv-btns').on('click', function () {

        $('.priv-modal-block').hide();

        var privType = $(this).data('priv-type');

        $('#AdPrivModalSelect_' + privType + '').show();

        $('#PrivAdModalGoodName').val(privType);

        $('#PrivAdModalId').val($(this).siblings('.ad-id-for-modal').data('adid'));

        $('.priv-colors label:first').css('border', '.2em solid red');

        $('#modalPrivilegeActivate').modal('show');

    });

    $('#AdPrivModalSendForm').on('click', function () {

        $.post('/ads/add-privileges', $('#AdPrivModalForm').serialize(), function (data) {
            $.AlertCorner(data);
        });

    });

    $('.priv-colors label').on('click', function(){

        $('.priv-colors label').css('border', '.1em solid #ffffff');

        $(this).css('border', '.2em solid red');

    });

    /*Dropdown SUB-menu (Don't hide menu)*/
    $('.dropdown-menu, .dd-submenu-list').click(function(e) {
        e.stopPropagation();
    });
    $('.dd-submenu').on('click', function(){
        $(this).children('.dd-submenu-list').toggle();
    });

});