$(document).ready(function () {

    /*AVATAR*/

    var avatarCords;
    var imageName;

    /*Show Control Buttons*/
    $('.user-avatar-img').on('click mouseenter', function () {
        if (!$('#avatarControlButtons').is(":visible")) {
            $('#avatarControlButtons').slideDown('normal');
        }
    }).on('mouseleave', function () {
        if ($('#avatarControlButtons').is(":visible")) {
            $('#avatarControlButtons').slideUp('normal');
        }
    });

    /*Input Date - User Birthday*/
    $('.input-group.date').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 1,
        endDate: $('#userBirthday').data('end'),
        autoclose: true,
        todayHighlight: true,
        language: $('#userBirthday').data('locale')
    });

    function showCoords(c)
    {
        avatarCords = c;
    };

    /*Avatar Upload*/
    $('#fileUploadAvatar').on('click', function () {
        var uploadAvatar = $('#fileUpload').fileupload({
            singleFileUploads: true,
            //maxNumberOfFiles: 10, //NOT WORKING
            maxFileSize: 10000000,
            acceptFileTypes: /(\.|\/)(gif|bmp|jpe?g|png)$/i,
            url: '/account/avatar/upload-temp',
            dataType: 'json',
            autoUpload: true,
            add: function (e, data) {
                if ($.inArray(data.files[0].type, ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/bmp']) != -1) {
                    $('#loadingProcessGif').show();
                    data.submit();
                } else {
                    alert('Формат файла или его вес не соответствуют требованиям\n\rFile Format or its weight does not comply')
                }
            },
            start: function (e, data) {
                $('#gifLoading').show();
            },
            stop: function (e, data) {
                $('#gifLoading').hide();
            },
            submit: function (e, data) {
                data.formData = {
                    _token: CSRF_TOKEN,
                };
            },
            done: function (e, data) {
                if (data.result.status == 'path') {
                    imageName = data.result.message;
                    if($('#forCropImage').data('Jcrop')){
                        $('#forCropImage').data('Jcrop').destroy();
                    }
                    $('#forCropImage').removeAttr('style').attr('src', '/' + $('#forCropImage').data('path') + imageName);
                    $('#loadingProcessGif').hide();
                    $('#avatarCropModal').modal('show');

                    var modalWidth = $('#avatarCropModal').width();

                    $('#forCropImage').Jcrop({
                        aspectRatio:1,
                        onSelect: showCoords,
                        minSize: [100,100],
                        setSelect: [50,50,250,250],
                        bgOpacity:.4,
                        boxWidth: modalWidth,
                    });
                }
                if(data.result.status == 'error') {
                    $.AlertCorner(data.result);
                }
            },
        });
    });

    /*Avatar Send Crop Cords and Image Name*/
    $('#uploadAvatarAfterCrop').on('click', function(){
        $('#loadingProcessGif').show();
        $.post('/account/avatar/upload', {
            imageName: imageName,
            cordX: avatarCords.x,
            cordY: avatarCords.y,
            cordH: avatarCords.h,
            cordW: avatarCords.w,
            _token: CSRF_TOKEN
        }).then(function (data) {
            if (data.status) {
                d = new Date();
                $('#user_avatar').attr('src', '/' + data.status + '?' + d.getTime());
                $('#loadingProcessGif').hide();
                $.AlertCorner(data);
                $('#deleteAvatarBtn').attr('data-toggle', 'modal');
            }
        }, function (data) {
            $.AlertCorner(data);
        });
    })

    /*Avatar Delete*/
    $('#fileDeleteAvatar').on('click', function () {
        $.post('/account/avatar/delete', {
            _token: CSRF_TOKEN
        }).then(function (data) {
            if (data.status) {
                d = new Date();
                $('#user_avatar').attr('src', '/' + $('#fileDeleteAvatar').data('default-avatar') + '?' + d.getTime());
                $.AlertCorner(data);
                $('#deleteAvatarBtn').removeAttr('data-toggle');
            }
        }, function (data) {
            //ERROR
        });
    });

});