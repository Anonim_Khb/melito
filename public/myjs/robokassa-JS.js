$(document).ready(function () {
    $('#paymentSum').on('change input', function () {
        paymentSumMonitoring($(this).val());
    });

    function paymentSumMonitoring(sum) {
        if (!sum || sum < $('#paymentSum').data('minimal')) {
            $('#paymentSaleBtn').attr('disabled', 'disabled');
        } else {
            $('#paymentSaleBtn').removeAttr('disabled');
        }
    }
});
