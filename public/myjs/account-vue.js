var accAds = new Vue({

    el: '#accountAds',

    data: {
        MyData: '',
        imageCords: [],
        coverImageNameCrop: '',
    },
    methods: {
        upAdFree: function (adId) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/account/ads/up',
                data: {id: adId, _token: CSRF_TOKEN},
                cache: false,
                success: function (data) {
                    accAds.MyData = JSON.parse(data);
                    accAds.sendDataToAlertCornerFunction(accAds.MyData);
                }
            });
        },
        upAdPremium: function (adId) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/account/ads/up-premium',
                data: {id: adId, _token: CSRF_TOKEN},
                cache: false,
                success: function (data) {
                    accAds.MyData = JSON.parse(data);
                    accAds.sendDataToAlertCornerFunction(accAds.MyData);
                }
            });
        },
        hideOrShowAd: function (adId) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/account/ads/show-hide',
                data: {id: adId, _token: CSRF_TOKEN},
                cache: false,
                success: function (data) {
                    accAds.MyData = JSON.parse(data);
                    accAds.sendDataToAlertCornerFunction(accAds.MyData);

                    if (accAds.MyData.status == 'closed') {
                        $('[data-shid="' + adId + '"] li').hide();
                        $('[data-shid="' + adId + '"] li[data-condition="closed"]').show();
                    }
                    if (accAds.MyData.status == 'active') {
                        $('[data-shid="' + adId + '"] li').show();
                        $('[data-shid="' + adId + '"] li[data-condition="closed"]').hide();
                    }
                }
            });
        },
        sendDataToAlertCornerFunction: function (MyData) {
            $.AlertCorner(MyData);
        },
        adCoverImageCrop: function (adId) {
            $('#loadingProcessGif').show();

            $.post('/ads/image/cover-edit', {
                adId: adId,
                _token: CSRF_TOKEN
            }).then(function (data) {

                if (data.status == 'ok') {
                    accAds.coverImageNameCrop = data.message;
                    if ($('#forCropAdImage').data('Jcrop')) {
                        $('#forCropAdImage').data('Jcrop').destroy();
                    }
                    d = new Date();
                    $('#forCropAdImage').removeAttr('style').attr('src', '/' + $('#forCropAdImage').data('path') + accAds.coverImageNameCrop + '?' + d.getTime());
                    $('#loadingProcessGif').hide();
                    $('#adCoverCropModal').modal('show');

                    $('#forCropAdImage').Jcrop({
                        aspectRatio: 1,
                        onSelect: accAds.adCoverCropCords,
                        minSize: [75, 75],
                        setSelect: [50, 50, 100, 100],
                        bgOpacity: .4,
                    });

                } else {
                    $('#loadingProcessGif').hide();
                    $.AlertCorner(data);
                }

            }, function (data) {
                //alert('ERROR');
            });
        },
        adCoverCropCords: function (c) {
            accAds.imageCords = c;
        },
    },
    ready: function () {
        $(document).ready(function () {
            $('#uploadAdCoverAfterCrop').on('click', function(){
                $('#loadingProcessGif').show();
                $.post('/ads/image/cover-crop', {
                    imageName: accAds.coverImageNameCrop,
                    cordX: Math.round(accAds.imageCords.x),
                    cordY: Math.round(accAds.imageCords.y),
                    cordH: Math.round(accAds.imageCords.h),
                    cordW: Math.round(accAds.imageCords.w),
                    _token: CSRF_TOKEN
                }).then(function (data) {

                    if ($.isNumeric(data.status)) {
                        $('#loadingProcessGif').hide();
                        $.AlertCorner(data);

                        d = new Date();
                        $('[data-adId="' + data.status + '"]:first').css('background-image', 'url("/' + $('#forCropAdImage').data('path') + accAds.coverImageNameCrop + '?' + d.getTime() + '")');
                    } else {
                        $('#loadingProcessGif').hide();
                        $.AlertCorner(data);
                    }

                }, function (data) {
                    //alert('ERROR');
                });

            });
        });
    }
});