var needSearch = new Vue({

    el: '#adsNeed',

    data: {
        model: {
            deal_type: 'rent',
            timerOne: 0,
            timerTwo: 0,
            reloadPage: 10,
            reloadPageReset: 10,
        },
    },
    methods: {
        cleanFieldRadioCheckbox: function (id) {
            $('#' + id + ' input').prop('checked', false);
            needSearch.changeInputValue(id);
        },
        cleanFieldValue: function (id) {
            $('#' + id + ' input').val('');
            needSearch.changeInputValue(id);
        },
        changeURL: function () {
            window.history.pushState(
                null,
                null,
                'search?' + $('#adsNeedForm').serialize()
            );
        },
        changeInputValue: function (fieldName) {

            needSearch.clearTimeoutAndInterval();

            if($(window).width() > 767) {

                var heightClick = $('#' + fieldName + '').position().top;

                $('#SearchAdsFormButton').css({'display':'block','top':heightClick});

            }

            $('#SearchAdsInfoBlock').css({'display':'block'});

            needSearch.model.timerTwo = setInterval(function(){
                needSearch.model.reloadPage -= 1;
            }, 1000);

            needSearch.model.timerOne = setTimeout(function () {
                needSearch.changeURL();
                location.reload();
            }, 10000);


        },
        clearTimeoutAndInterval: function () {
            clearInterval(needSearch.model.timerTwo);
            needSearch.model.timerTwo = null
            needSearch.model.reloadPage = needSearch.model.reloadPageReset;

            clearTimeout(needSearch.model.timerOne);

            $('#SearchAdsInfoBlock').css({'display':'none'});
        },
    },
    ready: function () {
        $(document).ready(function () {

            $('select').on('change', function () {

                needSearch.changeInputValue('adsCity');

            });

            needSearch.changeURL();

            changeDealType($('input[name="deal_type"]:checked').val());

            $('input[name="deal_type"]').change(function () {
                changeDealType($(this).val());
            });

            function changeDealType(value) {
                needSearch.model.deal_type = value;
            }

        });
    }
});