$(document).ready(function(){

    /*If Rules Not Accept*/
    $('#rulesAccepting').on('change', function () {
        if(!$('#rulesAccepting input[type="checkbox"]').is(':checked')) {
            $('#registerBtn button').attr('disabled', 'disabled').removeClass('btn-success').addClass('btn-default');
        } else {
            $('#registerBtn button').removeAttr('disabled').removeClass('btn-default').addClass('btn-success');
        }
    });

    /*Show ReSend Register Confirm Email*/
    $('#showReSendForm').on('click', function(){
        $(this).slideUp();
        $(this).siblings('#reSendForm').slideDown('normal');
    });

    /*Show Password Recovery Instruction*/
    $('#showPasswordRecoveryInstruction').on('click', function(){
        $(this).slideUp();
        $(this).siblings('#PasswordRecoveryInstruction').slideDown('normal');
    });

});