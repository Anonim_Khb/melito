$(document).ready(function () {

    function formatResult(myResult) {
        var $city = $(
            '<option value="' + myResult.id + '">' + myResult.text + ' (' + myResult.country + ')</option>'
        );
        return $city;
    };

    function resultSelection(selectData) {
        return selectData.text;
    };

    $(".select-city-js").select2({
        multiple: false,
        ajax: {
            url: "/ads/cities",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    myQuery: params.term,
                    page: params.page,
                    token: CSRF_TOKEN
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 10) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 0,
        templateResult: formatResult,
        templateSelection: resultSelection
    });

});