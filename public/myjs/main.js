var CSRF_TOKEN = $('meta[name="token"]').attr('content');

$(document).ready(function () {

    /*MATERIAL DESIGN*/
    $.material.init();

    /*TOOLTIP*/
    $('[data-toggle="tooltip"]').tooltip();


    /*POPOVER*/
    $("[data-toggle='popover']").popover({trigger: 'hover', container: 'body'});

    /*TARGET _BLANK*/
    $('a[data-blank="true"]').attr('target', '_blank');

    /*HIDE ALERT*/
    $('.alerts-top:visible').on('hide', function () {
        $(this).hide('slow');
    });
    setTimeout(function () {
        $('.alerts-top').slideUp('hide');
    }, 4400);


    /*DISABLE FORM SEND FOR ENTER*/
    $.SendFormEnterBtnDisabled = function (data) {

        $(data).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });

    }


    /*BLUR IF CLICK HEADER MENU*/
    function blurClickHeader() {
        $('#myHeader .navbar-nav').children('li').hasClass('open') ?
            $('.all-light-box').fadeIn(250) : $('.all-light-box').fadeOut(150);
    };
    setInterval(function () {
        blurClickHeader();
    }, 200);


    /*SEND FORM WITH CITY FROM HEADER*/
    $('#AdOfferFromHeader').on('click', function () {
        $('#SendCityHeaderForm').attr('action', $(this).data('href')).submit();
    });


    /*NOTIFICATIONS*/
    /*DON'T HIDE NOTIFICATION DROPDOWN*/
    $('#headerNotificationDropdown').click(function (e) {
        e.stopPropagation();
    });

    /*Hide Personal*/
    $('.header-notification-hide').on('click', function () {

        var notificationId = $(this).closest('.header-notification-block').data('id');

        var redirectURL = $(this).data('href');

        $.post('/notification/hide', {
            notification_id: notificationId,
            _token: CSRF_TOKEN
        }).then(function (data) {

            if (data == 'OK') {

                redirectURL ? window.location.replace(redirectURL) : '';

                $('[data-id="' + notificationId + '"]').closest('.list-notifications').fadeOut('normal');

                $('#headerNotificationCount').text(parseInt($('#headerNotificationCount').text()) - 1);

            }

        }, function (data) {
            alert('ERROR');
        });
    });

    /*Hide All Notification*/
    $('#hideAllHeaderNotifications').on('click', function () {

        $.post('/notification/hide/all', {
            _token: CSRF_TOKEN
        }).then(function (data) {

            if (data == 'OK') {

                $('.list-notifications, #headerNotificationCount, #hideAllHeaderNotifications').fadeOut('normal');

                $('#headers-none-notifications').show('slow');

            }

        }, function (data) {
            alert('ERROR');
        });
    })


});