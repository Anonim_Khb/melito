var adEdit = new Vue({

    el: '#adsEdit',

    data: {
        model: {
            add_pay: false,
            errors: [],
            btnDisable: false,
            photos: [],
            selectImagesTitle: '',
            imagesTitle: '',
            urlForImagesMini: '/melito_img/users_img/ads_offer_mini/',
        },
    },
    methods: {
        deleteImage: function (photo) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/ads/delete-image',
                data: {image: photo.filename, _token: CSRF_TOKEN},
                cache: false,
                success: function (data) {
                    if (data == 'OK') {
                        adEdit.model.photos.$remove(photo);

                        if (adEdit.model.imagesTitle == photo.filename) {
                            if (adEdit.model.photos != '') {
                                adEdit.model.imagesTitle = adEdit.model.photos[0].filename;
                            } else {
                                adEdit.model.imagesTitle = '';
                            }
                        }
                    }
                }
            });
        },
        sendForm: function () {
            $.post('/ads/edit/form', $('#adsEditForm').serialize(), function (data) {
                if (!$.isNumeric(data)) {
                    $('html, body').animate({
                        scrollTop: $("#validationErrors").offset().top - 85
                    }, 1000);
                    adEdit.model.errors = data;
                } else {
                    window.location.replace($('#deletedAdId').data('redirect'));
                }
            });
        },
        imagesUpload: function () {
            var adHash = $('#adHash').val();
            $('#fileUpload').fileupload({
                singleFileUploads: true,
                //maxNumberOfFiles: 10, //NOT WORKING
                maxFileSize: 10000000, // 10 MB
                minFileSize: undefined,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                url: '/ads/edit/images',
                dataType: 'json',
                autoUpload: true,
                add: function (e, data) {
                    $.each(data.files, function (index, file) {
                        if ($.inArray(file.type, ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/bmp']) != -1) {
                            data.submit();
                        } else {
                            alert('Формат файла или его вес не соответствуют требованиям\n\rFile Format or its weight does not comply')
                        }
                    });
                },
                start: function (e, data) {
                    adEdit.model.btnDisable = true;
                },
                stop: function (e, data) {
                    adEdit.model.btnDisable = false;
                },
                submit: function (e, data) {
                    data.formData = {
                        _token: CSRF_TOKEN,
                        hash: adHash,
                        ad_id: $('input[name="ad_id"]').val(),
                    };
                },
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        adEdit.model.photos.push(file);
                        adEdit.selectDefaultImagesTitle(file);
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        },
        selectImagesTitle: function (photo) {
            if (photo.status != 'text-danger') {
                adEdit.model.imagesTitle = photo.filename;
            }
        },
        selectDefaultImagesTitle: function (file) {
            if (adEdit.model.imagesTitle == '' && file.status != 'text-danger') {
                adEdit.model.imagesTitle = file.filename;
            }
        }
    },
    ready: function () {
        $(document).ready(function () {

            if (typeof photos != 'undefined' && photos != '[]') {
                adEdit.model.photos = JSON.parse(photos);

                $.each(adEdit.model.photos, function (index, file) {
                    if(file.title == 1) {
                        adEdit.selectDefaultImagesTitle(file);
                    }
                });
            }

        });
    }
});