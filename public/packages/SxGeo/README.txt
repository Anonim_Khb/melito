Установка:
 - Все в папке /home/mv/www/melito.local/html/public/packages/SxGeo/*
 - Переходим в /FUEL_PATH/public/packages/SxGeo/import.sql
 - - Меняем пути в нижних трех строкам к TSV-файлам.
 - - Копируем целиком содержимое и вставляем как SQL в БД.

Использование:
 - Полная информация тут: https://sypexgeo.net/ru/docs/
 - Коротко и ясно:
  <?php
    include ('packages/SxGeo/SxGeo.php');
    $SxGeo = new SxGeo('packages/SxGeo/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY);
    $ip = Request::getClientIp();
    //$place = $SxGeo->getCityFull($ip);
    $place = $SxGeo->getCityFull('95.70.51.51'); //FOR TEST
    $SxGeo->getCityFull($ip);
    $country = $place['country']['id'];
    $region = $place['region']['id'];
    $city = $place['city']['id'];
    //$city = $SxGeo->get('95.70.51.51'); //JUST GET CITY NAME and ID
    unset($SxGeo);
    print_r($place);
  ?>
